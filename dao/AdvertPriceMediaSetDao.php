<?php
class AdvertPriceMediaSetDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvertPriceMediaSet(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert_price_media_sets WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new AdvertPriceMediaSet();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);

			// クライントiphone
			$record->setActionPriceClientIphoneDocomo1($row["action_price_client_iphone_docomo_1"]);
			$record->setActionPriceClientIphoneSoftbank1($row["action_price_client_iphone_softbank_1"]);
			$record->setActionPriceClientIphoneAu1($row["action_price_client_iphone_au_1"]);
			$record->setActionPriceClientIphonePc1($row["action_price_client_iphone_pc_1"]);

			$record->setActionPriceClientIphoneDocomo2($row["action_price_client_iphone_docomo_2"]);
			$record->setActionPriceClientIphoneSoftbank2($row["action_price_client_iphone_softbank_2"]);
			$record->setActionPriceClientIphoneAu2($row["action_price_client_iphone_au_2"]);
			$record->setActionPriceClientIphonePc2($row["action_price_client_iphone_pc_2"]);

			$record->setActionPriceClientIphoneDocomo3($row["action_price_client_iphone_docomo_3"]);
			$record->setActionPriceClientIphoneSoftbank3($row["action_price_client_iphone_softbank_3"]);
			$record->setActionPriceClientIphoneAu3($row["action_price_client_iphone_au_3"]);
			$record->setActionPriceClientIphonePc3($row["action_price_client_iphone_pc_3"]);

			$record->setActionPriceClientIphoneDocomo4($row["action_price_client_iphone_docomo_4"]);
			$record->setActionPriceClientIphoneSoftbank4($row["action_price_client_iphone_softbank_4"]);
			$record->setActionPriceClientIphoneAu4($row["action_price_client_iphone_au_4"]);
			$record->setActionPriceClientIphonePc4($row["action_price_client_iphone_pc_4"]);

			$record->setActionPriceClientIphoneDocomo5($row["action_price_client_iphone_docomo_5"]);
			$record->setActionPriceClientIphoneSoftbank5($row["action_price_client_iphone_softbank_5"]);
			$record->setActionPriceClientIphoneAu5($row["action_price_client_iphone_au_5"]);
			$record->setActionPriceClientIphonePc5($row["action_price_client_iphone_pc_5"]);

			// メディアiphone
			$record->setActionPriceMediaIphoneDocomo1($row["action_price_media_iphone_docomo_1"]);
			$record->setActionPriceMediaIphoneSoftbank1($row["action_price_media_iphone_softbank_1"]);
			$record->setActionPriceMediaIphoneAu1($row["action_price_media_iphone_au_1"]);
			$record->setActionPriceMediaIphonePc1($row["action_price_media_iphone_pc_1"]);

			$record->setActionPriceMediaIphoneDocomo2($row["action_price_media_iphone_docomo_2"]);
			$record->setActionPriceMediaIphoneSoftbank2($row["action_price_media_iphone_softbank_2"]);
			$record->setActionPriceMediaIphoneAu2($row["action_price_media_iphone_au_2"]);
			$record->setActionPriceMediaIphonePc2($row["action_price_media_iphone_pc_2"]);

			$record->setActionPriceMediaIphoneDocomo3($row["action_price_media_iphone_docomo_3"]);
			$record->setActionPriceMediaIphoneSoftbank3($row["action_price_media_iphone_softbank_3"]);
			$record->setActionPriceMediaIphoneAu3($row["action_price_media_iphone_au_3"]);
			$record->setActionPriceMediaIphonePc3($row["action_price_media_iphone_pc_3"]);

			$record->setActionPriceMediaIphoneDocomo4($row["action_price_media_iphone_docomo_4"]);
			$record->setActionPriceMediaIphoneSoftbank4($row["action_price_media_iphone_softbank_4"]);
			$record->setActionPriceMediaIphoneAu4($row["action_price_media_iphone_au_4"]);
			$record->setActionPriceMediaIphonePc4($row["action_price_media_iphone_pc_4"]);

			$record->setActionPriceMediaIphoneDocomo5($row["action_price_media_iphone_docomo_5"]);
			$record->setActionPriceMediaIphoneSoftbank5($row["action_price_media_iphone_softbank_5"]);
			$record->setActionPriceMediaIphoneAu5($row["action_price_media_iphone_au_5"]);
			$record->setActionPriceMediaIphonePc5($row["action_price_media_iphone_pc_5"]);


			// クライントandroid
			$record->setActionPriceClientAndroidDocomo1($row["action_price_client_android_docomo_1"]);
			$record->setActionPriceClientAndroidSoftbank1($row["action_price_client_android_softbank_1"]);
			$record->setActionPriceClientAndroidAu1($row["action_price_client_android_au_1"]);
			$record->setActionPriceClientAndroidPc1($row["action_price_client_android_pc_1"]);

			$record->setActionPriceClientAndroidDocomo2($row["action_price_client_android_docomo_2"]);
			$record->setActionPriceClientAndroidSoftbank2($row["action_price_client_android_softbank_2"]);
			$record->setActionPriceClientAndroidAu2($row["action_price_client_android_au_2"]);
			$record->setActionPriceClientAndroidPc2($row["action_price_client_android_pc_2"]);

			$record->setActionPriceClientAndroidDocomo3($row["action_price_client_android_docomo_3"]);
			$record->setActionPriceClientAndroidSoftbank3($row["action_price_client_android_softbank_3"]);
			$record->setActionPriceClientAndroidAu3($row["action_price_client_android_au_3"]);
			$record->setActionPriceClientAndroidPc3($row["action_price_client_android_pc_3"]);

			$record->setActionPriceClientAndroidDocomo4($row["action_price_client_android_docomo_4"]);
			$record->setActionPriceClientAndroidSoftbank4($row["action_price_client_android_softbank_4"]);
			$record->setActionPriceClientAndroidAu4($row["action_price_client_android_au_4"]);
			$record->setActionPriceClientAndroidPc4($row["action_price_client_android_pc_4"]);

			$record->setActionPriceClientAndroidDocomo5($row["action_price_client_android_docomo_5"]);
			$record->setActionPriceClientAndroidSoftbank5($row["action_price_client_android_softbank_5"]);
			$record->setActionPriceClientAndroidAu5($row["action_price_client_android_au_5"]);
			$record->setActionPriceClientAndroidPc5($row["action_price_client_android_pc_5"]);

			// メディアandroid
			$record->setActionPriceMediaAndroidDocomo1($row["action_price_media_android_docomo_1"]);
			$record->setActionPriceMediaAndroidSoftbank1($row["action_price_media_android_softbank_1"]);
			$record->setActionPriceMediaAndroidAu1($row["action_price_media_android_au_1"]);
			$record->setActionPriceMediaAndroidPc1($row["action_price_media_android_pc_1"]);

			$record->setActionPriceMediaAndroidDocomo2($row["action_price_media_android_docomo_2"]);
			$record->setActionPriceMediaAndroidSoftbank2($row["action_price_media_android_softbank_2"]);
			$record->setActionPriceMediaAndroidAu2($row["action_price_media_android_au_2"]);
			$record->setActionPriceMediaAndroidPc2($row["action_price_media_android_pc_2"]);

			$record->setActionPriceMediaAndroidDocomo3($row["action_price_media_android_docomo_3"]);
			$record->setActionPriceMediaAndroidSoftbank3($row["action_price_media_android_softbank_3"]);
			$record->setActionPriceMediaAndroidAu3($row["action_price_media_android_au_3"]);
			$record->setActionPriceMediaAndroidPc3($row["action_price_media_android_pc_3"]);

			$record->setActionPriceMediaAndroidDocomo4($row["action_price_media_android_docomo_4"]);
			$record->setActionPriceMediaAndroidSoftbank4($row["action_price_media_android_softbank_4"]);
			$record->setActionPriceMediaAndroidAu4($row["action_price_media_android_au_4"]);
			$record->setActionPriceMediaAndroidPc4($row["action_price_media_android_pc_4"]);

			$record->setActionPriceMediaAndroidDocomo5($row["action_price_media_android_docomo_5"]);
			$record->setActionPriceMediaAndroidSoftbank5($row["action_price_media_android_softbank_5"]);
			$record->setActionPriceMediaAndroidAu5($row["action_price_media_android_au_5"]);
			$record->setActionPriceMediaAndroidPc5($row["action_price_media_android_pc_5"]);

			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);

			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();

		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvertPriceMediaSet($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertPriceMediaSet();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);

			// クライントiphone
			$record->setActionPriceClientIphoneDocomo1($row["action_price_client_iphone_docomo_1"]);
			$record->setActionPriceClientIphoneSoftbank1($row["action_price_client_iphone_softbank_1"]);
			$record->setActionPriceClientIphoneAu1($row["action_price_client_iphone_au_1"]);
			$record->setActionPriceClientIphonePc1($row["action_price_client_iphone_pc_1"]);

			$record->setActionPriceClientIphoneDocomo2($row["action_price_client_iphone_docomo_2"]);
			$record->setActionPriceClientIphoneSoftbank2($row["action_price_client_iphone_softbank_2"]);
			$record->setActionPriceClientIphoneAu2($row["action_price_client_iphone_au_2"]);
			$record->setActionPriceClientIphonePc2($row["action_price_client_iphone_pc_2"]);

			$record->setActionPriceClientIphoneDocomo3($row["action_price_client_iphone_docomo_3"]);
			$record->setActionPriceClientIphoneSoftbank3($row["action_price_client_iphone_softbank_3"]);
			$record->setActionPriceClientIphoneAu3($row["action_price_client_iphone_au_3"]);
			$record->setActionPriceClientIphonePc3($row["action_price_client_iphone_pc_3"]);

			$record->setActionPriceClientIphoneDocomo4($row["action_price_client_iphone_docomo_4"]);
			$record->setActionPriceClientIphoneSoftbank4($row["action_price_client_iphone_softbank_4"]);
			$record->setActionPriceClientIphoneAu4($row["action_price_client_iphone_au_4"]);
			$record->setActionPriceClientIphonePc4($row["action_price_client_iphone_pc_4"]);

			$record->setActionPriceClientIphoneDocomo5($row["action_price_client_iphone_docomo_5"]);
			$record->setActionPriceClientIphoneSoftbank5($row["action_price_client_iphone_softbank_5"]);
			$record->setActionPriceClientIphoneAu5($row["action_price_client_iphone_au_5"]);
			$record->setActionPriceClientIphonePc5($row["action_price_client_iphone_pc_5"]);

			// メディアiphone
			$record->setActionPriceMediaIphoneDocomo1($row["action_price_media_iphone_docomo_1"]);
			$record->setActionPriceMediaIphoneSoftbank1($row["action_price_media_iphone_softbank_1"]);
			$record->setActionPriceMediaIphoneAu1($row["action_price_media_iphone_au_1"]);
			$record->setActionPriceMediaIphonePc1($row["action_price_media_iphone_pc_1"]);

			$record->setActionPriceMediaIphoneDocomo2($row["action_price_media_iphone_docomo_2"]);
			$record->setActionPriceMediaIphoneSoftbank2($row["action_price_media_iphone_softbank_2"]);
			$record->setActionPriceMediaIphoneAu2($row["action_price_media_iphone_au_2"]);
			$record->setActionPriceMediaIphonePc2($row["action_price_media_iphone_pc_2"]);

			$record->setActionPriceMediaIphoneDocomo3($row["action_price_media_iphone_docomo_3"]);
			$record->setActionPriceMediaIphoneSoftbank3($row["action_price_media_iphone_softbank_3"]);
			$record->setActionPriceMediaIphoneAu3($row["action_price_media_iphone_au_3"]);
			$record->setActionPriceMediaIphonePc3($row["action_price_media_iphone_pc_3"]);

			$record->setActionPriceMediaIphoneDocomo4($row["action_price_media_iphone_docomo_4"]);
			$record->setActionPriceMediaIphoneSoftbank4($row["action_price_media_iphone_softbank_4"]);
			$record->setActionPriceMediaIphoneAu4($row["action_price_media_iphone_au_4"]);
			$record->setActionPriceMediaIphonePc4($row["action_price_media_iphone_pc_4"]);

			$record->setActionPriceMediaIphoneDocomo5($row["action_price_media_iphone_docomo_5"]);
			$record->setActionPriceMediaIphoneSoftbank5($row["action_price_media_iphone_softbank_5"]);
			$record->setActionPriceMediaIphoneAu5($row["action_price_media_iphone_au_5"]);
			$record->setActionPriceMediaIphonePc5($row["action_price_media_iphone_pc_5"]);


			// クライントandroid
			$record->setActionPriceClientAndroidDocomo1($row["action_price_client_android_docomo_1"]);
			$record->setActionPriceClientAndroidSoftbank1($row["action_price_client_android_softbank_1"]);
			$record->setActionPriceClientAndroidAu1($row["action_price_client_android_au_1"]);
			$record->setActionPriceClientAndroidPc1($row["action_price_client_android_pc_1"]);

			$record->setActionPriceClientAndroidDocomo2($row["action_price_client_android_docomo_2"]);
			$record->setActionPriceClientAndroidSoftbank2($row["action_price_client_android_softbank_2"]);
			$record->setActionPriceClientAndroidAu2($row["action_price_client_android_au_2"]);
			$record->setActionPriceClientAndroidPc2($row["action_price_client_android_pc_2"]);

			$record->setActionPriceClientAndroidDocomo3($row["action_price_client_android_docomo_3"]);
			$record->setActionPriceClientAndroidSoftbank3($row["action_price_client_android_softbank_3"]);
			$record->setActionPriceClientAndroidAu3($row["action_price_client_android_au_3"]);
			$record->setActionPriceClientAndroidPc3($row["action_price_client_android_pc_3"]);

			$record->setActionPriceClientAndroidDocomo4($row["action_price_client_android_docomo_4"]);
			$record->setActionPriceClientAndroidSoftbank4($row["action_price_client_android_softbank_4"]);
			$record->setActionPriceClientAndroidAu4($row["action_price_client_android_au_4"]);
			$record->setActionPriceClientAndroidPc4($row["action_price_client_android_pc_4"]);

			$record->setActionPriceClientAndroidDocomo5($row["action_price_client_android_docomo_5"]);
			$record->setActionPriceClientAndroidSoftbank5($row["action_price_client_android_softbank_5"]);
			$record->setActionPriceClientAndroidAu5($row["action_price_client_android_au_5"]);
			$record->setActionPriceClientAndroidPc5($row["action_price_client_android_pc_5"]);

			// メディアandroid
			$record->setActionPriceMediaAndroidDocomo1($row["action_price_media_android_docomo_1"]);
			$record->setActionPriceMediaAndroidSoftbank1($row["action_price_media_android_softbank_1"]);
			$record->setActionPriceMediaAndroidAu1($row["action_price_media_android_au_1"]);
			$record->setActionPriceMediaAndroidPc1($row["action_price_media_android_pc_1"]);

			$record->setActionPriceMediaAndroidDocomo2($row["action_price_media_android_docomo_2"]);
			$record->setActionPriceMediaAndroidSoftbank2($row["action_price_media_android_softbank_2"]);
			$record->setActionPriceMediaAndroidAu2($row["action_price_media_android_au_2"]);
			$record->setActionPriceMediaAndroidPc2($row["action_price_media_android_pc_2"]);

			$record->setActionPriceMediaAndroidDocomo3($row["action_price_media_android_docomo_3"]);
			$record->setActionPriceMediaAndroidSoftbank3($row["action_price_media_android_softbank_3"]);
			$record->setActionPriceMediaAndroidAu3($row["action_price_media_android_au_3"]);
			$record->setActionPriceMediaAndroidPc3($row["action_price_media_android_pc_3"]);

			$record->setActionPriceMediaAndroidDocomo4($row["action_price_media_android_docomo_4"]);
			$record->setActionPriceMediaAndroidSoftbank4($row["action_price_media_android_softbank_4"]);
			$record->setActionPriceMediaAndroidAu4($row["action_price_media_android_au_4"]);
			$record->setActionPriceMediaAndroidPc4($row["action_price_media_android_pc_4"]);

			$record->setActionPriceMediaAndroidDocomo5($row["action_price_media_android_docomo_5"]);
			$record->setActionPriceMediaAndroidSoftbank5($row["action_price_media_android_softbank_5"]);
			$record->setActionPriceMediaAndroidAu5($row["action_price_media_android_au_5"]);
			$record->setActionPriceMediaAndroidPc5($row["action_price_media_android_pc_5"]);

			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);

			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();

		return $record;
	}

	//idでデータを取得
	public function getAdvertPriceMediaSetById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}

	//session_idでデータを取得
	public function getAdvertPriceMediaSetBySessionId($session_id){
		$session_id = $this->mysqli->real_escape_string($session_id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE session_id = '$session_id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}

	//advert_id, media_idでデータを取得
	public function getAdvertPriceMediaSetByMidAid($advert_id, $media_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$media_id = $this->mysqli->real_escape_string($media_id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE advert_id = '$advert_id' AND media_id = '$media_id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}

	//advert_idでデータを取得
	public function getAdvertPriceMediaSetByAid($advert_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE advert_id = '$advert_id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}


	//IDを指定してデータの更新
	public function updateAdvertPriceMediaSet($record, &$result_message = ""){
		$record = $this->escapeStringAdvertPriceMediaSet($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_price_media_sets SET "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " media_id = '" . $record->getMediaId() . "', "
			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
			. " media_category_id = '" . $record->getMediaCategoryId() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "

			. " action_price_client_iphone_docomo_1 = '" . $record->getActionPriceClientIphoneDocomo1() . "', "
			. " action_price_client_iphone_softbank_1 = '" . $record->getActionPriceClientIphoneSoftbank1() . "', "
			. " action_price_client_iphone_au_1 = '" . $record->getActionPriceClientIphoneAu1() . "', "
			. " action_price_client_iphone_pc_1 = '" . $record->getActionPriceClientIphonePc1() . "', "

			. " action_price_client_iphone_docomo_2 = '" . $record->getActionPriceClientIphoneDocomo2() . "', "
			. " action_price_client_iphone_softbank_2 = '" . $record->getActionPriceClientIphoneSoftbank2() . "', "
			. " action_price_client_iphone_au_2 = '" . $record->getActionPriceClientIphoneAu2() . "', "
			. " action_price_client_iphone_pc_2 = '" . $record->getActionPriceClientIphonePc2() . "', "

			. " action_price_client_iphone_docomo_3 = '" . $record->getActionPriceClientIphoneDocomo3() . "', "
			. " action_price_client_iphone_softbank_3 = '" . $record->getActionPriceClientIphoneSoftbank3() . "', "
			. " action_price_client_iphone_au_3 = '" . $record->getActionPriceClientIphoneAu3() . "', "
			. " action_price_client_iphone_pc_3 = '" . $record->getActionPriceClientIphonePc3() . "', "

			. " action_price_client_iphone_docomo_4 = '" . $record->getActionPriceClientIphoneDocomo4() . "', "
			. " action_price_client_iphone_softbank_4 = '" . $record->getActionPriceClientIphoneSoftbank4() . "', "
			. " action_price_client_iphone_au_4 = '" . $record->getActionPriceClientIphoneAu4() . "', "
			. " action_price_client_iphone_pc_4 = '" . $record->getActionPriceClientIphonePc4() . "', "

			. " action_price_client_iphone_docomo_5 = '" . $record->getActionPriceClientIphoneDocomo5() . "', "
			. " action_price_client_iphone_softbank_5 = '" . $record->getActionPriceClientIphoneSoftbank5() . "', "
			. " action_price_client_iphone_au_5 = '" . $record->getActionPriceClientIphoneAu5() . "', "
			. " action_price_client_iphone_pc_5 = '" . $record->getActionPriceClientIphonePc5() . "', "

			. " action_price_media_iphone_docomo_1 = '" . $record->getActionPriceMediaIphoneDocomo1() . "', "
			. " action_price_media_iphone_softbank_1 = '" . $record->getActionPriceMediaIphoneSoftbank1() . "', "
			. " action_price_media_iphone_au_1 = '" . $record->getActionPriceMediaIphoneAu1() . "', "
			. " action_price_media_iphone_pc_1 = '" . $record->getActionPriceMediaIphonePc1() . "', "

			. " action_price_media_iphone_docomo_2 = '" . $record->getActionPriceMediaIphoneDocomo2() . "', "
			. " action_price_media_iphone_softbank_2 = '" . $record->getActionPriceMediaIphoneSoftbank2() . "', "
			. " action_price_media_iphone_au_2 = '" . $record->getActionPriceMediaIphoneAu2() . "', "
			. " action_price_media_iphone_pc_2 = '" . $record->getActionPriceMediaIphonePc2() . "', "

			. " action_price_media_iphone_docomo_3 = '" . $record->getActionPriceMediaIphoneDocomo3() . "', "
			. " action_price_media_iphone_softbank_3 = '" . $record->getActionPriceMediaIphoneSoftbank3() . "', "
			. " action_price_media_iphone_au_3 = '" . $record->getActionPriceMediaIphoneAu3() . "', "
			. " action_price_media_iphone_pc_3 = '" . $record->getActionPriceMediaIphonePc3() . "', "

			. " action_price_media_iphone_docomo_4 = '" . $record->getActionPriceMediaIphoneDocomo4() . "', "
			. " action_price_media_iphone_softbank_4 = '" . $record->getActionPriceMediaIphoneSoftbank4() . "', "
			. " action_price_media_iphone_au_4 = '" . $record->getActionPriceMediaIphoneAu4() . "', "
			. " action_price_media_iphone_pc_4 = '" . $record->getActionPriceMediaIphonePc4() . "', "

			. " action_price_media_iphone_docomo_5 = '" . $record->getActionPriceMediaIphoneDocomo5() . "', "
			. " action_price_media_iphone_softbank_5 = '" . $record->getActionPriceMediaIphoneSoftbank5() . "', "
			. " action_price_media_iphone_au_5 = '" . $record->getActionPriceMediaIphoneAu5() . "', "
			. " action_price_media_iphone_pc_5 = '" . $record->getActionPriceMediaIphonePc5() . "', "

			. " action_price_client_android_docomo_1 = '" . $record->getActionPriceClientAndroidDocomo1() . "', "
			. " action_price_client_android_softbank_1 = '" . $record->getActionPriceClientAndroidSoftbank1() . "', "
			. " action_price_client_android_au_1 = '" . $record->getActionPriceClientAndroidAu1() . "', "
			. " action_price_client_android_pc_1 = '" . $record->getActionPriceClientAndroidPc1() . "', "

			. " action_price_client_android_docomo_2 = '" . $record->getActionPriceClientAndroidDocomo2() . "', "
			. " action_price_client_android_softbank_2 = '" . $record->getActionPriceClientAndroidSoftbank2() . "', "
			. " action_price_client_android_au_2 = '" . $record->getActionPriceClientAndroidAu2() . "', "
			. " action_price_client_android_pc_2 = '" . $record->getActionPriceClientAndroidPc2() . "', "

			. " action_price_client_android_docomo_3 = '" . $record->getActionPriceClientAndroidDocomo3() . "', "
			. " action_price_client_android_softbank_3 = '" . $record->getActionPriceClientAndroidSoftbank3() . "', "
			. " action_price_client_android_au_3 = '" . $record->getActionPriceClientAndroidAu3() . "', "
			. " action_price_client_android_pc_3 = '" . $record->getActionPriceClientAndroidPc3() . "', "

			. " action_price_client_android_docomo_4 = '" . $record->getActionPriceClientAndroidDocomo4() . "', "
			. " action_price_client_android_softbank_4 = '" . $record->getActionPriceClientAndroidSoftbank4() . "', "
			. " action_price_client_android_au_4 = '" . $record->getActionPriceClientAndroidAu4() . "', "
			. " action_price_client_android_pc_4 = '" . $record->getActionPriceClientAndroidPc4() . "', "

			. " action_price_client_android_docomo_5 = '" . $record->getActionPriceClientAndroidDocomo5() . "', "
			. " action_price_client_android_softbank_5 = '" . $record->getActionPriceClientAndroidSoftbank5() . "', "
			. " action_price_client_android_au_5 = '" . $record->getActionPriceClientAndroidAu5() . "', "
			. " action_price_client_android_pc_5 = '" . $record->getActionPriceClientAndroidPc5() . "', "

			. " action_price_media_android_docomo_1 = '" . $record->getActionPriceMediaAndroidDocomo1() . "', "
			. " action_price_media_android_softbank_1 = '" . $record->getActionPriceMediaAndroidSoftbank1() . "', "
			. " action_price_media_android_au_1 = '" . $record->getActionPriceMediaAndroidAu1() . "', "
			. " action_price_media_android_pc_1 = '" . $record->getActionPriceMediaAndroidPc1() . "', "

			. " action_price_media_android_docomo_2 = '" . $record->getActionPriceMediaAndroidDocomo2() . "', "
			. " action_price_media_android_softbank_2 = '" . $record->getActionPriceMediaAndroidSoftbank2() . "', "
			. " action_price_media_android_au_2 = '" . $record->getActionPriceMediaAndroidAu2() . "', "
			. " action_price_media_android_pc_2 = '" . $record->getActionPriceMediaAndroidPc2() . "', "

			. " action_price_media_android_docomo_3 = '" . $record->getActionPriceMediaAndroidDocomo3() . "', "
			. " action_price_media_android_softbank_3 = '" . $record->getActionPriceMediaAndroidSoftbank3() . "', "
			. " action_price_media_android_au_3 = '" . $record->getActionPriceMediaAndroidAu3() . "', "
			. " action_price_media_android_pc_3 = '" . $record->getActionPriceMediaAndroidPc3() . "', "

			. " action_price_media_android_docomo_4 = '" . $record->getActionPriceMediaAndroidDocomo4() . "', "
			. " action_price_media_android_softbank_4 = '" . $record->getActionPriceMediaAndroidSoftbank4() . "', "
			. " action_price_media_android_au_4 = '" . $record->getActionPriceMediaAndroidAu4() . "', "
			. " action_price_media_android_pc_4 = '" . $record->getActionPriceMediaAndroidPc4() . "', "

			. " action_price_media_android_docomo_5 = '" . $record->getActionPriceMediaAndroidDocomo5() . "', "
			. " action_price_media_android_softbank_5 = '" . $record->getActionPriceMediaAndroidSoftbank5() . "', "
			. " action_price_media_android_au_5 = '" . $record->getActionPriceMediaAndroidAu5() . "', "
			. " action_price_media_android_pc_5 = '" . $record->getActionPriceMediaAndroidPc5() . "', "

			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "

			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//advert_id,media_idを指定してデータの更新
	public function updateAdvertPriceMediaAdvertMediaSet($record, $advert_id = "", $media_id = ""){
		$record = $this->escapeStringAdvertPriceMediaSet($record);

		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_price_media_sets SET "
//			. " advert_id = '" . $record->getAdvertId() . "', "
//			. " media_id = '" . $record->getMediaId() . "', "
//			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
//			. " media_category_id = '" . $record->getMediaCategoryId() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "

			. " action_price_client_iphone_docomo_1 = '" . $record->getActionPriceClientIphoneDocomo1() . "', "
			. " action_price_client_iphone_softbank_1 = '" . $record->getActionPriceClientIphoneSoftbank1() . "', "
			. " action_price_client_iphone_au_1 = '" . $record->getActionPriceClientIphoneAu1() . "', "
			. " action_price_client_iphone_pc_1 = '" . $record->getActionPriceClientIphonePc1() . "', "

			. " action_price_client_iphone_docomo_2 = '" . $record->getActionPriceClientIphoneDocomo2() . "', "
			. " action_price_client_iphone_softbank_2 = '" . $record->getActionPriceClientIphoneSoftbank2() . "', "
			. " action_price_client_iphone_au_2 = '" . $record->getActionPriceClientIphoneAu2() . "', "
			. " action_price_client_iphone_pc_2 = '" . $record->getActionPriceClientIphonePc2() . "', "

			. " action_price_client_iphone_docomo_3 = '" . $record->getActionPriceClientIphoneDocomo3() . "', "
			. " action_price_client_iphone_softbank_3 = '" . $record->getActionPriceClientIphoneSoftbank3() . "', "
			. " action_price_client_iphone_au_3 = '" . $record->getActionPriceClientIphoneAu3() . "', "
			. " action_price_client_iphone_pc_3 = '" . $record->getActionPriceClientIphonePc3() . "', "

			. " action_price_client_iphone_docomo_4 = '" . $record->getActionPriceClientIphoneDocomo4() . "', "
			. " action_price_client_iphone_softbank_4 = '" . $record->getActionPriceClientIphoneSoftbank4() . "', "
			. " action_price_client_iphone_au_4 = '" . $record->getActionPriceClientIphoneAu4() . "', "
			. " action_price_client_iphone_pc_4 = '" . $record->getActionPriceClientIphonePc4() . "', "

			. " action_price_client_iphone_docomo_5 = '" . $record->getActionPriceClientIphoneDocomo5() . "', "
			. " action_price_client_iphone_softbank_5 = '" . $record->getActionPriceClientIphoneSoftbank5() . "', "
			. " action_price_client_iphone_au_5 = '" . $record->getActionPriceClientIphoneAu5() . "', "
			. " action_price_client_iphone_pc_5 = '" . $record->getActionPriceClientIphonePc5() . "', "

			. " action_price_media_iphone_docomo_1 = '" . $record->getActionPriceMediaIphoneDocomo1() . "', "
			. " action_price_media_iphone_softbank_1 = '" . $record->getActionPriceMediaIphoneSoftbank1() . "', "
			. " action_price_media_iphone_au_1 = '" . $record->getActionPriceMediaIphoneAu1() . "', "
			. " action_price_media_iphone_pc_1 = '" . $record->getActionPriceMediaIphonePc1() . "', "

			. " action_price_media_iphone_docomo_2 = '" . $record->getActionPriceMediaIphoneDocomo2() . "', "
			. " action_price_media_iphone_softbank_2 = '" . $record->getActionPriceMediaIphoneSoftbank2() . "', "
			. " action_price_media_iphone_au_2 = '" . $record->getActionPriceMediaIphoneAu2() . "', "
			. " action_price_media_iphone_pc_2 = '" . $record->getActionPriceMediaIphonePc2() . "', "

			. " action_price_media_iphone_docomo_3 = '" . $record->getActionPriceMediaIphoneDocomo3() . "', "
			. " action_price_media_iphone_softbank_3 = '" . $record->getActionPriceMediaIphoneSoftbank3() . "', "
			. " action_price_media_iphone_au_3 = '" . $record->getActionPriceMediaIphoneAu3() . "', "
			. " action_price_media_iphone_pc_3 = '" . $record->getActionPriceMediaIphonePc3() . "', "

			. " action_price_media_iphone_docomo_4 = '" . $record->getActionPriceMediaIphoneDocomo4() . "', "
			. " action_price_media_iphone_softbank_4 = '" . $record->getActionPriceMediaIphoneSoftbank4() . "', "
			. " action_price_media_iphone_au_4 = '" . $record->getActionPriceMediaIphoneAu4() . "', "
			. " action_price_media_iphone_pc_4 = '" . $record->getActionPriceMediaIphonePc4() . "', "

			. " action_price_media_iphone_docomo_5 = '" . $record->getActionPriceMediaIphoneDocomo5() . "', "
			. " action_price_media_iphone_softbank_5 = '" . $record->getActionPriceMediaIphoneSoftbank5() . "', "
			. " action_price_media_iphone_au_5 = '" . $record->getActionPriceMediaIphoneAu5() . "', "
			. " action_price_media_iphone_pc_5 = '" . $record->getActionPriceMediaIphonePc5() . "', "

			. " action_price_client_android_docomo_1 = '" . $record->getActionPriceClientAndroidDocomo1() . "', "
			. " action_price_client_android_softbank_1 = '" . $record->getActionPriceClientAndroidSoftbank1() . "', "
			. " action_price_client_android_au_1 = '" . $record->getActionPriceClientAndroidAu1() . "', "
			. " action_price_client_android_pc_1 = '" . $record->getActionPriceClientAndroidPc1() . "', "

			. " action_price_client_android_docomo_2 = '" . $record->getActionPriceClientAndroidDocomo2() . "', "
			. " action_price_client_android_softbank_2 = '" . $record->getActionPriceClientAndroidSoftbank2() . "', "
			. " action_price_client_android_au_2 = '" . $record->getActionPriceClientAndroidAu2() . "', "
			. " action_price_client_android_pc_2 = '" . $record->getActionPriceClientAndroidPc2() . "', "

			. " action_price_client_android_docomo_3 = '" . $record->getActionPriceClientAndroidDocomo3() . "', "
			. " action_price_client_android_softbank_3 = '" . $record->getActionPriceClientAndroidSoftbank3() . "', "
			. " action_price_client_android_au_3 = '" . $record->getActionPriceClientAndroidAu3() . "', "
			. " action_price_client_android_pc_3 = '" . $record->getActionPriceClientAndroidPc3() . "', "

			. " action_price_client_android_docomo_4 = '" . $record->getActionPriceClientAndroidDocomo4() . "', "
			. " action_price_client_android_softbank_4 = '" . $record->getActionPriceClientAndroidSoftbank4() . "', "
			. " action_price_client_android_au_4 = '" . $record->getActionPriceClientAndroidAu4() . "', "
			. " action_price_client_android_pc_4 = '" . $record->getActionPriceClientAndroidPc4() . "', "

			. " action_price_client_android_docomo_5 = '" . $record->getActionPriceClientAndroidDocomo5() . "', "
			. " action_price_client_android_softbank_5 = '" . $record->getActionPriceClientAndroidSoftbank5() . "', "
			. " action_price_client_android_au_5 = '" . $record->getActionPriceClientAndroidAu5() . "', "
			. " action_price_client_android_pc_5 = '" . $record->getActionPriceClientAndroidPc5() . "', "

			. " action_price_media_android_docomo_1 = '" . $record->getActionPriceMediaAndroidDocomo1() . "', "
			. " action_price_media_android_softbank_1 = '" . $record->getActionPriceMediaAndroidSoftbank1() . "', "
			. " action_price_media_android_au_1 = '" . $record->getActionPriceMediaAndroidAu1() . "', "
			. " action_price_media_android_pc_1 = '" . $record->getActionPriceMediaAndroidPc1() . "', "

			. " action_price_media_android_docomo_2 = '" . $record->getActionPriceMediaAndroidDocomo2() . "', "
			. " action_price_media_android_softbank_2 = '" . $record->getActionPriceMediaAndroidSoftbank2() . "', "
			. " action_price_media_android_au_2 = '" . $record->getActionPriceMediaAndroidAu2() . "', "
			. " action_price_media_android_pc_2 = '" . $record->getActionPriceMediaAndroidPc2() . "', "

			. " action_price_media_android_docomo_3 = '" . $record->getActionPriceMediaAndroidDocomo3() . "', "
			. " action_price_media_android_softbank_3 = '" . $record->getActionPriceMediaAndroidSoftbank3() . "', "
			. " action_price_media_android_au_3 = '" . $record->getActionPriceMediaAndroidAu3() . "', "
			. " action_price_media_android_pc_3 = '" . $record->getActionPriceMediaAndroidPc3() . "', "

			. " action_price_media_android_docomo_4 = '" . $record->getActionPriceMediaAndroidDocomo4() . "', "
			. " action_price_media_android_softbank_4 = '" . $record->getActionPriceMediaAndroidSoftbank4() . "', "
			. " action_price_media_android_au_4 = '" . $record->getActionPriceMediaAndroidAu4() . "', "
			. " action_price_media_android_pc_4 = '" . $record->getActionPriceMediaAndroidPc4() . "', "

			. " action_price_media_android_docomo_5 = '" . $record->getActionPriceMediaAndroidDocomo5() . "', "
			. " action_price_media_android_softbank_5 = '" . $record->getActionPriceMediaAndroidSoftbank5() . "', "
			. " action_price_media_android_au_5 = '" . $record->getActionPriceMediaAndroidAu5() . "', "
			. " action_price_media_android_pc_5 = '" . $record->getActionPriceMediaAndroidPc5() . "', "

			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "

			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "' "
			. " WHERE "
			. " advert_id = '" . $advert_id . "' "
			. " AND "
			. " media_id = '" . $media_id . "' "
			. " AND deleted_at is NULL ";

		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}


	//新規データの登録
	public function insertAdvertPriceMediaSet($record, &$result_message = ""){
		$record = $this->escapeStringAdvertPriceMediaSet($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert_price_media_sets values ('', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getMediaId() . "', "
			. " '" . $record->getMediaPublisherId() . "', "
			. " '" . $record->getMediaCategoryId() . "', "
			. " '" . $record->getClickPriceClient() . "', "
			. " '" . $record->getClickPriceMedia() . "', "

			. " '" . $record->getActionPriceClientIphoneDocomo1() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank1() . "', "
			. " '" . $record->getActionPriceClientIphoneAu1() . "', "
			. " '" . $record->getActionPriceClientIphonePc1() . "', "

			. " '" . $record->getActionPriceClientIphoneDocomo2() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank2() . "', "
			. " '" . $record->getActionPriceClientIphoneAu2() . "', "
			. " '" . $record->getActionPriceClientIphonePc2() . "', "

			. " '" . $record->getActionPriceClientIphoneDocomo3() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank3() . "', "
			. " '" . $record->getActionPriceClientIphoneAu3() . "', "
			. " '" . $record->getActionPriceClientIphonePc3() . "', "

			. " '" . $record->getActionPriceClientIphoneDocomo4() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank4() . "', "
			. " '" . $record->getActionPriceClientIphoneAu4() . "', "
			. " '" . $record->getActionPriceClientIphonePc4() . "', "

			. " '" . $record->getActionPriceClientIphoneDocomo5() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank5() . "', "
			. " '" . $record->getActionPriceClientIphoneAu5() . "', "
			. " '" . $record->getActionPriceClientIphonePc5() . "', "

			. " '" . $record->getActionPriceMediaIphoneDocomo1() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank1() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu1() . "', "
			. " '" . $record->getActionPriceMediaIphonePc1() . "', "

			. " '" . $record->getActionPriceMediaIphoneDocomo2() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank2() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu2() . "', "
			. " '" . $record->getActionPriceMediaIphonePc2() . "', "

			. " '" . $record->getActionPriceMediaIphoneDocomo3() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank3() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu3() . "', "
			. " '" . $record->getActionPriceMediaIphonePc3() . "', "

			. " '" . $record->getActionPriceMediaIphoneDocomo4() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank4() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu4() . "', "
			. " '" . $record->getActionPriceMediaIphonePc4() . "', "

			. " '" . $record->getActionPriceMediaIphoneDocomo5() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank5() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu5() . "', "
			. " '" . $record->getActionPriceMediaIphonePc5() . "', "


			. " '" . $record->getActionPriceClientAndroidDocomo1() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank1() . "', "
			. " '" . $record->getActionPriceClientAndroidAu1() . "', "
			. " '" . $record->getActionPriceClientAndroidPc1() . "', "

			. " '" . $record->getActionPriceClientAndroidDocomo2() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank2() . "', "
			. " '" . $record->getActionPriceClientAndroidAu2() . "', "
			. " '" . $record->getActionPriceClientAndroidPc2() . "', "

			. " '" . $record->getActionPriceClientAndroidDocomo3() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank3() . "', "
			. " '" . $record->getActionPriceClientAndroidAu3() . "', "
			. " '" . $record->getActionPriceClientAndroidPc3() . "', "

			. " '" . $record->getActionPriceClientAndroidDocomo4() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank4() . "', "
			. " '" . $record->getActionPriceClientAndroidAu4() . "', "
			. " '" . $record->getActionPriceClientAndroidPc4() . "', "

			. " '" . $record->getActionPriceClientAndroidDocomo5() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank5() . "', "
			. " '" . $record->getActionPriceClientAndroidAu5() . "', "
			. " '" . $record->getActionPriceClientAndroidPc5() . "', "

			. " '" . $record->getActionPriceMediaAndroidDocomo1() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank1() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu1() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc1() . "', "

			. " '" . $record->getActionPriceMediaAndroidDocomo2() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank2() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu2() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc2() . "', "

			. " '" . $record->getActionPriceMediaAndroidDocomo3() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank3() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu3() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc3() . "', "

			. " '" . $record->getActionPriceMediaAndroidDocomo4() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank4() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu4() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc4() . "', "

			. " '" . $record->getActionPriceMediaAndroidDocomo5() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank5() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu5() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc5() . "', "

			. " '" . $record->getActionPriceClientPc1() . "', "
			. " '" . $record->getActionPriceClientPc2() . "', "
			. " '" . $record->getActionPriceClientPc3() . "', "
			. " '" . $record->getActionPriceClientPc4() . "', "
			. " '" . $record->getActionPriceClientPc5() . "', "

			. " '" . $record->getActionPriceMediaPc1() . "', "
			. " '" . $record->getActionPriceMediaPc2() . "', "
			. " '" . $record->getActionPriceMediaPc3() . "', "
			. " '" . $record->getActionPriceMediaPc4() . "', "
			. " '" . $record->getActionPriceMediaPc5() . "', "
			. " Now(), Now(), NULL) ";

		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvertPriceMediaSet($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_price_media_sets SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvertPriceMediaSet($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setMediaId($this->mysqli->real_escape_string($record->getMediaId()));
		$record->setMediaPublisherId($this->mysqli->real_escape_string($record->getMediaPublisherId()));
		$record->setMediaCategoryId($this->mysqli->real_escape_string($record->getMediaCategoryId()));
		$record->setClickPriceClient($this->mysqli->real_escape_string($record->getClickPriceClient()));
		$record->setClickPriceMedia($this->mysqli->real_escape_string($record->getClickPriceMedia()));

		$record->setActionPriceClientIphoneDocomo1($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo1()));
		$record->setActionPriceClientIphoneSoftbank1($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank1()));
		$record->setActionPriceClientIphoneAu1($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu1()));
		$record->setActionPriceClientIphonePc1($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc1()));

		$record->setActionPriceClientIphoneDocomo2($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo2()));
		$record->setActionPriceClientIphoneSoftbank2($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank2()));
		$record->setActionPriceClientIphoneAu2($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu2()));
		$record->setActionPriceClientIphonePc2($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc2()));

		$record->setActionPriceClientIphoneDocomo3($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo3()));
		$record->setActionPriceClientIphoneSoftbank3($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank3()));
		$record->setActionPriceClientIphoneAu3($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu3()));
		$record->setActionPriceClientIphonePc3($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc3()));

		$record->setActionPriceClientIphoneDocomo4($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo4()));
		$record->setActionPriceClientIphoneSoftbank4($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank4()));
		$record->setActionPriceClientIphoneAu4($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu4()));
		$record->setActionPriceClientIphonePc4($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc4()));

		$record->setActionPriceClientIphoneDocomo5($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo5()));
		$record->setActionPriceClientIphoneSoftbank5($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank5()));
		$record->setActionPriceClientIphoneAu5($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu5()));
		$record->setActionPriceClientIphonePc5($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc5()));

		$record->setActionPriceMediaIphoneDocomo1($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo1()));
		$record->setActionPriceMediaIphoneSoftbank1($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank1()));
		$record->setActionPriceMediaIphoneAu1($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu1()));
		$record->setActionPriceMediaIphonePc1($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc1()));

		$record->setActionPriceMediaIphoneDocomo2($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo2()));
		$record->setActionPriceMediaIphoneSoftbank2($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank2()));
		$record->setActionPriceMediaIphoneAu2($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu2()));
		$record->setActionPriceMediaIphonePc2($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc2()));

		$record->setActionPriceMediaIphoneDocomo3($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo3()));
		$record->setActionPriceMediaIphoneSoftbank3($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank3()));
		$record->setActionPriceMediaIphoneAu3($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu3()));
		$record->setActionPriceMediaIphonePc3($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc3()));

		$record->setActionPriceMediaIphoneDocomo4($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo4()));
		$record->setActionPriceMediaIphoneSoftbank4($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank4()));
		$record->setActionPriceMediaIphoneAu4($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu4()));
		$record->setActionPriceMediaIphonePc4($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc4()));

		$record->setActionPriceMediaIphoneDocomo5($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo5()));
		$record->setActionPriceMediaIphoneSoftbank5($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank5()));
		$record->setActionPriceMediaIphoneAu5($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu5()));
		$record->setActionPriceMediaIphonePc5($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc5()));


		$record->setActionPriceClientAndroidDocomo1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo1()));
		$record->setActionPriceClientAndroidSoftbank1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank1()));
		$record->setActionPriceClientAndroidAu1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu1()));
		$record->setActionPriceClientAndroidPc1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc1()));

		$record->setActionPriceClientAndroidDocomo2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo2()));
		$record->setActionPriceClientAndroidSoftbank2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank2()));
		$record->setActionPriceClientAndroidAu2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu2()));
		$record->setActionPriceClientAndroidPc2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc2()));

		$record->setActionPriceClientAndroidDocomo3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo3()));
		$record->setActionPriceClientAndroidSoftbank3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank3()));
		$record->setActionPriceClientAndroidAu3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu3()));
		$record->setActionPriceClientAndroidPc3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc3()));

		$record->setActionPriceClientAndroidDocomo4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo4()));
		$record->setActionPriceClientAndroidSoftbank4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank4()));
		$record->setActionPriceClientAndroidAu4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu4()));
		$record->setActionPriceClientAndroidPc4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc4()));

		$record->setActionPriceClientAndroidDocomo5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo5()));
		$record->setActionPriceClientAndroidSoftbank5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank5()));
		$record->setActionPriceClientAndroidAu5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu5()));
		$record->setActionPriceClientAndroidPc5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc5()));

		$record->setActionPriceMediaAndroidDocomo1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo1()));
		$record->setActionPriceMediaAndroidSoftbank1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank1()));
		$record->setActionPriceMediaAndroidAu1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu1()));
		$record->setActionPriceMediaAndroidPc1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc1()));

		$record->setActionPriceMediaAndroidDocomo2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo2()));
		$record->setActionPriceMediaAndroidSoftbank2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank2()));
		$record->setActionPriceMediaAndroidAu2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu2()));
		$record->setActionPriceMediaAndroidPc2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc2()));

		$record->setActionPriceMediaAndroidDocomo3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo3()));
		$record->setActionPriceMediaAndroidSoftbank3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank3()));
		$record->setActionPriceMediaAndroidAu3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu3()));
		$record->setActionPriceMediaAndroidPc3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc3()));

		$record->setActionPriceMediaAndroidDocomo4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo4()));
		$record->setActionPriceMediaAndroidSoftbank4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank4()));
		$record->setActionPriceMediaAndroidAu4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu4()));
		$record->setActionPriceMediaAndroidPc4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc4()));

		$record->setActionPriceMediaAndroidDocomo5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo5()));
		$record->setActionPriceMediaAndroidSoftbank5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank5()));
		$record->setActionPriceMediaAndroidAu5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu5()));
		$record->setActionPriceMediaAndroidPc5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc5()));

		$record->setActionPriceClientPc1($this->mysqli->real_escape_string($record->getActionPriceClientPc1()));
		$record->setActionPriceClientPc2($this->mysqli->real_escape_string($record->getActionPriceClientPc2()));
		$record->setActionPriceClientPc3($this->mysqli->real_escape_string($record->getActionPriceClientPc3()));
		$record->setActionPriceClientPc4($this->mysqli->real_escape_string($record->getActionPriceClientPc4()));
		$record->setActionPriceClientPc5($this->mysqli->real_escape_string($record->getActionPriceClientPc5()));

		$record->setActionPriceMediaPc1($this->mysqli->real_escape_string($record->getActionPriceMediaPc1()));
		$record->setActionPriceMediaPc2($this->mysqli->real_escape_string($record->getActionPriceMediaPc2()));
		$record->setActionPriceMediaPc3($this->mysqli->real_escape_string($record->getActionPriceMediaPc3()));
		$record->setActionPriceMediaPc4($this->mysqli->real_escape_string($record->getActionPriceMediaPc4()));
		$record->setActionPriceMediaPc5($this->mysqli->real_escape_string($record->getActionPriceMediaPc5()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>