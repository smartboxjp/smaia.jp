<?php
class AdvertReserveDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvertReserve(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert_reserve WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new AdvertReserve();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);

			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setMsText1($row["ms_text_1"]);
			$record->setMsEmail1($row["ms_email_1"]);
			$record->setMsImageType1($row["ms_image_type_1"]);
			$record->setMsImageUrl1($row["ms_image_url_1"]);
			$record->setMsText2($row["ms_text_2"]);
			$record->setMsEmail2($row["ms_email_2"]);
			$record->setMsImageType2($row["ms_image_type_2"]);
			$record->setMsImageUrl2($row["ms_image_url_2"]);
			$record->setMsText3($row["ms_text_3"]);
			$record->setMsEmail3($row["ms_email_3"]);
			$record->setMsImageType3($row["ms_image_type_3"]);
			$record->setMsImageUrl3($row["ms_image_url_3"]);
			$record->setMsText4($row["ms_text_4"]);
			$record->setMsEmail4($row["ms_email_4"]);
			$record->setMsImageType4($row["ms_image_type_4"]);
			$record->setMsImageUrl4($row["ms_image_url_4"]);
			$record->setMsText5($row["ms_text_5"]);
			$record->setMsEmail5($row["ms_email_5"]);
			$record->setMsImageType5($row["ms_image_type_5"]);
			$record->setMsImageUrl5($row["ms_image_url_5"]);
			$record->setReserveChangeDate($row["reserve_change_date"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvertReserve($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertReserve();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);

			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setMsText1($row["ms_text_1"]);
			$record->setMsEmail1($row["ms_email_1"]);
			$record->setMsImageType1($row["ms_image_type_1"]);
			$record->setMsImageUrl1($row["ms_image_url_1"]);
			$record->setMsText2($row["ms_text_2"]);
			$record->setMsEmail2($row["ms_email_2"]);
			$record->setMsImageType2($row["ms_image_type_2"]);
			$record->setMsImageUrl2($row["ms_image_url_2"]);
			$record->setMsText3($row["ms_text_3"]);
			$record->setMsEmail3($row["ms_email_3"]);
			$record->setMsImageType3($row["ms_image_type_3"]);
			$record->setMsImageUrl3($row["ms_image_url_3"]);
			$record->setMsText4($row["ms_text_4"]);
			$record->setMsEmail4($row["ms_email_4"]);
			$record->setMsImageType4($row["ms_image_type_4"]);
			$record->setMsImageUrl4($row["ms_image_url_4"]);
			$record->setMsText5($row["ms_text_5"]);
			$record->setMsEmail5($row["ms_email_5"]);
			$record->setMsImageType5($row["ms_image_type_5"]);
			$record->setMsImageUrl5($row["ms_image_url_5"]);
			$record->setReserveChangeDate($row["reserve_change_date"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getAdvertReserveById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_reserve WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertReserve($sql);
	}

	//データの更新
	public function updateAdvertReserve($record, &$result_message = ""){
		$record = $this->escapeStringAdvertReserve($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_reserve SET "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " site_url_pc = '" . $record->getSiteUrlPc() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "
			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "

			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "', "
			. " ms_text_1 = '" . $record->getMsText1() . "', "
			. " ms_email_1 = '" . $record->getMsEmail1() . "', "
			. " ms_image_type_1 = '" . $record->getMsImageType1() . "', "
			. " ms_image_url_1 = '" . $record->getMsImageUrl1() . "', "
			. " ms_text_2 = '" . $record->getMsText2() . "', "
			. " ms_email_2 = '" . $record->getMsEmail2() . "', "
			. " ms_image_type_2 = '" . $record->getMsImageType2() . "', "
			. " ms_image_url_2 = '" . $record->getMsImageUrl2() . "', "
			. " ms_text_3 = '" . $record->getMsText3() . "', "
			. " ms_email_3 = '" . $record->getMsEmail3() . "', "
			. " ms_image_type_3 = '" . $record->getMsImageType3() . "', "
			. " ms_image_url_3 = '" . $record->getMsImageUrl3() . "', "
			. " ms_text_4 = '" . $record->getMsText4() . "', "
			. " ms_email_4 = '" . $record->getMsEmail4() . "', "
			. " ms_image_type_4 = '" . $record->getMsImageType4() . "', "
			. " ms_image_url_4 = '" . $record->getMsImageUrl4() . "', "
			. " ms_text_5 = '" . $record->getMsText5() . "', "
			. " ms_email_5 = '" . $record->getMsEmail5() . "', "
			. " ms_image_type_5 = '" . $record->getMsImageType5() . "', "
			. " ms_image_url_5 = '" . $record->getMsImageUrl5() . "', "
			. " reserve_change_date = '" . $record->getReserveChangeDate() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertAdvertReserve($record, &$result_message = ""){
		$record = $this->escapeStringAdvertReserve($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert_reserve values ('', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getSiteUrlPc() . "', "
			. " '" . $record->getClickPriceClient() . "', "
			. " '" . $record->getClickPriceMedia() . "', "
			. " '" . $record->getActionPriceClientPc1() . "', "
			. " '" . $record->getActionPriceClientPc2() . "', "
			. " '" . $record->getActionPriceClientPc3() . "', "
			. " '" . $record->getActionPriceClientPc4() . "', "
			. " '" . $record->getActionPriceClientPc5() . "', "

			. " '" . $record->getActionPriceMediaPc1() . "', "
			. " '" . $record->getActionPriceMediaPc2() . "', "
			. " '" . $record->getActionPriceMediaPc3() . "', "
			. " '" . $record->getActionPriceMediaPc4() . "', "
			. " '" . $record->getActionPriceMediaPc5() . "', "
			. " '" . $record->getMsText1() . "', "
			. " '" . $record->getMsEmail1() . "', "
			. " '" . $record->getMsImageType1() . "', "
			. " '" . $record->getMsImageUrl1() . "', "
			. " '" . $record->getMsText2() . "', "
			. " '" . $record->getMsEmail2() . "', "
			. " '" . $record->getMsImageType2() . "', "
			. " '" . $record->getMsImageUrl2() . "', "
			. " '" . $record->getMsText3() . "', "
			. " '" . $record->getMsEmail3() . "', "
			. " '" . $record->getMsImageType3() . "', "
			. " '" . $record->getMsImageUrl3() . "', "
			. " '" . $record->getMsText4() . "', "
			. " '" . $record->getMsEmail4() . "', "
			. " '" . $record->getMsImageType4() . "', "
			. " '" . $record->getMsImageUrl4() . "', "
			. " '" . $record->getMsText5() . "', "
			. " '" . $record->getMsEmail5() . "', "
			. " '" . $record->getMsImageType5() . "', "
			. " '" . $record->getMsImageUrl5() . "', "
			. " '" . $record->getReserveChangeDate() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvertReserve($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_reserve SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvertReserve($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setSiteUrlPc($this->mysqli->real_escape_string($record->getSiteUrlPc()));
		$record->setClickPriceClient($this->mysqli->real_escape_string($record->getClickPriceClient()));
		$record->setClickPriceMedia($this->mysqli->real_escape_string($record->getClickPriceMedia()));
		$record->setActionPriceClientPc1($this->mysqli->real_escape_string($record->getActionPriceClientPc1()));
		$record->setActionPriceClientPc2($this->mysqli->real_escape_string($record->getActionPriceClientPc2()));
		$record->setActionPriceClientPc3($this->mysqli->real_escape_string($record->getActionPriceClientPc3()));
		$record->setActionPriceClientPc4($this->mysqli->real_escape_string($record->getActionPriceClientPc4()));
		$record->setActionPriceClientPc5($this->mysqli->real_escape_string($record->getActionPriceClientPc5()));

		$record->setActionPriceMediaPc1($this->mysqli->real_escape_string($record->getActionPriceMediaPc1()));
		$record->setActionPriceMediaPc2($this->mysqli->real_escape_string($record->getActionPriceMediaPc2()));
		$record->setActionPriceMediaPc3($this->mysqli->real_escape_string($record->getActionPriceMediaPc3()));
		$record->setActionPriceMediaPc4($this->mysqli->real_escape_string($record->getActionPriceMediaPc4()));
		$record->setActionPriceMediaPc5($this->mysqli->real_escape_string($record->getActionPriceMediaPc5()));
		$record->setMsText1($this->mysqli->real_escape_string($record->getMsText1()));
		$record->setMsEmail1($this->mysqli->real_escape_string($record->getMsEmail1()));
		$record->setMsImageType1($this->mysqli->real_escape_string($record->getMsImageType1()));
		$record->setMsImageUrl1($this->mysqli->real_escape_string($record->getMsImageUrl1()));
		$record->setMsText2($this->mysqli->real_escape_string($record->getMsText2()));
		$record->setMsEmail2($this->mysqli->real_escape_string($record->getMsEmail2()));
		$record->setMsImageType2($this->mysqli->real_escape_string($record->getMsImageType2()));
		$record->setMsImageUrl2($this->mysqli->real_escape_string($record->getMsImageUrl2()));
		$record->setMsText3($this->mysqli->real_escape_string($record->getMsText3()));
		$record->setMsEmail3($this->mysqli->real_escape_string($record->getMsEmail3()));
		$record->setMsImageType3($this->mysqli->real_escape_string($record->getMsImageType3()));
		$record->setMsImageUrl3($this->mysqli->real_escape_string($record->getMsImageUrl3()));
		$record->setMsText4($this->mysqli->real_escape_string($record->getMsText4()));
		$record->setMsEmail4($this->mysqli->real_escape_string($record->getMsEmail4()));
		$record->setMsImageType4($this->mysqli->real_escape_string($record->getMsImageType4()));
		$record->setMsImageUrl4($this->mysqli->real_escape_string($record->getMsImageUrl4()));
		$record->setMsText5($this->mysqli->real_escape_string($record->getMsText5()));
		$record->setMsEmail5($this->mysqli->real_escape_string($record->getMsEmail5()));
		$record->setMsImageType5($this->mysqli->real_escape_string($record->getMsImageType5()));
		$record->setMsImageUrl5($this->mysqli->real_escape_string($record->getMsImageUrl5()));
		$record->setReserveChangeDate($this->mysqli->real_escape_string($record->getReserveChangeDate()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>