<?php
class AdvertDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvert(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new Advert();
			$record->setId($row["id"]);
			$record->setAdvertClientId($row["advert_client_id"]);
			$record->setAdvertCategoryId($row["advert_category_id"]);

			$record->setSupportIphoneDocomo($row["support_iphone_docomo"]);
			$record->setSupportIphoneSoftbank($row["support_iphone_softbank"]);
			$record->setSupportIphoneAu($row["support_iphone_au"]);
			$record->setSupportIphonePc($row["support_iphone_pc"]);
			$record->setSupportAndroidDocomo($row["support_android_docomo"]);
			$record->setSupportAndroidSoftbank($row["support_android_softbank"]);
			$record->setSupportAndroidAu($row["support_android_au"]);
			$record->setSupportAndroidPc($row["support_android_pc"]);
			$record->setSupportPc($row["support_pc"]);

			$record->setAdvertName($row["advert_name"]);

			$record->setSiteUrlIphoneDocomo($row["site_url_iphone_docomo"]);
			$record->setSiteUrlIphoneSoftbank($row["site_url_iphone_softbank"]);
			$record->setSiteUrlIphoneAu($row["site_url_iphone_au"]);
			$record->setSiteIphonePc($row["site_url_iphone_pc"]);
			$record->setSiteUrlAndroidDocomo($row["site_url_android_docomo"]);
			$record->setSiteUrlAndroidSoftbank($row["site_url_android_softbank"]);
			$record->setSiteUrlAndroidAu($row["site_url_android_au"]);
			$record->setSiteUrlAndroidPc($row["site_url_android_pc"]);
			$record->setSiteUrlPc($row["site_url_pc"]);

			$record->setSiteOutline($row["site_outline"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);

			// クライントiphone
			$record->setActionPriceClientIphoneDocomo1($row["action_price_client_iphone_docomo_1"]);
			$record->setActionPriceClientIphoneSoftbank1($row["action_price_client_iphone_softbank_1"]);
			$record->setActionPriceClientIphoneAu1($row["action_price_client_iphone_au_1"]);
			$record->setActionPriceClientIphonePc1($row["action_price_client_iphone_pc_1"]);

			$record->setActionPriceClientIphoneDocomo2($row["action_price_client_iphone_docomo_2"]);
			$record->setActionPriceClientIphoneSoftbank2($row["action_price_client_iphone_softbank_2"]);
			$record->setActionPriceClientIphoneAu2($row["action_price_client_iphone_au_2"]);
			$record->setActionPriceClientIphonePc2($row["action_price_client_iphone_pc_2"]);

			$record->setActionPriceClientIphoneDocomo3($row["action_price_client_iphone_docomo_3"]);
			$record->setActionPriceClientIphoneSoftbank3($row["action_price_client_iphone_softbank_3"]);
			$record->setActionPriceClientIphoneAu3($row["action_price_client_iphone_au_3"]);
			$record->setActionPriceClientIphonePc3($row["action_price_client_iphone_pc_3"]);

			$record->setActionPriceClientIphoneDocomo4($row["action_price_client_iphone_docomo_4"]);
			$record->setActionPriceClientIphoneSoftbank4($row["action_price_client_iphone_softbank_4"]);
			$record->setActionPriceClientIphoneAu4($row["action_price_client_iphone_au_4"]);
			$record->setActionPriceClientIphonePc4($row["action_price_client_iphone_pc_4"]);

			$record->setActionPriceClientIphoneDocomo5($row["action_price_client_iphone_docomo_5"]);
			$record->setActionPriceClientIphoneSoftbank5($row["action_price_client_iphone_softbank_5"]);
			$record->setActionPriceClientIphoneAu5($row["action_price_client_iphone_au_5"]);
			$record->setActionPriceClientIphonePc5($row["action_price_client_iphone_pc_5"]);

			// メディアiphone
			$record->setActionPriceMediaIphoneDocomo1($row["action_price_media_iphone_docomo_1"]);
			$record->setActionPriceMediaIphoneSoftbank1($row["action_price_media_iphone_softbank_1"]);
			$record->setActionPriceMediaIphoneAu1($row["action_price_media_iphone_au_1"]);
			$record->setActionPriceMediaIphonePc1($row["action_price_media_iphone_pc_1"]);

			$record->setActionPriceMediaIphoneDocomo2($row["action_price_media_iphone_docomo_2"]);
			$record->setActionPriceMediaIphoneSoftbank2($row["action_price_media_iphone_softbank_2"]);
			$record->setActionPriceMediaIphoneAu2($row["action_price_media_iphone_au_2"]);
			$record->setActionPriceMediaIphonePc2($row["action_price_media_iphone_pc_2"]);

			$record->setActionPriceMediaIphoneDocomo3($row["action_price_media_iphone_docomo_3"]);
			$record->setActionPriceMediaIphoneSoftbank3($row["action_price_media_iphone_softbank_3"]);
			$record->setActionPriceMediaIphoneAu3($row["action_price_media_iphone_au_3"]);
			$record->setActionPriceMediaIphonePc3($row["action_price_media_iphone_pc_3"]);

			$record->setActionPriceMediaIphoneDocomo4($row["action_price_media_iphone_docomo_4"]);
			$record->setActionPriceMediaIphoneSoftbank4($row["action_price_media_iphone_softbank_4"]);
			$record->setActionPriceMediaIphoneAu4($row["action_price_media_iphone_au_4"]);
			$record->setActionPriceMediaIphonePc4($row["action_price_media_iphone_pc_4"]);

			$record->setActionPriceMediaIphoneDocomo5($row["action_price_media_iphone_docomo_5"]);
			$record->setActionPriceMediaIphoneSoftbank5($row["action_price_media_iphone_softbank_5"]);
			$record->setActionPriceMediaIphoneAu5($row["action_price_media_iphone_au_5"]);
			$record->setActionPriceMediaIphonePc5($row["action_price_media_iphone_pc_5"]);


			// クライントandroid
			$record->setActionPriceClientAndroidDocomo1($row["action_price_client_android_docomo_1"]);
			$record->setActionPriceClientAndroidSoftbank1($row["action_price_client_android_softbank_1"]);
			$record->setActionPriceClientAndroidAu1($row["action_price_client_android_au_1"]);
			$record->setActionPriceClientAndroidPc1($row["action_price_client_android_pc_1"]);

			$record->setActionPriceClientAndroidDocomo2($row["action_price_client_android_docomo_2"]);
			$record->setActionPriceClientAndroidSoftbank2($row["action_price_client_android_softbank_2"]);
			$record->setActionPriceClientAndroidAu2($row["action_price_client_android_au_2"]);
			$record->setActionPriceClientAndroidPc2($row["action_price_client_android_pc_2"]);

			$record->setActionPriceClientAndroidDocomo3($row["action_price_client_android_docomo_3"]);
			$record->setActionPriceClientAndroidSoftbank3($row["action_price_client_android_softbank_3"]);
			$record->setActionPriceClientAndroidAu3($row["action_price_client_android_au_3"]);
			$record->setActionPriceClientAndroidPc3($row["action_price_client_android_pc_3"]);

			$record->setActionPriceClientAndroidDocomo4($row["action_price_client_android_docomo_4"]);
			$record->setActionPriceClientAndroidSoftbank4($row["action_price_client_android_softbank_4"]);
			$record->setActionPriceClientAndroidAu4($row["action_price_client_android_au_4"]);
			$record->setActionPriceClientAndroidPc4($row["action_price_client_android_pc_4"]);

			$record->setActionPriceClientAndroidDocomo5($row["action_price_client_android_docomo_5"]);
			$record->setActionPriceClientAndroidSoftbank5($row["action_price_client_android_softbank_5"]);
			$record->setActionPriceClientAndroidAu5($row["action_price_client_android_au_5"]);
			$record->setActionPriceClientAndroidPc5($row["action_price_client_android_pc_5"]);

			// メディアandroid
			$record->setActionPriceMediaAndroidDocomo1($row["action_price_media_android_docomo_1"]);
			$record->setActionPriceMediaAndroidSoftbank1($row["action_price_media_android_softbank_1"]);
			$record->setActionPriceMediaAndroidAu1($row["action_price_media_android_au_1"]);
			$record->setActionPriceMediaAndroidPc1($row["action_price_media_android_pc_1"]);

			$record->setActionPriceMediaAndroidDocomo2($row["action_price_media_android_docomo_2"]);
			$record->setActionPriceMediaAndroidSoftbank2($row["action_price_media_android_softbank_2"]);
			$record->setActionPriceMediaAndroidAu2($row["action_price_media_android_au_2"]);
			$record->setActionPriceMediaAndroidPc2($row["action_price_media_android_pc_2"]);

			$record->setActionPriceMediaAndroidDocomo3($row["action_price_media_android_docomo_3"]);
			$record->setActionPriceMediaAndroidSoftbank3($row["action_price_media_android_softbank_3"]);
			$record->setActionPriceMediaAndroidAu3($row["action_price_media_android_au_3"]);
			$record->setActionPriceMediaAndroidPc3($row["action_price_media_android_pc_3"]);

			$record->setActionPriceMediaAndroidDocomo4($row["action_price_media_android_docomo_4"]);
			$record->setActionPriceMediaAndroidSoftbank4($row["action_price_media_android_softbank_4"]);
			$record->setActionPriceMediaAndroidAu4($row["action_price_media_android_au_4"]);
			$record->setActionPriceMediaAndroidPc4($row["action_price_media_android_pc_4"]);

			$record->setActionPriceMediaAndroidDocomo5($row["action_price_media_android_docomo_5"]);
			$record->setActionPriceMediaAndroidSoftbank5($row["action_price_media_android_softbank_5"]);
			$record->setActionPriceMediaAndroidAu5($row["action_price_media_android_au_5"]);
			$record->setActionPriceMediaAndroidPc5($row["action_price_media_android_pc_5"]);


			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);

			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);

			$record->getApprovalFlag($row["approval_flag"]);
			$record->getPriceType($row["price_type"]);

			$record->setMsText1($row["ms_text_1"]);
			$record->setMsEmail1($row["ms_email_1"]);
			$record->setMsImageType1($row["ms_image_type_1"]);
			$record->setMsImageUrl1($row["ms_image_url_1"]);
			$record->setMsText2($row["ms_text_2"]);
			$record->setMsEmail2($row["ms_email_2"]);
			$record->setMsImageType2($row["ms_image_type_2"]);
			$record->setMsImageUrl2($row["ms_image_url_2"]);
			$record->setMsText3($row["ms_text_3"]);
			$record->setMsEmail3($row["ms_email_3"]);
			$record->setMsImageType3($row["ms_image_type_3"]);
			$record->setMsImageUrl3($row["ms_image_url_3"]);
			$record->setMsText4($row["ms_text_4"]);
			$record->setMsEmail4($row["ms_email_4"]);
			$record->setMsImageType4($row["ms_image_type_4"]);
			$record->setMsImageUrl4($row["ms_image_url_4"]);
			$record->setMsText5($row["ms_text_5"]);
			$record->setMsEmail5($row["ms_email_5"]);
			$record->setMsImageType5($row["ms_image_type_5"]);
			$record->setMsImageUrl5($row["ms_image_url_5"]);
			$record->setUniqueClickType($row["unique_click_type"]);

			$record->setPointBackFlag($row["point_back_flag"]);
			$record->setAdultFlag($row["adult_flag"]);
			$record->setSeoFlag($row["seo_flag"]);
			$record->setListingFlag($row["listing_flag"]);
			$record->setMailMagazineFlag($row["mail_magazine_flag"]);
			$redord->getCommunityFlag($row["community_flag"]);

			$record->setAdvertStartDate($row["advert_start_date"]);
			$record->setAdvertEndDate($row["advert_end_date"]);
			$record->setUnrestraintFlag($row["unrestraint_flag"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvert($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new Advert();
			$record->setId($row["id"]);
			$record->setAdvertClientId($row["advert_client_id"]);
			$record->setAdvertCategoryId($row["advert_category_id"]);

			$record->setSupportIphoneDocomo($row["support_iphone_docomo"]);
			$record->setSupportIphoneSoftbank($row["support_iphone_softbank"]);
			$record->setSupportIphoneAu($row["support_iphone_au"]);
			$record->setSupportIphonePc($row["support_iphone_pc"]);
			$record->setSupportAndroidDocomo($row["support_android_docomo"]);
			$record->setSupportAndroidSoftbank($row["support_android_softbank"]);
			$record->setSupportAndroidAu($row["support_android_au"]);
			$record->setSupportAndroidPc($row["support_android_pc"]);
			$record->setSupportPc($row["support_pc"]);

			$record->setAdvertName($row["advert_name"]);

			$record->setSiteUrlIphoneDocomo($row["site_url_iphone_docomo"]);
			$record->setSiteUrlIphoneSoftbank($row["site_url_iphone_softbank"]);
			$record->setSiteUrlIphoneAu($row["site_url_iphone_au"]);
			$record->setSiteUrlIphonePc($row["site_url_iphone_pc"]);
			$record->setSiteUrlAndroidDocomo($row["site_url_android_docomo"]);
			$record->setSiteUrlAndroidSoftbank($row["site_url_android_softbank"]);
			$record->setSiteUrlAndroidAu($row["site_url_android_au"]);
			$record->setSiteUrlAndroidPc($row["site_url_android_pc"]);
			$record->setSiteUrlPc($row["site_url_pc"]);

			$record->setSiteOutline($row["site_outline"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);

			// クライントiphone
			$record->setActionPriceClientIphoneDocomo1($row["action_price_client_iphone_docomo_1"]);
			$record->setActionPriceClientIphoneSoftbank1($row["action_price_client_iphone_softbank_1"]);
			$record->setActionPriceClientIphoneAu1($row["action_price_client_iphone_au_1"]);
			$record->setActionPriceClientIphonePc1($row["action_price_client_iphone_pc_1"]);

			$record->setActionPriceClientIphoneDocomo2($row["action_price_client_iphone_docomo_2"]);
			$record->setActionPriceClientIphoneSoftbank2($row["action_price_client_iphone_softbank_2"]);
			$record->setActionPriceClientIphoneAu2($row["action_price_client_iphone_au_2"]);
			$record->setActionPriceClientIphonePc2($row["action_price_client_iphone_pc_2"]);

			$record->setActionPriceClientIphoneDocomo3($row["action_price_client_iphone_docomo_3"]);
			$record->setActionPriceClientIphoneSoftbank3($row["action_price_client_iphone_softbank_3"]);
			$record->setActionPriceClientIphoneAu3($row["action_price_client_iphone_au_3"]);
			$record->setActionPriceClientIphonePc3($row["action_price_client_iphone_pc_3"]);

			$record->setActionPriceClientIphoneDocomo4($row["action_price_client_iphone_docomo_4"]);
			$record->setActionPriceClientIphoneSoftbank4($row["action_price_client_iphone_softbank_4"]);
			$record->setActionPriceClientIphoneAu4($row["action_price_client_iphone_au_4"]);
			$record->setActionPriceClientIphonePc4($row["action_price_client_iphone_pc_4"]);

			$record->setActionPriceClientIphoneDocomo5($row["action_price_client_iphone_docomo_5"]);
			$record->setActionPriceClientIphoneSoftbank5($row["action_price_client_iphone_softbank_5"]);
			$record->setActionPriceClientIphoneAu5($row["action_price_client_iphone_au_5"]);
			$record->setActionPriceClientIphonePc5($row["action_price_client_iphone_pc_5"]);

			// メディアiphone
			$record->setActionPriceMediaIphoneDocomo1($row["action_price_media_iphone_docomo_1"]);
			$record->setActionPriceMediaIphoneSoftbank1($row["action_price_media_iphone_softbank_1"]);
			$record->setActionPriceMediaIphoneAu1($row["action_price_media_iphone_au_1"]);
			$record->setActionPriceMediaIphonePc1($row["action_price_media_iphone_pc_1"]);

			$record->setActionPriceMediaIphoneDocomo2($row["action_price_media_iphone_docomo_2"]);
			$record->setActionPriceMediaIphoneSoftbank2($row["action_price_media_iphone_softbank_2"]);
			$record->setActionPriceMediaIphoneAu2($row["action_price_media_iphone_au_2"]);
			$record->setActionPriceMediaIphonePc2($row["action_price_media_iphone_pc_2"]);

			$record->setActionPriceMediaIphoneDocomo3($row["action_price_media_iphone_docomo_3"]);
			$record->setActionPriceMediaIphoneSoftbank3($row["action_price_media_iphone_softbank_3"]);
			$record->setActionPriceMediaIphoneAu3($row["action_price_media_iphone_au_3"]);
			$record->setActionPriceMediaIphonePc3($row["action_price_media_iphone_pc_3"]);

			$record->setActionPriceMediaIphoneDocomo4($row["action_price_media_iphone_docomo_4"]);
			$record->setActionPriceMediaIphoneSoftbank4($row["action_price_media_iphone_softbank_4"]);
			$record->setActionPriceMediaIphoneAu4($row["action_price_media_iphone_au_4"]);
			$record->setActionPriceMediaIphonePc4($row["action_price_media_iphone_pc_4"]);

			$record->setActionPriceMediaIphoneDocomo5($row["action_price_media_iphone_docomo_5"]);
			$record->setActionPriceMediaIphoneSoftbank5($row["action_price_media_iphone_softbank_5"]);
			$record->setActionPriceMediaIphoneAu5($row["action_price_media_iphone_au_5"]);
			$record->setActionPriceMediaIphonePc5($row["action_price_media_iphone_pc_5"]);


			// クライントandroid
			$record->setActionPriceClientAndroidDocomo1($row["action_price_client_android_docomo_1"]);
			$record->setActionPriceClientAndroidSoftbank1($row["action_price_client_android_softbank_1"]);
			$record->setActionPriceClientAndroidAu1($row["action_price_client_android_au_1"]);
			$record->setActionPriceClientAndroidPc1($row["action_price_client_android_pc_1"]);

			$record->setActionPriceClientAndroidDocomo2($row["action_price_client_android_docomo_2"]);
			$record->setActionPriceClientAndroidSoftbank2($row["action_price_client_android_softbank_2"]);
			$record->setActionPriceClientAndroidAu2($row["action_price_client_android_au_2"]);
			$record->setActionPriceClientAndroidPc2($row["action_price_client_android_pc_2"]);

			$record->setActionPriceClientAndroidDocomo3($row["action_price_client_android_docomo_3"]);
			$record->setActionPriceClientAndroidSoftbank3($row["action_price_client_android_softbank_3"]);
			$record->setActionPriceClientAndroidAu3($row["action_price_client_android_au_3"]);
			$record->setActionPriceClientAndroidPc3($row["action_price_client_android_pc_3"]);

			$record->setActionPriceClientAndroidDocomo4($row["action_price_client_android_docomo_4"]);
			$record->setActionPriceClientAndroidSoftbank4($row["action_price_client_android_softbank_4"]);
			$record->setActionPriceClientAndroidAu4($row["action_price_client_android_au_4"]);
			$record->setActionPriceClientAndroidPc4($row["action_price_client_android_pc_4"]);

			$record->setActionPriceClientAndroidDocomo5($row["action_price_client_android_docomo_5"]);
			$record->setActionPriceClientAndroidSoftbank5($row["action_price_client_android_softbank_5"]);
			$record->setActionPriceClientAndroidAu5($row["action_price_client_android_au_5"]);
			$record->setActionPriceClientAndroidPc5($row["action_price_client_android_pc_5"]);

			// メディアandroid
			$record->setActionPriceMediaAndroidDocomo1($row["action_price_media_android_docomo_1"]);
			$record->setActionPriceMediaAndroidSoftbank1($row["action_price_media_android_softbank_1"]);
			$record->setActionPriceMediaAndroidAu1($row["action_price_media_android_au_1"]);
			$record->setActionPriceMediaAndroidPc1($row["action_price_media_android_pc_1"]);

			$record->setActionPriceMediaAndroidDocomo2($row["action_price_media_android_docomo_2"]);
			$record->setActionPriceMediaAndroidSoftbank2($row["action_price_media_android_softbank_2"]);
			$record->setActionPriceMediaAndroidAu2($row["action_price_media_android_au_2"]);
			$record->setActionPriceMediaAndroidPc2($row["action_price_media_android_pc_2"]);

			$record->setActionPriceMediaAndroidDocomo3($row["action_price_media_android_docomo_3"]);
			$record->setActionPriceMediaAndroidSoftbank3($row["action_price_media_android_softbank_3"]);
			$record->setActionPriceMediaAndroidAu3($row["action_price_media_android_au_3"]);
			$record->setActionPriceMediaAndroidPc3($row["action_price_media_android_pc_3"]);

			$record->setActionPriceMediaAndroidDocomo4($row["action_price_media_android_docomo_4"]);
			$record->setActionPriceMediaAndroidSoftbank4($row["action_price_media_android_softbank_4"]);
			$record->setActionPriceMediaAndroidAu4($row["action_price_media_android_au_4"]);
			$record->setActionPriceMediaAndroidPc4($row["action_price_media_android_pc_4"]);

			$record->setActionPriceMediaAndroidDocomo5($row["action_price_media_android_docomo_5"]);
			$record->setActionPriceMediaAndroidSoftbank5($row["action_price_media_android_softbank_5"]);
			$record->setActionPriceMediaAndroidAu5($row["action_price_media_android_au_5"]);
			$record->setActionPriceMediaAndroidPc5($row["action_price_media_android_pc_5"]);

			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);

			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);

			$record->setApprovalFlag($row["approval_flag"]);
			$record->setPriceType($row["price_type"]);

			$record->setMsText1($row["ms_text_1"]);
			$record->setMsEmail1($row["ms_email_1"]);
			$record->setMsImageType1($row["ms_image_type_1"]);
			$record->setMsImageUrl1($row["ms_image_url_1"]);
			$record->setMsText2($row["ms_text_2"]);
			$record->setMsEmail2($row["ms_email_2"]);
			$record->setMsImageType2($row["ms_image_type_2"]);
			$record->setMsImageUrl2($row["ms_image_url_2"]);
			$record->setMsText3($row["ms_text_3"]);
			$record->setMsEmail3($row["ms_email_3"]);
			$record->setMsImageType3($row["ms_image_type_3"]);
			$record->setMsImageUrl3($row["ms_image_url_3"]);
			$record->setMsText4($row["ms_text_4"]);
			$record->setMsEmail4($row["ms_email_4"]);
			$record->setMsImageType4($row["ms_image_type_4"]);
			$record->setMsImageUrl4($row["ms_image_url_4"]);
			$record->setMsText5($row["ms_text_5"]);
			$record->setMsEmail5($row["ms_email_5"]);
			$record->setMsImageType5($row["ms_image_type_5"]);
			$record->setMsImageUrl5($row["ms_image_url_5"]);
			$record->setUniqueClickType($row["unique_click_type"]);

			$record->setPointBackFlag($row["point_back_flag"]);
			$record->setAdultFlag($row["adult_flag"]);
			$record->setSeoFlag($row["seo_flag"]);
			$record->setListingFlag($row["listing_flag"]);
			$record->setMailMagazineFlag($row["mail_magazine_flag"]);
			$record->setCommunityFlag($row["community_flag"]);

			$record->setAdvertStartDate($row["advert_start_date"]);
			$record->setAdvertEndDate($row["advert_end_date"]);
			$record->setUnrestraintFlag($row["unrestraint_flag"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得 deleted_at is NULLを省く
	public function getAdvertByIdAll($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert WHERE id = '$id' ";
		return $this->getAdvert($sql);
	}

	//idでデータを取得
	public function getAdvertById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvert($sql);
	}

	//id statusでデータを取得
	public function getAdvertByIdStatus($id, $status){
		$id = $this->mysqli->real_escape_string($id);
		$status = $this->mysqli->real_escape_string($status);
		$sql = " SELECT * FROM advert WHERE id = '$id' AND status = '$status' AND deleted_at is NULL ";
		return $this->getAdvert($sql);
	}

	//advert_nameでデータを取得
	public function getAdvertByAdvertName($advert_name){
		$advert_name = $this->mysqli->real_escape_string($advert_name);
		$sql = " SELECT * FROM advert WHERE advert_name = '$advert_name' AND deleted_at is NULL ";
		return $this->getAdvert($sql);
	}

	//データの更新
	public function updateAdvert($record, &$result_message = ""){
		$record = $this->escapeStringAdvert($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert SET "
			. " advert_client_id = '" . $record->getAdvertClientId() . "', "
			. " advert_category_id = '" . $record->getAdvertCategoryId() . "', "

			. " support_iphone_docomo = '" . $record->getSupportIphoneDocomo() . "', "
			. " support_iphone_softbank = '" . $record->getSupportIphoneSoftbank() . "', "
			. " support_iphone_au = '" . $record->getSupportIphoneAu() . "', "
			. " support_iphone_pc = '" . $record->getSupportIphonePc() . "', "
			. " support_android_docomo = '" . $record->getSupportAndroidDocomo() . "', "
			. " support_android_softbank = '" . $record->getSupportAndroidSoftbank() . "', "
			. " support_android_au = '" . $record->getSupportAndroidAu() . "', "
			. " support_android_pc = '" . $record->getSupportAndroidPc() . "', "
			. " support_pc = '" . $record->getSupportPc() . "', "

			. " advert_name = '" . $record->getAdvertName() . "', "

			. " site_url_iphone_docomo = '" . $record->getSiteUrlIphoneDocomo() . "', "
			. " site_url_iphone_softbank = '" . $record->getSiteUrlIphoneSoftbank() . "', "
			. " site_url_iphone_au = '" . $record->getSiteUrlIphoneAu() . "', "
			. " site_url_iphone_pc = '" . $record->getSiteUrlIphonePc() . "', "
			. " site_url_android_docomo = '" . $record->getSiteUrlAndroidDocomo() . "', "
			. " site_url_android_softbank = '" . $record->getSiteUrlAndroidSoftbank() . "', "
			. " site_url_android_au = '" . $record->getSiteUrlAndroidAu() . "', "
			. " site_url_android_pc = '" . $record->getSiteUrlAndroidPc() . "', "
			. " site_url_pc = '" . $record->getSiteUrlPc() . "', "

			. " site_outline = '" . $record->getSiteOutline() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "

			. " action_price_client_iphone_docomo_1 = '" . $record->getActionPriceClientIphoneDocomo1() . "', "
			. " action_price_client_iphone_softbank_1 = '" . $record->getActionPriceClientIphoneSoftbank1() . "', "
			. " action_price_client_iphone_au_1 = '" . $record->getActionPriceClientIphoneAu1() . "', "
			. " action_price_client_iphone_pc_1 = '" . $record->getActionPriceClientIphonePc1() . "', "

			. " action_price_client_iphone_docomo_2 = '" . $record->getActionPriceClientIphoneDocomo2() . "', "
			. " action_price_client_iphone_softbank_2 = '" . $record->getActionPriceClientIphoneSoftbank2() . "', "
			. " action_price_client_iphone_au_2 = '" . $record->getActionPriceClientIphoneAu2() . "', "
			. " action_price_client_iphone_pc_2 = '" . $record->getActionPriceClientIphonePc2() . "', "

			. " action_price_client_iphone_docomo_3 = '" . $record->getActionPriceClientIphoneDocomo3() . "', "
			. " action_price_client_iphone_softbank_3 = '" . $record->getActionPriceClientIphoneSoftbank3() . "', "
			. " action_price_client_iphone_au_3 = '" . $record->getActionPriceClientIphoneAu3() . "', "
			. " action_price_client_iphone_pc_3 = '" . $record->getActionPriceClientIphonePc3() . "', "

			. " action_price_client_iphone_docomo_4 = '" . $record->getActionPriceClientIphoneDocomo4() . "', "
			. " action_price_client_iphone_softbank_4 = '" . $record->getActionPriceClientIphoneSoftbank4() . "', "
			. " action_price_client_iphone_au_4 = '" . $record->getActionPriceClientIphoneAu4() . "', "
			. " action_price_client_iphone_pc_4 = '" . $record->getActionPriceClientIphonePc4() . "', "

			. " action_price_client_iphone_docomo_5 = '" . $record->getActionPriceClientIphoneDocomo5() . "', "
			. " action_price_client_iphone_softbank_5 = '" . $record->getActionPriceClientIphoneSoftbank5() . "', "
			. " action_price_client_iphone_au_5 = '" . $record->getActionPriceClientIphoneAu5() . "', "
			. " action_price_client_iphone_pc_5 = '" . $record->getActionPriceClientIphonePc5() . "', "

			. " action_price_media_iphone_docomo_1 = '" . $record->getActionPriceMediaIphoneDocomo1() . "', "
			. " action_price_media_iphone_softbank_1 = '" . $record->getActionPriceMediaIphoneSoftbank1() . "', "
			. " action_price_media_iphone_au_1 = '" . $record->getActionPriceMediaIphoneAu1() . "', "
			. " action_price_media_iphone_pc_1 = '" . $record->getActionPriceMediaIphonePc1() . "', "

			. " action_price_media_iphone_docomo_2 = '" . $record->getActionPriceMediaIphoneDocomo2() . "', "
			. " action_price_media_iphone_softbank_2 = '" . $record->getActionPriceMediaIphoneSoftbank2() . "', "
			. " action_price_media_iphone_au_2 = '" . $record->getActionPriceMediaIphoneAu2() . "', "
			. " action_price_media_iphone_pc_2 = '" . $record->getActionPriceMediaIphonePc2() . "', "

			. " action_price_media_iphone_docomo_3 = '" . $record->getActionPriceMediaIphoneDocomo3() . "', "
			. " action_price_media_iphone_softbank_3 = '" . $record->getActionPriceMediaIphoneSoftbank3() . "', "
			. " action_price_media_iphone_au_3 = '" . $record->getActionPriceMediaIphoneAu3() . "', "
			. " action_price_media_iphone_pc_3 = '" . $record->getActionPriceMediaIphonePc3() . "', "

			. " action_price_media_iphone_docomo_4 = '" . $record->getActionPriceMediaIphoneDocomo4() . "', "
			. " action_price_media_iphone_softbank_4 = '" . $record->getActionPriceMediaIphoneSoftbank4() . "', "
			. " action_price_media_iphone_au_4 = '" . $record->getActionPriceMediaIphoneAu4() . "', "
			. " action_price_media_iphone_pc_4 = '" . $record->getActionPriceMediaIphonePc4() . "', "

			. " action_price_media_iphone_docomo_5 = '" . $record->getActionPriceMediaIphoneDocomo5() . "', "
			. " action_price_media_iphone_softbank_5 = '" . $record->getActionPriceMediaIphoneSoftbank5() . "', "
			. " action_price_media_iphone_au_5 = '" . $record->getActionPriceMediaIphoneAu5() . "', "
			. " action_price_media_iphone_pc_5 = '" . $record->getActionPriceMediaIphonePc5() . "', "

			. " action_price_client_android_docomo_1 = '" . $record->getActionPriceClientAndroidDocomo1() . "', "
			. " action_price_client_android_softbank_1 = '" . $record->getActionPriceClientAndroidSoftbank1() . "', "
			. " action_price_client_android_au_1 = '" . $record->getActionPriceClientAndroidAu1() . "', "
			. " action_price_client_android_pc_1 = '" . $record->getActionPriceClientAndroidPc1() . "', "

			. " action_price_client_android_docomo_2 = '" . $record->getActionPriceClientAndroidDocomo2() . "', "
			. " action_price_client_android_softbank_2 = '" . $record->getActionPriceClientAndroidSoftbank2() . "', "
			. " action_price_client_android_au_2 = '" . $record->getActionPriceClientAndroidAu2() . "', "
			. " action_price_client_android_pc_2 = '" . $record->getActionPriceClientAndroidPc2() . "', "

			. " action_price_client_android_docomo_3 = '" . $record->getActionPriceClientAndroidDocomo3() . "', "
			. " action_price_client_android_softbank_3 = '" . $record->getActionPriceClientAndroidSoftbank3() . "', "
			. " action_price_client_android_au_3 = '" . $record->getActionPriceClientAndroidAu3() . "', "
			. " action_price_client_android_pc_3 = '" . $record->getActionPriceClientAndroidPc3() . "', "

			. " action_price_client_android_docomo_4 = '" . $record->getActionPriceClientAndroidDocomo4() . "', "
			. " action_price_client_android_softbank_4 = '" . $record->getActionPriceClientAndroidSoftbank4() . "', "
			. " action_price_client_android_au_4 = '" . $record->getActionPriceClientAndroidAu4() . "', "
			. " action_price_client_android_pc_4 = '" . $record->getActionPriceClientAndroidPc4() . "', "

			. " action_price_client_android_docomo_5 = '" . $record->getActionPriceClientAndroidDocomo5() . "', "
			. " action_price_client_android_softbank_5 = '" . $record->getActionPriceClientAndroidSoftbank5() . "', "
			. " action_price_client_android_au_5 = '" . $record->getActionPriceClientAndroidAu5() . "', "
			. " action_price_client_android_pc_5 = '" . $record->getActionPriceClientAndroidPc5() . "', "

			. " action_price_media_android_docomo_1 = '" . $record->getActionPriceMediaAndroidDocomo1() . "', "
			. " action_price_media_android_softbank_1 = '" . $record->getActionPriceMediaAndroidSoftbank1() . "', "
			. " action_price_media_android_au_1 = '" . $record->getActionPriceMediaAndroidAu1() . "', "
			. " action_price_media_android_pc_1 = '" . $record->getActionPriceMediaAndroidPc1() . "', "

			. " action_price_media_android_docomo_2 = '" . $record->getActionPriceMediaAndroidDocomo2() . "', "
			. " action_price_media_android_softbank_2 = '" . $record->getActionPriceMediaAndroidSoftbank2() . "', "
			. " action_price_media_android_au_2 = '" . $record->getActionPriceMediaAndroidAu2() . "', "
			. " action_price_media_android_pc_2 = '" . $record->getActionPriceMediaAndroidPc2() . "', "

			. " action_price_media_android_docomo_3 = '" . $record->getActionPriceMediaAndroidDocomo3() . "', "
			. " action_price_media_android_softbank_3 = '" . $record->getActionPriceMediaAndroidSoftbank3() . "', "
			. " action_price_media_android_au_3 = '" . $record->getActionPriceMediaAndroidAu3() . "', "
			. " action_price_media_android_pc_3 = '" . $record->getActionPriceMediaAndroidPc3() . "', "

			. " action_price_media_android_docomo_4 = '" . $record->getActionPriceMediaAndroidDocomo4() . "', "
			. " action_price_media_android_softbank_4 = '" . $record->getActionPriceMediaAndroidSoftbank4() . "', "
			. " action_price_media_android_au_4 = '" . $record->getActionPriceMediaAndroidAu4() . "', "
			. " action_price_media_android_pc_4 = '" . $record->getActionPriceMediaAndroidPc4() . "', "

			. " action_price_media_android_docomo_5 = '" . $record->getActionPriceMediaAndroidDocomo5() . "', "
			. " action_price_media_android_softbank_5 = '" . $record->getActionPriceMediaAndroidSoftbank5() . "', "
			. " action_price_media_android_au_5 = '" . $record->getActionPriceMediaAndroidAu5() . "', "
			. " action_price_media_android_pc_5 = '" . $record->getActionPriceMediaAndroidPc5() . "', "


			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "

			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "', "

			. " approval_flag = '" . $record->getApprovalFlag() . "', "
			. " price_type = '" . $record->getPriceType() . "', "

			. " ms_text_1 = '" . $record->getMsText1() . "', "
			. " ms_email_1 = '" . $record->getMsEmail1() . "', "
			. " ms_image_type_1 = '" . $record->getMsImageType1() . "', "
			. " ms_image_url_1 = '" . $record->getMsImageUrl1() . "', "
			. " ms_text_2 = '" . $record->getMsText2() . "', "
			. " ms_email_2 = '" . $record->getMsEmail2() . "', "
			. " ms_image_type_2 = '" . $record->getMsImageType2() . "', "
			. " ms_image_url_2 = '" . $record->getMsImageUrl2() . "', "
			. " ms_text_3 = '" . $record->getMsText3() . "', "
			. " ms_email_3 = '" . $record->getMsEmail3() . "', "
			. " ms_image_type_3 = '" . $record->getMsImageType3() . "', "
			. " ms_image_url_3 = '" . $record->getMsImageUrl3() . "', "
			. " ms_text_4 = '" . $record->getMsText4() . "', "
			. " ms_email_4 = '" . $record->getMsEmail4() . "', "
			. " ms_image_type_4 = '" . $record->getMsImageType4() . "', "
			. " ms_image_url_4 = '" . $record->getMsImageUrl4() . "', "
			. " ms_text_5 = '" . $record->getMsText5() . "', "
			. " ms_email_5 = '" . $record->getMsEmail5() . "', "
			. " ms_image_type_5 = '" . $record->getMsImageType5() . "', "
			. " ms_image_url_5 = '" . $record->getMsImageUrl5() . "', "
			. " unique_click_type = '" . $record->getUniqueClickType() . "', "

			. " point_back_flag = '" . $record->getPointBackFlag() . "', "
			. " adult_flag = '" . $record->getAdultFlag() . "', "
			. " seo_flag = '" . $record->getSeoFlag() . "', "
			. " listing_flag = '" . $record->getListingFlag() . "', "
			. " mail_magazine_flag = '" . $record->getMailMagazineFlag() . "', "
			. " community_flag = '" . $record->getCommunityFlag() . "', "

			. " advert_start_date = '" . $record->getAdvertStartDate() . "', "
			. " advert_end_date = '" . $record->getAdvertEndDate() . "', "
			. " unrestraint_flag = '" . $record->getUnrestraintFlag() . "', "
			. " test_flag = '" . $record->getTestFlag() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";

//			echo $sql;


		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertAdvert($record, &$result_message = ""){
		$record = $this->escapeStringAdvert($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert values ('', "
			. " '" . $record->getAdvertClientId() . "', "
			. " '" . $record->getAdvertCategoryId() . "', "


			. " '" . $record->getSupportIphoneDocomo() . "', "
			. " '" . $record->getSupportIphoneSoftbank() . "', "
			. " '" . $record->getSupportIphoneAu() . "', "
			. " '" . $record->getSupportIphonePc() . "', "
			. " '" . $record->getSupportAndroidDocomo() . "', "
			. " '" . $record->getSupportAndroidSoftbank() . "', "
			. " '" . $record->getSupportAndroidAu() . "', "
			. " '" . $record->getSupportAndroidPc() . "', "
			. " '" . $record->getSupportPc() . "', "

			. " '" . $record->getAdvertName() . "', "

			. " '" . $record->getSiteUrlIphoneDocomo() . "', "
			. " '" . $record->getSiteUrlIphoneSoftbank() . "', "
			. " '" . $record->getSiteUrlIphoneAu() . "', "
			. " '" . $record->getSiteUrlIphonePc() . "', "
			. " '" . $record->getSiteUrlAndroidDocomo() . "', "
			. " '" . $record->getSiteUrlAndroidSoftbank() . "', "
			. " '" . $record->getSiteUrlAndroidAu() . "', "
			. " '" . $record->getSiteUrlAndroidPc() . "', "
			. " '" . $record->getSiteUrlPc() . "', "

			. " '" . $record->getSiteOutline() . "', "
			. " '" . $record->getClickPriceClient() . "', "
			. " '" . $record->getClickPriceMedia() . "', "

			. " '" . $record->getActionPriceClientIphoneDocomo1() . "', "
			. " '" . $record->getActionPriceClientIphoneDocomo2() . "', "
			. " '" . $record->getActionPriceClientIphoneDocomo3() . "', "
			. " '" . $record->getActionPriceClientIphoneDocomo4() . "', "
			. " '" . $record->getActionPriceClientIphoneDocomo5() . "', "

			. " '" . $record->getActionPriceClientIphoneSoftbank1() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank2() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank3() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank4() . "', "
			. " '" . $record->getActionPriceClientIphoneSoftbank5() . "', "

			. " '" . $record->getActionPriceClientIphoneAu1() . "', "
			. " '" . $record->getActionPriceClientIphoneAu2() . "', "
			. " '" . $record->getActionPriceClientIphoneAu3() . "', "
			. " '" . $record->getActionPriceClientIphoneAu4() . "', "
			. " '" . $record->getActionPriceClientIphoneAu5() . "', "

			. " '" . $record->getActionPriceClientIphonePc1() . "', "
			. " '" . $record->getActionPriceClientIphonePc2() . "', "
			. " '" . $record->getActionPriceClientIphonePc3() . "', "
			. " '" . $record->getActionPriceClientIphonePc4() . "', "
			. " '" . $record->getActionPriceClientIphonePc5() . "', "


			. " '" . $record->getActionPriceMediaIphoneDocomo1() . "', "
			. " '" . $record->getActionPriceMediaIphoneDocomo2() . "', "
			. " '" . $record->getActionPriceMediaIphoneDocomo3() . "', "
			. " '" . $record->getActionPriceMediaIphoneDocomo4() . "', "
			. " '" . $record->getActionPriceMediaIphoneDocomo5() . "', "

			. " '" . $record->getActionPriceMediaIphoneSoftbank1() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank2() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank3() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank4() . "', "
			. " '" . $record->getActionPriceMediaIphoneSoftbank5() . "', "

			. " '" . $record->getActionPriceMediaIphoneAu1() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu2() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu3() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu4() . "', "
			. " '" . $record->getActionPriceMediaIphoneAu5() . "', "

			. " '" . $record->getActionPriceMediaIphonePc1() . "', "
			. " '" . $record->getActionPriceMediaIphonePc2() . "', "
			. " '" . $record->getActionPriceMediaIphonePc3() . "', "
			. " '" . $record->getActionPriceMediaIphonePc4() . "', "
			. " '" . $record->getActionPriceMediaIphonePc5() . "', "


			. " '" . $record->getActionPriceClientAndroidDocomo1() . "', "
			. " '" . $record->getActionPriceClientAndroidDocomo2() . "', "
			. " '" . $record->getActionPriceClientAndroidDocomo3() . "', "
			. " '" . $record->getActionPriceClientAndroidDocomo4() . "', "
			. " '" . $record->getActionPriceClientAndroidDocomo5() . "', "

			. " '" . $record->getActionPriceClientAndroidSoftbank1() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank2() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank3() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank4() . "', "
			. " '" . $record->getActionPriceClientAndroidSoftbank5() . "', "

			. " '" . $record->getActionPriceClientAndroidAu1() . "', "
			. " '" . $record->getActionPriceClientAndroidAu2() . "', "
			. " '" . $record->getActionPriceClientAndroidAu3() . "', "
			. " '" . $record->getActionPriceClientAndroidAu4() . "', "
			. " '" . $record->getActionPriceClientAndroidAu5() . "', "

			. " '" . $record->getActionPriceClientAndroidPc1() . "', "
			. " '" . $record->getActionPriceClientAndroidPc2() . "', "
			. " '" . $record->getActionPriceClientAndroidPc3() . "', "
			. " '" . $record->getActionPriceClientAndroidPc4() . "', "
			. " '" . $record->getActionPriceClientAndroidPc5() . "', "

			. " '" . $record->getActionPriceMediaAndroidDocomo1() . "', "
			. " '" . $record->getActionPriceMediaAndroidDocomo2() . "', "
			. " '" . $record->getActionPriceMediaAndroidDocomo3() . "', "
			. " '" . $record->getActionPriceMediaAndroidDocomo4() . "', "
			. " '" . $record->getActionPriceMediaAndroidDocomo5() . "', "

			. " '" . $record->getActionPriceMediaAndroidSoftbank1() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank2() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank3() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank4() . "', "
			. " '" . $record->getActionPriceMediaAndroidSoftbank5() . "', "

			. " '" . $record->getActionPriceMediaAndroidAu1() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu2() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu3() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu4() . "', "
			. " '" . $record->getActionPriceMediaAndroidAu5() . "', "

			. " '" . $record->getActionPriceMediaAndroidPc1() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc2() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc3() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc4() . "', "
			. " '" . $record->getActionPriceMediaAndroidPc5() . "', "

			. " '" . $record->getActionPriceClientPc1() . "', "
			. " '" . $record->getActionPriceClientPc2() . "', "
			. " '" . $record->getActionPriceClientPc3() . "', "
			. " '" . $record->getActionPriceClientPc4() . "', "
			. " '" . $record->getActionPriceClientPc5() . "', "

			. " '" . $record->getActionPriceMediaPc1() . "', "
			. " '" . $record->getActionPriceMediaPc2() . "', "
			. " '" . $record->getActionPriceMediaPc3() . "', "
			. " '" . $record->getActionPriceMediaPc4() . "', "
			. " '" . $record->getActionPriceMediaPc5() . "', "

			. " '" . $record->getApprovalFlag() . "', "
			. " '" . $record->getPriceType() . "', "

			. " '" . $record->getMsText1() . "', "
			. " '" . $record->getMsEmail1() . "', "
			. " '" . $record->getMsImageType1() . "', "
			. " '" . $record->getMsImageUrl1() . "', "
			. " '" . $record->getMsText2() . "', "
			. " '" . $record->getMsEmail2() . "', "
			. " '" . $record->getMsImageType2() . "', "
			. " '" . $record->getMsImageUrl2() . "', "
			. " '" . $record->getMsText3() . "', "
			. " '" . $record->getMsEmail3() . "', "
			. " '" . $record->getMsImageType3() . "', "
			. " '" . $record->getMsImageUrl3() . "', "
			. " '" . $record->getMsText4() . "', "
			. " '" . $record->getMsEmail4() . "', "
			. " '" . $record->getMsImageType4() . "', "
			. " '" . $record->getMsImageUrl4() . "', "
			. " '" . $record->getMsText5() . "', "
			. " '" . $record->getMsEmail5() . "', "
			. " '" . $record->getMsImageType5() . "', "
			. " '" . $record->getMsImageUrl5() . "', "
			. " '" . $record->getUniqueClickType() . "', "

			. " '" . $record->getPointBackFlag() . "', "
			. " '" . $record->getAdultFlag() . "', "
			. " '" . $record->getSeoFlag() . "', "
			. " '" . $record->getListingFlag() . "', "
			. " '" . $record->getMailMagazineFlag() . "', "
			. " '" . $record->getCommunityFlag() . "', "

			. " '" . $record->getAdvertStartDate() . "', "
			. " '" . $record->getAdvertEndDate() . "', "
			. " '" . $record->getUnrestraintFlag() . "', "
			. " '" . $record->getTestFlag() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";

//		echo "<hr />" . $sql . "<hr />";

		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvert($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvert($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertClientId($this->mysqli->real_escape_string($record->getAdvertClientId()));
		$record->setAdvertCategoryId($this->mysqli->real_escape_string($record->getAdvertCategoryId()));

		$record->setSupportIphoneDocomo($this->mysqli->real_escape_string($record->getSupportIphoneDocomo()));
		$record->setSupportIphoneSoftbank($this->mysqli->real_escape_string($record->getSupportIphoneSoftbank()));
		$record->setSupportIphoneAu($this->mysqli->real_escape_string($record->getSupportIphoneAu()));
		$record->setSupportIphonePc($this->mysqli->real_escape_string($record->getSupportIphonePc()));
		$record->setSupportAndroidDocomo($this->mysqli->real_escape_string($record->getSupportAndroidDocomo()));
		$record->setSupportAndroidSoftbank($this->mysqli->real_escape_string($record->getSupportAndroidSoftbank()));
		$record->setSupportAndroidAu($this->mysqli->real_escape_string($record->getSupportAndroidAu()));
		$record->setSupportAndroidPc($this->mysqli->real_escape_string($record->getSupportAndroidPc()));
		$record->setSupportPc($this->mysqli->real_escape_string($record->getSupportPc()));

		$record->setAdvertName($this->mysqli->real_escape_string($record->getAdvertName()));

		$record->setSiteUrlIphoneDocomo($this->mysqli->real_escape_string($record->getSiteUrlIphoneDocomo()));
		$record->setSiteUrlIphoneSoftbank($this->mysqli->real_escape_string($record->getSiteUrlIphoneSoftbank()));
		$record->setSiteUrlIphoneAu($this->mysqli->real_escape_string($record->getSiteUrlIphoneAu()));
		$record->setSiteUrlIphonePc($this->mysqli->real_escape_string($record->getSiteUrlIphonePc()));
		$record->setSiteUrlAndroidDocomo($this->mysqli->real_escape_string($record->getSiteUrlAndroidDocomo()));
		$record->setSiteUrlAndroidSoftbank($this->mysqli->real_escape_string($record->getSiteUrlAndroidSoftbank()));
		$record->setSiteUrlAndroidAu($this->mysqli->real_escape_string($record->getSiteUrlAndroidAu()));
		$record->setSiteUrlAndroidPc($this->mysqli->real_escape_string($record->getSiteUrlAndroidPc()));
		$record->setSiteUrlPc($this->mysqli->real_escape_string($record->getSiteUrlPc()));

		$record->setSiteOutline($this->mysqli->real_escape_string($record->getSiteOutline()));
		$record->setClickPriceClient($this->mysqli->real_escape_string($record->getClickPriceClient()));
		$record->setClickPriceMedia($this->mysqli->real_escape_string($record->getClickPriceMedia()));


		$record->setActionPriceClientIphoneDocomo1($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo1()));
		$record->setActionPriceClientIphoneSoftbank1($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank1()));
		$record->setActionPriceClientIphoneAu1($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu1()));
		$record->setActionPriceClientIphonePc1($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc1()));

		$record->setActionPriceClientIphoneDocomo2($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo2()));
		$record->setActionPriceClientIphoneSoftbank2($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank2()));
		$record->setActionPriceClientIphoneAu2($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu2()));
		$record->setActionPriceClientIphonePc2($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc2()));

		$record->setActionPriceClientIphoneDocomo3($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo3()));
		$record->setActionPriceClientIphoneSoftbank3($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank3()));
		$record->setActionPriceClientIphoneAu3($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu3()));
		$record->setActionPriceClientIphonePc3($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc3()));

		$record->setActionPriceClientIphoneDocomo4($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo4()));
		$record->setActionPriceClientIphoneSoftbank4($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank4()));
		$record->setActionPriceClientIphoneAu4($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu4()));
		$record->setActionPriceClientIphonePc4($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc4()));

		$record->setActionPriceClientIphoneDocomo5($this->mysqli->real_escape_string($record->getActionPriceClientIphoneDocomo5()));
		$record->setActionPriceClientIphoneSoftbank5($this->mysqli->real_escape_string($record->getActionPriceClientIphoneSoftbank5()));
		$record->setActionPriceClientIphoneAu5($this->mysqli->real_escape_string($record->getActionPriceClientIphoneAu5()));
		$record->setActionPriceClientIphonePc5($this->mysqli->real_escape_string($record->getActionPriceClientIphonePc5()));

		$record->setActionPriceMediaIphoneDocomo1($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo1()));
		$record->setActionPriceMediaIphoneSoftbank1($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank1()));
		$record->setActionPriceMediaIphoneAu1($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu1()));
		$record->setActionPriceMediaIphonePc1($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc1()));

		$record->setActionPriceMediaIphoneDocomo2($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo2()));
		$record->setActionPriceMediaIphoneSoftbank2($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank2()));
		$record->setActionPriceMediaIphoneAu2($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu2()));
		$record->setActionPriceMediaIphonePc2($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc2()));

		$record->setActionPriceMediaIphoneDocomo3($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo3()));
		$record->setActionPriceMediaIphoneSoftbank3($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank3()));
		$record->setActionPriceMediaIphoneAu3($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu3()));
		$record->setActionPriceMediaIphonePc3($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc3()));

		$record->setActionPriceMediaIphoneDocomo4($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo4()));
		$record->setActionPriceMediaIphoneSoftbank4($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank4()));
		$record->setActionPriceMediaIphoneAu4($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu4()));
		$record->setActionPriceMediaIphonePc4($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc4()));

		$record->setActionPriceMediaIphoneDocomo5($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneDocomo5()));
		$record->setActionPriceMediaIphoneSoftbank5($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneSoftbank5()));
		$record->setActionPriceMediaIphoneAu5($this->mysqli->real_escape_string($record->getActionPriceMediaIphoneAu5()));
		$record->setActionPriceMediaIphonePc5($this->mysqli->real_escape_string($record->getActionPriceMediaIphonePc5()));


		$record->setActionPriceClientAndroidDocomo1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo1()));
		$record->setActionPriceClientAndroidSoftbank1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank1()));
		$record->setActionPriceClientAndroidAu1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu1()));
		$record->setActionPriceClientAndroidPc1($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc1()));

		$record->setActionPriceClientAndroidDocomo2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo2()));
		$record->setActionPriceClientAndroidSoftbank2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank2()));
		$record->setActionPriceClientAndroidAu2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu2()));
		$record->setActionPriceClientAndroidPc2($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc2()));

		$record->setActionPriceClientAndroidDocomo3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo3()));
		$record->setActionPriceClientAndroidSoftbank3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank3()));
		$record->setActionPriceClientAndroidAu3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu3()));
		$record->setActionPriceClientAndroidPc3($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc3()));

		$record->setActionPriceClientAndroidDocomo4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo4()));
		$record->setActionPriceClientAndroidSoftbank4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank4()));
		$record->setActionPriceClientAndroidAu4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu4()));
		$record->setActionPriceClientAndroidPc4($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc4()));

		$record->setActionPriceClientAndroidDocomo5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidDocomo5()));
		$record->setActionPriceClientAndroidSoftbank5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidSoftbank5()));
		$record->setActionPriceClientAndroidAu5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidAu5()));
		$record->setActionPriceClientAndroidPc5($this->mysqli->real_escape_string($record->getActionPriceClientAndroidPc5()));

		$record->setActionPriceMediaAndroidDocomo1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo1()));
		$record->setActionPriceMediaAndroidSoftbank1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank1()));
		$record->setActionPriceMediaAndroidAu1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu1()));
		$record->setActionPriceMediaAndroidPc1($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc1()));

		$record->setActionPriceMediaAndroidDocomo2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo2()));
		$record->setActionPriceMediaAndroidSoftbank2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank2()));
		$record->setActionPriceMediaAndroidAu2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu2()));
		$record->setActionPriceMediaAndroidPc2($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc2()));

		$record->setActionPriceMediaAndroidDocomo3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo3()));
		$record->setActionPriceMediaAndroidSoftbank3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank3()));
		$record->setActionPriceMediaAndroidAu3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu3()));
		$record->setActionPriceMediaAndroidPc3($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc3()));

		$record->setActionPriceMediaAndroidDocomo4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo4()));
		$record->setActionPriceMediaAndroidSoftbank4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank4()));
		$record->setActionPriceMediaAndroidAu4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu4()));
		$record->setActionPriceMediaAndroidPc4($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc4()));

		$record->setActionPriceMediaAndroidDocomo5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidDocomo5()));
		$record->setActionPriceMediaAndroidSoftbank5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidSoftbank5()));
		$record->setActionPriceMediaAndroidAu5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidAu5()));
		$record->setActionPriceMediaAndroidPc5($this->mysqli->real_escape_string($record->getActionPriceMediaAndroidPc5()));


		$record->setActionPriceClientPc1($this->mysqli->real_escape_string($record->getActionPriceClientPc1()));
		$record->setActionPriceClientPc2($this->mysqli->real_escape_string($record->getActionPriceClientPc2()));
		$record->setActionPriceClientPc3($this->mysqli->real_escape_string($record->getActionPriceClientPc3()));
		$record->setActionPriceClientPc4($this->mysqli->real_escape_string($record->getActionPriceClientPc4()));
		$record->setActionPriceClientPc5($this->mysqli->real_escape_string($record->getActionPriceClientPc5()));

		$record->setActionPriceMediaPc1($this->mysqli->real_escape_string($record->getActionPriceMediaPc1()));
		$record->setActionPriceMediaPc2($this->mysqli->real_escape_string($record->getActionPriceMediaPc2()));
		$record->setActionPriceMediaPc3($this->mysqli->real_escape_string($record->getActionPriceMediaPc3()));
		$record->setActionPriceMediaPc4($this->mysqli->real_escape_string($record->getActionPriceMediaPc4()));
		$record->setActionPriceMediaPc5($this->mysqli->real_escape_string($record->getActionPriceMediaPc5()));

		$record->setApprovalFlag($this->mysqli->real_escape_string($record->getApprovalFlag()));
		$record->setPriceType($this->mysqli->real_escape_string($record->getPriceType()));

		$record->setMsText1($this->mysqli->real_escape_string($record->getMsText1()));
		$record->setMsEmail1($this->mysqli->real_escape_string($record->getMsEmail1()));
		$record->setMsImageType1($this->mysqli->real_escape_string($record->getMsImageType1()));
		$record->setMsImageUrl1($this->mysqli->real_escape_string($record->getMsImageUrl1()));
		$record->setMsText2($this->mysqli->real_escape_string($record->getMsText2()));
		$record->setMsEmail2($this->mysqli->real_escape_string($record->getMsEmail2()));
		$record->setMsImageType2($this->mysqli->real_escape_string($record->getMsImageType2()));
		$record->setMsImageUrl2($this->mysqli->real_escape_string($record->getMsImageUrl2()));
		$record->setMsText3($this->mysqli->real_escape_string($record->getMsText3()));
		$record->setMsEmail3($this->mysqli->real_escape_string($record->getMsEmail3()));
		$record->setMsImageType3($this->mysqli->real_escape_string($record->getMsImageType3()));
		$record->setMsImageUrl3($this->mysqli->real_escape_string($record->getMsImageUrl3()));
		$record->setMsText4($this->mysqli->real_escape_string($record->getMsText4()));
		$record->setMsEmail4($this->mysqli->real_escape_string($record->getMsEmail4()));
		$record->setMsImageType4($this->mysqli->real_escape_string($record->getMsImageType4()));
		$record->setMsImageUrl4($this->mysqli->real_escape_string($record->getMsImageUrl4()));
		$record->setMsText5($this->mysqli->real_escape_string($record->getMsText5()));
		$record->setMsEmail5($this->mysqli->real_escape_string($record->getMsEmail5()));
		$record->setMsImageType5($this->mysqli->real_escape_string($record->getMsImageType5()));
		$record->setMsImageUrl5($this->mysqli->real_escape_string($record->getMsImageUrl5()));
		$record->setUniqueClickType($this->mysqli->real_escape_string($record->getUniqueClickType()));

		$record->setPointBackFlag($this->mysqli->real_escape_string($record->getPointBackFlag()));
		$record->setAdultFlag($this->mysqli->real_escape_string($record->getAdultFlag()));
		$record->setSeoFlag($this->mysqli->real_escape_string($record->getSeoFlag()));
		$record->setListingFlag($this->mysqli->real_escape_string($record->getListingFlag()));
		$record->setMailMagazineFlag($this->mysqli->real_escape_string($record->getMailMagazineFlag()));
		$record->setCommunityFlag($this->mysqli->real_escape_string($record->getCommunityFlag()));

		$record->setAdvertStartDate($this->mysqli->real_escape_string($record->getAdvertStartDate()));
		$record->setAdvertEndDate($this->mysqli->real_escape_string($record->getAdvertEndDate()));
		$record->setUnrestraintFlag($this->mysqli->real_escape_string($record->getUnrestraintFlag()));
		$record->setTestFlag($this->mysqli->real_escape_string($record->getTestFlag()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>