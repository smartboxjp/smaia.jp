<?php
class WithdrawalDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllActionLog(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM withdrawal_logs WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new ActionLog();
			$record->setId($row["id"]);
			$record->setSessionId($row["session_id"]);
			$record->setCarrierId($row["carrier_id"]);
			$record->setUserAgent($row["user_agent"]);
			$record->setUid($row["uid"]);
			$record->setIpAddress($row["ip_address"]);
			$record->setHostName($row["host_name"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setAdvertClientId($row["advert_client_id"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClient($row["action_price_client"]);
			$record->setActionPriceMedia($row["action_price_media"]);

			$record->setApprovalFlag($row["approval_flag"]);
			$record->setPriceType($row["price_type"]);
			$record->setOrderNum($row["order_num"]);

			$record->setLinkUrl($row["link_url"]);
			$record->setActionCompleteDate($row["action_complete_date"]);
			$record->setConfirmFlag($row["confirm_flag"]);
			$record->setPointBackParameter($row["point_back_parameter"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setStatus($row["status"]);
			//---------------------------------------------------
			// 7/21 追加
			$record->setAuid($row["auid"]);
			//---------------------------------------------------
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//-------------------------------------------
	// 7/22 追加
	public function getAuidLog($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows == 0){
			return true;
		}else{
			return false;
		}
	}
	//-------------------------------------------

	//指定されたデータの取得
	private function getActionLog($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new ActionLog();
			$record->setId($row["id"]);
			$record->setSessionId($row["session_id"]);
			$record->setCarrierId($row["carrier_id"]);
			$record->setUserAgent($row["user_agent"]);
			$record->setUid($row["uid"]);
			$record->setIpAddress($row["ip_address"]);
			$record->setHostName($row["host_name"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setAdvertClientId($row["advert_client_id"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClient($row["action_price_client"]);
			$record->setActionPriceMedia($row["action_price_media"]);

			$record->setApprovalFlag($row["approval_flag"]);
			$record->setPriceType($row["price_type"]);
			$record->setOrderNum($row["order_num"]);

			$record->setLinkUrl($row["link_url"]);
			$record->setActionCompleteDate($row["action_complete_date"]);
			$record->setConfirmFlag($row["confirm_flag"]);
			$record->setPointBackParameter($row["point_back_parameter"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setStatus($row["status"]);
			//---------------------------------------------------
			// 7/21 追加
			$record->setAuid($row["auid"]);
			//---------------------------------------------------
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
//		return $result;
		return $record;
	}

	//idでデータを取得
	public function getActionLogById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM withdrawal_logs WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getActionLog($sql);
	}

	//session_idでデータを取得
	public function getActionLogBySessionId($session_id){
		$session_id = $this->mysqli->real_escape_string($session_id);
		$sql = " SELECT * FROM withdrawal_logs WHERE session_id = '$session_id' AND deleted_at is NULL ";
		return $this->getActionLog($sql);
	}



	//データの更新
	public function updateActionLog($record, &$result_message = ""){
		$record = $this->escapeStringActionLog($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE withdrawal_logs SET "
			. " session_id = '" . $record->getSessionId() . "', "
			. " carrier_id = '" . $record->getCarrierId() . "', "
			. " user_agent = '" . $record->getUserAgent() . "', "
			. " uid = '" . $record->getUid() . "', "
			. " ip_address = '" . $record->getIpAddress() . "', "
			. " host_name = '" . $record->getHostName() . "', "
			. " media_id = '" . $record->getMediaId() . "', "
			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " advert_client_id = '" . $record->getAdvertClientId() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "
			. " action_price_client = '" . $record->getActionPriceClient() . "', "
			. " action_price_media = '" . $record->getActionPriceMedia() . "', "

			. " approval_flag = '" . $record->getApprovalFlag() . "', "
			. " price_type = '" . $record->getPriceType() . "', "
			. " order_num = '" . $record->getOrderNum() . "', "

			. " link_url = '" . $record->getLinkUrl() . "', "
			. " action_complete_date = '" . $record->getActionCompleteDate() . "', "
			. " confirm_flag = '" . $record->getConfirmFlag() . "', "
			. " point_back_parameter = '" . $record->getPointBackParameter() . "', "
			. " point_back_url = '" . $record->getPointBackUrl() . "', "
			. " status = '" . $record->getStatus() . "', "
			//-------------------------------------------
			// 7/21 追加
			. " auid = '" . $record->getAuid() . "' "
			//-------------------------------------------
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";


			if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		//return $sql;
		return true;
	}

	//新規データの登録
	public function insertActionLog($record, &$result_message = ""){
		$record = $this->escapeStringActionLog($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO withdrawal_logs values ('', "
			. " '" . $record->getSessionId() . "', "
			. " '" . $record->getUid() . "', "
			. " '" . $record->getBrowserType() . "', "
			. " '" . $record->getCarrierId() . "', "
			. " '" . $record->getUserAgent() . "', "
			. " '" . $record->getIpAddress() . "', "
			. " '" . $record->getHostName() . "', "
			. " '" . $record->getMediaId() . "', "
			. " '" . $record->getMediaPublisherId() . "', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getAdvertClientId() . "', "
			. " '" . $record->getClickPriceClient() . "', "
			. " '" . $record->getClickPriceMedia() . "', "
			. " '" . $record->getActionPriceClient() . "', "
			. " '" . $record->getActionPriceMedia() . "', "

			. " '" . $record->getApprovalFlag() . "', "
			. " '" . $record->getPriceType() . "', "
			. " '" . $record->getOrderNum() . "', "

			. " '" . $record->getLinkUrl() . "', "
			. " '" . $record->getActionCompleteDate() . "', "
			. " '" . $record->getConfirmFlag() . "', "
			. " '" . $record->getPointBackParameter() . "', "
			. " '" . $record->getPointBackUrl() . "', "
			. " '" . $record->getStatus() . "', "
			//-------------------------------------------
			// 7/21 追加
			. " '" . $record->getAuid() . "', "
			//-------------------------------------------
			. " Now(), Now(), NULL) ";

		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteActionLog($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE withdrawal_logs SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//テスト
	public function testUpDate($sql){
		is_null($this->mysqli) and $this->connect();

		if($this->mysqli->query($sql)){
			return true;
		}else{
			return false;
		}
	}


	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringActionLog($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setSessionId($this->mysqli->real_escape_string($record->getSessionId()));
		$record->setCarrierId($this->mysqli->real_escape_string($record->getCarrierId()));
		$record->setUserAgent($this->mysqli->real_escape_string($record->getUserAgent()));
		$record->setUid($this->mysqli->real_escape_string($record->getUid()));
		$record->setIpAddress($this->mysqli->real_escape_string($record->getIpAddress()));
		$record->setHostName($this->mysqli->real_escape_string($record->getHostName()));
		$record->setMediaId($this->mysqli->real_escape_string($record->getMediaId()));
		$record->setMediaPublisherId($this->mysqli->real_escape_string($record->getMediaPublisherId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setAdvertClientId($this->mysqli->real_escape_string($record->getAdvertClientId()));
		$record->setClickPriceClient($this->mysqli->real_escape_string($record->getClickPriceClient()));
		$record->setClickPriceMedia($this->mysqli->real_escape_string($record->getClickPriceMedia()));
		$record->setActionPriceClient($this->mysqli->real_escape_string($record->getActionPriceClient()));
		$record->setActionPriceMedia($this->mysqli->real_escape_string($record->getActionPriceMedia()));

		$record->setApprovalFlag($this->mysqli->real_escape_string($record->getApprovalFlag()));
		$record->setPriceType($this->mysqli->real_escape_string($record->getPriceType()));
		$record->setOrderNum($this->mysqli->real_escape_string($record->getOrderNum()));

		$record->setLinkUrl($this->mysqli->real_escape_string($record->getLinkUrl()));
		$record->setActionCompleteDate($this->mysqli->real_escape_string($record->getActionCompleteDate()));
		$record->setConfirmFlag($this->mysqli->real_escape_string($record->getConfirmFlag()));
		$record->setPointBackParameter($this->mysqli->real_escape_string($record->getPointBackParameter()));
		$record->setPointBackUrl($this->mysqli->real_escape_string($record->getPointBackUrl()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		//-------------------------------------------
		// 7/21 追加
		$record->setAuid($this->mysqli->real_escape_string($record->getAuid()));
		//-------------------------------------------
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>