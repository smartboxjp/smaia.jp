<?php
class PrefDao extends CommonDao{

  //コンストラクタ
  function __construct(){
    parent::__construct();
  }

  //デストラクタ
  function __destruct(){
    parent::__destruct();
  }

  //全都道府県データの取得
  public function getAllPref(){
    is_null($this->mysqli) and $this->connect();
    $result = $this->mysqli->query("SELECT * FROM prefs");
    $pref_array = array();

    while($row = $result->fetch_array(MYSQLI_ASSOC)){
      $pref = new Pref();
      $pref->setId($row["id"]);
      $pref->setName($row["name"]);
      $pref_array[] = $pref;
    }
    $result->close();
    return $pref_array;
  }

  //指定された都道府県データの取得
  private function getPref($sql){
    is_null($this->mysqli) and $this->connect();
    $result = $this->mysqli->query($sql);

    $pref = null;

    if($result->num_rows != 0){
      $row = $result->fetch_array(MYSQLI_ASSOC);
      $pref = new Pref();
      $pref->setId($row["id"]);
      $pref->setName($row["name"]);
    }
    $result->close();

    return $pref;
  }

  //idで都道府県データを取得
  public function getPrefById($id){
    $id = $this->mysqli->real_escape_string($id);
    $sql = "SELECT * FROM prefs WHERE id = '$id'";
    return $this->getPref($sql);
  }

  //都道府県名で都道府県データを取得
  public function getPrefByName($name){
    $name = $this->mysqli->real_escape_string($name);
    $sql = "SELECT * FROM prefs WHERE name = '$name'";
    return $this->getPref($sql);
  }

}
?>