<?php
class MediaPublisherDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllMediaPublisher(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM media_publishers WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new MediaPublisher();
			$record->setId($row["id"]);
			$record->setLoginUserId($row["login_user_id"]);
			$record->setMediaGroupId($row["media_group_id"]);
			$record->setPublisherName($row["publisher_name"]);
			$record->setContactPerson($row["contact_person"]);
			$record->setTel($row["tel"]);
			$record->setFax($row["fax"]);
			$record->setEmail($row["email"]);
			$record->setZipcode1($row["zipcode1"]);
			$record->setZipcode2($row["zipcode2"]);
			$record->setPref($row["pref"]);
			$record->setAddress1($row["address1"]);
			$record->setAddress2($row["address2"]);
			$record->setTransferType($row["transfer_type"]);
			$record->setBankName($row["bank_name"]);
			$record->setBranchName($row["branch_name"]);
			$record->setAccountType($row["account_type"]);
			$record->setAccountHolder($row["account_holder"]);
			$record->setAccountNumber($row["account_number"]);
			$record->setPostalAccountHolder($row["postal_account_holder"]);
			$record->setPostalAccountNumber($row["postal_account_number"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getMediaPublisher($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new MediaPublisher();
			$record->setId($row["id"]);
			$record->setLoginUserId($row["login_user_id"]);
			$record->setMediaGroupId($row["media_group_id"]);
			$record->setPublisherName($row["publisher_name"]);
			$record->setContactPerson($row["contact_person"]);
			$record->setTel($row["tel"]);
			$record->setFax($row["fax"]);
			$record->setEmail($row["email"]);
			$record->setZipcode1($row["zipcode1"]);
			$record->setZipcode2($row["zipcode2"]);
			$record->setPref($row["pref"]);
			$record->setAddress1($row["address1"]);
			$record->setAddress2($row["address2"]);
			$record->setTransferType($row["transfer_type"]);
			$record->setBankName($row["bank_name"]);
			$record->setBranchName($row["branch_name"]);
			$record->setAccountType($row["account_type"]);
			$record->setAccountHolder($row["account_holder"]);
			$record->setAccountNumber($row["account_number"]);
			$record->setPostalAccountHolder($row["postal_account_holder"]);
			$record->setPostalAccountNumber($row["postal_account_number"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getMediaPublisherById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM media_publishers WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getMediaPublisher($sql);
	}

	//publisher_nameでデータを取得
	public function getMediaPublisherByPublisherName($publisher_name){
		$publisher_name = $this->mysqli->real_escape_string($publisher_name);
		$sql = " SELECT * FROM media_publishers WHERE publisher_name = '$publisher_name' AND deleted_at is NULL ";
		return $this->getMediaPublisher($sql);
	}

	//login_user_idでデータを取得
	public function getMediaPublisherByLoginUserId($login_user_id){
		$login_user_id = $this->mysqli->real_escape_string($login_user_id);
		$sql = " SELECT * FROM media_publishers WHERE login_user_id = '$login_user_id' AND deleted_at is NULL ";
		return $this->getMediaPublisher($sql);
	}

	//データの更新
	public function updateMediaPublisher($record, &$result_message = ""){
		$record = $this->escapeStringMediaPublisher($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE media_publishers SET "
			. " login_user_id = '" . $record->getLoginUserId() . "', "
			. " media_group_id = '" . $record->getMediaGroupId() . "', "
			. " publisher_name = '" . $record->getPublisherName() . "', "
			. " contact_person = '" . $record->getContactPerson() . "', "
			. " tel = '" . $record->getTel() . "', "
			. " fax = '" . $record->getFax() . "', "
			. " email = '" . $record->getEmail() . "', "
			. " zipcode1 = '" . $record->getZipcode1() . "', "
			. " zipcode2 = '" . $record->getZipcode2() . "', "
			. " pref = '" . $record->getPref() . "', "
			. " address1 = '" . $record->getAddress1() . "', "
			. " address2 = '" . $record->getAddress2() . "', "
			. " transfer_type = '" . $record->getTransferType() . "', "
			. " bank_name = '" . $record->getBankName() . "', "
			. " branch_name = '" . $record->getBranchName() . "', "
			. " account_type = '" . $record->getAccountType() . "', "
			. " account_holder = '" . $record->getAccountHolder() . "', "
			. " account_number = '" . $record->getAccountNumber() . "', "
			. " postal_account_holder = '" . $record->getPostalAccountHolder() . "', "
			. " postal_account_number = '" . $record->getPostalAccountNumber() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertMediaPublisher($record, &$result_message = ""){
		$record = $this->escapeStringMediaPublisher($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO media_publishers values ('', "
			. " '" . $record->getLoginUserId() . "', "
			. " '" . $record->getMediaGroupId() . "', "
			. " '" . $record->getPublisherName() . "', "
			. " '" . $record->getContactPerson() . "', "
			. " '" . $record->getTel() . "', "
			. " '" . $record->getFax() . "', "
			. " '" . $record->getEmail() . "', "
			. " '" . $record->getZipcode1() . "', "
			. " '" . $record->getZipcode2() . "', "
			. " '" . $record->getPref() . "', "
			. " '" . $record->getAddress1() . "', "
			. " '" . $record->getAddress2() . "', "
			. " '" . $record->getTransferType() . "', "
			. " '" . $record->getBankName() . "', "
			. " '" . $record->getBranchName() . "', "
			. " '" . $record->getAccountType() . "', "
			. " '" . $record->getAccountHolder() . "', "
			. " '" . $record->getAccountNumber() . "', "
			. " '" . $record->getPostalAccountHolder() . "', "
			. " '" . $record->getPostalAccountNumber() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteMediaPublisher($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE media_publishers SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringMediaPublisher($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setLoginUserId($this->mysqli->real_escape_string($record->getLoginUserId()));
		$record->setMediaGroupId($this->mysqli->real_escape_string($record->getMediaGroupId()));
		$record->setPublisherName($this->mysqli->real_escape_string($record->getPublisherName()));
		$record->setContactPerson($this->mysqli->real_escape_string($record->getContactPerson()));
		$record->setTel($this->mysqli->real_escape_string($record->getTel()));
		$record->setFax($this->mysqli->real_escape_string($record->getFax()));
		$record->setEmail($this->mysqli->real_escape_string($record->getEmail()));
		$record->setZipcode1($this->mysqli->real_escape_string($record->getZipcode1()));
		$record->setZipcode2($this->mysqli->real_escape_string($record->getZipcode2()));
		$record->setPref($this->mysqli->real_escape_string($record->getPref()));
		$record->setAddress1($this->mysqli->real_escape_string($record->getAddress1()));
		$record->setAddress2($this->mysqli->real_escape_string($record->getAddress2()));
		$record->setTransferType($this->mysqli->real_escape_string($record->getTransferType()));
		$record->setBankName($this->mysqli->real_escape_string($record->getBankName()));
		$record->setBranchName($this->mysqli->real_escape_string($record->getBranchName()));
		$record->setAccountType($this->mysqli->real_escape_string($record->getAccountType()));
		$record->setAccountHolder($this->mysqli->real_escape_string($record->getAccountHolder()));
		$record->setAccountNumber($this->mysqli->real_escape_string($record->getAccountNumber()));
		$record->setPostalAccountHolder($this->mysqli->real_escape_string($record->getPostalAccountHolder()));
		$record->setPostalAccountNumber($this->mysqli->real_escape_string($record->getPostalAccountNumber()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>