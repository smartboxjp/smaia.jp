<?php
class PointBackLogDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllPointBackLog(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM point_back_logs WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new ActionLog();
			$record->setId($row["id"]);
			$record->setSessionId($row["session_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setPointBackParameter($row["point_back_parameter"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getPointBackLog($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new ActionLog();
			$record->setId($row["id"]);
			$record->setSessionId($row["session_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setPointBackParameter($row["point_back_parameter"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getPointBackLogById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM point_back_logs WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getPointBackLog($sql);
	}

	//session_idでデータを取得
	public function getPointBackLogBySessionId($session_id){
		$session_id = $this->mysqli->real_escape_string($session_id);
		$sql = " SELECT * FROM point_back_logs WHERE session_id = '$session_id' AND deleted_at is NULL ";
		return $this->getPointBackLog($sql);
	}

	//データの更新
	public function updatePointBackLog($record, &$result_message = ""){
		$record = $this->escapeStringPointBackLog($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE point_back_logs SET "
			. " session_id = '" . $record->getSessionId() . "', "
			. " media_id = '" . $record->getMediaId() . "', "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " point_back_parameter = '" . $record->getPointBackParameter() . "', "
			. " point_back_url = '" . $record->getPointBackUrl() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertPointBackLog($record, &$result_message = ""){
		$record = $this->escapeStringPointBackLog($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO point_back_logs values ('', "
			. " '" . $record->getSessionId() . "', "
			. " '" . $record->getMediaId() . "', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getPointBackParameter() . "', "
			. " '" . $record->getPointBackUrl() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deletePointBackLog($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE point_back_logs SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringPointBackLog($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setSessionId($this->mysqli->real_escape_string($record->getSessionId()));
		$record->setMediaId($this->mysqli->real_escape_string($record->getMediaId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setPointBackParameter($this->mysqli->real_escape_string($record->getPointBackParameter()));
		$record->setPointBackUrl($this->mysqli->real_escape_string($record->getPointBackUrl()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>