<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/PointBackLogDao.php' );
require_once( '../dto/PointBackLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );

require_once( './referer_logs.php'  );


// ******************************************************************
// アクセスに必要なパラメータ
// bid 自社で生成したセッションID
// at 成果か未成果かを判別 認証が手動である場合等に使用
// on 商品発注数 セットされていない場合自動的に発注数は1となる
// ******************************************************************


// GETパラメータbidがセットされているか
if((isset($_GET['bid']) && $_GET['bid'] != '') || (isset($_GET['afcd']) && $_GET['afcd'] != '')){



	// --------------------------------------------------------------
	// 手動認証対応 at=アクションがあがったかorまだアクションでないか
	// パラメータの内容 1:未アクション 2:アクション
	if(isset($_GET['at']) && $_GET['at'] != "") {
		$at = $_GET['at'];
	} else {
		// atがセットされていない場合処理を終了
		// エラーを表示
		echo "error001";
		exit();
	}
	
	// atが1または2でない場合処理を終了
	// エラーを表示
	if($at <> "1" && $at <> "2") {
		echo "eroor002";
		exit();
	}
	// --------------------------------------------------------------

	// オブジェクト生成
	// DB接続クラス生成
	$common_dao = new CommonDao();
	// action_log_daoクラス生成
	$action_log_dao = new ActionLogDao();
	// action_logクラス生成
	$advert_dao = new AdvertDao();
	// media_daoクラス生成
	$media_dao = new MediaDao();

	// ログを取るタイミング

	// GET送信で受け取ったパラメータを取得
	$bid = $_GET['bid'];

	if(isset($_GET['afcd']) && $_GET['afcd'] != '') {
		$bid = $_GET['afcd'];
	}

	// -----------------------------------------------------
	// 発注数を取得
	if(isset($_GET['on']) && $_GET['on'] != ""){
		// GETパラメータにセットされている場合
		$on = $_GET['on'];
	} else {
		// GETパラメータにセットされていない場合1をセット
		$on = 1;
	}
	// -----------------------------------------------------

	// エラーフラグ
	$error_flag = 0;
	// 結果メッセージ
	$result_msg = "";

	//受け取ったセッションIDからレコードを取得
	// **************************************************************************
	// action_logクラス生成
	// **************************************************************************
	$action_log = new ActionLog();
	// bidを条件にaction_logからレコードを取得
	$action_log = $action_log_dao->getActionLogBySessionId($bid);
	if(!is_null($action_log)) {	//登録されているレコードか確認
		// 該当レコードが存在する

		// bidを変数へ代入
		$session_id = $bid;
		// 広告IDを取得
		$advert_id = $action_log->getAdvertId();
		// 媒体IDを取得
		$media_id = $action_log->getMediaId();
		// ポイントバックパラメータ(キックバックパラメータ)を取得
		$point_back_parameter = $action_log->getPointBackParameter();
		// ポイントバックURL(キックバックURL)を取得
		$point_back_url = $action_log->getPointBackUrl();
		// 登録日付を取得
		$created_at = $action_log->getCreatedAt();

//_/_/_/_/_/_/_/ IP制限2014.12.16 _/_/_/_/_/_/_/
$ip = $_SERVER["REMOTE_ADDR"];
$logs = $ip."：";
$sql = " SELECT ip_address FROM advert_ip WHERE advert_id = '$advert_id' ";

// クエリを実行しレコードを取得
$db_result = $common_dao->db_query($sql);
// DB結果
if($db_result) {
	$check_ip = false;
	for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
	{
		$logs .= $db_result[$db_loop][ip_address]."/";
		if($ip==trim($db_result[$db_loop][ip_address]))
		{
			$check_ip = true;
		}
	}
	//IPが一個以上登録されていて一致してるIPが無い場合
	if(count($db_result)>0 && $check_ip==false)
	{
//		exit;
	}
}
//_/_/_/_/_/_/_/ IP制限2014.12.16 _/_/_/_/_/_/_/

		// **********************************************************************
		// advertクラスを生成
		// **********************************************************************
		$advert = new Advert();

		// advertオブジェクトがNULLか ※このIF文の用途不明
		if(!is_null($advert)) {
			// NULLでない場合
			// ユニーククリックタイプを取得
			$unique_click_type = $advert->getUniqueClickType();
			// テストフラグを取得
			$test_flag = $advert->getTestFlag();
			// ポイントバックフラグ
			$point_back_flag = $advert->getPointBackFlag();

			// テストフラグが1と等しくないか
			if($test_flag != 1) {
				// テストフラグが1でない場合
				// ユニーククリックタイプが1と等しいか
				if($unique_click_type == 1) {
					// 本日の日付 年月を取得
					$monthly = date("Ym");
					// SELECT文の発行
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND DATE_FORMAT(action_complete_date,'%Y%m') = '$monthly' ";

					// クエリを実行しレコードを取得
					$db_result = $common_dao->db_query($sql);
					// DB結果
					if($db_result) {
						// 該当レコードが存在する場合
						// エラーフラグに1を代入
						$error_flag = 1;
						echo "error003";

					} else {
						// 該当レコードが存在しない場合
					}

				// ユニーククリックタイプが2と等しいか
				} elseif($unique_click_type == 2) {
					// SELECT文の発行
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND action_complete_date > DATE_SUB(NOW(), INTERVAL 7 DAY) ";

					// クエリを実行しレコードを取得する
					$db_result = $common_dao->db_query($sql);
					// DB結果
					if($db_result) {
						// 該当レコードが存在する場合
						// エラーフラグに1を代入
						$error_flag = 1;
						echo "error004";

					} else {
						// 該当レコードが存在しない場合
					}

				// ユニーククリックタイプが3と等しいか
				} elseif($unique_click_type == 3) {
					// 本日の日付 年月日を取得
					$daily = date("Ymd");
					// SELECT文を発行
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND DATE_FORMAT(action_complete_date,'%Y%m%d') = '$daily' ";

					// クエリを実行しレコードを取得
					$db_result = $common_dao->db_query($sql);
					// DB結果
					if($db_result) {
						// 該当レコードが存在する場合
						// エラーフラグに1を代入
						$error_flag = 1;
						echo "error005";

					} else {
						// 該当レコードが存在しない場合
					}

				// ユニーククリックタイプと等しいか
				} elseif($unique_click_type == 4) {
					// 本日の日付 年月日を取得
					$daily = date("Ymd");
					// SELECT文を発行
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' ";

					// クエリを実行しレコードを取得
					$db_result = $common_dao->db_query($sql);
					// DB結果
					if($db_result) {
						// 該当レコードが存在する場合
						// エラーフラグにを代入
						$error_flag = 1;
						echo "error006";

					} else {
						// 該当レコ―ドが存在する場合
					}
				}
			}

		} else {
			// advertオブジェクトがNULLの場合
			// エラーフラグに1を代入
			$error_flag = 1;
		}

		// **********************************************************************
		// mediaクラスを生成
		// **********************************************************************
		$media = new Media();
		// 媒体IDを条件にmediaテーブルのレコードを取得
		$media = $media_dao->getMediaById($media_id);
		// レコードがNULLでないか
		if(!is_null($media)) {
			// レコードがNULLでない場合


		} else {
			// レコードがNULLの場合
			// エラーフラグに1を代入
			$error_flag = 1;

		}

		// 本日の年月日時分秒を取得
		$ac = date("YmdHis");

		// 英文形式の日付をタイムスタンプに変換し取得
		$c_date = strtotime($action_log->getCreatedAt());
		// 登録日付の年を取得
		$c_year = date("Y", $c_date);
		// 登録日付の月を取得
		$c_month = date("m", $c_date);
		// 登録日付の日を取得
		$c_day = date("d", $c_date);
		// 登録日付の時を取得
		$c_hour = date("H", $c_date);
		// 登録日付の分を取得
		$c_minute = date("i", $c_date);
		// 登録日付の秒を取得
		$c_second = date("s", $c_date);


		// 英文形式の日付をタイムスタンプに変換し取得
		$a_date = strtotime($ac);
		// ac日付の年を取得
		$a_year = date("Y", $a_date);
		// ac日付の月を取得
		$a_month = date("m", $a_date);
		// ac日付の日を取得
		$a_day = date("d", $a_date);
		// ac日付の時を取得
		$a_hour = date("H", $a_date);
		// ac日付の分を取得
		$a_minute = date("i", $a_date);
		// ac日付の秒を取得
		$a_second = date("s", $a_date);

		// 登録日付とac日付を秒の長整数に変換し比較する
		// ac日付が登録日付以上であるか
		if(mktime($c_hour, $c_minute, $c_second, $c_month, $c_day, $c_year) <= mktime($a_hour, $a_minute, $a_second, $a_month, $a_day, $a_year)) {
			// ac日付が登録日付以上である
			// acを変数へ格納
			$action_complete_date = $ac;

		} else {
			// ac日付が登録日付以上でない
			// エラーフラグに1を代入
			$error_flag = 1;

		}

	} else {
		// 該当レコードが存在しない
		// エラーフラグに1を代入
		$error_flag = 1;

	}

//----------------------------------------------------------------------------------
//	// リファラ
//	if( $_SERVER['HTTP_REFERER'] != NULL ){
//		$ref = $_SERVER['HTTP_REFERER'];
//	}
//	else{
//		$ref = "NO REFERER";
//	}
//
//	$c_referer_logs = new C_refererLogs();
//	$c_referer_logs->M_getReferer("2", $advert_id, $media_id, $carrier_id, $ref);
//----------------------------------------------------------------------------------

	// エラーフラグが0
	if($error_flag == 0) {
		// action_log_daoクラス トランザクション スタート
		$action_log_dao->transaction_start();
		// action_logクラスのget/setメソッドに値をセット
		// アクション日付をセット
		$action_log->setActionCompleteDate($action_complete_date);

		// 発注数をセット
		$action_log->setOrderNum($on);

		// ステータスセット 2:アクション 1:未アクション
		$action_log->setStatus($at);

		//Updateを実行
		$db_result = $action_log_dao->UpdateActionLog($action_log, $result_message);
		// DB結果
		if($db_result) {

			// ※テスト出力
			//echo "tes:" . $at . "/" . $on;
			// 成果ではないので、ここで処理終了
			if($at == "1") {
				// action_log_daoクラス トランザクション エンド
				$action_log_dao->transaction_end();
				exit();
			}

			// 更新成功
			// action_log_daoクラス トランザクション エンド
			$action_log_dao->transaction_end();

			// ポイントバックURL(キックバックURL)が空でないか
			// または
			// ポイントバックフラグが2
			if($point_back_url != "" || $point_back_flag == 2) {

				//ポイントバック成果通知処理
				//$point_back_url = $action_log->getPointBackUrl();

				$res_id = $point_back_parameter;

// --------------------------------------------------------------------------------------------
//				// 成果返却タイプ
//				if($response_type == 1) {
//					// 成果 セッションID
//					$res_id = $point_back_parameter;
//					// ##ID##とポイントバックパラメータ(キックバックパラメータ)を置き換え
//					$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);
//
//				} elseif($response_type == 0) {
//					// 成果 個体識別
//					$res_id = urlencode($uid);
//					// ##ID##と個体識別を置き換え
//					$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);
//
//				} elseif($response_type == 2) {
//					// 成果 セッションIDと個体識別
//					// ##ID##とポイントバックパラメータ(キックバックパラメータ)を置き換え
//					$point_back_url = ereg_replace("##ID##", $point_back_parameter, $point_back_url);
//					// ##UID##と個体識別を置き換え
//					$point_back_url = ereg_replace("##UID##", urlencode($uid), $point_back_url);
//
//				}
// --------------------------------------------------------------------------------------------

				$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);
				// ##CID##と広告IDを置き換え
				$point_back_url = ereg_replace("##CID##", $action_log->getAdvertId(), $point_back_url);
				// ##CLICK_DATE##と登録日付を置き換え
				$point_back_url = ereg_replace("##CLICK_DATE##", date("YmdHis", $action_log->getCreatedAt()), $point_back_url);
				// ##ACTION_DATE##とアクション完了日付を置き換え
				$point_back_url = ereg_replace("##ACTION_DATE##", date("YmdHis", $action_log->getActionCompleteDate()), $point_back_url);

				// ポイントバックURL(キックバックURL)を配列に分割
				$url_array = parse_url($point_back_url);

				// parse_url queryがセットされているか
				if(isset($url_array['query'])){
					// parse_url queryの文字列に?を付与し変数へ格納
					$query = "?" . $url_array['query'];
				}

				// parse_url hostを変数へ格納
				$host = $url_array['host'];
				//parse_url pathに変数$queryを付与し変数へ格納
				$path = $url_array['path'] . $query;

				// String型でかつpathの値が""か
				if($path === ""){
					// "/"を変数に代入
					$path = "/";
				}

				$port = 80;             // HTTP なので80
				$timeout = 30;             // 接続に失敗した場合の待ち時間

				// **************************************************************
				// ソケットの送信
				// **************************************************************
				// ポイントバックログのステータスに1を代入
				$pb_status = 1;
				$sock = fsockopen($host, $port, $errno, $errstr, $timeout);  // サーバに接続する
				if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
					// エラーメッセージを発行
					echo "SOCK OPEN ERROR<br>";
					// ポイントバックログのステータスに2を代入
					$pb_status = 2;

				} else {
					// HTTP ヘッダ部分の送信になる。
					fwrite($sock, "GET http://" . $host . $path . " HTTP/1.0\r\n");
					// ヘッダの終了を通知
					fwrite($sock, "\r\n\r\n");

				}
				// ソケットクローズ
				fclose($sock);


				//ポイントバック通知結果をデータベースに登録
				// オブジェクト生成
				// point_back_log_daoクラス生成
				$point_back_log_dao = new PointBackLogDao();
				// point_back_logクラス生成
				$point_back_log = new PointBackLog();

				// point_back_log_daoクラス トランザクション スタート
				$point_back_log_dao->transaction_start();

				// point_back_log_daoクラスのget/setメソッドへセット
				// セッションIDをセット
				$point_back_log->setSessionId($session_id);
				// 媒体IDをセット
				$point_back_log->setMediaId($media_id);
				// 広告IDをセット
				$point_back_log->setAdvertId($advert_id);

				// 個体識別をセット
				$point_back_log->setPointBackParameter($uid);
				// ポイントバックURL(キックバックURL)をセット
				$point_back_log->setPointBackUrl($point_back_url);
				// ステータスをセット
				$point_back_log->setStatus($pb_status);

				//INSERTを実行
				$db_result = $point_back_log_dao->insertPointBackLog($point_back_log, $result_message);
				// DB結果
				if($db_result) {
					// レコード追加成功
					// point_back_log_daoクラス トランザクション エンド
					$point_back_log_dao->transaction_end();
					exit();

				} else {
					// レコード追加失敗
					// point_back_log_daoクラス トランザクション ロールバック
					$point_back_log_dao->transaction_rollback();
					exit();

				}
			}

		} else {
			// レコード更新失敗
			// $action_log_daoクラス トランザクション ロールバック
			$action_log_dao->transaction_rollback();
			echo "error007";
			exit();

		}

	}else{

	}

}else{
	exit();
}

function compareDate($year1, $month1, $day1, $year2, $month2, $day2) {
    $dt1 = mktime(0, 0, 0, $month1, $day1, $year1);
    $dt2 = mktime(0, 0, 0, $month2, $day2, $year2);
    $diff = $dt1 - $dt2;
    $diffDay = $diff / 86400;//1日は86400秒
    return $diffDay;
}
?>