<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

//require_once( './referer_logs.php'  );

	echo "test001<br />";

// GETパラメータm、aがセットされているか
if(isset($_GET['m']) && $_GET['m'] != '' && isset($_GET['a']) && $_GET['a'] != ''){


	echo "test002<br />";


	// オブジェクトの生成
	// DB接続クラスの生成
	$common_dao = new CommonDao();
	// action_log_daoクラスの生成
	$action_log_dao = new ActionLogDao();

	// セッションIDを生成
	// ※randにてランダムに英数字を取得
	// ※uniqidにて23文字の一意の文字列を取得
	// ※md5にて32桁のハッシュ値を取得
	$session_id = md5(uniqid(rand(), true));

	// キャリアID
	$carrier_id = "0";

	// ユーザーエージェントを取得
	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	// 個体識別
//	$uid = "";

	// IPアドレスの取得
	$ip_address = $_SERVER['REMOTE_ADDR'];
	// ホスト名を取得
	$host_name = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
	// GET送信で取得したパラメータ 媒体IDを取得
	$media_id = $_GET['m'];
	// 媒体発行者ID
	$media_publisher_id = "";
	// GET送信で取得したパラメータ 広告IDを取得
	$advert_id = $_GET['a'];
	// 広告主
	$advert_client_id = "";
	// クリック単価の初期化
	$click_price = 0;
	// アクション単価の初期化
	$action_price = 0;

	// 認証フラグ
	$approval_flag = 0;
	// 定額/定率 タイプ
	$price_type = 0;

	// ポイントバックパラメータ(キックバックパラメータ)
	$point_back_parameter = "";
	// ステータスの初期化
	$status = 1;

	//-------------------------------------------
	// 7/21 追加
	// ライブレボリューション用ユニークID
	$auid = "NULL";
	//-------------------------------------------

	// エラーフラグの初期化
	$error_flag = 0;

	// オブジェクトの生成
	// connect_log_daoクラスの生成
	$connect_log_dao = new ConnectLogDao();
	// connect_logクラスの生成
	$connect_log = new ConnectLog();

//	下記のように修正が必要ではないか コード↓
	// 媒体ID、広告ID、ステータスを条件にレコードを取得
	$connect_log = $connect_log_dao->getConnectLogByMidAidStatus($media_id, $advert_id, 2);
//	$connect_log = $connect_log_dao->getConnectLogByMidAid($media_id, $advert_id);

	// レコードがNULLか
	if(is_null($connect_log)) {
		// エラーメッセージの発行
		echo "error:(001)未提携です。";
		exit();
	}

	echo "test003<br />";

	//受け取った媒体IDからレコードを取得
	// オブジェクトの生成
	// media_daoクラスの生成
	$media_dao = new MediaDao();
	// mediaクラスの生成
	$media = new Media();

//	下記のように修正が必要ではないか コード↓
	// 媒体ID、ステータスを条件にレコードを取得
	$media = $media_dao->getMediaByIdStatus($media_id, 2);
//	$media = $media_dao->getMediaById($media_id);

	// レコードがNULLでないか
	if(!is_null($media)) {	//登録されている媒体か確認
		// 媒体発行者IDを取得
		$media_publisher_id = $media->getMediaPublisherId();
		// 媒体カテゴリーIDを取得
		$media_category_id = $media->getMediaCategoryId();
		// レスポンスタイプを取得
		$response_type = $media->getResponseType();
		// ポイントバックURL(キックバックURL)を取得
		$point_back_url = $media->getPointBackUrl();

//		// レスポンスタイプが1または2か
//		if($response_type == 1 || $response_type == 2) {
//			// ポイントバックパラメータ(キックバックパラメータ)を取得
//			$point_back_parameter = $sid;
//		}

		echo "test004<br />";

		// GETパラメータsidがセットされているか
		if(isset($_GET['sid']) && $_GET['sid'] != "") {
			// GET送信で受け取ったパラメータを取得
			$point_back_parameter = $_GET['sid'];
		}



	// レコードがNULL
	} else {
		// エラーメッセージを発行
		echo "error:(002)正規の媒体ではありません。";
		exit();
	}

	//受け取った広告IDからレコードを取得
	// オブジェクトの生成
	// advert_daoクラスの生成
	$advert_dao = new AdvertDao();
	// advertクラスの生成
	$advert = new Advert();
	// 広告ID、ステータスを条件にレコードを取得
	$advert = $advert_dao->getAdvertByIdStatus($advert_id, 2);
	// レコードがNULLでないか
	if(!is_null($advert)) {	//登録されている広告か確認
		// 広告主IDを取得
		$advert_client_id = $advert->getAdvertClientId();

		echo "test005<br />";

		// オブジェクトの生成
		// advert_price_media_set_daoクラスの生成
		$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
		// advert_price_media_setクラスの生成
		$advert_price_madia_set = new AdvertPriceMediaSet();
		// 広告ID、媒体IDを条件にレコードを取得
		$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

		// レコードがNULLでないか
		if(!is_null($advert_price_madia_set)) {
			// 媒体別単価設定に設定されている(レコードが存在する)

			// advert_price_media_set_dao
			// クリック単価(グロス)の取得
			$click_price_client = $advert_price_madia_set->getClickPriceClient();
			// クリック単価(グロス)の値が0と等しいか
			if($click_price_client == "0"){
				// advert
				// クリック単価(グロス)の取得
				$click_price_client = $advert->getClickPriceClient();
			}

			// advert_price_media_set_dao
			// クリック単価(ネット)の取得
			$click_price_media = $advert_price_madia_set->getClickPriceMedia();
			// クリック単価(ネット)の値が0と等しいか
			if($click_price_media == "0"){
				// advert
				// クリック単価(ネット)の取得
				$click_price_media = $advert->getClickPriceMedia();
			}

			// グロス --------------------------------------------------------------------------
			// advert_price_media_set_dao
			// pcのアクション単価(グロス)を取得
			$action_price_client_pc_1 = $advert_price_madia_set->getActionPriceClientPc1();
			// pcのアクション単価(グロス)が0と等しいか
			if($action_price_client_pc_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_pc_1 = $advert->getActionPriceClientPc1();
			}


			// ---------------------------------------------------------------------------------
			// 20120606 追加
			// ---------------------------------------------------------------------------------
			// iphone pc
			$action_price_client_iphone_pc_1 = $advert_price_madia_set->getActionPriceClientIphonePc1();
			if($action_price_client_iphone_pc_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_iphone_pc_1 = $advert->getActionPriceClientIphonePc1();
			}

			// iphone docomo
			$action_price_client_iphone_docomo_1 = $advert_price_madia_set->getActionPriceClientIphoneDocomo1();
			if($action_price_client_iphone_docomo_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_iphone_docomo_1 = $advert->getActionPriceClientIphoneDocomo1();
			}

			// iphone softbanck
			$action_price_client_iphone_softbank_1 = $advert_price_madia_set->getActionPriceClientIphoneSoftbank1();
			if($action_price_client_iphone_softbank_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_iphone_softbank_1 = $advert->getActionPriceClientIphoneSoftbank1();
			}

			// iphone au
			$action_price_client_iphone_au_1 = $advert_price_madia_set->getActionPriceClientIphoneAu1();
			if($action_price_client_iphone_au_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_iphone_au_1 = $advert->getActionPriceClientIphoneAu1();
			}
			// ---------------------------------------------------------------------------------
			// 20120606 追加 ここまで
			// ---------------------------------------------------------------------------------


			// android pc
			$action_price_client_android_pc_1 = $advert_price_madia_set->getActionPriceClientAndroidPc1();
			if($action_price_client_android_pc_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_android_pc_1 = $advert->getActionPriceClientAndroidPc1();
			}

			// android docomo
			$action_price_client_android_docomo_1 = $advert_price_madia_set->getActionPriceClientAndroidDocomo1();
			if($action_price_client_android_docomo_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_android_docomo_1 = $advert->getActionPriceClientAndroidDocomo1();
			}

			// android softbank
			$action_price_client_android_softbank_1 = $advert_price_madia_set->getActionPriceClientAndroidSoftbank1();
			if($action_price_client_android_softbank_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_android_softbank_1 = $advert->getActionPriceClientAndroidSoftbank1();
			}

			// android au
			$action_price_client_android_au_1 = $advert_price_madia_set->getActionPriceClientAndroidAu1();
			if($action_price_client_android_au_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_android_au_1 = $advert->getActionPriceClientAndroidAu1();
			}


			// ネット --------------------------------------------------------------------------
			// advert_price_media_set_dao
			// pcのアクション単価(ネット)を取得
			$action_price_media_pc_1 = $advert_price_madia_set->getActionPriceMediaPc1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_pc_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_pc_1 = $advert->getActionPriceMediaPc1();
			}

			// ---------------------------------------------------------------------------------
			// 20120606 追加
			// ---------------------------------------------------------------------------------
			// iphone pc
			$action_price_media_iphone_pc_1 = $advert_price_madia_set->getActionPriceMediaIphonePc1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_iphone_pc_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_iphone_pc_1 = $advert->getActionPriceMediaIphonePc1();
			}

			// iphone docomo
			$action_price_media_iphone_docomo_1 = $advert_price_madia_set->getActionPriceMediaIphoneDocomo1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_iphone_docomo_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_iphone_docomo_1 = $advert->getActionPriceMediaIphoneDocomo1();
			}

			// iphone softbanck
			$action_price_media_iphone_softbank_1 = $advert_price_madia_set->getActionPriceMediaIphoneSoftbank1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_iphone_softbank_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_iphone_softbank_1 = $advert->getActionPriceMediaIphoneSoftbank1();
			}

			// iphone au
			$action_price_media_iphone_au_1 = $advert_price_madia_set->getActionPriceMediaIphoneAu1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_iphone_au_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_iphone_au_1 = $advert->getActionPriceMediaIphoneAu1();
			}
			// ---------------------------------------------------------------------------------
			// 20120606 追加 ここまで
			// ---------------------------------------------------------------------------------


			// android pc
			$action_price_media_android_pc_1 = $advert_price_madia_set->getActionPriceMediaAndroidPc1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_android_pc_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_android_pc_1 = $advert->getActionPriceMediaAndroidPc1();
			}

			// android docomo
			$action_price_media_android_docomo_1 = $advert_price_madia_set->getActionPriceMediaAndroidDocomo1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_android_docomo_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_android_docomo_1 = $advert->getActionPriceMediaAndroidDocomo1();
			}

			// android softbank
			$action_price_media_android_softbank_1 = $advert_price_madia_set->getActionPriceMediaAndroidSoftbank1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_android_softbank_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_android_softbank_1 = $advert->getActionPriceMediaAndroidSoftbank1();
			}

			// android au
			$action_price_media_android_au_1 = $advert_price_madia_set->getActionPriceMediaAndroidAu1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_android_au_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_android_au_1 = $advert->getActionPriceMediaAndroidAu1();
			}


		} else {
			// 媒体別単価設定に設定されていない(レコードが存在しない)

			// クリック単価(グロス)を取得
			$click_price_client = $advert->getClickPriceClient();
			// クリック単価(ネット)を取得
			$click_price_media = $advert->getClickPriceMedia();

			$action_price_client_iphone_docomo_1 = $advert->getActionPriceClientIphoneDocomo1();
			$action_price_client_iphone_softbank_1 = $advert->getActionPriceClientIphoneSoftbank1();
			$action_price_client_iphone_au_1 = $advert->getActionPriceClientIphoneAu1();
			$action_price_client_iphone_pc_1 = $advert->getActionPriceClientIphonePc1();

			$action_price_media_iphone_docomo_1 = $advert->getActionPriceMediaIphoneDocomo1();
			$action_price_media_iphone_softbank_1 = $advert->getActionPriceMediaIphoneSoftbank1();
			$action_price_media_iphone_au_1 = $advert->getActionPriceMediaIphoneAu1();
			$action_price_media_iphone_pc_1 = $advert->getActionPriceMediaIphonePc1();

			$action_price_client_android_docomo_1 = $advert->getActionPriceClientAndroidDocomo1();
			$action_price_client_android_softbank_1 = $advert->getActionPriceClientAndroidSoftbank1();
			$action_price_client_android_au_1 = $advert->getActionPriceClientAndroidAu1();
			$action_price_client_android_pc_1 = $advert->getActionPriceClientAndroidPc1();

			$action_price_media_android_docomo_1 = $advert->getActionPriceMediaAndroidDocomo1();
			$action_price_media_android_softbank_1 = $advert->getActionPriceMediaAndroidSoftbank1();
			$action_price_media_android_au_1 = $advert->getActionPriceMediaAndroidAu1();
			$action_price_media_android_pc_1 = $advert->getActionPriceMediaAndroidPc1();

			// pcアクション単価(グロス)を取得
			$action_price_client_pc_1 = $advert->getActionPriceClientPc1();

			// pcアクション単価(ネット)を取得
			$action_price_media_pc_1 = $advert->getActionPriceMediaPc1();
		}

		echo "test005<br />";

	} else {
		// エラーメッセージを発行
		echo "error:(003)正規の広告ではありません。";
		exit();
	}

	// キャリアチェック
	// docomo
	// ユーザーエージェントにDoCoMoの文字列が含まれているか
	if(ereg("^DoCoMo", $user_agent)) {
		// キャリアIDに1を代入
		$error_flag = 1;

	// softbank
	// ユーザーエージェントにJ-PHONE,Vodafone,SoftBankの文字列が含まれているか
	} else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $user_agent)) {
		// キャリアIDに2を代入
		$error_flag = 2;

	// au
	// ユーザーエージェントにUP.Browser,KDDIが含まれているか
	} else if(ereg("^UP.Browser|^KDDI", $user_agent)) {
		// キャリアIDに3を代入
		$error_flag = 3;


	// pc,その他
	} else {

		// キャリア取得
		if(isset($_GET['carrier_id']) && $_GET['carrier_id'] != "") {
			$carrier_id = $_GET['carrier_id'];
		} else {
			// キャリアを取得できない
			$carrier_id = "0";
		}

		// ブラウザの判別
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false) {
			// iPhoneの場合
			$browser_type = "1";
		} else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false) {
			// Androidの場合
			$browser_type = "2";
		} else {
			// PCの場合
			$browser_type = "3";
		}

		// 単価の設定
		if($browser_type == "1") {
			// iphoneの場合
			if($carrier_id == "1") {
				// docomo
				$action_price_client = $action_price_client_iphone_docomo_1;
				$action_price_media = $action_price_media_iphone_docomo_1;
			} elseif($carrier_id == "2") {
				// softbank
				$action_price_client = $action_price_client_iphone_softbank_1;
				$action_price_media = $action_price_media_iphone_softbank_1;
			} elseif($carrier_id == "3") {
				// au
				$action_price_client = $action_price_client_iphone_au_1;
				$action_price_media = $action_price_media_iphone_au_1;
			} else {
				// pc
				$action_price_client = $action_price_client_iphone_pc_1;
				$action_price_media = $action_price_media_iphone_pc_1;
			}

			// 広告入稿URL
			$advert_url = $advert->getSiteUrlIphoneDocomo();

		} elseif($browser_type == "2") {
			// iphoneの場合
			if($carrier_id == "1") {
				// docomo
				$action_price_client = $action_price_client_android_docomo_1;
				$action_price_media = $action_price_media_android_docomo_1;
			} elseif($carrier_id == "2") {
				// softbank
				$action_price_client = $action_price_client_android_softbank_1;
				$action_price_media = $action_price_media_android_softbank_1;
			} elseif($carrier_id == "3") {
				// au
				$action_price_client = $action_price_client_android_au_1;
				$action_price_media = $action_price_media_android_au_1;
			} else {
				// pc
				$action_price_client = $action_price_client_android_pc_1;
				$action_price_media = $action_price_media_android_pc_1;
			}

			// 広告入稿URL
			$advert_url = $advert->getSiteUrlAndroidDocomo();

		} else {
			// pcアクション単価(グロス)を取得
			$action_price_client = $action_price_client_pc_1;
			// pcアクション単価(ネット)を取得
			$action_price_media = $action_price_media_pc_1;

			// 広告入稿URL
			$advert_url = $advert->getSiteUrlPc();

		}

		// 認証フラグ
		$approval_flag = $advert->getApprovalFlag();

		if($approval_flag == "1") {
			// 全認証
			$at = "2";
		} elseif($approval_flag == "2") {
			//  手動認証
			$at = "1";
		} else {
			echo "error";
			exit();
		}

		// 定額/定率 タイプ
		$price_type = $advert->getPriceType();
//		// 広告入稿URL
//		$advert_url = $advert->getSiteUrlPc();

	}

//----------------------------------------------------------------------------------
//	// リファラ
//	if( $_SERVER['HTTP_REFERER'] != NULL ){
//		$ref = $_SERVER['HTTP_REFERER'];
//	}
//	else{
//		$ref = "NO REFERER";
//	}
//
//	$c_referer_logs = new C_refererLogs();
//	$c_referer_logs->M_getReferer("1", $advert_id, $media_id, $carrier_id, $ref);
//----------------------------------------------------------------------------------

echo $error_flag;

	// エラーフラグが0と等しいか
	if($error_flag == 0){
		// 広告遷移先URLに##ID##が含まれているか
		if(stripos($advert_url, "##ID##")) {
			// ##ID##部分をsessionIDに置換
			$advert_url = ereg_replace("##ID##", $session_id, $advert_url);

		// 広告遷移先URLに?が含まれているか
		} elseif(!stripos($advert_url, "?")) {
			// 広告遷移先URLに?が含まれていない
			// 広告遷移先URLに?bid=session_idを付加する
			$advert_url .= "?bid=$session_id";

		} else {
			// 広告遷移先URLに?が含まれている
			// 広告遷移先URLに&bid=session_idを付加する
			$advert_url .= "&bid=$session_id";
		}


		$advert_url .= "&at=$at";

		// action_log_daoのトランザクションをスタート
		$action_log_dao->transaction_start();
		// action_logクラスの生成
		$action_log = new ActionLog();
		// action_logのget/setメソッドにセット
		// セッションIDをセット
		$action_log->setSessionId($session_id);
		// ユーザーエージェントをセット
		$action_log->setUserAgent($user_agent);
		// uidをセット
		$action_log->setUid($uid);
		// ブラウザタイプをセット
		$action_log->setBrowserType($browser_type);
		// IPアドレスをセット
		$action_log->setIpAddress($ip_address);
		// ホスト名をセット
		$action_log->setHostName($host_name);
		// 媒体IDをセット
		$action_log->setMediaId($media_id);
		// 媒体発行者IDをセット
		$action_log->setMediaPublisherId($media_publisher_id);
		// 広告IDをセット
		$action_log->setAdvertId($advert_id);
		// 広告主IDをセット
		$action_log->setAdvertClientId($advert_client_id);
		// クリック単価(グロス)をセット
		$action_log->setClickPriceClient($click_price_client);
		// クリック単価(ネット)をセット
		$action_log->setClickPriceMedia($click_price_media);
		// アクション単価(グロス)をセット
		$action_log->setActionPriceClient($action_price_client);
		// アクション単価(ネット)をセット
		$action_log->setActionPriceMedia($action_price_media);

		$action_log->setOrderNum(1);

		// アクション単価(ネット)をセット
		$action_log->setApprovalFlag($approval_flag);
		// アクション単価(ネット)をセット
		$action_log->setPriceType($price_type);

		// 広告遷移先URLをセット
		$action_log->setLinkUrl($advert_url);
		// ポイントバックパラメータ(キックバックパラメータ)をセット
		$action_log->setPointBackParameter($point_back_parameter);
		// ポイントバックURL(キックバックURL)をセット
		$action_log->setPointBackUrl($point_back_url);
		// ステータスをセット
		$action_log->setStatus($status);

		//-------------------------------------------
		// 7/21 追加
		// ライブレボリューション専用ユニークID
		$action_log->setAuid($auid);
		//-------------------------------------------


		//INSERTを実行
		$db_result = $action_log_dao->insertActionLog($action_log, $result_message);

		echo "<br />" . $advert_url;

		// DB結果
		if($db_result) {
			// insert成功
			// トランザクション エンド
			$action_log_dao->transaction_end();
			// 広告遷移先へ遷移
			header('Location: '.$advert_url);
			exit();

		} else {
			// insert失敗
			// トランザクション ロールバック
			$action_log_dao->transaction_rollback();
			// メッセージ表示
			echo $resut_message . "error";
			exit();

		}
	} else {
		// エラーフラグが1の場合
		// エラーメッセージを発行
		echo "error:(004)このキャリアに対応した広告ではありません。";
		exit();

	}

}else{
	// GETパラメータm,aがセットされてなかった場合
	exit();

}
?>