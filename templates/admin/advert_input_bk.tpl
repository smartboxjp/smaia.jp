{include file='./header.tpl' page_title='広告('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

{if $form_data.id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<form method="POST" action="./advert_view_setup.php">
				<input type="submit" value="表示広告設定" />
				<input type="hidden" name="advert_id" value="{$form_data.id}" />
			</form>
		</td>
		<td>
			<form method="POST" action="./advert_price_setup.php">
				<input type="submit" value="媒体別単価設定" />
				<input type="hidden" name="advert_id" value="{$form_data.id}" />
			</form>
		</td>
	</tr>
</table>
{/if}


<form method="POST" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="0">

	{if $advert_price_media_set_message != ''}
	<tr>
		<th colspan="3" id="th_title">{$advert_price_media_set_message}</th>
	</tr>
	{/if}

	<tr>
		<th colspan="2" id="th_title">広告主</th>
		<td>
			<select name="advert_client_id">
			{foreach from=$advert_client_array item="data" name="advert_client_list"}
				<option value="{$data.id}"{if $form_data.advert_client_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告カテゴリー</th>
		<td>
			<select name="advert_category_id">
			{foreach from=$advert_category_array item="data" name="advert_category_list"}
				<option value="{$data.id}"{if $form_data.advert_category_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告名</th>
		<td>
			<input type="text" size="50" name="advert_name" value="{$form_data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>

	<tr>
		<th colspan="2" rowspan="1" id="th_title">サイトURL(iphone)</th>
		<td>
			<input type="text" size="100" name="site_url_iphone_docomo" value="{$form_data.site_url_iphone_docomo|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
<!--	<tr>-->
<!--		<td>-->
<!--			<input type="text" size="100" name="site_url_iphone_softbank" value="{$form_data.site_url_iphone_softbank|htmlspecialchars:$smarty.const.ENT_QUOTES}" />-->
<!--		</td>-->
<!--	</tr>-->
<!--	<tr>-->
<!--		<td>-->
<!--			<input type="text" size="100" name="site_url_iphone_au" value="{$form_data.site_url_iphone_au|htmlspecialchars:$smarty.const.ENT_QUOTES}" />-->
<!--		</td>-->
<!--	</tr>-->
	<tr>
		<th colspan="2" rowspan="1" id="th_title">サイトURL(android)</th>
		<td>
			<input type="text" size="100" name="site_url_android_docomo" value="{$form_data.site_url_android_docomo|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
<!--	<tr>-->
<!--		<td>-->
<!--			<input type="text" size="100" name="site_url_android_softbank" value="{$form_data.site_url_android_softbank|htmlspecialchars:$smarty.const.ENT_QUOTES}" />-->
<!--		</td>-->
<!--	</tr>-->
<!--	<tr>-->
<!--		<td>-->
<!--			<input type="text" size="100" name="site_url_android_au" value="{$form_data.site_url_android_au|htmlspecialchars:$smarty.const.ENT_QUOTES}" />-->
<!--		</td>-->
<!--	</tr>-->
	<tr>
		<th colspan="2" id="th_title">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="{$form_data.site_url_pc|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<!-- 追加 -->

	<tr>
		<th colspan="2" id="th_title">サイトURLについて</th>
		<td>
			＜セッションIDの付加について＞<br />
			サイトURLに?が含まれない場合「?bid=」で挿入される<br />
			http://example.jp/→http://example.jp/?bid=***<br />
			サイトURLに?が含まれる場合「&bid=」で挿入される<br />
			http://example.jp/?a=b→http://example.jp/?a=b&bid=***<br />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告概要</th>
		<td>
			<textarea cols="70" rows="5" name="site_outline">{$form_data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">クリック単価(クライアント)</th>
		<td>
			<input type="text" size="10" name="click_price_client" value="{$form_data.click_price_client|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">クリック単価(メディア)</th>
		<td>
			<input type="text" size="10" name="click_price_media" value="{$form_data.click_price_media|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
		</td>
	</tr>

	<!-- 追加 -->
	<tr>
		<th colspan="2" id="th_title">定率/定額選択</th>
		<td>
			<input type="radio" name="price_type" value="1"{if $form_data.price_type == 1 || $form_data.price_type == ""} checked="checked"{/if} /><label>定額</label>
			<input type="radio" name="price_type" value="2"{if $form_data.price_type == 2} checked="checked"{/if} /><label>定率</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">認証方法</th>
		<td>
			<input type="radio" name="approval_flag" value="1"{if $form_data.approval_flag == 1 || $form_data.approval_flag == ""} checked="checked"{/if} /><label>全認証</label>
			<input type="radio" name="approval_flag" value="2"{if $form_data.approval_flag == 2} checked="checked"{/if} /><label>手動</label>
		</td>
	</tr>
	<!-- 追加 -->


	<!-- 追加 -->
	<tr>
		<th colspan="2" id="th_title">iphoneキャリア選択</th>
		<td>
<!--			<input type="checkbox" name="support_iphone_docomo" value="1"{if $form_data.support_iphone_docomo == 1} checked="checked"{/if} />docomo-->
<!--			<input type="checkbox" name="support_iphone_softbank" value="1"{if $form_data.support_iphone_softbank == 1} checked="checked"{/if} />softbank-->
<!--			<input type="checkbox" name="support_iphone_au" value="1"{if $form_data.support_iphone_au == 1} checked="checked"{/if} />au-->
			<input type="checkbox" name="support_iphone_pc" value="1"{if $form_data.support_iphone_pc == 1} checked="checked"{/if} />その他
		</td>
	</tr>

	<tr>
		<th colspan="2" id="th_title">クライアント アクション単価(金額)(iphone)</th>
		<td>
			[成果1]
			<input type="text" size="10" name="action_price_client_iphone_pc_1" value="{$form_data.action_price_client_iphone_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_client_iphone_softbank_1" value="{$form_data.action_price_client_iphone_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_client_iphone_au_1" value="{$form_data.action_price_client_iphone_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果2]
			<input type="text" size="10" name="action_price_client_iphone_pc_2" value="{$form_data.action_price_client_iphone_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_client_iphone_softbank_2" value="{$form_data.action_price_client_iphone_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_client_iphone_au_2" value="{$form_data.action_price_client_iphone_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果3]
			<input type="text" size="10" name="action_price_client_iphone_pc_3" value="{$form_data.action_price_client_iphone_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_client_iphone_softbank_3" value="{$form_data.action_price_client_iphone_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_client_iphone_au_3" value="{$form_data.action_price_client_iphone_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果4]
			<input type="text" size="10" name="action_price_client_iphone_pc_4" value="{$form_data.action_price_client_iphone_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_client_iphone_softbank_4" value="{$form_data.action_price_client_iphone_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_client_iphone_au_4" value="{$form_data.action_price_client_iphone_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果5]
			<input type="text" size="10" name="action_price_client_iphone_pc_5" value="{$form_data.action_price_client_iphone_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_client_iphone_softbank_5" value="{$form_data.action_price_client_iphone_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_client_iphone_au_5" value="{$form_data.action_price_client_iphone_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メディア アクション単価(金額)(iphone)</th>
		<td>
			[成果1]
			<input type="text" size="10" name="action_price_media_iphone_pc_1" value="{$form_data.action_price_media_iphone_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_media_iphone_softbank_1" value="{$form_data.action_price_media_iphone_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_media_iphone_au_1" value="{$form_data.action_price_media_iphone_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果2]
			<input type="text" size="10" name="action_price_media_iphone_pc_2" value="{$form_data.action_price_media_iphone_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_media_iphone_softbank_2" value="{$form_data.action_price_media_iphone_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_media_iphone_au_2" value="{$form_data.action_price_media_iphone_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果3]
			<input type="text" size="10" name="action_price_media_iphone_pc_3" value="{$form_data.action_price_media_iphone_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_media_iphone_softbank_3" value="{$form_data.action_price_media_iphone_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_media_iphone_au_3" value="{$form_data.action_price_media_iphone_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果4]
			<input type="text" size="10" name="action_price_media_iphone_pc_4" value="{$form_data.action_price_media_iphone_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_media_iphone_softbank_4" value="{$form_data.action_price_media_iphone_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_media_iphone_au_4" value="{$form_data.action_price_media_iphone_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
			[成果5]
			<input type="text" size="10" name="action_price_media_iphone_pc_5" value="{$form_data.action_price_media_iphone_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
<!--			softbank:<input type="text" size="10" name="action_price_media_iphone_softbank_5" value="{$form_data.action_price_media_iphone_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
<!--			au:<input type="text" size="10" name="action_price_media_iphone_au_5" value="{$form_data.action_price_media_iphone_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円-->
			<br />
		</td>
	</tr>

	<tr>
		<th colspan="2" id="th_title">androidキャリア選択</th>
		<td>
			<input type="checkbox" name="support_android_docomo" value="1"{if $form_data.support_android_docomo == 1} checked="checked"{/if} />docomo
			<input type="checkbox" name="support_android_softbank" value="1"{if $form_data.support_android_softbank == 1} checked="checked"{/if} />softbank
			<input type="checkbox" name="support_android_au" value="1"{if $form_data.support_android_au == 1} checked="checked"{/if} />au
			<input type="checkbox" name="support_android_pc" value="1"{if $form_data.support_android_pc == 1} checked="checked"{/if} />その他
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">クライアント アクション単価(金額)(android)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_client_android_docomo_1" value="{$form_data.action_price_client_android_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_android_softbank_1" value="{$form_data.action_price_client_android_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_android_au_1" value="{$form_data.action_price_client_android_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_client_android_pc_1" value="{$form_data.action_price_client_android_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_client_android_docomo_2" value="{$form_data.action_price_client_android_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_android_softbank_2" value="{$form_data.action_price_client_android_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_android_au_2" value="{$form_data.action_price_client_android_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_client_android_pc_2" value="{$form_data.action_price_client_android_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_client_android_docomo_3" value="{$form_data.action_price_client_android_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_android_softbank_3" value="{$form_data.action_price_client_android_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_android_au_3" value="{$form_data.action_price_client_android_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_client_android_pc_3" value="{$form_data.action_price_client_android_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_client_android_docomo_4" value="{$form_data.action_price_client_android_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_android_softbank_4" value="{$form_data.action_price_client_android_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_android_au_4" value="{$form_data.action_price_client_android_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_client_android_pc_4" value="{$form_data.action_price_client_android_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_client_android_docomo_5" value="{$form_data.action_price_client_android_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_android_softbank_5" value="{$form_data.action_price_client_android_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_android_au_5" value="{$form_data.action_price_client_android_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_client_android_pc_5" value="{$form_data.action_price_client_android_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メディア アクション単価(金額)(android)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_media_android_docomo_1" value="{$form_data.action_price_media_android_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_android_softbank_1" value="{$form_data.action_price_media_android_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_android_au_1" value="{$form_data.action_price_media_android_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_media_android_pc_1" value="{$form_data.action_price_media_android_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_media_android_docomo_2" value="{$form_data.action_price_media_android_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_android_softbank_2" value="{$form_data.action_price_media_android_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_android_au_2" value="{$form_data.action_price_media_android_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_media_android_pc_2" value="{$form_data.action_price_media_android_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_media_android_docomo_3" value="{$form_data.action_price_media_android_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_android_softbank_3" value="{$form_data.action_price_media_android_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_android_au_3" value="{$form_data.action_price_media_android_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_media_android_pc_3" value="{$form_data.action_price_media_android_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_media_android_docomo_4" value="{$form_data.action_price_media_android_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_android_softbank_4" value="{$form_data.action_price_media_android_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_android_au_4" value="{$form_data.action_price_media_android_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_media_android_pc_4" value="{$form_data.action_price_media_android_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_media_android_docomo_5" value="{$form_data.action_price_media_android_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_android_softbank_5" value="{$form_data.action_price_media_android_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_android_au_5" value="{$form_data.action_price_media_android_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			その他:<input type="text" size="10" name="action_price_media_android_pc_5" value="{$form_data.action_price_media_android_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			<br />
		</td>
	</tr>
	<!-- 追加 -->

	<tr>
		<th colspan="2" id="th_title">その他</th>
		<td>
			<input type="checkbox" name="support_pc" value="1"{if $form_data.support_pc == 1} checked="checked"{/if} />PC
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">クライアント アクション単価(金額)(PC)</th>
		<td>
			[成果1]
			<input type="text" size="10" name="action_price_client_pc_1" value="{$form_data.action_price_client_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果2]
			<input type="text" size="10" name="action_price_client_pc_2" value="{$form_data.action_price_client_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果3]
			<input type="text" size="10" name="action_price_client_pc_3" value="{$form_data.action_price_client_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果4]
			<input type="text" size="10" name="action_price_client_pc_4" value="{$form_data.action_price_client_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果5]
			<input type="text" size="10" name="action_price_client_pc_5" value="{$form_data.action_price_client_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メディア アクション単価(金額)(PC)</th>
		<td>
			[成果1]
			<input type="text" size="10" name="action_price_media_pc_1" value="{$form_data.action_price_media_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果2]
			<input type="text" size="10" name="action_price_media_pc_2" value="{$form_data.action_price_media_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果3]
			<input type="text" size="10" name="action_price_media_pc_3" value="{$form_data.action_price_media_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果4]
			<input type="text" size="10" name="action_price_media_pc_4" value="{$form_data.action_price_media_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果5]
			<input type="text" size="10" name="action_price_media_pc_5" value="{$form_data.action_price_media_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_1">{$form_data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_2">{$form_data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_3">{$form_data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_4">{$form_data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_5">{$form_data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ1)</th>
		<td>
{if $form_data.ms_image_path_1 != ""}
			<img src="{$form_data.ms_image_path_1}" alt="イメージ1" /><br />
{/if}
			<input type="radio" name="ms_image_type_1" value="1"{if $form_data.ms_image_type_1 == 1 || $form_data.ms_image_type_1 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_1" /><br/>
			<input type="radio" name="ms_image_type_1" value="2"{if $form_data.ms_image_type_1 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_1" value="{if $form_data.ms_image_type_1 == 2}{$form_data.ms_image_url_1|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_file_path_1" value="{$form_data.ms_image_file_path_1}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ2)</th>
		<td>
{if $form_data.ms_image_path_2 != ""}
			<img src="{$form_data.ms_image_path_2}" alt="イメージ2" /><br />
{/if}
			<input type="radio" name="ms_image_type_2" value="1"{if $form_data.ms_image_type_2 == 1 || $form_data.ms_image_type_2 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_2" /><br/>
			<input type="radio" name="ms_image_type_2" value="2"{if $form_data.ms_image_type_2 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_2" value="{if $form_data.ms_image_type_2 == 2}{$form_data.ms_image_url_2|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_file_path_2" value="{$form_data.ms_image_file_path_2}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ3)</th>
		<td>
{if $form_data.ms_image_path_3 != ""}
			<img src="{$form_data.ms_image_path_3}" alt="イメージ3" /><br />
{/if}
			<input type="radio" name="ms_image_type_3" value="1"{if $form_data.ms_image_type_3 == 1 || $form_data.ms_image_type_3 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_3" /><br/>
			<input type="radio" name="ms_image_type_3" value="2"{if $form_data.ms_image_type_3 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_3" value="{if $form_data.ms_image_type_3 == 2}{$form_data.ms_image_url_3|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_file_path_3" value="{$form_data.ms_image_file_path_3}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ4)</th>
		<td>
{if $form_data.ms_image_path_4 != ""}
			<img src="{$form_data.ms_image_path_4}" alt="イメージ4" /><br />
{/if}
			<input type="radio" name="ms_image_type_4" value="1"{if $form_data.ms_image_type_4 == 1 || $form_data.ms_image_type_4 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_4" /><br/>
			<input type="radio" name="ms_image_type_4" value="2"{if $form_data.ms_image_type_4 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_4" value="{if $form_data.ms_image_type_4 == 2}{$form_data.ms_image_url_4|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_file_path_4" value="{$form_data.ms_image_file_path_4}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ5)</th>
		<td>
{if $form_data.ms_image_path_5 != ""}
			<img src="{$form_data.ms_image_path_5}" alt="イメージ5" /><br />
{/if}
			<input type="radio" name="ms_image_type_5" value="1"{if $form_data.ms_image_type_5 == 1 || $form_data.ms_image_type_5 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_5" /><br/>
			<input type="radio" name="ms_image_type_5" value="2"{if $form_data.ms_image_type_5 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_5" value="{if $form_data.ms_image_type_5 == 2}{$form_data.ms_image_url_5|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_file_path_5" value="{$form_data.ms_image_file_path_5}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_1">{$form_data.ms_email_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_2">{$form_data.ms_email_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_3">{$form_data.ms_email_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_4">{$form_data.ms_email_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_5">{$form_data.ms_email_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ユニーククリック種別</th>
		<td>
			<input type="radio" name="unique_click_type" value="1"{if $form_data.unique_click_type == 1 || $form_data.unique_click_type == ""} checked="checked"{/if} /><label>マンスリー</label>
			<input type="radio" name="unique_click_type" value="2"{if $form_data.unique_click_type == 2} checked="checked"{/if} /><label>ウィークリー</label>
			<input type="radio" name="unique_click_type" value="3"{if $form_data.unique_click_type == 3} checked="checked"{/if} /><label>デイリー</label>
			<input type="radio" name="unique_click_type" value="4"{if $form_data.unique_click_type == 4} checked="checked"{/if} /><label>全てユニーク</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ポイントバック</th>
		<td>
			<input type="radio" name="point_back_flag" value="1"{if $form_data.point_back_flag == 1 || $form_data.point_back_flag == ""} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="point_back_flag" value="2"{if $form_data.point_back_flag == 2} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">アダルト</th>
		<td>
			<input type="radio" name="adult_flag" value="1"{if $form_data.adult_flag == 1 || $form_data.adult_flag == ""} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="adult_flag" value="2"{if $form_data.adult_flag == 2} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>

	<!-- 追加 -->

	<tr>
		<th colspan="2" id="th_title">SEO</th>
		<td>
			<input type="radio" name="seo_flag" value="1"{if $form_data.seo_flag == 1 || $form_data.seo_flag == ""} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="seo_flag" value="2"{if $form_data.seo_flag == 2} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>

	<tr>
		<th colspan="2" id="th_title">リスティング</th>
		<td>
			<input type="radio" name="listing_flag" value="1"{if $form_data.listing_flag == 1 || $form_data.listing_flag == ""} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="listing_flag" value="2"{if $form_data.listing_flag == 2} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メールマガジン</th>
		<td>
			<input type="radio" name="mail_magazine_flag" value="1"{if $form_data.mail_magazine_flag == 1 || $form_data.mail_magazine_flag == ""} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="mail_magazine_flag" value="2"{if $form_data.mail_magazine_flag == 2} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">コミュニティ</th>
		<td>
			<input type="radio" name="community_flag" value="1"{if $form_data.community_flag == 1 || $form_data.mail_magazine_flag == ""} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="community_flag" value="2"{if $form_data.community_flag	 == 2} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>
	<!-- 追加 -->

	<tr>
		<th colspan="2" id="th_title">出稿開始日</th>
		<td>
			<select name="as_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_as_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="as_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_as_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="as_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_as_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">出稿終了日</th>
		<td>
			<select name="ae_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_ae_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="ae_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_ae_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="ae_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_ae_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
			<input type="checkbox" name="unrestraint_flag" value="1"{if $form_data.unrestraint_flag == 1} checked="checked"{/if} /><label>無制限</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">テストフラグ</th>
		<td>
			<input type="checkbox" name="test_flag" value="1"{if $form_data.test_flag == 1} checked="checked"{/if} />テスト用
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"{if $form_data.status == 1 || $form_data.status == ""} checked="checked"{/if} /><label>予約</label>
			<input type="radio" name="status" value="2"{if $form_data.status == 2} checked="checked"{/if} /><label>出稿中</label>
			<input type="radio" name="status" value="3"{if $form_data.status == 3} checked="checked"{/if} /><label>終了</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

{include file='./hooter.tpl'}