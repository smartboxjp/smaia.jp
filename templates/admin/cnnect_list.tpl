{include file='./header.tpl' page_title='媒体認証一覧'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体認証一覧</h2>

<!-- エラーメッセージがあった場合表示 -->
<div id=message>
	{if $error_message != '' }
		<div id="error_message">
			<h3>ERROR:{$error_message}</h3>
		</div><!-- error_message -->
	{/if}

	<!-- インフォメッセージがあった場合表示 -->
	{if $info_message != '' }
		<div id="info_message">
			<h3>INFO:{$info_message}</h3>
		</div><!-- info_message -->
	{/if}
</div><!-- message -->



<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td id="th_title">検索方法</td>
		<td>
			<input type="radio" name="method" value="0"{if $search.method == 0 || $search.method == ""} checked="checked"{/if} /><label>指定なし</label>
			<input type="radio" name="method" value="2"{if $search.method == 2} checked="checked"{/if} /><label>正規</label>
			<input type="radio" name="method" value="1"{if $search.method == 1} checked="checked"{/if} /><label>提携解除</label>
			<!-- <input type="radio" name="method" value="3"{if $search.method == 3} checked="checked"{/if} /><label>提携待ち</label> -->
		</td>
	</tr>
	<tr>
		<td id="th_title">登録日時ソート</td>
		<td>
			<input type="radio" name="order" value="ASC"{if $search.order == "ASC" || $search.method == ""} checked="checked"{/if} /><label>昇順</label>
			<input type="radio" name="order" value="DESC"{if $search.order == "DESC"} checked="checked"{/if} /><label>降順</label>
		</td>
	</tr>
	<tr>
		<th id="th_title">媒体発行者</th>
		<td>
			<select name="media_publisher_id">
				<option value="0"{if $search.media_publisher_id == '0'} selected="selected"{/if}>指定なし</option>
				{foreach from=$media_publisher_array item="data" name="media_publisher_list"}
					<option value="{$data.id}"{if $search.media_publisher_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>

<!--
		<th id="th_title">登録日時</th>
		<td>
			<input type="checkbox" name="created_at_flag" value="1"{if $search.created_at_flag == 1} checked="checked"{/if} />
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="s_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_s_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select> ～
			<select name="e_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_e_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="e_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_e_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="e_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_e_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
 -->

	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="7" id="th_title">【該当{$list_count}件】<br />{$page_count_link}</th>
	</tr>
	<tr>
		<th id="th_title">ID</th>
		<th id="th_title">媒体発行者名</th>
		<th id="th_title">サイト名</th>
		<th id="th_title">広告</th>
		<th id="th_title">ステータス</th>
		<th id="th_title">登録日時</th>
		<th id="th_title">　</th>
	</tr>
{foreach from=$list item="data" name="list"}
	<tr>
		<td>{$data.id}</td>
		<td><a href="./media_publisher.php?mode=edit&id={$data.media_publisher_id}">{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a>
		</td>
		<td><a href="./media.php?mode=edit&id={$data.media_id}">{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>
		{if $data.status == 1}
			認証解除
		{elseif $data.status == 2}
			正規
		{elseif $data.status == 3}
			認証待ち
		{/if}
		</td>
		<td>{$data.created_at}</td>
		<td>
		{if $data.status == '2'}
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="承認解除" style="background-color:#FFE4B5;" onclick="return confirm('認証解除してよろしいですか？');" />
				<input type="hidden" name="mode" value="delete" />
				<input type="hidden" name="id" value="{$data.id}" />
				<input type="hidden" name="media_id" value="{$data.media_id}" />
				<input type="hidden" name="advert_id" value="{$data.advert_id}" />
			</form>
		{else}
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="承認する" style="background-color:#AAD8E6;" />
				<input type="hidden" name="mode" value="edit" />
				<input type="hidden" name="id" value="{$data.id}" />
				<input type="hidden" name="media_id" value="{$data.media_id}" />
				<input type="hidden" name="advert_id" value="{$data.advert_id}" />
			</form>
		{/if}
		</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}