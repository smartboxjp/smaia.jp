{include file='./header.tpl' page_title='広告('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->


<table cellpadding="0" cellspacing="0">
{if $mode == 'new_regist_client' || $mode == 'new_regist_advert' || $mode == 'new_regist_data'}

<!-- 広告主の選択 -->
<form method="POST" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data">
	<tr>
		<th colspan="2" id="th_title">広告主</th>
		<td>
			<select name="advert_client_id">
			{foreach from=$advert_client_array item="data" name="advert_client_list"}
				<option value="{$data.id}"{if $form_data.advert_client_id == $data.id || $advert_client_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
		<td>
			<input type="submit" value="選択" />
			<input type="hidden" name="mode" value="new_regist_client" />
			<!-- <input type="hidden" name="advert_client_id" value="{$form_data.advert_client_id}" /> -->
		</td>

	</tr>
</form>

	{if $mode == 'new_regist_advert' || $mode == 'new_regist_data'}
	<!-- 広告名の選択 -->
	<form method="POST" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data">
		<tr>
			<th colspan="2" id="th_title">広告名</th>
			<td>
				<select name="id">
				{foreach from=$advert_id_array item="data" name="advert_category_list"}
					<option value="{$data.id}"{if $form_data.id == $data.id} selected="selected"{/if}>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
				{/foreach}
				</select>
			</td>
			<td>
				<input type="submit" value="選択" />
				<input type="hidden" name="mode" value="new_regist_advert" />
				<!-- <input type="hidden" name="id" value="{$form_data.id}" /> -->
				<input type="hidden" name="advert_client_id" value="{$advert_client_id}" />
			</td>

		</tr>
	</form>
	{/if}

{elseif $mode == 'edit'}
	<tr>
		<th colspan="2" id="th_title">広告主</th>
		<td>
			{$form_data.advert_client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告名</th>
		<td>
			{$form_data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}
		</td>
	</tr>
{/if}
</table>


<!-- 広告主、広告名が選択されいる -->
{if $mode == 'new_regist_data' || $mode == 'edit' }
<table cellpadding="0" cellspacing="0">

<form method="POST" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data">

<!-- 広告主、広告名が選択されいる時カテゴリ表示 -->

	<tr>
		<th colspan="2" id="th_title">広告カテゴリー</th>
		<td>
			{$form_data.advert_categories_name|htmlspecialchars:$smarty.const.ENT_QUOTES}
		</td>
	</tr>

<!-- 広告バナー、テキストの選択 -->

	<!-- テキスト1 -->
	{if $form_data.ms_text_1 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト1)</th>
		<td>
			<input type="radio" name="ms_text_type" value="1"{if $form_data.ms_text_num  == 1 || $form_data.ms_text_num == ""} checked="checked"{/if} /><label>テキスト1を選択</label>
			<br />
			<textarea cols="70" rows="2" name="ms_text_1">{$form_data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	{/if}

	<!-- テキスト2 -->
	{if $form_data.ms_text_2 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト2)</th>
		<td>
			<input type="radio" name="ms_text_type" value="2"{if $form_data.ms_text_num == 2 } checked="checked"{/if} /><label>テキスト2を選択</label>
			<br />
			<textarea cols="70" rows="2" name="ms_text_2">{$form_data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	{/if}

	<!-- テキスト3 -->
	{if $form_data.ms_text_3 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト3)</th>
		<td>
			<input type="radio" name="ms_text_type" value="3"{if $form_data.ms_text_num == 3 } checked="checked"{/if} /><label>テキスト3を選択</label>
			<br />
			<textarea cols="70" rows="2" name="ms_text_3">{$form_data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	{/if}

	<!-- テキスト4 -->
	{if $form_data.ms_text_4 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト4)</th>
		<td>
			<input type="radio" name="ms_text_type" value="4"{if $form_data.ms_text_num == 4 } checked="checked"{/if} /><label>テキスト4を選択</label>
			<br />
			<textarea cols="70" rows="2" name="ms_text_4">{$form_data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	{/if}

	<!-- テキスト5 -->
	{if $form_data.ms_text_5 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト5)</th>
		<td>
			<input type="radio" name="ms_text_type" value="5"{if $form_data.ms_text_num == 5 } checked="checked"{/if} /><label>テキスト5を選択</label>
			<br />
			<textarea cols="70" rows="2" name="ms_text_5">{$form_data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	{/if}

	<!-- テキストがない場合 -->
	{if $form_data.ms_text_1 == "" &&
	 $form_data.ms_text_2 == "" &&
	 $form_data.ms_text_3 == "" &&
	 $form_data.ms_text_4 == "" &&
	 $form_data.ms_text_5 == "" }
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト5)</th>
		<td>
			※テキストが設定されていません
			<input type="hidden" name="ms_text_type" value="0" />
		</td>
	</tr>
	{/if}

	<!-- イメージ_1 -->
	{if $form_data.ms_image_url_1 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ1)</th>
		<td>
			<input type="radio" name="ms_image_type" value="1"{if $form_data.ms_image_url_num == 1 || $form_data.ms_image_url_num == "" } checked="checked"{/if} /><label>イメージ1を選択</label>
			<br />
			<img src="../advert_image/{$form_data.ms_image_url_1}" alt="イメージ1" /><br />
		</td>
	</tr>
	{/if}

	<!-- イメージ2 -->
	{if $form_data.ms_image_url_2 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ2)</th>
		<td>
			<input type="radio" name="ms_image_type" value="2"{if $form_data.ms_image_url_num == 2 } checked="checked"{/if} /><label>イメージ2を選択</label>
			<br />
			<img src="../advert_image/{$form_data.ms_image_url_2}" alt="イメージ2" /><br />
		</td>
	</tr>
	{/if}

	<!-- イメージ3 -->
	{if $form_data.ms_image_url_3 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ3)</th>
		<td>
			<input type="radio" name="ms_image_type" value="3"{if $form_data.ms_image_url_num == 3 } checked="checked"{/if} /><label>イメージ3を選択</label>
			<br />
			<img src="../advert_image/{$form_data.ms_image_url_3}" alt="イメージ3" /><br />
		</td>
	</tr>
	{/if}

	<!-- イメージ4 -->
	{if $form_data.ms_image_url_4 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ4)</th>
		<td>
			<input type="radio" name="ms_image_type" value="4"{if $form_data.ms_image_url_num == 4 } checked="checked"{/if} /><label>イメージ4を選択</label>
			<br />
			<img src="../advert_image/{$form_data.ms_image_url_4}" alt="イメージ4" /><br />
		</td>
	</tr>
	{/if}

	<!-- イメージ5 -->
	{if $form_data.ms_image_url_5 != ""}
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ5)</th>
		<td>
			<input type="radio" name="ms_image_type" value="5"{if $form_data.ms_image_url_num == 5 } checked="checked"{/if} /><label>イメージ5を選択</label>
			<br />
			<img src="../advert_image/{$form_data.ms_image_url_5}" alt="イメージ5" /><br />
		</td>
	</tr>
	{/if}

	<!-- イメージがない場合 -->
	{if $form_data.ms_image_url_1 == '' &&
	 $form_data.ms_image_url_2 == '' &&
	 $form_data.ms_image_url_3 == '' &&
	 $form_data.ms_image_url_4 == '' &&
	 $form_data.ms_image_url_5 == '' }
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ)</th>
		<td>
			※イメージが設定されていません
			<input type="hidden" name="ms_image_type" value="0" />
		</td>
	</tr>
	{/if}

	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			{if $mode == 'new_regist_data'}
				<input type="hidden" name="mode" value="insert_commit" />
				<input type="hidden" name="id" value="{$form_data.id}" />
			{elseif  $mode == 'edit'}
				<input type="hidden" name="mode" value="update_commit" />
				<input type="hidden" name="id" value="{$form_data.advert_attention_id}" />
			{/if}
		</td>
	</tr>
</form>
</table>
{/if}


</div><!-- contents -->

{include file='./hooter.tpl'}