{include file='./header.tpl' page_title='広告主('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告主({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告グループ</th>
		<td>
			<select name="advert_group_id">
			{foreach from=$advert_group_array item="data" name="advert_group_list"}
				<option value="{$data.id}"{if $form_data.advert_group_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ログインID ※6-16桁</th>
		<td>
			<input type="text" size="20" name="login_id" value="{$form_data.login_id|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">パスワード ※6-16桁</th>
		<td>
			<input type="text" size="20" name="login_pass" value="{$form_data.login_pass|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">会社名</th>
		<td>
			<input type="text" size="50" name="client_name" value="{$form_data.client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">担当者名</th>
		<td>
			<input type="text" size="50" name="contact_person" value="{$form_data.contact_person|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">TEL</th>
		<td>
			<input type="text" size="30" name="tel" value="{$form_data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">FAX</th>
		<td>
			<input type="text" size="30" name="fax" value="{$form_data.fax|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メールアドレス</th>
		<td>
			<input type="text" size="30" name="email" value="{$form_data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">郵便番号</th>
		<td>
			<input type="text" size="10" name="zipcode1" value="{$form_data.zipcode1|htmlspecialchars:$smarty.const.ENT_QUOTES}" /> - <input type="text" size="10" name="zipcode2" value="{$form_data.zipcode2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">都道府県</th>
		<td>
			<select name="pref">
			{foreach from=$pref_array key="key" item="data" name="pref_list"}
				<option value="{$data.id}"{if $data.id == $form_data.pref} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">住所１（市区町村）</th>
		<td>
			<input type="text" size="50" name="address1" value="{$form_data.address1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">住所２（番地、建物）</th>
		<td>
			<input type="text" size="50" name="address2" value="{$form_data.address2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"{if $form_data.status == 1 || $form_data.status == ""} checked="checked"{/if} /><label>仮登録</label>
			<input type="radio" name="status" value="2"{if $form_data.status == 2} checked="checked"{/if} /><label>正規</label>
			<input type="radio" name="status" value="3"{if $form_data.status == 3} checked="checked"{/if} /><label>退会</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
</form>

</div><!-- contents -->

{include file='./hooter.tpl'}