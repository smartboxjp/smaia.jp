﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{config_load file='admin_app.conf'}
<title>{#title#}&nbsp;{$page_title}</title>
<link rel="stylesheet" type="text/css" href="../css/reset.css" media="all" />
<link rel="stylesheet" type="text/css" href="../css/admin_main.css" media="all" />
</head>
<body>

<!-- ヘッダー -->
<div id="my_header">
	<p>{#title#}</p>
</div><!-- header -->

<div id="container">