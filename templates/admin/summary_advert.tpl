{include file='./header.tpl' page_title='広告別集計'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告別集計</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th id="th_title">広告主</th>
		<td>
			<select name="advert_client_id">
				<option value="0">指定しない</option>
{foreach from=$advert_client_array item="data" name="advert_client_list"}
				<option value="{$data.id}"{if $search.advert_client_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td rowspan="2" id="th_title">集計日時</td>
		<td>
			<input type="radio" name="select_date_type" value="1"{if $search.select_date_type == 1} checked="checked"{/if} />年月指定
			<select name="monthly_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $search.monthly_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="monthly_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $search.monthly_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="select_date_type" value="2"{if $search.select_date_type == 2} checked="checked"{/if} />期間指定
			<select name="between_start_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $search.between_start_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="between_start_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $search.between_start_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="between_start_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $search.between_start_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select> ～
			<select name="between_end_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $search.between_end_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="between_end_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $search.between_end_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="between_end_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $search.between_end_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="7" id="th_title">
{if $search.advert_client_id != 0}
			{$advert_client_array[$search.advert_client_id].name|htmlspecialchars:$smarty.const.ENT_QUOTES}
{/if}
{if $search.select_date_type == 1}
			[{$search.monthly_year}年{$search.monthly_month}月 集計]
{elseif $search.select_date_type == 2}
			[{$search.between_start_year}年{$search.between_start_month}月{$search.between_start_day}日～{$search.between_end_year}年{$search.between_end_month}月{$search.between_end_day}日 集計]
{/if}
		</th>
	</tr>
	<tr>
		<th colspan="7" id="th_title">【該当{$list_count}件】</th>
	</tr>
	<tr>

<!--		<th id="th_title">広告名</th>-->

		<th id="th_title"><a href="?ac_id={$ac_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&sort_price_advert_name={$sort_price_advert_name|default:'desc'}">広告名{$mark_sort_prise_advert_name|default:'[▼]'}</a></th>

<!--		<th id="th_title">広告主名</th>-->

		<th id="th_title"><a href="?ac_id={$ac_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&sort_price_client_name={$sort_price_client_name|default:'desc'}">広告主{$mark_sort_prise_client_name|default:'[▼]'}</a></th>

		<th id="th_title">クリック数</th>
		<th id="th_title">アクション数</th>
		<th id="th_title">アクション単価</th>
		<th id="th_title">発注数</th>
		<th id="th_title"><a href="?ac_id={$ac_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&sort_price={$sort_price|default:'desc'}">金額合計{$mark_sort_prise|default:'[▼]'}</a></th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td><a href="./advert.php?mode=edit&id={$data.advert_id}">{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./advert_client.php?mode=edit&id={$data.advert_client_id}">{$data.client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./summary_click_detail.php?a_id={$data.advert_id}&ac_id={$data.advert_client_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&aggregate_flag=advert">{$data.click_count|number_format}</a></td>
		<td><a href="./summary_action_detail.php?a_id={$data.advert_id}&ac_id={$data.advert_client_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&aggregate_flag=advert">{$data.action_count|number_format}</a></td>

		<td>&yen;{$data.action_price_client|number_format}</td>
		<td>{$data.order_num}</td>
		<td>&yen;{$data.total_price|number_format}</td>
	</tr>
{/foreach}

	<tr>
		<td colspan="2" id="th_title">
			合計
		</td>
		<td id="th_title">
			<!-- クリック数 -->
			{$sum_click_count}
		</td>
		<td id="th_title">
			<!-- アクション数 -->
			{$sum_action_count}
		</td>
		<td id="th_title">
			-
		</td>
		<td id="th_title">
			-
		</td>
		<td id="th_title">
			<!-- 合計 -->
			&yen;{$sum_total_price}
		</td>
	</tr>
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}