{include file='./header.tpl' page_title='成果ログ'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>成果ログ</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
<!--	<tr>-->
<!--		<th colspan="10">-->
<!--			<input type="button" value="CSVダウンロード" onclick="location.href='./result_log_download.php?date={$download_date}&type={$log_type}'" />-->
<!--		</th>-->
<!--	</tr>-->
	<tr>
		<th colspan="10">【該当{$list_count}件】
{if $list_count > 1000}
	{if $prev == 0}
			<span class="prev">PREV</span>
	{else}
			<a class="prev" href="?log_type={$log_type}&mode={$mode}&download_date={$download_date}&page={$prev}">PREV</a>
	{/if}
	{section name=cnt start=1 loop=$page_max+1}
		{if $smarty.section.cnt.index == $page}
			<span>[{$smarty.section.cnt.index}]</span>
		{else}
			&nbsp;<a href="?log_type={$log_type}&mode={$mode}&download_date={$download_date}&page={$smarty.section.cnt.index}">{$smarty.section.cnt.index}</a>&nbsp;
		{/if}
	{/section}
	{if $next == 0}
		<span class="next">NEXT</span>
	{else}
		<a class="next" href="?log_type={$log_type}&mode={$mode}&download_date={$download_date}&page={$next}">NEXT</a>
	{/if}
{/if}
		</th>
	</tr>
	<tr>
		<th>ポイントバック通知日時</th>
		<th>媒体ID</th>
		<th>広告ID</th>
		<th>ユーザ識別ID</th>
		<th>ポイントバック通知URL</th>
		<th>ステータス</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.created_at}</td>
		<td>{$data.media_id}</td>
		<td>{$data.advert_id}</td>
		<td>{$data.point_back_parameter}</td>
		<td>{$data.point_back_url}</td>
		<td>
{if $data.status == 1}
			成功
{elseif $data.status == 2}
			失敗
{/if}
		</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}