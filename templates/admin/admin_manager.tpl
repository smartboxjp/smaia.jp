{include file='./header.tpl' page_title='お問い合わせ一覧'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>お問い合わせ一覧</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="5" id="th_title">【該当{$list_count}件】</th>
	</tr>

	<tr>
		<th id="th_title">クラス</th>
		<th id="th_title">ログインID</th>
		<th id="th_title">ログインPASS</th>
		<th id="th_title">制限</th>
		<th id="th_title">　</th>
	</tr>

{foreach from=$list key="key" item="data" name="list"}
	<tr>
	<form method="POST" action="{$smarty.server.PHP_SELF}">

		<td>
			<select name="login_class">
			<option value="Public" {if $data.login_class == "Public"} selected="selected"{/if}>Public</option>
			<option value="Private" {if $data.login_class == "Private"} selected="selected"{/if}>Private</option>
			<option value="Protect" {if $data.login_class == "Protect"} selected="selected"{/if}>Protect</option>
			<option value="Partner" {if $data.login_class == "Partner"} selected="selected"{/if}>Partner</option>
			</select>
		</td>
		<td><input type="text" name="login_id" value="{$data.login_id}" /></td>
		<td><input type="text" name="login_pass" value="{$data.login_pass}" /></td>
		<td>{if $data.login_class == 'Public'}なし{else}あり{/if}</td>
		<td>

				<input type="submit" value="変更" />
				<input type="hidden" name="mode" value="detail" />
				<input type="hidden" name="id" value="{$data.id}" />

		</td>

	</form>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}
