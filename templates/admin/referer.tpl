{include file='./header.tpl' page_title='リファラー'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>リファラー</h2>

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td id="th_title">検索方法</td>
		<td>
			<input type="radio" name="status" value="1"{if $status == 1 || $search.method == ""} checked="checked"{/if} /><label>クリック</label>
			<input type="radio" name="status" value="2"{if $status == 2} checked="checked"{/if} /><label>アクション</label>
		</td>
	</tr>

	<tr>
		<td id="th_title">登録日時ソート</td>
		<td>
			<input type="radio" name="order" value="ASC"{if $order == "ASC" || $search.method == ""} checked="checked"{/if} /><label>昇順</label>
			<input type="radio" name="order" value="DESC"{if $order == "DESC"} checked="checked"{/if} /><label>降順</label>
		</td>
	</tr>

	<tr>
		<td id="th_title">広告主</td>
		<td>
			<select name="advert_client_id">
				<option value="0">指定しない</option>
				{foreach from=$advert_clients_name item="data" name="advert_clients_name"}
					<option value="{$data.id}"{if $advert_client_id == $data.id} selected="selected"{/if}>{$data.client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
				{/foreach}
			</select>
		</td>
	</tr>

	<tr>
		<td id="th_title">登録日時</td>
		<td>
			<input type="checkbox" name="date_check" value="1"{if $date_check == 1} checked="checked"{/if} />
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="s_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_s_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select> ～
			<select name="e_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_e_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="e_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_e_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="e_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_e_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>



<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="6" id="th_title">【該当{$list_count}件】<br />{$page_count_link}</th>
	</tr>
	<tr>
		<th id="th_title" >ID</th>
		<th id="th_title" width="148">広告主</th>
		<th id="th_title" width="148">広告名</th>
		<th id="th_title" width="148">媒体名</th>
		<th id="th_title" width="200">URL</th>
		<th id="th_title">登録日時</th>
	</tr>

{foreach from=$referer_list item="data" name="list"}
	<tr>
		<td>{$data.id}</td>
		<td width="148">{$data.client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td width="148">{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td width="148">{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>

		<td width="200">{$data.referer|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.created_at|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/foreach}
</table>



</div>

<!-- フッター -->
{include file='./hooter.tpl'}