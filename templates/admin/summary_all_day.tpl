{include file='./header.tpl' title='全体集計(日別)'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>全体集計(日別)</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th id="th_title">年月</th>
		<th id="th_title">売上額</th>
		<th id="th_title">支払い額</th>
		<th id="th_title">報酬手数料</th>
		<th id="th_title">クリック数</th>
		<th id="th_title">アクション数</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.summary_date}</td>
		<td>&yen;{$data.sales|number_format}</td>
		<td>&yen;{$data.amounts|number_format}</td>
		<td>&yen;{$data.fees|number_format}</td>
		<td>{$data.click_count|number_format}</td>
		<td>{$data.action_count|number_format}</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}