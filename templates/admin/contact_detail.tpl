{include file='./header.tpl' page_title='お問い合わせ詳細'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>お問い合わせ詳細</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table>
	<tr>
		<th id="th_title">問い合わせ日</th>
		<td class='toi_datail'>{$form_data.commit_at}</td>
	</tr>
	<tr>
		<th id="th_title">お問い合わせ主</td>
		<td class='toi_datail'>{$form_data.user_name}</th>
	</tr>
	<tr>
		<th id="th_title">都道府県</td>
		<td class='toi_datail'>{$form_data.pref}</th>
	</tr>
	<tr>
		<th id="th_title">住所１</th>
		<td class='toi_datail'>{$form_data.address1}</td>
	</tr>
	<tr>
		<th id="th_title">住所２</th>
		<td class='toi_datail'>{$form_data.address2}</td>
	</tr>
	<tr>
		<th id="th_title">電話番号</th>
		<td class='toi_datail'>{$form_data.tel}</td>
	</tr>
	<tr>
		<th id="th_title">メールアドレス</th>
		<td class='toi_datail'>{$form_data.email}</td>
	</tr>
	<tr>
		<th id="th_title">内容</th>
		<td class='toi_datail'>{$form_data.textarea}</td>
	</tr>
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}
