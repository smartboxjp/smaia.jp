{include file='./header_web.tpl' page_title='TOP'}

{include file='./menu.tpl'}

<br />

<div id="side_contents_2">
{include file='./side_contents_2.tpl'}
</div><!-- side_contents -->

<div id="main_contents_2">

<div id="content_iti">
      <div class="padding">
<!--        <h2 class="padding"><img src="images/web/h2_client.gif" alt="クライアント" width="141" height="30" /></h2>-->
        <div id="side_contents_header">
			<p><b>クライアント</b></p>
		</div>
		<br />

        <p><strong>クライアント（広告主）様</strong>のサイトへ誘導するバナー・リンク（広告）を
        <strong>メディア（法人・個人が運営しているウェブサイトやメールマガジンなど）</strong>が掲載し、
        その広告から広告主様のサイトへ誘導された<strong>ユーザー</strong>の会員登録や購入などに対して
        報酬をお支払いいただく手法です。</p>
        <br />
        <p>ユーザーが広告をクリックしない限り、報酬も発生しないシステムなので、<strong>広告掲載に対するリスクがなく費用対効果に優れています。</strong>
          現在のインターネットマーケティングにおいて最も効果的かつ一般的な手法となっています！ </p>
    </div>
    <!--
    <p class="right"><a href="#"><img src="images/web/btn_toi.gif" alt="お問い合わせ" width="88" height="18" /></a></p>
    -->
    <div class="img_center">
      <p><img src="images/web/client_flow.jpg" width="100%" height="468" alt="イメージ"></p>
    </div>
    <div class="padding">
      <strong>手順は簡単です。</strong><br /><br />
      <p>①クライアント様がsmai@に広告を依頼します。</p>
      <p>②メディア様に広告が表示されます。</p>
      <p>③ユーザーが広告を閲覧し、広告をクリックします。</p>
      <p>④ユーザーが広告の商品を購入します。</p>
      <p>⑤クライアント様がユーザーに商品を届けます。</p>
      <p>⑤クライアント様がsmai@に広告料金を支払います。</p>
      <p>⑥smai@がメデイア様に成果報酬をお支払いします。</p>
    </div>
    <!--
    <p class="right"><a href="#"><img src="images/web/btn_toi.gif" alt="お問い合わせ" width="88" height="18"></a></p>
  	-->
  </div>
</div><!-- main_contents -->

{include file='./hooter_web.tpl'}