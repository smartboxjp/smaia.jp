{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl'}

<div id="my_contents">

<h2>レポート</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<input type="hidden" name="m_id" value="{$media_id}" />
<input type="hidden" name="a_id" value="{$advert_id}" />
<table cellpadding="0" cellspacing="0">
	<tr>
		<th style="width: 140px;" id="th_title">メディア</th>
		<th style="width: 500px;" id="th_title">広告</th>
	</tr>
	<tr>
		<td>
		{if $media_id == ""}
			<p><span style="font-weight:bold;">全メディア</span></p>
		{else}
			<p><a href="{$smarty.server.PHP_SELF}">全メディア</a></p>
		{/if}
{foreach from=$media_array key="key" item="data" name="media_list"}
		{if $media_id == $data.id}
			<p><span style="font-weight:bold;">{$data.media_name}</span></p>
		{else}
			<p><a href="?m_id={$data.id}">{$data.media_name}</a></p>
		{/if}
{/foreach}
		</td>
		<td>
			<p>
			{if $advert_id == ""}
				<span style="font-weight:bold;">全広告</span>
			{else}
				<a href="?m_id={$media_id}">全広告</a>
			{/if}
{foreach from=$advert_array key="key" item="data" name="advert_list"}
			{if $advert_id == $data.advert_id}
				<span> / </span><span style="font-weight:bold;">{$data.advert_name}</span>
			{else}
				<span> / </span><a href="?m_id={$media_id}&a_id={$data.advert_id}">{$data.advert_name}</a>
			{/if}
{/foreach}
			</p>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			時間別
			<a href="./report_advert.php">広告別</a>
			<a href="./report_media.php">メディア別</a>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="date_type" value="1"{if $date_type == 1} checked="checked"{/if} /><label>月別</label>
			<input type="radio" name="date_type" value="2"{if $date_type == 2} checked="checked"{/if} /><label>日別</label>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="carrier" value="1"{if $carrier == 1} checked="checked"{/if} /><label>キャリア合計</label>
			<input type="radio" name="carrier" value="2"{if $carrier == 2} checked="checked"{/if} /><label>キャリア別</label>
		</td>
	</tr>
	<tr>
		<td>
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
{if $date_type == 2}
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
{/if}
			<input type="submit" value="表示" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

{if $carrier == 1}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th rowspan="2" id="th_title">年月</th>
		<th colspan="2" id="th_title">クリック報酬</th>
		<th colspan="2" id="th_title">アフィリエイト報酬</th>
		<th rowspan="2" id="th_title">合計</th>
	</tr>
	<tr>
		<th id="th_title">クリック数</th>
		<th id="th_title">報酬金額</th>
		<th id="th_title">アクション数</th>
		<th id="th_title">報酬金額</th>
	</tr>
{foreach from=$summary key="key" item="data" name="summary"}
	<tr>
		<td>{$data.summary_date}</td>
		<td>{$data.click_count|number_format}</td>
		<td>&yen;{$data.click_price|number_format}</td>
		<td>{$data.action_count|number_format}</td>
		<td>&yen;{$data.action_price|number_format}</td>
		<td>&yen;{$data.total_price|number_format}</td>
	</tr>
{/foreach}
	<tr>
		<td>合計</td>
		<td>{$all.click_count|number_format}</td>
		<td>&yen;{$all.click_price|number_format}</td>
		<td>{$all.action_count|number_format}</td>
		<td>&yen;{$all.action_price|number_format}</td>
		<td>&yen;{$all.total_price|number_format}</td>
	</tr>
</table>
{elseif $carrier == 2}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title"></th>
		<th colspan="2" id="th_title">クリック報酬</th>
		<th colspan="2" id="th_title">アフィリエイト報酬</th>
		<th rowspan="2" id="th_title">合計</th>
	</tr>
	<tr>
		<th id="th_title">年月</th>
		<th id="th_title">キャリア</th>
		<th id="th_title">クリック数</th>
		<th id="th_title">報酬金額</th>
		<th id="th_title">アクション数</th>
		<th id="th_title">報酬金額</th>
	</tr>
{foreach from=$summary key="key" item="data" name="summary"}
	<!-- iphone -->
	<tr>
		<td rowspan="6">{$data.summary_date}</td>
		<td>iphone</td>
		<td>{$data.docomo.click_count|number_format}</td>
		<td>&yen;{$data.iphone.click_price|number_format}</td>
		<td>{$data.iphone.action_count|number_format}</td>
		<td>&yen;{$data.iphone.action_price|number_format}</td>
		<td>&yen;{$data.iphone.total_price|number_format}</td>
	</tr>

	<!-- android -->
	<tr>

		<td>android docomo</td>
		<td>{$data.android_docomo.click_count|number_format}</td>
		<td>&yen;{$data.android_docomo.click_price|number_format}</td>
		<td>{$data.android_docomo.action_count|number_format}</td>
		<td>&yen;{$data.android_docomo.action_price|number_format}</td>
		<td>&yen;{$data.android_docomo.total_price|number_format}</td>
	</tr>
	<tr>
		<td>android softbank</td>
		<td>{$data.android_softbank.click_count|number_format}</td>
		<td>&yen;{$data.android_softbank.click_price|number_format}</td>
		<td>{$data.android_softbank.action_count|number_format}</td>
		<td>&yen;{$data.android_softbank.action_price|number_format}</td>
		<td>&yen;{$data.android_softbank.total_price|number_format}</td>
	</tr>
	<tr>
		<td>android au</td>
		<td>{$data.android_au.click_count|number_format}</td>
		<td>&yen;{$data.android_au.click_price|number_format}</td>
		<td>{$data.android_au.action_count|number_format}</td>
		<td>&yen;{$data.android_au.action_price|number_format}</td>
		<td>&yen;{$data.android_au.total_price|number_format}</td>
	</tr>
	<tr>
		<td>android pc</td>
		<td>{$data.android_pc.click_count|number_format}</td>
		<td>&yen;{$data.android_pc.click_price|number_format}</td>
		<td>{$data.android_pc.action_count|number_format}</td>
		<td>&yen;{$data.android_pc.action_price|number_format}</td>
		<td>&yen;{$data.android_pc.total_price|number_format}</td>
	</tr>

	<!-- pc -->
	<tr>
		<td>pc</td>
		<td>{$data.pc.click_count|number_format}</td>
		<td>&yen;{$data.pc.click_price|number_format}</td>
		<td>{$data.pc.action_count|number_format}</td>
		<td>&yen;{$data.pc.action_price|number_format}</td>
		<td>&yen;{$data.pc.total_price|number_format}</td>
	</tr>
{/foreach}
	<tr>
		<td colspan="2" id="th_title">合計</td>
		<td>{$all.click_count|number_format}</td>
		<td>&yen;{$all.click_price|number_format}</td>
		<td>{$all.action_count|number_format}</td>
		<td>&yen;{$all.action_price|number_format}</td>
		<td>&yen;{$all.total_price|number_format}</td>
	</tr>
</table>
{/if}
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}