<div id="my_navigation">

	<p><a href="../index.php?client_logout=y">ログアウト</a></p>
	<p>{$user_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</p>
	<h4 alt="2">■クライアント管理</h4>
	<ul>
		<li>
			<a href="./report.php">
			<img src="../images/client_images/advert_report_data_button.gif" />
			</a>
		</li>

		{if $advert_withdrawal_view_user}

			<li>
				<a href="./withdrawal_report.php">
				<img src="../images/client_images/advert_report_data_button_2.gif" />
				</a>
			</li>

		{/if}
		<li>
			<a href="./info.php">
			<img src="../images/client_images/advert_users_data_button.gif" />
			</a>
		</li>

		{if $user_name == '株式会社リンクエッジ' || $user_name == '株式会社テスト1'}

			<li>
				<a href="./setup.php">
				<img src="../images/client_images/advert_setting_button.gif" />
				</a>
			</li>

		{/if}

	</ul>
</div>
