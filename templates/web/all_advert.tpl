{include file='./header_web.tpl' page_title='TOP'}

{include file='./menu.tpl'}
<br />

<div id="side_contents_2">
	{include file='./side_contents_2.tpl'}
</div><!-- side_contents -->

<div id="main_contents_2">
<div id="content_iti">
	<div class="padding">
		<div id="side_contents_header">
			<p><b>広告一覧</b></p>
		</div>
		<br />
		<p>新着の広告情報です、日付の新しい順に最大30件表示しています。</p>
		<br />
<!-- おすすめ広告 ---------------------------------------------------------------------  -->

		{foreach from=$coming_str item=STR}
			<img src="./images/web/arrow.gif"" />

			　{$STR.created_date}

			　{$STR.advert_name}
			<br />
			<hr style="border-top: 2px dotted #c0c0c0" />
		{/foreach}

	</div>
</div>
</div><!-- main_contents -->

{include file='./hooter_web.tpl'}
