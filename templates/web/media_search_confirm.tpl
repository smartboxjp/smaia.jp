{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl' user_name=$user_name}

<div id="my_contents">

<h2>広告検索</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告情報</th>
	</tr>
	<tr>
		<th id="th_title">広告カテゴリー</th>
		<td>{$data.advert_category_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">広告名</th>
		<td>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">コンテンツ種別</th>
		<td>
{if $data.content_type == 1}
			一般
{elseif $data.content_type == 2}
			公式
{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">対応キャリア</th>
		<td>
			{if $data.support_docomo == 1} docomo{/if}
			{if $data.support_softbank == 1} softbank{/if}
			{if $data.support_au == 1} au{/if}
			{if $data.support_pc == 1} pc{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">広告概要</th>
		<td>{$data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES|nl2br}</td>
	</tr>
	<tr>
		<th id="th_title">クリック単価</th>
		<td>&yen;{$data.click_price_media|number_format}</td>
	</tr>
	<tr>
		<th id="th_title">アクション単価</th>
		<td>
			docomo:&yen;{$data.action_price_media_docomo_1|number_format}
			softbank:&yen;{$data.action_price_media_softbank_1|number_format}
			au:&yen;{$data.action_price_media_au_1|number_format}
			pc:&yen;{$data.action_price_media_pc_1|number_format}
		</td>
	</tr>
	<tr>
		<th id="th_title">ポイントバック</th>
		<td>
			{if $data.point_back_flag == 1}不可{/if}
			{if $data.point_back_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">アダルト</th>
		<td>
			{if $data.adult_flag == 1}不可{/if}
			{if $data.adult_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">出会い</th>
		<td>
			{if $data.dating_flag == 1}不可{/if}
			{if $data.dating_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">出稿開始日</th>
		<td>{$data.advert_start_date}</td>
	</tr>
	<tr>
		<th id="th_title">出稿終了日</th>
		<td>
{if $data.unrestraint_flag == 1}
		無期限
{else}
		{$data.advert_end_date}
{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">提携状態</th>
		<td>未提携</td>
	</tr>
	<tr>
		<td colspan="2">
			<p>提携申請を行います。よろしいですか？</p>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="提携申請" />
				<input type="hidden" name="mode" value="apply" />
				<input type="hidden" name="advert_id" value="{$data.id}" />
				<input type="hidden" name="media_id" value="{$media_id}" />
			</form>
		</td>
	</tr>
</table>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}