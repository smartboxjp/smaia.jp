{include file='./header_web.tpl' page_title='TOP'}

{include file='./menu.tpl'}

<br />

<div id="side_contents_2">
{include file='./side_contents_2.tpl'}
</div><!-- side_contents -->

<div id="main_contents_2">
<div id="content_iti">
<!--<h2><img src="images/web/h2_Registration.gif" alt="メディア登録" width="160" height="30" class="padding" /></h2>-->
	<div id="side_contents_header">
		<p><b>メディアオーナー様登録</b></p>
	</div>
	<br />


<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
<br />
</div><!-- message -->

<!--
<div id="main_contents">
-->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">媒体グループ</th>
		<td>
			<select name="media_group_id">
			{foreach from=$media_group_array item="data" name="media_group_list"}
				<option value="{$data.id}"{if $form_data.media_group_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">ログインID ※（6-16桁）</th>
		<td>
			<input type="text" size="20" maxlength="16" name="login_id" value="{$form_data.login_id|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">パスワード ※（6-16桁）</th>
		<td>
			<input type="text" size="20" maxlength="16" name="login_pass" value="{$form_data.login_pass|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">企業名/個人名</th>
		<td>
			<input type="text" size="40" maxlength="30" name="publisher_name" value="{$form_data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">担当者名 ※（法人の場合のみ）</th>
		<td>
			<input type="text" size="40" maxlength="30" name="contact_person" value="{$form_data.contact_person|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">TEL※</th>
		<td>
			<input type="text" size="30" maxlength="13"  name="tel" value="{$form_data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">FAX</th>
		<td>
			<input type="text" size="30" maxlength="13" name="fax" value="{$form_data.fax|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">メールアドレス※</th>
		<td>
			<input type="text" size="30" maxlength="30" name="email" value="{$form_data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">郵便番号※</th>
		<td>
			<input type="text" size="10" maxlength="3" name="zipcode1" value="{$form_data.zipcode1|htmlspecialchars:$smarty.const.ENT_QUOTES}" /> - <input type="text" size="10" maxlength="4" name="zipcode2" value="{$form_data.zipcode2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">都道府県</th>
		<td>
			<select name="pref">
			{foreach from=$pref_array key="key" item="data" name="pref_list"}
				<option value="{$data.id}"{if $data.id == $form_data.pref} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">住所１※（市区町村）</th>
		<td>
			<input type="text" size="40" maxlength="40" name="address1" value="{$form_data.address1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">住所２※（番地、建物）</th>
		<td>
			<input type="text" size="40" maxlength="40" name="address2" value="{$form_data.address2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">振込先種別</th>
		<td>
			<input type="radio" name="transfer_type" value="1"{if $form_data.transfer_type == 1 || $form_data.transfer_type == ""} checked="checked"{/if} /><label>自行</label>
			<input type="radio" name="transfer_type" value="2"{if $form_data.transfer_type == 2} checked="checked"{/if} /><label>他行</label>
			<input type="radio" name="transfer_type" value="3"{if $form_data.transfer_type == 3} checked="checked"{/if} /><label>郵便局</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">振込先銀行名※</th>
		<td>
			<input type="text" size="40" maxlength="40" name="bank_name" value="{$form_data.bank_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">支店名※</th>
		<td>
			<input type="text" size="40" maxlength="40" name="branch_name" value="{$form_data.branch_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">口座種別※</th>
		<td>
			<input type="radio" name="account_type" value="1"{if $form_data.account_type == 1 || $form_data.account_type == ""} checked="checked"{/if} /><label>普通</label>
			<input type="radio" name="account_type" value="2"{if $form_data.account_type == 2} checked="checked"{/if} /><label>当座</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">口座名義※</th>
		<td>
			<input type="text" size="40" maxlength="40"  name="account_holder" value="{$form_data.account_holder|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">口座番号※</th>
		<td>
			<input type="text" size="40" maxlength="40" name="account_number" value="{$form_data.account_number|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">郵便局振込先名義※</th>
		<td>
			<input type="text" size="40" maxlength="40" name="postal_account_holder" value="{$form_data.postal_account_holder|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">郵便局口座番号※</th>
		<td>
			<input type="text" size="40" maxlength="40" name="postal_account_number" value="{$form_data.postal_account_number|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<!--
	<tr>
		<th colspan="2">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"{if $form_data.status == 1 || $form_data.status == ""} checked="checked"{/if} /><label>仮登録</label>
			<input type="radio" name="status" value="2"{if $form_data.status == 2} checked="checked"{/if} /><label>正規</label>
			<input type="radio" name="status" value="3"{if $form_data.status == 3} checked="checked"{/if} /><label>退会</label>
		</td>
	</tr>
	<tr>
	-->
	</table>
<!-------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--
	<table cellpadding="0" cellspacing="0">
		<tr>
		<th colspan="2">媒体カテゴリー名</th>
		<td>
			<input type="text" size="50" name="media_category" value="{$form_data.media_category|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイト名※</th>
		<td>
			<input type="text" size="50" name="media_name" value="{$form_data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">対応キャリア※</th>
		<td>
			<input type="checkbox" name="support_docomo" value="1"{if $form_data.support_docomo == 1} checked="checked"{/if} /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"{if $form_data.support_softbank == 1} checked="checked"{/if} /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"{if $form_data.support_au == 1} checked="checked"{/if} /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"{if $form_data.support_pc == 1} checked="checked"{/if} /><label>pc</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(docomo)</th>
		<td>
			<input type="text" size="100" name="site_url_docomo" value="{$form_data.site_url_docomo|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(softbank)</th>
		<td>
			<input type="text" size="100" name="site_url_softbank" value="{$form_data.site_url_softbank|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(au)</th>
		<td>
			<input type="text" size="100" name="site_url_au" value="{$form_data.site_url_au|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="{$form_data.site_url_pc|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">メディア種別※</th>
		<td>
			<input type="radio" name="media_type" value="1"{if $form_data.media_type == 1 || $form_data.media_type == ""} checked="checked"{/if} /><label>サイト</label>
			<input type="radio" name="media_type" value="2"{if $form_data.media_type == 2} checked="checked"{/if} /><label>メール</label>
			<input type="radio" name="media_type" value="3"{if $form_data.media_type == 3} checked="checked"{/if} /><label>その他</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">PV/日※（発行部数）</th>
		<td>
			<input type="text" size="30" name="page_view_day" value="{$form_data.page_view_day|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイト概要</th>
		<td>
			<textarea cols="70" rows="5" name="site_outline">{$form_data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">ID種別</th>
		<td>
			<input type="radio" name="response_type" value="0"{if $form_data.response_type == 0 || $form_data.response_type == ""} checked="checked"{/if} /><label>個体識別ID</label>
			<input type="radio" name="response_type" value="1"{if $form_data.response_type == 1} checked="checked"{/if} /><label>セッションID</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">ポイントバック通知先URL※</th>
		<td>
			<input type="text" size="100" name="point_back_url" value="{$form_data.point_back_url|htmlspecialchars:$smarty.const.ENT_QUOTES}" /><br />
			##ID##⇒「ID種別」で選択したID<br />
			##CID##⇒広告ID<br />
			##CLICK_DATE##⇒クリック日時<br />
			##ACTION_DATE##⇒成果発生日時<br /><br />
			＜ユーザ識別IDの付加について＞<br />
			ユーザ識別IDを付加する位置に「##ID##」を記入する<br />
			http://example.jp/?id=***→http://example.jp/?id=##ID##<br />
			http://example.jp/***→http://example.jp/##ID##<br />
			http://example.jp/?pid=***→http://example.jp/?pid=##ID##<br /><br />
{if $form_data.id != ""}
			<input type="text" size="100" name="point_test_url" value="{$form_data.point_test_url|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" /><br />
			<input type="button" name="test_url_make" value="テストURL発行" onclick="document.form1.point_test_url.value ='http://adbond.jp/action/point_test.php?guid=ON&m={$form_data.id}&a=0';" />
{/if}
		</td>
	</tr>
-->
<!-------------------------------------------------------------------------------------------------------------------------------------------------------->
	<tr>
		<td colspan="3">
			<br />
			<input type="submit" value="登録" style="background-color:#48D1CC; width:80px;" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
			<input type="reset" name="reset" id="reset" value="リセット" style="background-color:#48D1CC; width:80px;" />
		</td>
	</tr>
</table>
</form>

</div>
</div>
<!-- contents -->
{include file='./hooter_web.tpl'}
