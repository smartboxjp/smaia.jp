<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );

session_start();

if(isset($_SESSION['advert_logon_token']) && $_SESSION['advert_logon_token'] != ''){
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_login_user = new AdvertLoginUser();
	$advert_login_user = $_SESSION['advert_login_user'];

	$login_user_id = $advert_login_user->getid();
	$user_name = $advert_login_user->getUserName();
	$login_id = $advert_login_user->getLoginId();
	$login_pass = $advert_login_user->getLoginPass();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);
	$smarty->assign("advert_withdrawal_view_user", $_SESSION['advert_withdrawal_view_user'] );


	// ページを表示
	$smarty->display("./client_top.tpl");
}else{
	header('Location: ../index.php?error=1');
	exit();
}
?>