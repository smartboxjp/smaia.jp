<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );

require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );

// セッションスタート
session_start();

// ログイントークンがセットされているか
if(isset($_SESSION['advert_logon_token']) && $_SESSION['advert_logon_token'] != ''){
	// オブジェクトの生成
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_login_user = new AdvertLoginUser();
	// セッション変数の中身を変数へ格納
	$advert_login_user = $_SESSION['advert_login_user'];

	// 変数をセット
	$login_user_id = $advert_login_user->getid();
	$user_name = $advert_login_user->getUserName();
	$login_id = $advert_login_user->getLoginId();
	$login_pass = $advert_login_user->getLoginPass();

	// オブジェクトの生成
	$common_dao = new CommonDao();
	$advert_client_dao = new AdvertClientDao();
	$advert_client = new AdvertClient();
	// 広告主情報を取得
	$advert_client = $advert_client_dao->getAdvertClientByLoginUserId($login_user_id);
	// 広告主IDを取得
	$advert_client_id = $advert_client->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();
	// Smarty変数へユーザー名を格納
	$smarty->assign("user_name", $user_name);
	$smarty->assign("advert_withdrawal_view_user", $_SESSION['advert_withdrawal_view_user'] );

	// advertのSELECT文を発行
	$get_select_sql = " SELECT * FROM advert "
	. " WHERE "
	. " advert_client_id = '$advert_client_id' "
	. " AND "
	. " status = '2' "
	. " AND "
	. " deleted_at IS NULL "
	. " ORDER BY id ASC ";
	// クエリ実行
	$db_result = $common_dao->db_query($get_select_sql);
	$advert_array = $db_result;

	if(!$db_result) {
		echo "DBの接続に失敗しました。";
		exit();
	}

	// ループカウント
	$loop_count = 0;
	// 広告ID
	$advert_id = 0;
	// 今日の年月を取得
	$search_date = date("Y-m");
		// 英文形式の日付をタイムスタンプに変換し取得
		$a_date = strtotime($search_date);
		// 検索年を取得
		$search_year = date("Y", $a_date);
		// 検索月を取得
		$search_month = date("m", $a_date);

	// 検索フラグ
	$search_flag = false;


	// *************************************************************************
	// POST/GET 取得
	// *************************************************************************
	// 広告ID
	if(isset($_GET['advert_id']) && $_GET['advert_id'] != "") {
		$advert_id = $_GET['advert_id'];
	} elseif(isset($_POST['advert_id']) && $_POST['advert_id'] != "") {
		$advert_id = $_POST['advert_id'];
	}

	// 検索年
	if(isset($_GET['set_year']) && $_GET['set_year'] != "") {
		$search_year = $_GET['set_year'];
	} elseif(isset($_POST['set_year']) && $_POST['set_year'] != "") {
		$search_year = $_POST['set_year'];
	}

	// 検索月
	if(isset($_GET['set_month']) && $_GET['set_month'] != "") {
		$search_month = $_GET['set_month'];
	} elseif(isset($_POST['set_month']) && $_POST['set_month'] != "") {
		$search_month = $_POST['set_month'];
	}

	// 検索フラグ
	if(isset($_GET['search_flag']) && $_GET['search_flag'] != "") {
		$search_flag = $_GET['search_flag'];
	} elseif(isset($_POST['search_flag']) && $_POST['search_flag'] != "") {
		$search_flag = $_POST['search_flag'];
	}

	// 検索の年月を取得
	if($search_flag) {
		// 月の値が一桁の場合、頭に"0"を付与
		if($search_month < 10){
			(string)$search_month = "0" . (int)$search_month;
		}

		$search_date = date($search_year . "-" . $search_month);
	}


	// *************************************************************************
	// 広告全件表示
	// *************************************************************************
	// 広告IDが0の場合
	if($advert_id == "0") {

		// ループ
		foreach ($advert_array as $key) {
			// action_logsのSELECT文を発行
			$get_select_sql = " SELECT * FROM action_logs "
			. " WHERE "
			. " advert_id = '" . $advert_array[$loop_count]['id'] . "' "
			. " AND "
			. " action_complete_date like '" . $search_date . "%' "
			. " AND "
			. " status = '2' "
			. " AND "
			. " deleted_at IS NULL ";
			// クエリ実行
			$db_result = $common_dao->db_query($get_select_sql);
			$action_logs_array = $db_result;
			// 該当件数を変数に格納
			$advert_array[$loop_count]['action_couunt'] = count($action_logs_array);

			// withdrawal_logsのSELECT文を発行
			$get_select_sql = " SELECT * FROM  withdrawal_logs "
			. " WHERE "
			. " advert_id = '" . $advert_array[$loop_count]['id'] . "' "
			. " AND "
			. " created_at like '" . $search_date . "%' "
			. " AND "
			. " status = '2' "
			. " AND "
			. " deleted_at IS NULL ";
			// クエリ実行
			$db_result = $common_dao->db_query($get_select_sql);
			$withdrawal_logs_array = $db_result;
			// 該当件数を変数に格納
			$advert_array[$loop_count]['withdrawal_couunt'] = count($withdrawal_logs_array);

			if($advert_array[$loop_count]['withdrawal_couunt'] != 0 && $advert_array[$loop_count]['action_couunt'] != 0) {

				// 該当件数のパーセントを変数に格納 小数点第3位を四捨五入
				$advert_array[$loop_count]['percent'] = round($advert_array[$loop_count]['withdrawal_couunt'] / $advert_array[$loop_count]['action_couunt'], 3) * 100;

			}

			// ループカウント
			$loop_count += 1;

		}

	// *************************************************************************
	// 広告1件表示
	// *************************************************************************
	// 広告IDが0でない場合
	} elseif($advert_id != "0") {

		// action_logsのSELECT文を発行
		$get_select_sql = " SELECT "
		. " DATE_FORMAT(action_complete_date,'%Y-%m-%d') AS summary_day, "
		. " COUNT(advert_id) AS count_advert_id "
		. " FROM action_logs "
		. " WHERE "
		. " advert_id = '" . $advert_id . "' "
		. " AND "
		. " action_complete_date like '" . $search_date . "%' "
		. " AND "
		. " status = '2' "
		. " AND "
		. " deleted_at IS NULL "
		. " GROUP BY summary_day ";
		// クエリ実行
		$db_result = $common_dao->db_query($get_select_sql);
		$action_logs_array = $db_result;

		// withdrawal_logsのSELECT文を発行
		$get_select_sql = " SELECT "
		. " DATE_FORMAT(created_at,'%Y-%m-%d') AS summary_day, "
		. " COUNT(advert_id) AS count_advert_id "
		. " FROM  withdrawal_logs "
		. " WHERE "
		. " advert_id = '" . $advert_id . "' "
		. " AND "
		. " created_at like '" . $search_date . "%' "
		. " AND "
		. " status = '2' "
		. " AND "
		. " deleted_at IS NULL "
		. " GROUP BY summary_day ";;
		// クエリ実行
		$db_result = $common_dao->db_query($get_select_sql);
		$withdrawal_logs_array = $db_result;

		// advert_arrayを複製
		$advert_day_array = $advert_array;

		// 年月から月の最終日を取得
		$last_mun = date("t", mktime(0, 0, 0, $search_month, 1, $search_year));

		while ($loop_count < $last_mun) {

			// 日付を配列に格納 1日に設定
			$ac = date($search_year . $search_month . "01") + $loop_count;
				// 英文形式の日付をタイムスタンプに変換し取得
				$a_date = strtotime($ac);
				// ac日付の年を取得
				$a_year = date("Y", $a_date);
				// ac日付の月を取得
				$a_month = date("m", $a_date);
				// ac日付の日を取得
				$a_day = date("d", $a_date);

			$advert_day_array[$loop_count]['date'] = date($a_year . "-" . $a_month . "-" . $a_day);

			// アクション日付
			foreach ($action_logs_array as $key => $val) {

				if($val['summary_day'] == $advert_day_array[$loop_count]['date']) {
					$advert_day_array[$loop_count]['action_id_count'] = $val['count_advert_id'];
					break;
				} else {
					$advert_day_array[$loop_count]['action_id_count'] = 0;
				}

			}

			// 退会日付
			foreach ($withdrawal_logs_array as $key => $val) {

				if($val['summary_day'] == $advert_day_array[$loop_count]['date']) {
					$advert_day_array[$loop_count]['withdrawal_id_count'] = $val['count_advert_id'];
					break;
				} else {
					$advert_day_array[$loop_count]['withdrawal_id_count'] = 0;
				}

			}

			// アクション数の合計値を求める
			$advert_day_array[0]['action_id_count_total'] = $advert_day_array[0]['action_id_count_total'] + $advert_day_array[$loop_count]['action_id_count'];
			// 退会数の合計値を求める
			$advert_day_array[0]['withdrawal_id_count_total'] = $advert_day_array[0]['withdrawal_id_count_total'] + $advert_day_array[$loop_count]['withdrawal_id_count'];

			// ループカウント
			$loop_count += 1;

		}

		if($advert_day_array[0]['withdrawal_id_count_total'] != 0 && $advert_day_array[0]['action_id_count_total'] != 0) {
			// 該当件数のパーセントを変数に格納 小数点第3位を四捨五入
			$advert_day_array[0]['percent'] = round($advert_day_array[0]['withdrawal_id_count_total'] / $advert_day_array[0]['action_id_count_total'], 3) * 100;
		}

	}
	// *************************************************************************
	// Smarty変数へ格納
	// *************************************************************************
	// Smarty変数へadvert_arrayを格納
	$smarty->assign("advert_array", $advert_array);
	// Smarty変数へadvert_arrayを格納
	$smarty->assign("advert_day_array", $advert_day_array);

	// Smarty変数へadvert_idを格納
	$smarty->assign("advert_id", $advert_id);
	// Smarty変数へset_yearを格納
	$smarty->assign("search_year", $search_year);
	// Smarty変数へset_monthを格納
	$smarty->assign("search_month", $search_month);



	// ページを表示
	$smarty->display("./client_withdrawal_report.tpl");

}
?>