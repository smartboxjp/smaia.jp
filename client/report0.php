<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );

session_start();

if(isset($_SESSION['advert_logon_token']) && $_SESSION['advert_logon_token'] != ''){
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_login_user = new AdvertLoginUser();
	$advert_login_user = $_SESSION['advert_login_user'];

	$login_user_id = $advert_login_user->getid();
	$user_name = $advert_login_user->getUserName();
	$login_id = $advert_login_user->getLoginId();
	$login_pass = $advert_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$advert_client_dao = new AdvertClientDao();
	$advert_client = new AdvertClient();
	$advert_client = $advert_client_dao->getAdvertClientByLoginUserId($login_user_id);
	$advert_client_id = $advert_client->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	//広告一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM advert "
				. " WHERE deleted_at is NULL "
				. " AND advert_client_id = " . $advert_client->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("advert_array", $db_result);
	}else{
		//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	$advert_id = do_escape_quotes($_POST['advert_id']);
	$smarty->assign("advert_id", $advert_id);

	$date_type = (isset($_POST['date_type'])) ? do_escape_quotes($_POST['date_type']) : 1;
	$carrier = (isset($_POST['carrier'])) ? do_escape_quotes($_POST['carrier']) : 1;

	$now_date = getdate();
	$s_year = (isset($_POST['s_year'])) ? do_escape_quotes($_POST['s_year']) : $now_date['year'];
	$s_month = (isset($_POST['s_month'])) ? do_escape_quotes($_POST['s_month']) : $now_date['mon'];
	$s_day = (isset($_POST['s_day'])) ? do_escape_quotes($_POST['s_day']) : $now_date['mday'];

	$smarty->assign("date_type", $date_type);
	$smarty->assign("carrier", $carrier);

	$smarty->assign("set_s_year", $s_year);
	$smarty->assign("set_s_month", $s_month);
	$smarty->assign("set_s_day", $s_day);

	$common_dao = new CommonDao();

	$sql = " SELECT ";

	if($date_type == 1) {
		$sql .= " IF(status = 2, DATE_FORMAT(action_complete_date,'%Y年%m月'), DATE_FORMAT(created_at,'%Y年%m月')) as summary_month, ";
	} elseif($date_type == 2) {
		$sql .= " IF(status = 2, DATE_FORMAT(action_complete_date,'%Y年%m月%d日'), DATE_FORMAT(created_at,'%Y年%m月%d日')) as summary_month, ";
	}

	if($carrier == 2) {
		$sql .= " carrier_id, ";
	}

	$sql .= " SUM(click_price_client) as click_price_client, "
			. " SUM(IF(status = 2, action_price_client, NULL)) as action_price_client, "
			. " COUNT(status) as click_count, "
			. " COUNT(IF(status = 2, status, NULL)) as action_count "
			. " FROM action_logs "
			. " WHERE deleted_at is NULL "
			. " AND advert_client_id = '$advert_client_id' "
			. " AND (status = 1 OR status = 2) ";

	if($advert_id != "") {
		$sql .= " AND advert_id = '$advert_id' ";
	}

	if($date_type == 2) {
		$sql .= " AND ( "
				. " (status = 1 AND DATE_FORMAT(created_at,'%Y%c') = '$s_year$s_month') "
				. " OR "
				. " (status = 2 AND DATE_FORMAT(action_complete_date,'%Y%c') = '$s_year$s_month') "
				. " ) ";
	}

	if($carrier == 1) {
		$sql .= " GROUP BY summary_month ";
	} elseif($carrier == 2) {
		$sql .= " GROUP BY summary_month, carrier_id ";
	}

	$sql .= " ORDER BY created_at DESC ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){

		foreach($db_result as $key => $val) {
			$date = $val['summary_month'];
			$count[$key] = $val["summary_month"];

			if($val['carrier_id'] == 1) {
				$c_id = "docomo";
			} elseif($val['carrier_id'] == 2) {
				$c_id = "softbank";
			} elseif($val['carrier_id'] == 3) {
				$c_id = "au";
			} elseif($val['carrier_id'] == 4) {
				$c_id = "pc";
			}

			if($carrier == 1) {

				$summary[$date]['summary_date'] = $val['summary_month'];
				$summary[$date]['click_count'] = $val['click_count'];
				$summary[$date]['click_price'] = $val['click_price_client'];
				$summary[$date]['action_count'] = $val['action_count'];
				$summary[$date]['action_price'] = $val['action_price_client'];
				$summary[$date]['total_price'] = $val['click_price_client'] + $val['action_price_client'];

			} elseif($carrier == 2) {

				$summary[$date]['summary_date'] = $val['summary_month'];
				$summary[$date][$c_id]['click_count'] = $val['click_count'];
				$summary[$date][$c_id]['click_price'] = $val['click_price_client'];
				$summary[$date][$c_id]['action_count'] = $val['action_count'];
				$summary[$date][$c_id]['action_price'] = $val['action_price_client'];
				$summary[$date][$c_id]['total_price'] = $val['click_price_client'] + $val['action_price_client'];

			}

			$all['click_count'] += $val['click_count'];
			$all['click_price'] += $val['click_price_client'];
			$all['action_count'] += $val['action_count'];
			$all['action_price'] += $val['action_price_client'];
			$all['total_price'] += $val['click_price_client'] + $val['action_price_client'];
		}

		array_multisort($count, SORT_DESC, $summary);

		$smarty->assign("summary", $summary);
		$smarty->assign("all", $all);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./client_report.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>