<?php
require_once ("ini.php");
//require_once ("emoji.php");

//require ("nusoap/lib/nusoap.php");

// 全てのエラー出力をオフにする
//error_reporting(0);


//=======================================================================
// 関数名：Caarrire Check (check_carrire)
// 引数  ：なし
// 戻り値：キャリア文字列
// 作成日：2010/03/02
//=======================================================================
function check_carrier() {
	//$arr_docomo_ip = array("210.136.161.", "210.153.84.", "210.153.86.", "124.146.174.", "124.146.175.");

	// au
	if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {
		return 'ezweb';

	// softbank
	} elseif (isset($_SERVER['HTTP_X_JPHONE_UID'])) {
		return 'softbank';

	// docomo
	} elseif (ereg("^J-PHONE|^Vodafone|^SoftBank", $_SERVER['HTTP_USER_AGENT'])) {
		return 'softbank';
	} elseif (ereg("^DoCoMo", $_SERVER['HTTP_USER_AGENT'])) {
		if (ereg("docomo.ne.jp", gethostbyaddr($_SERVER["REMOTE_ADDR"]))) {
			return 'docomo';
		}
		return;

	// e-mobile
	} elseif (isset($_SERVER['HTTP_X_EM_UID'])) {
		return 'emobile';

		if ($_SERVER['HTTP_X_EM_UID'] == "") {
			header("Location:" . SVR_DOM . "sb_uid.html");
			exit;
		}
	} else {
		return;
	}
}


//=======================================================================
//【機能】認証チェック
//
//【引数】arg [I] 非会員のとき登録ページを表示させるか
//
//【返値】EzWeb：認証後トランザクションID
//        SoftBank：UID
//
//【メモ】
//=======================================================================
function check_member($bln_ng_regview) {

	// キャリアチェック
	$carrier = check_carrier();

	// 端末ID 取得
	$sub_no = get_subno();

	// NG URL 設定
	if ($bln_ng_regview === true) {
		$ng_url = SVR_DOM . REG_PAGE;
	} else {
		$ng_url = SVR_DOM . substr($_SERVER['SCRIPT_NAME'], 1) . $param;
	}

	// 登録ユーザー検索
	$common_dao = new CommonDao();

	$select_sql = " select * from users where uid = '$sub_no' and deleted_at is NULL ";

	$db_result = $common_dao->db_query($select_sql);

	// 保存したトランザクションID が存在しない場合エラーページへ
	if($db_result[0] != ''){
		// 会員
		return $sub_no;
	}else{
		// 非会員

		// NG Pageへリダイレクトする
		if ($bln_ng_regview === true) {
			$ng_url = SVR_DOM . REG_PAGE;
			header("Location:" . $ng_url);
			exit;

		// NG Pageへリダイレクトしない
		} else {
			return false;
		}
	}

}

//=======================================================================
//【機能】端末ID 取得
//
//【引数】なし
//
//【返値】端末ID
//
//【メモ】
//=======================================================================
//function get_subno() {
//
//	if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {
//		$subno = $_SERVER['HTTP_X_UP_SUBNO'];
//
//	} else if (isset($_SERVER['HTTP_X_JPHONE_UID'])) {
//		$subno = substr($_SERVER['HTTP_X_JPHONE_UID'],1);
//
//	} else if (isset($_SERVER['HTTP_X_DCMGUID'])) {
//		$subno = $_SERVER['HTTP_X_DCMGUID'];
//
//	} else if ($_POST['uid'] != "NULLGWDOCOMO") {
//		if($_GET['uid'] != "NULLGWDOCOMO"){
//			$subno = $_GET['uid'];
//		}else{
//			$subno = $_POST['uid'];
//		}
//
//	}
//
//	if ($subno == "") {
//		header("Location:" . SVR_DOM . "sb_uid.html");
//		exit;
//
//	} else {
//		return $subno;
//	}
//}
function get_subno() {

	if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {
		$subno = $_SERVER['HTTP_X_UP_SUBNO'];

	} else if (isset($_SERVER['HTTP_X_JPHONE_UID'])) {
		$subno = substr($_SERVER['HTTP_X_JPHONE_UID'],1);

	} else if ($_GET['uid'] != "NULLGWDOCOMO") {
			$subno = $_GET['uid'];

	} else if ($_POST['uid'] != "NULLGWDOCOMO") {
		$subno = $_POST['uid'];

	} else if (isset($_SERVER['HTTP_X_DCMGUID']) && $_SERVER['HTTP_X_DCMGUID'] != '') {
		$subno = $_SERVER['HTTP_X_DCMGUID'];

	}

	if ($subno == "") {
		//header("Location:" . SVR_DOM . "sb_uid.html");
		//exit;

	} else {
		return $subno;
	}
}

//=======================================================================
//【機能】会員登録
//
//【引数】なし
//
//【返値】
//
//【メモ】
//=======================================================================
function reg_member() {

	// キャリアチェック
	$carrier = check_carrier();

	// ASP 経由で登録の場合登録コースを取得
	//$course = get_asp_course($conn, $carrier);
	$course = "";

	// 端末ID 取得
	$subno = get_subno();

	// 現時点でどのコースに入っているかチェック
	// DB 接続
	$common_dao = new CommonDao();

	$select_sql = " select course_id from join_logs "
				. " where uid = '$subno' and withdrawal_date is NULL "
				. " and deleted_at is NULL ";

	$g300_flag = 0;
	$g500_flag = 0;
	$g1000_flag = 0;

	$db_result = $common_dao->db_query($select_sql);
	if($db_result){
		foreach ($db_result as $val){
			$row = array();
			$row = $val;

			if($row['course_id'] == 1){
				$g300_flag = 1;
			}elseif($row['course_id'] == 2){
				$g500_flag = 1;
			}elseif($row['course_id'] == 3){
				$g1000_flag = 1;
			}
		}
	}

	switch ($carrier) {
	case "docomo":
		$reg_data = array();
		$reg_data[0] = array('hidden_name' => 'ci', 'val' => DOCOMO_CI);
		$reg_data[1] = array('hidden_name' => 'nl', 'val' => DOCOMO_NL);
		$reg_data[2] = array('hidden_name' => 'rl', 'val' => DOCOMO_RL);
		return $reg_data;
		break;

	case "softbank":

		// 各登録コースのURL
		if($g300_flag != 1){
			$set_url300  = "confon://?uid=1&sid=" . SB_SID_G300;
			$set_url300 .= "&nl=" . SVR_DOM . "sb_mem_reg.php?course=300";
			$set_url300 .= "&cl=" . SVR_DOM . "cancel.html";
		}else{
			$set_url300  = "";
		}

		if($g500_flag != 1){
			$set_url500  = "confon://?uid=1&sid=" . SB_SID_G500;
			$set_url500 .= "&nl=" . SVR_DOM . "sb_mem_reg.php?course=500";
			$set_url500 .= "&cl=" . SVR_DOM . "cancel.html";
		}else{
			$set_url500  = "";
		}

		if($g1000_flag != 1){
			$set_url1000  = "confon://?uid=1&sid=" . SB_SID_G1000;
			$set_url1000 .= "&nl=" . SVR_DOM . "sb_mem_reg.php?course=1000";
			$set_url1000 .= "&cl=" . SVR_DOM . "cancel.html";
		}else{
			$set_url1000  = "";
		}

		if ($course == "") {
			$url_array = array("url300" => $set_url300, "url500" => $set_url500, "url1000" => $set_url1000);

		} else {
			// アフィリ経由で登録する場合、指定されたコースのみ登録リンクを表示
			//$smarty->assign('url' . $course , ${"set_url" . $course});
		}
		return $url_array;
		break;

	case "ezweb":
		// 各登録コースのURL
		if($g300_flag != 1){
			$set_300[0] = array('url'         => 'http://auth.collect.kddi.com/mob/KSReq');
			$set_300[1] = array('hidden_name' => 'cp_cd'    , 'val' => EZ_CP);
			$set_300[2] = array('hidden_name' => 'cp_srv_cd', 'val' => EZ_SRV);
			$set_300[3] = array('hidden_name' => 'item_cd'  , 'val' => ITEM_CD_G3);
			$set_300[4] = array('hidden_name' => 'odr_sts'  , 'val' => '3');
			$set_300[5] = array('hidden_name' => 'ok_url'   , 'val' => SVR_DOM . "ez_mem_reg.php?course=300");
			$set_300[6] = array('hidden_name' => 'ng_url'   , 'val' => SVR_DOM . "cancel.html");
		}else{
			$set_300 = array();
		}

		if($g500_flag != 1){
			$set_500[0] = array('url'         => 'http://auth.collect.kddi.com/mob/KSReq');
			$set_500[1] = array('hidden_name' => 'cp_cd'    , 'val' => EZ_CP);
			$set_500[2] = array('hidden_name' => 'cp_srv_cd', 'val' => EZ_SRV);
			$set_500[3] = array('hidden_name' => 'item_cd'  , 'val' => ITEM_CD_G5);
			$set_500[4] = array('hidden_name' => 'odr_sts'  , 'val' => '3');
			$set_500[5] = array('hidden_name' => 'ok_url'   , 'val' => SVR_DOM . "ez_mem_reg.php?course=500");
			$set_500[6] = array('hidden_name' => 'ng_url'   , 'val' => SVR_DOM . "cancel.html");
		}else{
			$set_500 = array();
		}

		if($g1000_flag != 1){
			$set_1000[0] = array('url'         => 'http://auth.collect.kddi.com/mob/KSReq');
			$set_1000[1] = array('hidden_name' => 'cp_cd'    , 'val' => EZ_CP);
			$set_1000[2] = array('hidden_name' => 'cp_srv_cd', 'val' => EZ_SRV);
			$set_1000[3] = array('hidden_name' => 'item_cd'  , 'val' => ITEM_CD_G10);
			$set_1000[4] = array('hidden_name' => 'odr_sts'  , 'val' => '3');
			$set_1000[5] = array('hidden_name' => 'ok_url'   , 'val' => SVR_DOM . "ez_mem_reg.php?course=1000");
			$set_1000[6] = array('hidden_name' => 'ng_url'   , 'val' => SVR_DOM . "cancel.html");
		}else{
			$set_1000 = array();
		}

		if ($course == "") {
			$url_array = array("url300" => $set_300, "url500" => $set_500, "url1000" => $set_1000);

		} else {

			// アフィリ経由で登録する場合、指定されたコースのみ登録リンクを表示
			//$smarty->assign('url' . $course , ${"url" . $course});
		}
		return $url_array;
		break;

	default:
		return;
		break;
	}

}

//=======================================================================
//【機能】退会処理
//
//【引数】なし
//
//【返値】
//
//【メモ】
//=======================================================================
function leave_member() {

	// キャリアチェック
	$carrier = check_carrier();

	// ASP 経由で登録の場合登録コースを取得
	//$course = get_asp_course($conn, $carrier);
	$course = "";

	// 端末ID 取得
	$subno = get_subno();

	// 現時点でどのコースに入っているかチェック
	// DB 接続
	$common_dao = new CommonDao();

	$select_sql = " select course_id from join_logs "
				. " where uid = '$subno' and withdrawal_date is NULL "
				. " and deleted_at is NULL ";

	$g300_flag = 0;
	$g500_flag = 0;
	$g1000_flag = 0;

	$db_result = $common_dao->db_query($select_sql);
	if($db_result){
		foreach ($db_result as $val){
			$row = array();
			$row = $val;

			if($row['course_id'] == 1){
				$g300_flag = 1;
			}elseif($row['course_id'] == 2){
				$g500_flag = 1;
			}elseif($row['course_id'] == 3){
				$g1000_flag = 1;
			}
		}
	}


	switch ($carrier) {
	case "docomo":
		$leave_data = array();
		$leave_data[0] = array('hidden_name' => 'ci', 'val' => DOCOMO_CI);
		$leave_data[1] = array('hidden_name' => 'nl', 'val' => DOCOMO_LEAVE_NL);
		$leave_data[2] = array('hidden_name' => 'rl', 'val' => DOCOMO_LEAVE_RL);
		return $leave_data;
		break;

	case "softbank":

		// 各退会コースのURL
		if($g300_flag == 1){
			$set_url300  = "confoff://?uid=1&sid=" . SB_SID_G300;
			$set_url300 .= "&nl=" . SVR_DOM . "sb_mem_leave.php?course=300";
			$set_url300 .= "&cl=" . SVR_DOM . "cancel.html";
		}else{
			$set_url300  = "";
		}

		if($g500_flag == 1){
			$set_url500  = "confoff://?uid=1&sid=" . SB_SID_G500;
			$set_url500 .= "&nl=" . SVR_DOM . "sb_mem_leave.php?course=500";
			$set_url500 .= "&cl=" . SVR_DOM . "cancel.html";
		}else{
			$set_url500  = "";
		}

		if($g1000_flag == 1){
			$set_url1000  = "confoff://?uid=1&sid=" . SB_SID_G1000;
			$set_url1000 .= "&nl=" . SVR_DOM . "sb_mem_leave.php?course=1000";
			$set_url1000 .= "&cl=" . SVR_DOM . "cancel.html";
		}else{
			$set_url1000  = "";
		}

		if ($course == "") {
			$url_array = array("url300" => $set_url300, "url500" => $set_url500, "url1000" => $set_url1000);

		} else {
			// アフィリ経由で登録する場合、指定されたコースのみ登録リンクを表示
			//$smarty->assign('url' . $course , ${"set_url" . $course});
		}
		return $url_array;
		break;

	case "ezweb":
		if($g300_flag == 1){
			$set_300[0] = array('url'         => 'http://auth.collect.kddi.com/mob/KKReq');
			$set_300[1] = array('hidden_name' => 'cp_cd'    , 'val' => EZ_CP);
			$set_300[2] = array('hidden_name' => 'cp_srv_cd', 'val' => EZ_SRV);
			$set_300[3] = array('hidden_name' => 'item_cd'  , 'val' => ITEM_CD_G3);
			$set_300[5] = array('hidden_name' => 'ok_url'   , 'val' => SVR_DOM . "ez_mem_leave.php?course=300");
			$set_300[6] = array('hidden_name' => 'ng_url'   , 'val' => SVR_DOM . "cancel.html");
		}else{
			$set_300 = array();
		}

		if($g500_flag == 1){
			$set_500[0] = array('url'         => 'http://auth.collect.kddi.com/mob/KKReq');
			$set_500[1] = array('hidden_name' => 'cp_cd'    , 'val' => EZ_CP);
			$set_500[2] = array('hidden_name' => 'cp_srv_cd', 'val' => EZ_SRV);
			$set_500[3] = array('hidden_name' => 'item_cd'  , 'val' => ITEM_CD_G5);
			$set_500[5] = array('hidden_name' => 'ok_url'   , 'val' => SVR_DOM . "ez_mem_leave.php?course=500");
			$set_500[6] = array('hidden_name' => 'ng_url'   , 'val' => SVR_DOM . "cancel.html");
		}else{
			$set_500 = array();
		}

		if($g1000_flag == 1){
			$set_1000[0] = array('url'         => 'http://auth.collect.kddi.com/mob/KKReq');
			$set_1000[1] = array('hidden_name' => 'cp_cd'    , 'val' => EZ_CP);
			$set_1000[2] = array('hidden_name' => 'cp_srv_cd', 'val' => EZ_SRV);
			$set_1000[3] = array('hidden_name' => 'item_cd'  , 'val' => ITEM_CD_G10);
			$set_1000[5] = array('hidden_name' => 'ok_url'   , 'val' => SVR_DOM . "ez_mem_leave.php?course=1000");
			$set_1000[6] = array('hidden_name' => 'ng_url'   , 'val' => SVR_DOM . "cancel.html");
		}else{
			$set_1000 = array();
		}

		if ($course == "") {
			$url_array = array("url300" => $set_300, "url500" => $set_500, "url1000" => $set_1000);

		} else {
			// アフィリ経由で登録する場合、指定されたコースのみ登録リンクを表示
			//$smarty->assign('url' . $course , ${"set_url" . $course});

		}
		return $url_array;
		break;

	default:
		break;
	}

}

//=======================================================================
//【機能】単一課金認証開始処理(DoCoMo)
//
//【引数】
//
//【返値】
//
//【メモ】
//=======================================================================
function do_add_point() {

	// キャリアチェック
	$carrier = check_carrier();

	switch ($carrier) {
	case "docomo":
		break;

	case "softbank":
		break;

	case "ezweb":
		break;

	default:
		header("Location: " . SVR_DOM);
	}

}

//=======================================================================
//【機能】単一課金認証開始処理(SB)
//
//【引数】
//
//【返値】SoftBank：UID
//
//【メモ】
//=======================================================================
function sb_add_point() {

	// キャリアチェック
	$carrier = check_carrier();

	switch ($carrier) {
	case "docomo":
		break;

	case "softbank":

		// 各登録コースのURL
		$set_url300  = "confon://?uid=1&sid=" . SB_SID_J300;
		$set_url300 .= "&nl=" . SVR_DOM . "sb_mem_add.php?course=300";
		$set_url300 .= "&cl=" . SVR_DOM . "cancel.html";

		$set_url500  = "confon://?uid=1&sid=" . SB_SID_J500;
		$set_url500 .= "&nl=" . SVR_DOM . "sb_mem_add.php?course=500";
		$set_url500 .= "&cl=" . SVR_DOM . "cancel.html";

		$set_url1000  = "confon://?uid=1&sid=" . SB_SID_J1000;
		$set_url1000 .= "&nl=" . SVR_DOM . "sb_mem_add.php?course=1000";
		$set_url1000 .= "&cl=" . SVR_DOM . "cancel.html";

		$url_array = array("url300" => $set_url300, "url500" => $set_url500, "url1000" => $set_url1000);

		return $url_array;
		break;

	case "ezweb":
		break;

	default:
		header("Location: " . SVR_DOM);
	}

}

//=======================================================================
//【機能】単一課金認証開始処理(au)
//
//【引数】arg [I]
//
//【返値】EzWeb：認証後トランザクションID
//        SoftBank：UID
//
//【メモ】
//=======================================================================
function ez_add_point($item_cd) {

	// キャリアチェック
	$carrier = check_carrier();

	switch ($carrier) {
	case "docomo":
		break;

	case "softbank":
		break;

	case "ezweb":

		// DB 接続
		$common_dao = new CommonDao();

		if (isset($_GET["tran_id"])) {
			// ***************************************************
			// 認証後処理
			// ***************************************************

			$select_sql = " select * from trans_data "
						. " where uid = '" . $_SERVER['HTTP_X_UP_SUBNO'] . "'"
						. " and tran_id = '" . $_GET["tran_id"] . "'";

			$db_result = $common_dao->db_query($select_sql);

			// 保存したトランザクションID が存在しない場合エラーページへ
			if($db_result){
				header("Location:" . SVR_DOM . "err.html");
				exit();
			}

			return $_GET["tran_id"];

		} else {

			// 要求元のパラメータを文字列にしてセット
			foreach ($_GET as $key => $val) {
				$param .= "&" . $key . "=" . $val;
			}

			foreach ($_POST as $key => $val) {
				$param .= "&" . $key . "=" . $val;
			}

			if ($param != "") {
				$param = "?" . substr($param, 1);
			}

			// 要求パラメータセット
			$kparam[0] = array (
					'cp_cd'     => EZ_CP,
					'cp_srv_cd' => EZ_SRV,
					'item_cd'   => $item_cd,	// 商品コード
					'odr_sts'   => '1',			// 購入要求ステータス
					'ok_url'    => SVR_DOM . substr($_SERVER['SCRIPT_NAME'], 1) . $param,
					'ng_url'    => SVR_DOM . "cancel.html",
				);

			// SOAP 通信（KNPrevReqN：単一課金認証要求処理）
			$ws = new soapclient(SVR_DOM . 'wsdl_1.2/real/KNPrevReqN.wsdl', true);
			$proxy = $ws->getProxy();
			$resp = $proxy->call("trx_KNPrevReqN", $kparam);

			$rslt_cd = $resp['rslt_cd'];				// 応答結果格納
			$rsn_cd  = $resp['rsn_cd'];
			$tran_id = $resp['tran_id'];

			// 結果コードの評価
			if ($rslt_cd == '00') {
				// トランザクションID を保存

				$select_sql = " select * from trans_data "
							. " where uid = '" . $_SERVER['HTTP_X_UP_SUBNO'] . "'";

				$db_result = $common_dao->db_query($select_sql);

				if(!$db_result){
					// 非会員で都度購入の人
					$update_sql = " insert into trans_data values ( "
							. "'" . $_SERVER['HTTP_X_UP_SUBNO'] . "', '" . $tran_id . "')";
				}else{
					// 会員で都度購入の人
					$update_sql = " update trans_data set "
								. " tran_id = '" . $tran_id . "' "
								. " WHERE uid = '" . $_SERVER['HTTP_X_UP_SUBNO'] . "'";
				}
				$db_update = $common_dao->db_update($update_sql);
				if($db_update == 1){
					// UPDATE 成功
				}else{
					// UPDATE 失敗
					unset($proxy);
					header("Location: " . SVR_DOM . "err.html");
					exit();
				}

				// 認証要求処理
				$url = "http://auth.collect.kddi.com/mob/KNReq?cp_cd=" . EZ_CP . "&tran_id=" . $tran_id;

				header("Location: " . $url);

			} else {
				// エラー
				unset($proxy);
				//echo "result coce ->" . $rslt_cd;
				header("Location: " . SVR_DOM . "err.html");
				exit();
			}

			unset($proxy);
		}
		break;

	default:
		header("Location: " . SVR_DOM);
	}

}


//=======================================================================
//【機能】確定要求処理
//
//【引数】arg [I]
//
//【返値】
//
//【メモ】
//=======================================================================
function commit_charge() {

	// 要求パラメータセット
	$kparam[0] = array (
			'cp_cd'     => EZ_CP,
			'tran_id'   => $_GET["tran_id"],
			'odr_sts'   => '5',			// 事後課金確定
			'commit_limit_unit' => '',
			'commit_limit'      => 0,
		);

	// SOAP 通信（KNPrevReqN：単一課金認証要求処理）
	$ws = new soapclient(SVR_DOM . '/wsdl_1.2/real/KTReq.wsdl', true);
	$proxy = $ws->getProxy();
	$resp = $proxy->call("trx_KTReq", $kparam);

	$rslt_cd = $resp['rslt_cd'];				// 応答結果格納
	$rsn_cd  = $resp['rsn_cd'];

	// 結果コードの評価
	if ($rslt_cd == '00') {

	} else {
		// エラー
		//echo "result coce ->" . $rslt_cd;
		unset($proxy);
		header("Location: " . SVR_DOM . "err.html");
		exit();
	}

	unset($proxy);
}

/*
//=======================================================================
//【機能】トランザクションID を登録
//
//【引数】なし
//
//【返値】
//
//【メモ】
//=======================================================================
function reg_trans() {

	// キャリアチェック
	$carrier = check_carrier();

	switch ($carrier) {
	case "docomo":
		break;

	case "softbank":
		break;

	case "ezweb":
		// トランザクションID を保存
		// DB 接続
		$common_dao = new CommonDao();

		$update_sql = "DELETE FROM trans_data WHERE uid = '" . $_SERVER['HTTP_X_UP_SUBNO'] . "'";

		$db_update = $common_dao->db_update($update_sql);
		if($db_update == 1){
			// UPDATE 成功
		}else{
			// UPDATE 失敗
		}

		$update_sql = "INSERT INTO trans_data (uid, tran_id) VALUES ('"
				 . $_SERVER['HTTP_X_UP_SUBNO'] . "', '" . $_GET['tran_id'] . "')";

		$db_update = $common_dao->db_update($update_sql);
		if($db_update == 1){
			// UPDATE 成功
		}else{
			// UPDATE 失敗
		}

		break;

	default:
		// トランザクションID を保存
		// DB 接続
		$common_dao = new CommonDao();

		$update_sql = "DELETE FROM trans_data WHERE uid = '" . $_SERVER['HTTP_X_UP_SUBNO'] . "'";

		$db_update = $common_dao->db_update($update_sql);
		if($db_update == 1){
			// UPDATE 成功
		}else{
			// UPDATE 失敗
		}

		$update_sql = "INSERT INTO trans_data (uid, tran_id) VALUES ('"
					 . $_SERVER['HTTP_X_UP_SUBNO'] . "', '" . $_GET['tran_id'] . "')";

		$db_update = $common_dao->db_update($update_sql);
		if($db_update == 1){
			// UPDATE 成功
		}else{
			// UPDATE 失敗
		}

		break;
	}

}
*/
/*
//=======================================================================
//【機能】会員登録後の必要パラメータを書き出し
//
//【引数】なし
//
//【返値】
//
//【メモ】
//=======================================================================
function set_certification_param() {

	// キャリアチェック
	$carrier = check_carrier();

	switch ($carrier) {
	case "docomo":
		break;

	case "softbank":
		break;

	case "ezweb":
		echo "<input type='hidden' name='tran_id' value='" . $_GET['tran_id'] . "'>\n";
		echo "<input type='hidden' name='rslt_cd' value='" . $_GET['rslt_cd'] . "'>\n";
		break;

	default:
		echo "<input type='hidden' name='tran_id' value='" . $_GET['tran_id'] . "'>\n";
		echo "<input type='hidden' name='rslt_cd' value='" . $_GET['rslt_cd'] . "'>\n";
		break;
	}
}
*/
//=======================================================================
//【機能】対応機種チェック
//
//【引数】キャリアID carrier_id（docomo：1／au：2／softbank：3）
//
//【返値】対応機種：true／非対応機種：false
//
//【メモ】
//=======================================================================
function check_mobile($carrier_id) {

	// UA String
	$ua_string = $_SERVER["HTTP_USER_AGENT"];

	// 対応機種情報を取得
	$select_sql = " select pattern_name from compliant_models "
				. " where carrire_id = '$carrier_id' and deleted_at is NULL ";

	$common_dao = new CommonDao();

	$db_result = $common_dao->db_query($select_sql);
	if($db_result){
		$check_falg = true;

		foreach ($db_result as $val){
			$row = array();
			$row = $val;

			if(strpos($ua_string, $row['pattern_name']) > 0){
				$check_falg = false;
				return true;
			}
		}

		if($check_falg){
			return false;
		}
	}else{
		return false;
	}

	exit;
}

//=======================================================================
//【機能】対応機種モデルID取得
//
//【引数】キャリアID carrier_id（docomo：1／au：2／softbank：3）
//
//【返値】対応機種：true／非対応機種：false
//
//【メモ】
//=======================================================================
function get_model_id($carrier_id) {

	// UA String
	$ua_string = $_SERVER["HTTP_USER_AGENT"];

	// 対応機種情報を取得
	$select_sql = " select id, pattern_name from compliant_models "
				. " where carrire_id = '$carrier_id' and deleted_at is NULL ";

	$common_dao = new CommonDao();

	$db_result = $common_dao->db_query($select_sql);
	if($db_result){
		$check_falg = true;

		foreach ($db_result as $val){
			$row = array();
			$row = $val;

			if(strpos($ua_string, $row['pattern_name']) > 0){
				$check_falg = false;

				return $row['id'];
			}
		}

		if($check_falg){
			return null;
		}
	}else{
		return null;
	}

	exit;
}

//=======================================================================
// 指定した文字間を抜き出し
//=======================================================================
function getStrValue($strBase, $strKey1, $strKey2, $intKeyLen1=0, $intKeyLen2=0) {

	if (strpos($strBase, $strKey1) == 0) {
		return "";
	}

	if ($intKeyLen1 > 0) {
		$intLength1 = $intKeyLen1;
	} else {
		$intLength1 = strlen($strKey1) - 1;
    }

	if ($intKeyLen2 > 0) {
		$intLength2 = $intKeyLen2;
	} else {
		$intLength2 = 1;
    }

	$tmp_val = substr($strBase, strpos($strBase, $strKey1) + $intLength1);
	$tmp_val = substr($tmp_val, 1, strpos($tmp_val, $strKey2) - $intLength2);
	return $tmp_val;
}

?>