<?php
	// 基本設定
//	define("SVR_DOM"    , "http://localhost/web_app/mobile_cms_02/");	// ドメイン名
//	define("SVR_DOM"    , "http://m-torakentei.jp/");	// ドメイン名
	define("DOC_ROOT"   , "/public_html/");				// ドキュメントルート
	define("ERR_PAGE"   , "err.html");						// エラーページ
	//define("REG_PAGE"   , "reg.php");						// 登録ページ
	define("TOP_MEM"    , "index.php");						// トップページ
	//define("ORI_MEM_REG", "reg2.php");						// 独自会員管理登録用ページ

//	define("C_NAME"      , "(株)廣済堂");					// 運営者
//	define("SITE_NAME"   , "阪神タイガース検定");			// サイト名
//	define("SUPPORT_MAIL", "support@m-torakeintai.jp");		// サポートメール
//	define("SUPPORT_TEL" , "0120-888482");					// サポート電話
//	define("SUPPORT_TEL_2" , "078-393-5123");				// サポート電話2

	define("ASP_ID" , "1");				// ASP ID
	define("SITE_ID" , "1");			// SITE ID

	// docomo 設定
//	define("DOCOMO_CI"      , "00006005314");
//	define("DOCOMO_NL"      , "http://m-torakentei.jp/do_mem_reg.php%3Fuid%3DNULLGWDOCOMO");
//	define("DOCOMO_RL"      , "m-torakentei.jp/docomo/regist.php");
//	define("DOCOMO_LEAVE_NL", "http://m-torakentei.jp/do_mem_leave.php?uid=NULLGWDOCOMO");
//	define("DOCOMO_LEAVE_RL", "m-torakentei.jp/docomo/leave.php");

	// ez 設定
//	define("EZ_CP"  , "02653");			// Ez CP ID
//	define("EZ_SRV" , "86001");			// Ez サービスコード
//	define("ITEM_CD_G3",  "G0300");		// 商品コード 月額300
//	define("ITEM_CD_G5",  "G0500");		// 商品コード 月額500
//	define("ITEM_CD_G10", "G1000");		// 商品コード 月額1000
//	define("ITEM_CD_T3",  "T0300");		// 商品コード 従量300
//	define("ITEM_CD_T5",  "T0500");		// 商品コード 従量500
//	define("ITEM_CD_T10", "T1000");		// 商品コード 従量1000

	// SoftBank 設定
	//define("SB_SID", "");		// 課金ID
//	define("SB_PID",      "PV47");			// PID
//	define("SB_SID_G300", "EDSM");		// 課金ID 月額300
//	define("SB_SID_G500", "EDSN");		// 課金ID 月額500
//	define("SB_SID_G1000", "EDSO");		// 課金ID 月額1000
//	define("SB_SID_J300", "EDSP");		// 課金ID 従量300
//	define("SB_SID_J500", "EDSQ");		// 課金ID 従量500
//	define("SB_SID_J1000", "EDSR");		// 課金ID 従量1000

	// DB 設定
	//define("DB_HOST", 'localhost');
	//define("DB_USER", 'root');
	//define("DB_PASS", '');
	//define("DB_NAME", 'mobile_cms_02');
?>