<?php
define('SMARTY_DIR', 'Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');

require_once( './common/CommonDao.php' );
require_once( './dao/MediaLoginUserDao.php' );
require_once( './dto/MediaLoginUser.php' );
require_once( './dao/AdvertLoginUserDao.php' );
require_once( './dto/AdvertLoginUser.php' );

session_start();
session_regenerate_id(true);

// Smartyオブジェクト取得
$smarty = new Smarty();
$smarty->template_dir = './templates/web/';
$smarty->compile_id   = 'web';
$smarty->compile_dir  = './templates_c/';
$smarty->config_dir   = './config/';
$smarty->cache_dir    = './cache/';


if($_POST['media_logon'] == 'yy'){
	$common_dao = new CommonDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user_dao = new MediaLoginUserDao();

	$login_id = $common_dao->db_string_escape($_POST['login_id']);
	$login_pass = $common_dao->db_string_escape($_POST['login_pass']);

	$media_login_user = $media_login_user_dao->getMediaLoginUserByIdPass($login_id, $login_pass);

	if($media_login_user){
		$_SESSION['media_login_user'] = $media_login_user;
		$_SESSION['media_logon_token'] = md5(uniqid(mt_rand(), TRUE));

		$smarty->assign("info_message", "ログオンしました。");


		// **********************************************************************
		// テスト用付け足し処理 ※後で消す
		if($login_id == "aaaaa" && $login_pass == "admin"){
			header('Location: ./media/top_test.php');
			exit();
		}
		// **********************************************************************


		// ページを表示
		header('Location: ./media/top.php');
		exit();
	}else{
		$smarty->assign("error_message", "ログインIDまたはログインパスワードが違います。");

		// ページを表示
		header('Location: ./index.php?media_login_error=1');
//		$smarty->display("./index.tpl");
		exit();
	}
}elseif($_GET['media_logout'] == 'y'){
	$_SESSION['media_login_user'] = null;
	$_SESSION['media_logon_token'] = "";

	// セッション変数を全て解除する
	$_SESSION = array();

	// セッションを切断するにはセッションクッキーも削除する。
	// Note: セッション情報だけでなくセッションを破壊する。
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-42000, '/');
	}

	// 最終的に、セッションを破壊する
	session_destroy();

	// ページを表示
	header('Location: /');
	exit();
}elseif($_POST['client_logon'] == 'yy'){
	$common_dao = new CommonDao();
	$advert_login_user = new AdvertLoginUser();
	$advert_login_user_dao = new AdvertLoginUserDao();

	$login_id = $common_dao->db_string_escape($_POST['login_id']);
	$login_pass = $common_dao->db_string_escape($_POST['login_pass']);

	$advert_login_user = $advert_login_user_dao->getAdvertLoginUserByIdPass($login_id, $login_pass);

	$chk_flag = 0;

	// 後でクラスを使用する
	if($advert_login_user){
		$get_select_sql = " SELECT "
		. " * "
		. " FROM "
		. " advert_withdrawal_view_user "
		. " WHERE "
		. " login_user_id = '" . $advert_login_user->getId() . "' "
		. " AND "
		. " deleted_at IS NULL ";
	
		$db_result = $common_dao->db_query($get_select_sql);
	
		if($db_result) {
			$_SESSION['advert_withdrawal_view_user'] = TRUE;
		} else {
			$_SESSION['advert_withdrawal_view_user'] = FALSE;
		}

	
		$_SESSION['advert_login_user'] = $advert_login_user;
		$_SESSION['advert_logon_token'] = md5(uniqid(mt_rand(), TRUE));

		$smarty->assign("info_message", "ログオンしました。");

		// ページを表示
		header('Location: ./client/top.php');
		exit();
	}else{
		$smarty->assign("error_message", "ログインIDまたはログインパスワードが違います。");

		// ページを表示
		header('Location: ./index.php?client_logon_error=1');
//		$smarty->display("./index.tpl");
		exit();
	}
}elseif($_GET['client_logout'] == 'y'){
	$_SESSION['advert_login_user'] = null;
	$_SESSION['advert_logon_token'] = "";

	// セッション変数を全て解除する
	$_SESSION = array();

	// セッションを切断するにはセッションクッキーも削除する。
	// Note: セッション情報だけでなくセッションを破壊する。
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-42000, '/');
	}

	// 最終的に、セッションを破壊する
	session_destroy();

	// ページを表示
	header('Location: /');
	exit();
}else{
	// ページを表示
	header('Location: /');
	exit();
}
?>