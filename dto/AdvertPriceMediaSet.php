<?php
class AdvertPriceMediaSet {

	// プロパティ
	private $id = "";
	private $advert_id = "";
	private $media_id = "";
	private $media_publisher_id = "";
	private $media_category_id = "";
	private $click_price_client = "";
	private $click_price_media = "";

	private $action_price_client_iphone_docomo_1 = "";
	private $action_price_client_iphone_softbank_1 = "";
	private $action_price_client_iphone_au_1 = "";
	private $action_price_client_iphone_pc_1 = "";
	private $action_price_client_iphone_docomo_2 = "";
	private $action_price_client_iphone_softbank_2 = "";
	private $action_price_client_iphone_au_2 = "";
	private $action_price_client_iphone_pc_2 = "";
	private $action_price_client_iphone_docomo_3 = "";
	private $action_price_client_iphone_softbank_3 = "";
	private $action_price_client_iphone_au_3 = "";
	private $action_price_client_iphone_pc_3 = "";
	private $action_price_client_iphone_docomo_4 = "";
	private $action_price_client_iphone_softbank_4 = "";
	private $action_price_client_iphone_au_4 = "";
	private $action_price_client_iphone_pc_4 = "";
	private $action_price_client_iphone_docomo_5 = "";
	private $action_price_client_iphone_softbank_5 = "";
	private $action_price_client_iphone_au_5 = "";
	private $action_price_client_iphone_pc_5 = "";

	private $action_price_media_iphone_docomo_1 = "";
	private $action_price_media_iphone_softbank_1 = "";
	private $action_price_media_iphone_au_1 = "";
	private $action_price_media_iphone_pc_1 = "";
	private $action_price_media_iphone_docomo_2 = "";
	private $action_price_media_iphone_softbank_2 = "";
	private $action_price_media_iphone_au_2 = "";
	private $action_price_media_iphone_pc_2 = "";
	private $action_price_media_iphone_docomo_3 = "";
	private $action_price_media_iphone_softbank_3 = "";
	private $action_price_media_iphone_au_3 = "";
	private $action_price_media_iphone_pc_3 = "";
	private $action_price_media_iphone_docomo_4 = "";
	private $action_price_media_iphone_softbank_4 = "";
	private $action_price_media_iphone_au_4 = "";
	private $action_price_media_iphone_pc_4 = "";
	private $action_price_media_iphone_docomo_5 = "";
	private $action_price_media_iphone_softbank_5 = "";
	private $action_price_media_iphone_au_5 = "";
	private $action_price_media_iphone_pc_5 = "";

	private $action_price_client_android_docomo_1 = "";
	private $action_price_client_android_softbank_1 = "";
	private $action_price_client_android_au_1 = "";
	private $action_price_client_android_pc_1 = "";
	private $action_price_client_android_docomo_2 = "";
	private $action_price_client_android_softbank_2 = "";
	private $action_price_client_android_au_2 = "";
	private $action_price_client_android_pc_2 = "";
	private $action_price_client_android_docomo_3 = "";
	private $action_price_client_android_softbank_3 = "";
	private $action_price_client_android_au_3 = "";
	private $action_price_client_android_pc_3 = "";
	private $action_price_client_android_docomo_4 = "";
	private $action_price_client_android_softbank_4 = "";
	private $action_price_client_android_au_4 = "";
	private $action_price_client_android_pc_4 = "";
	private $action_price_client_android_docomo_5 = "";
	private $action_price_client_android_softbank_5 = "";
	private $action_price_client_android_au_5 = "";
	private $action_price_client_android_pc_5 = "";

	private $action_price_media_android_docomo_1 = "";
	private $action_price_media_android_softbank_1 = "";
	private $action_price_media_android_au_1 = "";
	private $action_price_media_android_pc_1 = "";
	private $action_price_media_android_docomo_2 = "";
	private $action_price_media_android_softbank_2 = "";
	private $action_price_media_android_au_2 = "";
	private $action_price_media_android_pc_2 = "";
	private $action_price_media_android_docomo_3 = "";
	private $action_price_media_android_softbank_3 = "";
	private $action_price_media_android_au_3 = "";
	private $action_price_media_android_pc_3 = "";
	private $action_price_media_android_docomo_4 = "";
	private $action_price_media_android_softbank_4 = "";
	private $action_price_media_android_au_4 = "";
	private $action_price_media_android_pc_4 = "";
	private $action_price_media_android_docomo_5 = "";
	private $action_price_media_android_softbank_5 = "";
	private $action_price_media_android_au_5 = "";
	private $action_price_media_android_pc_5 = "";

	private $action_price_client_pc_1 = "";
	private $action_price_client_pc_2 = "";
	private $action_price_client_pc_3 = "";
	private $action_price_client_pc_4 = "";
	private $action_price_client_pc_5 = "";

	private $action_price_media_pc_1 = "";
	private $action_price_media_pc_2 = "";
	private $action_price_media_pc_3 = "";
	private $action_price_media_pc_4 = "";
	private $action_price_media_pc_5 = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->advert_id . ","
						. $this->media_id . ","
						. $this->media_publisher_id . ","
						. $this->media_category_id . ","
						. $this->click_price_client . ","
						. $this->click_price_media . ","

						. $this->action_price_client_iphone_docomo_1 . ","
						. $this->action_price_client_iphone_softbank_1 . ","
						. $this->action_price_client_iphone_au_1 . ","
						. $this->action_price_client_iphone_pc_1 . ","
						. $this->action_price_client_iphone_docomo_2 . ","
						. $this->action_price_client_iphone_softbank_2 . ","
						. $this->action_price_client_iphone_au_2 . ","
						. $this->action_price_client_iphone_pc_2 . ","
						. $this->action_price_client_iphone_docomo_3 . ","
						. $this->action_price_client_iphone_softbank_3 . ","
						. $this->action_price_client_iphone_au_3 . ","
						. $this->action_price_client_iphone_pc_3 . ","
						. $this->action_price_client_iphone_docomo_4 . ","
						. $this->action_price_client_iphone_softbank_4 . ","
						. $this->action_price_client_iphone_au_4 . ","
						. $this->action_price_client_iphone_pc_4 . ","
						. $this->action_price_client_iphone_docomo_5 . ","
						. $this->action_price_client_iphone_softbank_5 . ","
						. $this->action_price_client_iphone_au_5 . ","
						. $this->action_price_client_iphone_pc_5 . ","

						. $this->action_price_media_iphone_docomo_1 . ","
						. $this->action_price_media_iphone_softbank_1 . ","
						. $this->action_price_media_iphone_au_1 . ","
						. $this->action_price_media_iphone_pc_1 . ","
						. $this->action_price_media_iphone_docomo_2 . ","
						. $this->action_price_media_iphone_softbank_2 . ","
						. $this->action_price_media_iphone_au_2 . ","
						. $this->action_price_media_iphone_pc_2 . ","
						. $this->action_price_media_iphone_docomo_3 . ","
						. $this->action_price_media_iphone_softbank_3 . ","
						. $this->action_price_media_iphone_au_3 . ","
						. $this->action_price_media_iphone_pc_3 . ","
						. $this->action_price_media_iphone_docomo_4 . ","
						. $this->action_price_media_iphone_softbank_4 . ","
						. $this->action_price_media_iphone_au_4 . ","
						. $this->action_price_media_iphone_pc_4 . ","
						. $this->action_price_media_iphone_docomo_5 . ","
						. $this->action_price_media_iphone_softbank_5 . ","
						. $this->action_price_media_iphone_au_5 . ","
						. $this->action_price_media_iphone_pc_5 . ","

						. $this->action_price_client_android_docomo_1 . ","
						. $this->action_price_client_android_softbank_1 . ","
						. $this->action_price_client_android_au_1 . ","
						. $this->action_price_client_android_pc_1 . ","
						. $this->action_price_client_android_docomo_2 . ","
						. $this->action_price_client_android_softbank_2 . ","
						. $this->action_price_client_android_au_2 . ","
						. $this->action_price_client_android_pc_2 . ","
						. $this->action_price_client_android_docomo_3 . ","
						. $this->action_price_client_android_softbank_3 . ","
						. $this->action_price_client_android_au_3 . ","
						. $this->action_price_client_android_pc_3 . ","
						. $this->action_price_client_android_docomo_4 . ","
						. $this->action_price_client_android_softbank_4 . ","
						. $this->action_price_client_android_au_4 . ","
						. $this->action_price_client_android_pc_4 . ","
						. $this->action_price_client_android_docomo_5 . ","
						. $this->action_price_client_android_softbank_5 . ","
						. $this->action_price_client_android_au_5 . ","
						. $this->action_price_client_android_pc_5 . ","

						. $this->action_price_media_android_docomo_1 . ","
						. $this->action_price_media_android_softbank_1 . ","
						. $this->action_price_media_android_au_1 . ","
						. $this->action_price_media_android_pc_1 . ","
						. $this->action_price_media_android_docomo_2 . ","
						. $this->action_price_media_android_softbank_2 . ","
						. $this->action_price_media_android_au_2 . ","
						. $this->action_price_media_android_pc_2 . ","
						. $this->action_price_media_android_docomo_3 . ","
						. $this->action_price_media_android_softbank_3 . ","
						. $this->action_price_media_android_au_3 . ","
						. $this->action_price_media_android_pc_3 . ","
						. $this->action_price_media_android_docomo_4 . ","
						. $this->action_price_media_android_softbank_4 . ","
						. $this->action_price_media_android_au_4 . ","
						. $this->action_price_media_android_pc_4 . ","
						. $this->action_price_media_android_docomo_5 . ","
						. $this->action_price_media_android_softbank_5 . ","
						. $this->action_price_media_android_au_5 . ","
						. $this->action_price_media_android_pc_5 . ","

						. $this->action_price_client_pc_1 . ","
						. $this->action_price_client_pc_2 . ","
						. $this->action_price_client_pc_3 . ","
						. $this->action_price_client_pc_4 . ","
						. $this->action_price_client_pc_5 . ","

						. $this->action_price_media_pc_1 . ","
						. $this->action_price_media_pc_2 . ","
						. $this->action_price_media_pc_3 . ","
						. $this->action_price_media_pc_4 . ","
						. $this->action_price_media_pc_5 . ","

						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}

	public function setAdvertId($val){
		$this->advert_id = $val;
	}

	// media_id
	public function getMediaId(){
		return $this->media_id;
	}

	public function setMediaId($val){
		$this->media_id = $val;
	}

	// media_publisher_id
	public function getMediaPublisherId(){
		return $this->media_publisher_id;
	}

	public function setMediaPublisherId($val){
		$this->media_publisher_id = $val;
	}

	// media_category_id
	public function getMediaCategoryId(){
		return $this->media_category_id;
	}

	public function setMediaCategoryId($val){
		$this->media_category_id = $val;
	}

	// click_price_client
	public function getClickPriceClient(){
		return $this->click_price_client;
	}

	public function setClickPriceClient($val){
		$this->click_price_client = $val;
	}

	// click_price_media
	public function getClickPriceMedia(){
		return $this->click_price_media;
	}

	public function setClickPriceMedia($val){
		$this->click_price_media = $val;
	}

// -------------------------------------- 20111012 追加
	// iphoneクライアント
	// action_price_client_iphone_docomo_1
	public function getActionPriceClientIphoneDocomo1(){
		return $this->action_price_client_iphone_docomo_1;
	}

	public function setActionPriceClientIphoneDocomo1($val){
		$this->action_price_client_iphone_docomo_1 = $val;
	}

	// action_price_client_iphone_softbank_1
	public function getActionPriceClientIphoneSoftbank1(){
		return $this->action_price_client_iphone_softbank_1;
	}

	public function setActionPriceClientIphoneSoftbank1($val){
		$this->action_price_client_iphone_softbank_1 = $val;
	}

	// action_price_client_iphone_au_1
	public function getActionPriceClientIphoneAu1(){
		return $this->action_price_client_iphone_au_1;
	}

	public function setActionPriceClientIphoneAu1($val){
		$this->action_price_client_iphone_au_1 = $val;
	}

	// action_price_client_iphone_pc_1
	public function getActionPriceClientIphonePc1(){
		return $this->action_price_client_iphone_pc_1;
	}

	public function setActionPriceClientIphonePc1($val){
		$this->action_price_client_iphone_pc_1 = $val;
	}

	// action_price_client_iphone_docomo_2
	public function getActionPriceClientIphoneDocomo2(){
		return $this->action_price_client_iphone_docomo_2;
	}

	public function setActionPriceClientIphoneDocomo2($val){
		$this->action_price_client_iphone_docomo_2 = $val;
	}

	// action_price_client_iphone_softbank_2
	public function getActionPriceClientIphoneSoftbank2(){
		return $this->action_price_client_iphone_softbank_2;
	}

	public function setActionPriceClientIphoneSoftbank2($val){
		$this->action_price_client_iphone_softbank_2 = $val;
	}

	// action_price_client_iphone_au_2
	public function getActionPriceClientIphoneAu2(){
		return $this->action_price_client_iphone_au_2;
	}

	public function setActionPriceClientIphoneAu2($val){
		$this->action_price_client_iphone_au_2 = $val;
	}

	// action_price_client_iphone_pc_2
	public function getActionPriceClientIphonePc2(){
		return $this->action_price_client_iphone_pc_2;
	}

	public function setActionPriceClientIphonePc2($val){
		$this->action_price_client_iphone_pc_2 = $val;
	}

	// action_price_client_iphone_docomo_3
	public function getActionPriceClientIphoneDocomo3(){
		return $this->action_price_client_iphone_docomo_3;
	}

	public function setActionPriceClientIphoneDocomo3($val){
		$this->action_price_client_iphone_docomo_3 = $val;
	}

	// action_price_client_iphone_softbank_3
	public function getActionPriceClientIphoneSoftbank3(){
		return $this->action_price_client_iphone_softbank_3;
	}

	public function setActionPriceClientIphoneSoftbank3($val){
		$this->action_price_client_iphone_softbank_3 = $val;
	}

	// action_price_client_iphone_au_3
	public function getActionPriceClientIphoneAu3(){
		return $this->action_price_client_iphone_au_3;
	}

	public function setActionPriceClientIphoneAu3($val){
		$this->action_price_client_iphone_au_3 = $val;
	}

	// action_price_client_iphone_pc_3
	public function getActionPriceClientIphonePc3(){
		return $this->action_price_client_iphone_pc_3;
	}

	public function setActionPriceClientIphonePc3($val){
		$this->action_price_client_iphone_pc_3 = $val;
	}

	// action_price_client_iphone_docomo_4
	public function getActionPriceClientIphoneDocomo4(){
		return $this->action_price_client_iphone_docomo_4;
	}

	public function setActionPriceClientIphoneDocomo4($val){
		$this->action_price_client_iphone_docomo_4 = $val;
	}

	// action_price_client_iphone_softbank_4
	public function getActionPriceClientIphoneSoftbank4(){
		return $this->action_price_client_iphone_softbank_4;
	}

	public function setActionPriceClientIphoneSoftbank4($val){
		$this->action_price_client_iphone_softbank_4 = $val;
	}

	// action_price_client_iphone_au_4
	public function getActionPriceClientIphoneAu4(){
		return $this->action_price_client_iphone_au_4;
	}

	public function setActionPriceClientIphoneAu4($val){
		$this->action_price_client_iphone_au_4 = $val;
	}

	// action_price_client_iphone_pc_4
	public function getActionPriceClientIphonePc4(){
		return $this->action_price_client_iphone_pc_4;
	}

	public function setActionPriceClientIphonePc4($val){
		$this->action_price_client_iphone_pc_4 = $val;
	}

	// action_price_client_iphone_docomo_5
	public function getActionPriceClientIphoneDocomo5(){
		return $this->action_price_client_iphone_docomo_5;
	}

	public function setActionPriceClientIphoneDocomo5($val){
		$this->action_price_client_iphone_docomo_5 = $val;
	}

	// action_price_client_iphone_softbank_5
	public function getActionPriceClientIphoneSoftbank5(){
		return $this->action_price_client_iphone_softbank_5;
	}

	public function setActionPriceClientIphoneSoftbank5($val){
		$this->action_price_client_iphone_softbank_5 = $val;
	}

	// action_price_client_iphone_au_5
	public function getActionPriceClientIphoneAu5(){
		return $this->action_price_client_iphone_au_5;
	}

	public function setActionPriceClientIphoneAu5($val){
		$this->action_price_client_iphone_au_5 = $val;
	}

	// action_price_client_iphone_pc_5
	public function getActionPriceClientIphonePc5(){
		return $this->action_price_client_iphone_pc_5;
	}

	public function setActionPriceClientIphonePc5($val){
		$this->action_price_client_iphone_pc_5 = $val;
	}

	// iphoneメディア
	// action_price_media_iphone_docomo_1
	public function getActionPriceMediaIphoneDocomo1(){
		return $this->action_price_media_iphone_docomo_1;
	}

	public function setActionPriceMediaIphoneDocomo1($val){
		$this->action_price_media_iphone_docomo_1 = $val;
	}

	// action_price_media_iphone_softbank_1
	public function getActionPriceMediaIphoneSoftbank1(){
		return $this->action_price_media_iphone_softbank_1;
	}

	public function setActionPriceMediaIphoneSoftbank1($val){
		$this->action_price_media_iphone_softbank_1 = $val;
	}

	// action_price_media_iphone_au_1
	public function getActionPriceMediaIphoneAu1(){
		return $this->action_price_media_iphone_au_1;
	}

	public function setActionPriceMediaIphoneAu1($val){
		$this->action_price_media_iphone_au_1 = $val;
	}

	// action_price_media_iphone_pc_1
	public function getActionPriceMediaIphonePc1(){
		return $this->action_price_media_iphone_pc_1;
	}

	public function setActionPriceMediaIphonePc1($val){
		$this->action_price_media_iphone_pc_1 = $val;
	}

	// action_price_media_iphone_docomo_2
	public function getActionPriceMediaIphoneDocomo2(){
		return $this->action_price_media_iphone_docomo_2;
	}

	public function setActionPriceMediaIphoneDocomo2($val){
		$this->action_price_media_iphone_docomo_2 = $val;
	}

	// action_price_media_iphone_softbank_2
	public function getActionPriceMediaIphoneSoftbank2(){
		return $this->action_price_media_iphone_softbank_2;
	}

	public function setActionPriceMediaIphoneSoftbank2($val){
		$this->action_price_media_iphone_softbank_2 = $val;
	}

	// action_price_media_iphone_au_2
	public function getActionPriceMediaIphoneAu2(){
		return $this->action_price_media_iphone_au_2;
	}

	public function setActionPriceMediaIphoneAu2($val){
		$this->action_price_media_iphone_au_2 = $val;
	}

	// action_price_media_iphone_pc_2
	public function getActionPriceMediaIphonePc2(){
		return $this->action_price_media_iphone_pc_2;
	}

	public function setActionPriceMediaIphonePc2($val){
		$this->action_price_media_iphone_pc_2 = $val;
	}

	// action_price_media_iphone_docomo_3
	public function getActionPriceMediaIphoneDocomo3(){
		return $this->action_price_media_iphone_docomo_3;
	}

	public function setActionPriceMediaIphoneDocomo3($val){
		$this->action_price_media_iphone_docomo_3 = $val;
	}

	// action_price_media_iphone_softbank_3
	public function getActionPriceMediaIphoneSoftbank3(){
		return $this->action_price_media_iphone_softbank_3;
	}

	public function setActionPriceMediaIphoneSoftbank3($val){
		$this->action_price_media_iphone_softbank_3 = $val;
	}

	// action_price_media_iphone_au_3
	public function getActionPriceMediaIphoneAu3(){
		return $this->action_price_media_iphone_au_3;
	}

	public function setActionPriceMediaIphoneAu3($val){
		$this->action_price_media_iphone_au_3 = $val;
	}

	// action_price_media_iphone_pc_3
	public function getActionPriceMediaIphonePc3(){
		return $this->action_price_media_iphone_pc_3;
	}

	public function setActionPriceMediaIphonePc3($val){
		$this->action_price_media_iphone_pc_3 = $val;
	}

	// action_price_media_iphone_docomo_4
	public function getActionPriceMediaIphoneDocomo4(){
		return $this->action_price_media_iphone_docomo_4;
	}

	public function setActionPriceMediaIphoneDocomo4($val){
		$this->action_price_media_iphone_docomo_4 = $val;
	}

	// action_price_media_iphone_softbank_4
	public function getActionPriceMediaIphoneSoftbank4(){
		return $this->action_price_media_iphone_softbank_4;
	}

	public function setActionPriceMediaIphoneSoftbank4($val){
		$this->action_price_media_iphone_softbank_4 = $val;
	}

	// action_price_media_iphone_au_4
	public function getActionPriceMediaIphoneAu4(){
		return $this->action_price_media_iphone_au_4;
	}

	public function setActionPriceMediaIphoneAu4($val){
		$this->action_price_media_iphone_au_4 = $val;
	}

	// action_price_media_iphone_pc_4
	public function getActionPriceMediaIphonePc4(){
		return $this->action_price_media_iphone_pc_4;
	}

	public function setActionPriceMediaIphonePc4($val){
		$this->action_price_media_iphone_pc_4 = $val;
	}

	// action_price_media_iphone_docomo_5
	public function getActionPriceMediaIphoneDocomo5(){
		return $this->action_price_media_iphone_docomo_5;
	}

	public function setActionPriceMediaIphoneDocomo5($val){
		$this->action_price_media_iphone_docomo_5 = $val;
	}

	// action_price_media_iphone_softbank_5
	public function getActionPriceMediaIphoneSoftbank5(){
		return $this->action_price_media_iphone_softbank_5;
	}

	public function setActionPriceMediaIphoneSoftbank5($val){
		$this->action_price_media_iphone_softbank_5 = $val;
	}

	// action_price_media_iphone_au_5
	public function getActionPriceMediaIphoneAu5(){
		return $this->action_price_media_iphone_au_5;
	}

	public function setActionPriceMediaIphoneAu5($val){
		$this->action_price_media_iphone_au_5 = $val;
	}

	// action_price_media_iphone_pc_5
	public function getActionPriceMediaIphonePc5(){
		return $this->action_price_media_iphone_pc_5;
	}

	public function setActionPriceMediaIphonePc5($val){
		$this->action_price_media_iphone_pc_5 = $val;
	}

	// androidクライアント
	// action_price_client_android_docomo_1
	public function getActionPriceClientAndroidDocomo1(){
		return $this->action_price_client_android_docomo_1;
	}

	public function setActionPriceClientAndroidDocomo1($val){
		$this->action_price_client_android_docomo_1 = $val;
	}

	// action_price_client_android_softbank_1
	public function getActionPriceClientAndroidSoftbank1(){
		return $this->action_price_client_android_softbank_1;
	}

	public function setActionPriceClientAndroidSoftbank1($val){
		$this->action_price_client_android_softbank_1 = $val;
	}

	// action_price_client_android_au_1
	public function getActionPriceClientAndroidAu1(){
		return $this->action_price_client_android_au_1;
	}

	public function setActionPriceClientAndroidAu1($val){
		$this->action_price_client_android_au_1 = $val;
	}

	// action_price_client_android_pc_1
	public function getActionPriceClientAndroidPc1(){
		return $this->action_price_client_android_pc_1;
	}

	public function setActionPriceClientAndroidPc1($val){
		$this->action_price_client_android_pc_1 = $val;
	}

	// action_price_client_android_docomo_2
	public function getActionPriceClientAndroidDocomo2(){
		return $this->action_price_client_android_docomo_2;
	}

	public function setActionPriceClientAndroidDocomo2($val){
		$this->action_price_client_android_docomo_2 = $val;
	}

	// action_price_client_android_softbank_2
	public function getActionPriceClientAndroidSoftbank2(){
		return $this->action_price_client_android_softbank_2;
	}

	public function setActionPriceClientAndroidSoftbank2($val){
		$this->action_price_client_android_softbank_2 = $val;
	}

	// action_price_client_android_au_2
	public function getActionPriceClientAndroidAu2(){
		return $this->action_price_client_android_au_2;
	}

	public function setActionPriceClientAndroidAu2($val){
		$this->action_price_client_android_au_2 = $val;
	}

	// action_price_client_android_pc_2
	public function getActionPriceClientAndroidPc2(){
		return $this->action_price_client_android_pc_2;
	}

	public function setActionPriceClientAndroidPc2($val){
		$this->action_price_client_android_pc_2 = $val;
	}

	// action_price_client_android_docomo_3
	public function getActionPriceClientAndroidDocomo3(){
		return $this->action_price_client_android_docomo_3;
	}

	public function setActionPriceClientAndroidDocomo3($val){
		$this->action_price_client_android_docomo_3 = $val;
	}

	// action_price_client_android_softbank_3
	public function getActionPriceClientAndroidSoftbank3(){
		return $this->action_price_client_android_softbank_3;
	}

	public function setActionPriceClientAndroidSoftbank3($val){
		$this->action_price_client_android_softbank_3 = $val;
	}

	// action_price_client_android_au_3
	public function getActionPriceClientAndroidAu3(){
		return $this->action_price_client_android_au_3;
	}

	public function setActionPriceClientAndroidAu3($val){
		$this->action_price_client_android_au_3 = $val;
	}

	// action_price_client_android_pc_3
	public function getActionPriceClientAndroidPc3(){
		return $this->action_price_client_android_pc_3;
	}

	public function setActionPriceClientAndroidPc3($val){
		$this->action_price_client_android_pc_3 = $val;
	}

	// action_price_client_android_docomo_4
	public function getActionPriceClientAndroidDocomo4(){
		return $this->action_price_client_android_docomo_4;
	}

	public function setActionPriceClientAndroidDocomo4($val){
		$this->action_price_client_android_docomo_4 = $val;
	}

	// action_price_client_android_softbank_4
	public function getActionPriceClientAndroidSoftbank4(){
		return $this->action_price_client_android_softbank_4;
	}

	public function setActionPriceClientAndroidSoftbank4($val){
		$this->action_price_client_android_softbank_4 = $val;
	}

	// action_price_client_android_au_4
	public function getActionPriceClientAndroidAu4(){
		return $this->action_price_client_android_au_4;
	}

	public function setActionPriceClientAndroidAu4($val){
		$this->action_price_client_android_au_4 = $val;
	}

	// action_price_client_android_pc_4
	public function getActionPriceClientAndroidPc4(){
		return $this->action_price_client_android_pc_4;
	}

	public function setActionPriceClientAndroidPc4($val){
		$this->action_price_client_android_pc_4 = $val;
	}

	// action_price_client_android_docomo_5
	public function getActionPriceClientAndroidDocomo5(){
		return $this->action_price_client_android_docomo_5;
	}

	public function setActionPriceClientAndroidDocomo5($val){
		$this->action_price_client_android_docomo_5 = $val;
	}

	// action_price_client_android_softbank_5
	public function getActionPriceClientAndroidSoftbank5(){
		return $this->action_price_client_android_softbank_5;
	}

	public function setActionPriceClientAndroidSoftbank5($val){
		$this->action_price_client_android_softbank_5 = $val;
	}

	// action_price_client_android_au_5
	public function getActionPriceClientAndroidAu5(){
		return $this->action_price_client_android_au_5;
	}

	public function setActionPriceClientAndroidAu5($val){
		$this->action_price_client_android_au_5 = $val;
	}

	// action_price_client_android_pc_5
	public function getActionPriceClientAndroidPc5(){
		return $this->action_price_client_android_pc_5;
	}

	public function setActionPriceClientAndroidPc5($val){
		$this->action_price_client_android_pc_5 = $val;
	}

	// androidメディア
	// action_price_media_android_docomo_1
	public function getActionPriceMediaAndroidDocomo1(){
		return $this->action_price_media_android_docomo_1;
	}

	public function setActionPriceMediaAndroidDocomo1($val){
		$this->action_price_media_android_docomo_1 = $val;
	}

	// action_price_media_android_softbank_1
	public function getActionPriceMediaAndroidSoftbank1(){
		return $this->action_price_media_android_softbank_1;
	}

	public function setActionPriceMediaAndroidSoftbank1($val){
		$this->action_price_media_android_softbank_1 = $val;
	}

	// action_price_media_android_au_1
	public function getActionPriceMediaAndroidAu1(){
		return $this->action_price_media_android_au_1;
	}

	public function setActionPriceMediaAndroidAu1($val){
		$this->action_price_media_android_au_1 = $val;
	}

	// action_price_media_android_pc_1
	public function getActionPriceMediaAndroidPc1(){
		return $this->action_price_media_android_pc_1;
	}

	public function setActionPriceMediaAndroidPc1($val){
		$this->action_price_media_android_pc_1 = $val;
	}

	// action_price_media_android_docomo_2
	public function getActionPriceMediaAndroidDocomo2(){
		return $this->action_price_media_android_docomo_2;
	}

	public function setActionPriceMediaAndroidDocomo2($val){
		$this->action_price_media_android_docomo_2 = $val;
	}

	// action_price_media_android_softbank_2
	public function getActionPriceMediaAndroidSoftbank2(){
		return $this->action_price_media_android_softbank_2;
	}

	public function setActionPriceMediaAndroidSoftbank2($val){
		$this->action_price_media_android_softbank_2 = $val;
	}

	// action_price_media_android_au_2
	public function getActionPriceMediaAndroidAu2(){
		return $this->action_price_media_android_au_2;
	}

	public function setActionPriceMediaAndroidAu2($val){
		$this->action_price_media_android_au_2 = $val;
	}

	// action_price_media_android_pc_2
	public function getActionPriceMediaAndroidPc2(){
		return $this->action_price_media_android_pc_2;
	}

	public function setActionPriceMediaAndroidPc2($val){
		$this->action_price_media_android_pc_2 = $val;
	}

	// action_price_media_android_docomo_3
	public function getActionPriceMediaAndroidDocomo3(){
		return $this->action_price_media_android_docomo_3;
	}

	public function setActionPriceMediaAndroidDocomo3($val){
		$this->action_price_media_android_docomo_3 = $val;
	}

	// action_price_media_android_softbank_3
	public function getActionPriceMediaAndroidSoftbank3(){
		return $this->action_price_media_android_softbank_3;
	}

	public function setActionPriceMediaAndroidSoftbank3($val){
		$this->action_price_media_android_softbank_3 = $val;
	}

	// action_price_media_android_au_3
	public function getActionPriceMediaAndroidAu3(){
		return $this->action_price_media_android_au_3;
	}

	public function setActionPriceMediaAndroidAu3($val){
		$this->action_price_media_android_au_3 = $val;
	}

	// action_price_media_android_pc_3
	public function getActionPriceMediaAndroidPc3(){
		return $this->action_price_media_android_pc_3;
	}

	public function setActionPriceMediaAndroidPc3($val){
		$this->action_price_media_android_pc_3 = $val;
	}

	// action_price_media_android_docomo_4
	public function getActionPriceMediaAndroidDocomo4(){
		return $this->action_price_media_android_docomo_4;
	}

	public function setActionPriceMediaAndroidDocomo4($val){
		$this->action_price_media_android_docomo_4 = $val;
	}

	// action_price_media_android_softbank_4
	public function getActionPriceMediaAndroidSoftbank4(){
		return $this->action_price_media_android_softbank_4;
	}

	public function setActionPriceMediaAndroidSoftbank4($val){
		$this->action_price_media_android_softbank_4 = $val;
	}

	// action_price_media_android_au_4
	public function getActionPriceMediaAndroidAu4(){
		return $this->action_price_media_android_au_4;
	}

	public function setActionPriceMediaAndroidAu4($val){
		$this->action_price_media_android_au_4 = $val;
	}

	// action_price_media_android_pc_4
	public function getActionPriceMediaAndroidPc4(){
		return $this->action_price_media_android_pc_4;
	}

	public function setActionPriceMediaAndroidPc4($val){
		$this->action_price_media_android_pc_4 = $val;
	}

	// action_price_media_android_docomo_5
	public function getActionPriceMediaAndroidDocomo5(){
		return $this->action_price_media_android_docomo_5;
	}

	public function setActionPriceMediaAndroidDocomo5($val){
		$this->action_price_media_android_docomo_5 = $val;
	}

	// action_price_media_android_softbank_5
	public function getActionPriceMediaAndroidSoftbank5(){
		return $this->action_price_media_android_softbank_5;
	}

	public function setActionPriceMediaAndroidSoftbank5($val){
		$this->action_price_media_android_softbank_5 = $val;
	}

	// action_price_media_android_au_5
	public function getActionPriceMediaAndroidAu5(){
		return $this->action_price_media_android_au_5;
	}

	public function setActionPriceMediaAndroidAu5($val){
		$this->action_price_media_android_au_5 = $val;
	}

	// action_price_media_android_pc_5
	public function getActionPriceMediaAndroidPc5(){
		return $this->action_price_media_android_pc_5;
	}

	public function setActionPriceMediaAndroidPc5($val){
		$this->action_price_media_android_pc_5 = $val;
	}

// -------------------------------------- 20111012 追加ここまで

	// action_price_client_pc_1
	public function getActionPriceClientPc1(){
		return $this->action_price_client_pc_1;
	}

	public function setActionPriceClientPc1($val){
		$this->action_price_client_pc_1 = $val;
	}



	// action_price_client_pc_2
	public function getActionPriceClientPc2(){
		return $this->action_price_client_pc_2;
	}

	public function setActionPriceClientPc2($val){
		$this->action_price_client_pc_2 = $val;
	}



	// action_price_client_pc_3
	public function getActionPriceClientPc3(){
		return $this->action_price_client_pc_3;
	}

	public function setActionPriceClientPc3($val){
		$this->action_price_client_pc_3 = $val;
	}



	// action_price_client_pc_4
	public function getActionPriceClientPc4(){
		return $this->action_price_client_pc_4;
	}

	public function setActionPriceClientPc4($val){
		$this->action_price_client_pc_4 = $val;
	}



	// action_price_client_pc_5
	public function getActionPriceClientPc5(){
		return $this->action_price_client_pc_5;
	}

	public function setActionPriceClientPc5($val){
		$this->action_price_client_pc_5 = $val;
	}



	// action_price_media_pc_1
	public function getActionPriceMediaPc1(){
		return $this->action_price_media_pc_1;
	}

	public function setActionPriceMediaPc1($val){
		$this->action_price_media_pc_1 = $val;
	}



	// action_price_media_pc_2
	public function getActionPriceMediaPc2(){
		return $this->action_price_media_pc_2;
	}

	public function setActionPriceMediaPc2($val){
		$this->action_price_media_pc_2 = $val;
	}



	// action_price_media_pc_3
	public function getActionPriceMediaPc3(){
		return $this->action_price_media_pc_3;
	}

	public function setActionPriceMediaPc3($val){
		$this->action_price_media_pc_3 = $val;
	}



	// action_price_media_pc_4
	public function getActionPriceMediaPc4(){
		return $this->action_price_media_pc_4;
	}

	public function setActionPriceMediaPc4($val){
		$this->action_price_media_pc_4 = $val;
	}



	// action_price_media_pc_5
	public function getActionPriceMediaPc5(){
		return $this->action_price_media_pc_5;
	}

	public function setActionPriceMediaPc5($val){
		$this->action_price_media_pc_5 = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}