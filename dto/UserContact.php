<?php
class UserContact {

	// プロパティ
	private $id = "";
	private $user_name = "";
	private $pref = "";
	private $address1 = "";
	private $address2 = "";
	private $tel = "";
	private $email = "";
	private $textarea = "";
	private $commit_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->user_name . ","
						. $this->pref . ","
						. $this->address1 . ","
						. $this->address2 . ","
						. $this->tel . ","
						. $this->email . ","
						. $this->textarea . ","
						. $this->commit_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// user_name
	public function getUserName(){
		return $this->user_name;
	}

	public function setUserName($val){
		$this->user_name = $val;
	}

	// login_id
	public function getPref(){
		return $this->pref;
	}

	public function setPref($val){
		$this->pref = $val;
	}

	// login_pass
	public function getAddress1(){
		return $this->address1;
	}

	public function setAddress1($val){
		$this->address1 = $val;
	}

	// last_login_at
	public function getAddress2(){
		return $this->address2;
	}

	public function setAddress2($val){
		$this->address2 = $val;
	}

	// created_at
	public function getTel(){
		return $this->tel;
	}

	public function setTel($val){
		$this->tel = $val;
	}

	// updated_at
	public function getEmail(){
		return $this->email;
	}

	public function setEmail($val){
		$this->email = $val;
	}

	// deleted_at
	public function getTextarea(){
		return $this->textarea;
	}

	public function setTextarea($val){
		$this->textarea = $val;
	}

	// commit_at
	public function getCommitAt(){
		return $this->commit_at;
	}

	public function setCommitAt($val){
		$this->commit_at = $val;
	}
}