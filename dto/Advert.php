<?php
class Advert {

	// プロパティ
	private $id = "";
	private $advert_client_id = "";
	private $advert_category_id = "";

	private $support_iphone_docomo = "";
	private $support_iphone_softbank = "";
	private $support_iphone_au = "";
	private $support_iphone_pc = "";
	private $support_android_docomo = "";
	private $support_android_softbank = "";
	private $support_android_au = "";
	private $support_android_pc = "";
	private $support_pc = "";

	private $advert_name = "";

	private $site_url_iphone_docomo = "";
	private $site_url_iphone_softbank = "";
	private $site_url_iphone_au = "";
	private $site_url_iphone_pc = "";
	private $site_url_android_docomo = "";
	private $site_url_android_softbank = "";
	private $site_url_android_au = "";
	private $site_url_android_pc = "";
	private $site_url_pc = "";

	private $site_outline = "";
	private $click_price_client = "";
	private $click_price_media = "";

	private $action_price_client_iphone_docomo_1 = "";
	private $action_price_client_iphone_softbank_1 = "";
	private $action_price_client_iphone_au_1 = "";
	private $action_price_client_iphone_pc_1 = "";
	private $action_price_client_iphone_docomo_2 = "";
	private $action_price_client_iphone_softbank_2 = "";
	private $action_price_client_iphone_au_2 = "";
	private $action_price_client_iphone_pc_2 = "";
	private $action_price_client_iphone_docomo_3 = "";
	private $action_price_client_iphone_softbank_3 = "";
	private $action_price_client_iphone_au_3 = "";
	private $action_price_client_iphone_pc_3 = "";
	private $action_price_client_iphone_docomo_4 = "";
	private $action_price_client_iphone_softbank_4 = "";
	private $action_price_client_iphone_au_4 = "";
	private $action_price_client_iphone_pc_4 = "";
	private $action_price_client_iphone_docomo_5 = "";
	private $action_price_client_iphone_softbank_5 = "";
	private $action_price_client_iphone_au_5 = "";
	private $action_price_client_iphone_pc_5 = "";

	private $action_price_media_iphone_docomo_1 = "";
	private $action_price_media_iphone_softbank_1 = "";
	private $action_price_media_iphone_au_1 = "";
	private $action_price_media_iphone_pc_1 = "";
	private $action_price_media_iphone_docomo_2 = "";
	private $action_price_media_iphone_softbank_2 = "";
	private $action_price_media_iphone_au_2 = "";
	private $action_price_media_iphone_pc_2 = "";
	private $action_price_media_iphone_docomo_3 = "";
	private $action_price_media_iphone_softbank_3 = "";
	private $action_price_media_iphone_au_3 = "";
	private $action_price_media_iphone_pc_3 = "";
	private $action_price_media_iphone_docomo_4 = "";
	private $action_price_media_iphone_softbank_4 = "";
	private $action_price_media_iphone_au_4 = "";
	private $action_price_media_iphone_pc_4 = "";
	private $action_price_media_iphone_docomo_5 = "";
	private $action_price_media_iphone_softbank_5 = "";
	private $action_price_media_iphone_au_5 = "";
	private $action_price_media_iphone_pc_5 = "";

	private $action_price_client_android_docomo_1 = "";
	private $action_price_client_android_softbank_1 = "";
	private $action_price_client_android_au_1 = "";
	private $action_price_client_android_pc_1 = "";
	private $action_price_client_android_docomo_2 = "";
	private $action_price_client_android_softbank_2 = "";
	private $action_price_client_android_au_2 = "";
	private $action_price_client_android_pc_2 = "";
	private $action_price_client_android_docomo_3 = "";
	private $action_price_client_android_softbank_3 = "";
	private $action_price_client_android_au_3 = "";
	private $action_price_client_android_pc_3 = "";
	private $action_price_client_android_docomo_4 = "";
	private $action_price_client_android_softbank_4 = "";
	private $action_price_client_android_au_4 = "";
	private $action_price_client_android_pc_4 = "";
	private $action_price_client_android_docomo_5 = "";
	private $action_price_client_android_softbank_5 = "";
	private $action_price_client_android_au_5 = "";
	private $action_price_client_android_pc_5 = "";

	private $action_price_media_android_docomo_1 = "";
	private $action_price_media_android_softbank_1 = "";
	private $action_price_media_android_au_1 = "";
	private $action_price_media_android_pc_1 = "";
	private $action_price_media_android_docomo_2 = "";
	private $action_price_media_android_softbank_2 = "";
	private $action_price_media_android_au_2 = "";
	private $action_price_media_android_pc_2 = "";
	private $action_price_media_android_docomo_3 = "";
	private $action_price_media_android_softbank_3 = "";
	private $action_price_media_android_au_3 = "";
	private $action_price_media_android_pc_3 = "";
	private $action_price_media_android_docomo_4 = "";
	private $action_price_media_android_softbank_4 = "";
	private $action_price_media_android_au_4 = "";
	private $action_price_media_android_pc_4 = "";
	private $action_price_media_android_docomo_5 = "";
	private $action_price_media_android_softbank_5 = "";
	private $action_price_media_android_au_5 = "";
	private $action_price_media_android_pc_5 = "";

	private $action_price_client_pc_1 = "";
	private $action_price_client_pc_2 = "";
	private $action_price_client_pc_3 = "";
	private $action_price_client_pc_4 = "";
	private $action_price_client_pc_5 = "";

	private $action_price_media_pc_1 = "";
	private $action_price_media_pc_2 = "";
	private $action_price_media_pc_3 = "";
	private $action_price_media_pc_4 = "";
	private $action_price_media_pc_5 = "";

	private $approval_flag = "";
	private $price_type = "";

	private $ms_text_1 = "";
	private $ms_email_1 = "";
	private $ms_image_type_1 = "";
	private $ms_image_url_1 = "";
	private $ms_text_2 = "";
	private $ms_email_2 = "";
	private $ms_image_type_2 = "";
	private $ms_image_url_2 = "";
	private $ms_text_3 = "";
	private $ms_email_3 = "";
	private $ms_image_type_3 = "";
	private $ms_image_url_3 = "";
	private $ms_text_4 = "";
	private $ms_email_4 = "";
	private $ms_image_type_4 = "";
	private $ms_image_url_4 = "";
	private $ms_text_5 = "";
	private $ms_email_5 = "";
	private $ms_image_type_5 = "";
	private $ms_image_url_5 = "";
	private $unique_click_type = "";

	private $point_back_flag = "";
	private $adult_flag = "";
	private $seo_flag = "";
	private $listing_flag = "";
	private $mail_magazine_flag = "";
	private $community_flag = "";

	private $advert_start_date = "";
	private $advert_end_date = "";
	private $unrestraint_flag = "";
	private $test_flag = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->advert_client_id . ","
						. $this->advert_category_id . ","

						. $this->support_iphone_docomo . ","
						. $this->support_iphone_softbank . ","
						. $this->support_iphone_au . ","
						. $this->support_iphone_pc . ","
						. $this->support_android_docomo . ","
						. $this->support_android_softbank . ","
						. $this->support_android_au . ","
						. $this->support_android_pc . ","
						. $this->support_pc . ","

						. $this->advert_name . ","

						. $this->site_url_iphone_docomo . ","
						. $this->site_url_iphone_softbank . ","
						. $this->site_url_iphone_au . ","
						. $this->site_url_iphone_pc . ","
						. $this->site_url_android_docomo . ","
						. $this->site_url_android_softbank . ","
						. $this->site_url_android_au . ","
						. $this->site_url_android_pc . ","
						. $this->site_url_pc . ","

						. $this->site_outline . ","
						. $this->click_price_client . ","
						. $this->click_price_media . ","

						. $this->action_price_client_iphone_docomo_1 . ","
						. $this->action_price_client_iphone_softbank_1 . ","
						. $this->action_price_client_iphone_au_1 . ","
						. $this->action_price_client_iphone_pc_1 . ","
						. $this->action_price_client_iphone_docomo_2 . ","
						. $this->action_price_client_iphone_softbank_2 . ","
						. $this->action_price_client_iphone_au_2 . ","
						. $this->action_price_client_iphone_pc_2 . ","
						. $this->action_price_client_iphone_docomo_3 . ","
						. $this->action_price_client_iphone_softbank_3 . ","
						. $this->action_price_client_iphone_au_3 . ","
						. $this->action_price_client_iphone_pc_3 . ","
						. $this->action_price_client_iphone_docomo_4 . ","
						. $this->action_price_client_iphone_softbank_4 . ","
						. $this->action_price_client_iphone_au_4 . ","
						. $this->action_price_client_iphone_pc_4 . ","
						. $this->action_price_client_iphone_docomo_5 . ","
						. $this->action_price_client_iphone_softbank_5 . ","
						. $this->action_price_client_iphone_au_5 . ","
						. $this->action_price_client_iphone_pc_5 . ","

						. $this->action_price_media_iphone_docomo_1 . ","
						. $this->action_price_media_iphone_softbank_1 . ","
						. $this->action_price_media_iphone_au_1 . ","
						. $this->action_price_media_iphone_pc_1 . ","
						. $this->action_price_media_iphone_docomo_2 . ","
						. $this->action_price_media_iphone_softbank_2 . ","
						. $this->action_price_media_iphone_au_2 . ","
						. $this->action_price_media_iphone_pc_2 . ","
						. $this->action_price_media_iphone_docomo_3 . ","
						. $this->action_price_media_iphone_softbank_3 . ","
						. $this->action_price_media_iphone_au_3 . ","
						. $this->action_price_media_iphone_pc_3 . ","
						. $this->action_price_media_iphone_docomo_4 . ","
						. $this->action_price_media_iphone_softbank_4 . ","
						. $this->action_price_media_iphone_au_4 . ","
						. $this->action_price_media_iphone_pc_4 . ","
						. $this->action_price_media_iphone_docomo_5 . ","
						. $this->action_price_media_iphone_softbank_5 . ","
						. $this->action_price_media_iphone_au_5 . ","
						. $this->action_price_media_iphone_pc_5 . ","

						. $this->action_price_client_android_docomo_1 . ","
						. $this->action_price_client_android_softbank_1 . ","
						. $this->action_price_client_android_au_1 . ","
						. $this->action_price_client_android_pc_1 . ","
						. $this->action_price_client_android_docomo_2 . ","
						. $this->action_price_client_android_softbank_2 . ","
						. $this->action_price_client_android_au_2 . ","
						. $this->action_price_client_android_pc_2 . ","
						. $this->action_price_client_android_docomo_3 . ","
						. $this->action_price_client_android_softbank_3 . ","
						. $this->action_price_client_android_au_3 . ","
						. $this->action_price_client_android_pc_3 . ","
						. $this->action_price_client_android_docomo_4 . ","
						. $this->action_price_client_android_softbank_4 . ","
						. $this->action_price_client_android_au_4 . ","
						. $this->action_price_client_android_pc_4 . ","
						. $this->action_price_client_android_docomo_5 . ","
						. $this->action_price_client_android_softbank_5 . ","
						. $this->action_price_client_android_au_5 . ","
						. $this->action_price_client_android_pc_5 . ","

						. $this->action_price_media_android_docomo_1 . ","
						. $this->action_price_media_android_softbank_1 . ","
						. $this->action_price_media_android_au_1 . ","
						. $this->action_price_media_android_pc_1 . ","
						. $this->action_price_media_android_docomo_2 . ","
						. $this->action_price_media_android_softbank_2 . ","
						. $this->action_price_media_android_au_2 . ","
						. $this->action_price_media_android_pc_2 . ","
						. $this->action_price_media_android_docomo_3 . ","
						. $this->action_price_media_android_softbank_3 . ","
						. $this->action_price_media_android_au_3 . ","
						. $this->action_price_media_android_pc_3 . ","
						. $this->action_price_media_android_docomo_4 . ","
						. $this->action_price_media_android_softbank_4 . ","
						. $this->action_price_media_android_au_4 . ","
						. $this->action_price_media_android_pc_4 . ","
						. $this->action_price_media_android_docomo_5 . ","
						. $this->action_price_media_android_softbank_5 . ","
						. $this->action_price_media_android_au_5 . ","
						. $this->action_price_media_android_pc_5 . ","

						. $this->action_price_client_pc_1 . ","
						. $this->action_price_client_pc_2 . ","
						. $this->action_price_client_pc_3 . ","
						. $this->action_price_client_pc_4 . ","
						. $this->action_price_client_pc_5 . ","

						. $this->action_price_media_pc_1 . ","
						. $this->action_price_media_pc_2 . ","
						. $this->action_price_media_pc_3 . ","
						. $this->action_price_media_pc_4 . ","
						. $this->action_price_media_pc_5 . ","

						. $this->approval_flag . ","
						. $this->price_type . ","

						. $this->ms_text_1 . ","
						. $this->ms_email_1 . ","
						. $this->ms_image_type_1 . ","
						. $this->ms_image_url_1 . ","
						. $this->ms_text_2 . ","
						. $this->ms_email_2 . ","
						. $this->ms_image_type_2 . ","
						. $this->ms_image_url_2 . ","
						. $this->ms_text_3 . ","
						. $this->ms_email_3 . ","
						. $this->ms_image_type_3 . ","
						. $this->ms_image_url_3 . ","
						. $this->ms_text_4 . ","
						. $this->ms_email_4 . ","
						. $this->ms_image_type_4 . ","
						. $this->ms_image_url_4 . ","
						. $this->ms_text_5 . ","
						. $this->ms_email_5 . ","
						. $this->ms_image_type_5 . ","
						. $this->ms_image_url_5 . ","
						. $this->unique_click_type . ","

						. $this->point_back_flag . ","
						. $this->adult_flag . ","
						. $this->seo_flag . ","
						. $this->listing_flag . ","
						. $this->mail_magazine_flag . ","
						. $this->community_flag . ","

						. $this->advert_start_date . ","
						. $this->advert_end_date . ","
						. $this->unrestraint_flag . ","
						. $this->test_flag . ","
						. $this->status . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// advert_client_id
	public function getAdvertClientId(){
		return $this->advert_client_id;
	}

	public function setAdvertClientId($val){
		$this->advert_client_id = $val;
	}

	// advert_category_id
	public function getAdvertCategoryId(){
		return $this->advert_category_id;
	}

	public function setAdvertCategoryId($val){
		$this->advert_category_id = $val;
	}

// -------------------------------------- 20111013 追加

	// support_iphone_docomo
	public function getSupportIphoneDocomo(){
		return $this->support_iphone_docomo;
	}

	public function setSupportIphoneDocomo($val){
		$this->support_iphone_docomo = $val;
	}

	// support_iphone_softbank
	public function getSupportIphoneSoftbank(){
		return $this->support_iphone_softbank;
	}

	public function setSupportIphoneSoftbank($val){
		$this->support_iphone_softbank = $val;
	}

	// support_iphone_au
	public function getSupportIphoneAu(){
		return $this->support_iphone_au;
	}

	public function setSupportIphoneAu($val){
		$this->support_iphone_au = $val;
	}

	// support_iphone_pc
	public function getSupportIphonePc(){
		return $this->support_iphone_pc;
	}

	public function setSupportIphonePc($val){
		$this->support_iphone_pc = $val;
	}

	// support_android_docomo
	public function getSupportAndroidDocomo(){
		return $this->support_android_docomo;
	}

	public function setSupportAndroidDocomo($val){
		$this->support_android_docomo = $val;
	}

	// support_android_softbank
	public function getSupportAndroidSoftbank(){
		return $this->support_android_softbank;
	}

	public function setSupportAndroidSoftbank($val){
		$this->support_android_softbank = $val;
	}

	// support_android_au
	public function getSupportAndroidAu(){
		return $this->support_android_au;
	}

	public function setSupportAndroidAu($val){
		$this->support_android_au = $val;
	}

	// support_android_pc
	public function getSupportAndroidPc(){
		return $this->support_android_pc;
	}

	public function setSupportAndroidPc($val){
		$this->support_android_pc = $val;
	}

	// support_pc
	public function getSupportPc(){
		return $this->support_pc;
	}

	public function setSupportPc($val){
		$this->support_pc = $val;
	}
// -------------------------------------- 20111013 追加 ここまで


	// advert_name
	public function getAdvertName(){
		return $this->advert_name;
	}

	public function setAdvertName($val){
		$this->advert_name = $val;
	}


// -------------------------------------- 20111013 追加

	// site_url_iphone_docomo
	public function getSiteUrlIphoneDocomo(){
		return $this->site_url_iphone_docomo;
	}

	public function setSiteUrlIphoneDocomo($val){
		$this->site_url_iphone_docomo = $val;
	}

	// site_url_iphone_softbank
	public function getSiteUrlIphoneSoftbank(){
		return $this->site_url_iphone_softbank;
	}

	public function setSiteUrlIphoneSoftbank($val){
		$this->site_url_iphone_softbank = $val;
	}

	// site_url_iphone_au
	public function getSiteUrlIphoneAu(){
		return $this->site_url_iphone_au;
	}

	public function setSiteUrlIphoneAu($val){
		$this->site_url_iphone_au = $val;
	}

	// site_url_iphone_pc
	public function getSiteUrlIphonePc(){
		return $this->site_url_iphone_pc;
	}

	public function setSiteUrlIphonePc($val){
		$this->site_url_iphone_pc = $val;
	}

	// site_url_android_docomo
	public function getSiteUrlAndroidDocomo(){
		return $this->site_url_android_docomo;
	}

	public function setSiteUrlAndroidDocomo($val){
		$this->site_url_android_docomo = $val;
	}

	// site_url_android_softbank
	public function getSiteUrlAndroidSoftbank(){
		return $this->site_url_android_softbank;
	}

	public function setSiteUrlAndroidSoftbank($val){
		$this->site_url_android_softbank = $val;
	}

	// site_url_android_au
	public function getSiteUrlAndroidAu(){
		return $this->site_url_android_au;
	}

	public function setSiteUrlAndroidAu($val){
		$this->site_url_android_au = $val;
	}

	// site_url_android_pc
	public function getSiteUrlAndroidPc(){
		return $this->site_url_android_pc;
	}

	public function setSiteUrlAndroidPc($val){
		$this->site_url_android_pc = $val;
	}

	// site_url_pc
	public function getSiteUrlPc(){
		return $this->site_url_pc;
	}

	public function setSiteUrlPc($val){
		$this->site_url_pc = $val;
	}

// -------------------------------------- 20111013 追加 ここまで


	// site_outline
	public function getSiteOutline(){
		return $this->site_outline;
	}

	public function setSiteOutline($val){
		$this->site_outline = $val;
	}

	// click_price_client
	public function getClickPriceClient(){
		return $this->click_price_client;
	}

	public function setClickPriceClient($val){
		$this->click_price_client = $val;
	}

	// click_price_media
	public function getClickPriceMedia(){
		return $this->click_price_media;
	}

	public function setClickPriceMedia($val){
		$this->click_price_media = $val;
	}


// -------------------------------------- 20111012 追加
	// iphoneクライアント
	// action_price_client_iphone_docomo_1
	public function getActionPriceClientIphoneDocomo1(){
		return $this->action_price_client_iphone_docomo_1;
	}

	public function setActionPriceClientIphoneDocomo1($val){
		$this->action_price_client_iphone_docomo_1 = $val;
	}

	// action_price_client_iphone_softbank_1
	public function getActionPriceClientIphoneSoftbank1(){
		return $this->action_price_client_iphone_softbank_1;
	}

	public function setActionPriceClientIphoneSoftbank1($val){
		$this->action_price_client_iphone_softbank_1 = $val;
	}

	// action_price_client_iphone_au_1
	public function getActionPriceClientIphoneAu1(){
		return $this->action_price_client_iphone_au_1;
	}

	public function setActionPriceClientIphoneAu1($val){
		$this->action_price_client_iphone_au_1 = $val;
	}

	// action_price_client_iphone_pc_1
	public function getActionPriceClientIphonePc1(){
		return $this->action_price_client_iphone_pc_1;
	}

	public function setActionPriceClientIphonePc1($val){
		$this->action_price_client_iphone_pc_1 = $val;
	}

	// action_price_client_iphone_docomo_2
	public function getActionPriceClientIphoneDocomo2(){
		return $this->action_price_client_iphone_docomo_2;
	}

	public function setActionPriceClientIphoneDocomo2($val){
		$this->action_price_client_iphone_docomo_2 = $val;
	}

	// action_price_client_iphone_softbank_2
	public function getActionPriceClientIphoneSoftbank2(){
		return $this->action_price_client_iphone_softbank_2;
	}

	public function setActionPriceClientIphoneSoftbank2($val){
		$this->action_price_client_iphone_softbank_2 = $val;
	}

	// action_price_client_iphone_au_2
	public function getActionPriceClientIphoneAu2(){
		return $this->action_price_client_iphone_au_2;
	}

	public function setActionPriceClientIphoneAu2($val){
		$this->action_price_client_iphone_au_2 = $val;
	}

	// action_price_client_iphone_pc_2
	public function getActionPriceClientIphonePc2(){
		return $this->action_price_client_iphone_pc_2;
	}

	public function setActionPriceClientIphonePc2($val){
		$this->action_price_client_iphone_pc_2 = $val;
	}

	// action_price_client_iphone_docomo_3
	public function getActionPriceClientIphoneDocomo3(){
		return $this->action_price_client_iphone_docomo_3;
	}

	public function setActionPriceClientIphoneDocomo3($val){
		$this->action_price_client_iphone_docomo_3 = $val;
	}

	// action_price_client_iphone_softbank_3
	public function getActionPriceClientIphoneSoftbank3(){
		return $this->action_price_client_iphone_softbank_3;
	}

	public function setActionPriceClientIphoneSoftbank3($val){
		$this->action_price_client_iphone_softbank_3 = $val;
	}

	// action_price_client_iphone_au_3
	public function getActionPriceClientIphoneAu3(){
		return $this->action_price_client_iphone_au_3;
	}

	public function setActionPriceClientIphoneAu3($val){
		$this->action_price_client_iphone_au_3 = $val;
	}

	// action_price_client_iphone_pc_3
	public function getActionPriceClientIphonePc3(){
		return $this->action_price_client_iphone_pc_3;
	}

	public function setActionPriceClientIphonePc3($val){
		$this->action_price_client_iphone_pc_3 = $val;
	}

	// action_price_client_iphone_docomo_4
	public function getActionPriceClientIphoneDocomo4(){
		return $this->action_price_client_iphone_docomo_4;
	}

	public function setActionPriceClientIphoneDocomo4($val){
		$this->action_price_client_iphone_docomo_4 = $val;
	}

	// action_price_client_iphone_softbank_4
	public function getActionPriceClientIphoneSoftbank4(){
		return $this->action_price_client_iphone_softbank_4;
	}

	public function setActionPriceClientIphoneSoftbank4($val){
		$this->action_price_client_iphone_softbank_4 = $val;
	}

	// action_price_client_iphone_au_4
	public function getActionPriceClientIphoneAu4(){
		return $this->action_price_client_iphone_au_4;
	}

	public function setActionPriceClientIphoneAu4($val){
		$this->action_price_client_iphone_au_4 = $val;
	}

	// action_price_client_iphone_pc_4
	public function getActionPriceClientIphonePc4(){
		return $this->action_price_client_iphone_pc_4;
	}

	public function setActionPriceClientIphonePc4($val){
		$this->action_price_client_iphone_pc_4 = $val;
	}

	// action_price_client_iphone_docomo_5
	public function getActionPriceClientIphoneDocomo5(){
		return $this->action_price_client_iphone_docomo_5;
	}

	public function setActionPriceClientIphoneDocomo5($val){
		$this->action_price_client_iphone_docomo_5 = $val;
	}

	// action_price_client_iphone_softbank_5
	public function getActionPriceClientIphoneSoftbank5(){
		return $this->action_price_client_iphone_softbank_5;
	}

	public function setActionPriceClientIphoneSoftbank5($val){
		$this->action_price_client_iphone_softbank_5 = $val;
	}

	// action_price_client_iphone_au_5
	public function getActionPriceClientIphoneAu5(){
		return $this->action_price_client_iphone_au_5;
	}

	public function setActionPriceClientIphoneAu5($val){
		$this->action_price_client_iphone_au_5 = $val;
	}

	// action_price_client_iphone_pc_5
	public function getActionPriceClientIphonePc5(){
		return $this->action_price_client_iphone_pc_5;
	}

	public function setActionPriceClientIphonePc5($val){
		$this->action_price_client_iphone_pc_5 = $val;
	}

	// iphoneメディア
	// action_price_media_iphone_docomo_1
	public function getActionPriceMediaIphoneDocomo1(){
		return $this->action_price_media_iphone_docomo_1;
	}

	public function setActionPriceMediaIphoneDocomo1($val){
		$this->action_price_media_iphone_docomo_1 = $val;
	}

	// action_price_media_iphone_softbank_1
	public function getActionPriceMediaIphoneSoftbank1(){
		return $this->action_price_media_iphone_softbank_1;
	}

	public function setActionPriceMediaIphoneSoftbank1($val){
		$this->action_price_media_iphone_softbank_1 = $val;
	}

	// action_price_media_iphone_au_1
	public function getActionPriceMediaIphoneAu1(){
		return $this->action_price_media_iphone_au_1;
	}

	public function setActionPriceMediaIphoneAu1($val){
		$this->action_price_media_iphone_au_1 = $val;
	}

	// action_price_media_iphone_pc_1
	public function getActionPriceMediaIphonePc1(){
		return $this->action_price_media_iphone_pc_1;
	}

	public function setActionPriceMediaIphonePc1($val){
		$this->action_price_media_iphone_pc_1 = $val;
	}

	// action_price_media_iphone_docomo_2
	public function getActionPriceMediaIphoneDocomo2(){
		return $this->action_price_media_iphone_docomo_2;
	}

	public function setActionPriceMediaIphoneDocomo2($val){
		$this->action_price_media_iphone_docomo_2 = $val;
	}

	// action_price_media_iphone_softbank_2
	public function getActionPriceMediaIphoneSoftbank2(){
		return $this->action_price_media_iphone_softbank_2;
	}

	public function setActionPriceMediaIphoneSoftbank2($val){
		$this->action_price_media_iphone_softbank_2 = $val;
	}

	// action_price_media_iphone_au_2
	public function getActionPriceMediaIphoneAu2(){
		return $this->action_price_media_iphone_au_2;
	}

	public function setActionPriceMediaIphoneAu2($val){
		$this->action_price_media_iphone_au_2 = $val;
	}

	// action_price_media_iphone_pc_2
	public function getActionPriceMediaIphonePc2(){
		return $this->action_price_media_iphone_pc_2;
	}

	public function setActionPriceMediaIphonePc2($val){
		$this->action_price_media_iphone_pc_2 = $val;
	}

	// action_price_media_iphone_docomo_3
	public function getActionPriceMediaIphoneDocomo3(){
		return $this->action_price_media_iphone_docomo_3;
	}

	public function setActionPriceMediaIphoneDocomo3($val){
		$this->action_price_media_iphone_docomo_3 = $val;
	}

	// action_price_media_iphone_softbank_3
	public function getActionPriceMediaIphoneSoftbank3(){
		return $this->action_price_media_iphone_softbank_3;
	}

	public function setActionPriceMediaIphoneSoftbank3($val){
		$this->action_price_media_iphone_softbank_3 = $val;
	}

	// action_price_media_iphone_au_3
	public function getActionPriceMediaIphoneAu3(){
		return $this->action_price_media_iphone_au_3;
	}

	public function setActionPriceMediaIphoneAu3($val){
		$this->action_price_media_iphone_au_3 = $val;
	}

	// action_price_media_iphone_pc_3
	public function getActionPriceMediaIphonePc3(){
		return $this->action_price_media_iphone_pc_3;
	}

	public function setActionPriceMediaIphonePc3($val){
		$this->action_price_media_iphone_pc_3 = $val;
	}

	// action_price_media_iphone_docomo_4
	public function getActionPriceMediaIphoneDocomo4(){
		return $this->action_price_media_iphone_docomo_4;
	}

	public function setActionPriceMediaIphoneDocomo4($val){
		$this->action_price_media_iphone_docomo_4 = $val;
	}

	// action_price_media_iphone_softbank_4
	public function getActionPriceMediaIphoneSoftbank4(){
		return $this->action_price_media_iphone_softbank_4;
	}

	public function setActionPriceMediaIphoneSoftbank4($val){
		$this->action_price_media_iphone_softbank_4 = $val;
	}

	// action_price_media_iphone_au_4
	public function getActionPriceMediaIphoneAu4(){
		return $this->action_price_media_iphone_au_4;
	}

	public function setActionPriceMediaIphoneAu4($val){
		$this->action_price_media_iphone_au_4 = $val;
	}

	// action_price_media_iphone_pc_4
	public function getActionPriceMediaIphonePc4(){
		return $this->action_price_media_iphone_pc_4;
	}

	public function setActionPriceMediaIphonePc4($val){
		$this->action_price_media_iphone_pc_4 = $val;
	}

	// action_price_media_iphone_docomo_5
	public function getActionPriceMediaIphoneDocomo5(){
		return $this->action_price_media_iphone_docomo_5;
	}

	public function setActionPriceMediaIphoneDocomo5($val){
		$this->action_price_media_iphone_docomo_5 = $val;
	}

	// action_price_media_iphone_softbank_5
	public function getActionPriceMediaIphoneSoftbank5(){
		return $this->action_price_media_iphone_softbank_5;
	}

	public function setActionPriceMediaIphoneSoftbank5($val){
		$this->action_price_media_iphone_softbank_5 = $val;
	}

	// action_price_media_iphone_au_5
	public function getActionPriceMediaIphoneAu5(){
		return $this->action_price_media_iphone_au_5;
	}

	public function setActionPriceMediaIphoneAu5($val){
		$this->action_price_media_iphone_au_5 = $val;
	}

	// action_price_media_iphone_pc_5
	public function getActionPriceMediaIphonePc5(){
		return $this->action_price_media_iphone_pc_5;
	}

	public function setActionPriceMediaIphonePc5($val){
		$this->action_price_media_iphone_pc_5 = $val;
	}

	// androidクライアント
	// action_price_client_android_docomo_1
	public function getActionPriceClientAndroidDocomo1(){
		return $this->action_price_client_android_docomo_1;
	}

	public function setActionPriceClientAndroidDocomo1($val){
		$this->action_price_client_android_docomo_1 = $val;
	}

	// action_price_client_android_softbank_1
	public function getActionPriceClientAndroidSoftbank1(){
		return $this->action_price_client_android_softbank_1;
	}

	public function setActionPriceClientAndroidSoftbank1($val){
		$this->action_price_client_android_softbank_1 = $val;
	}

	// action_price_client_android_au_1
	public function getActionPriceClientAndroidAu1(){
		return $this->action_price_client_android_au_1;
	}

	public function setActionPriceClientAndroidAu1($val){
		$this->action_price_client_android_au_1 = $val;
	}

	// action_price_client_android_pc_1
	public function getActionPriceClientAndroidPc1(){
		return $this->action_price_client_android_pc_1;
	}

	public function setActionPriceClientAndroidPc1($val){
		$this->action_price_client_android_pc_1 = $val;
	}

	// action_price_client_android_docomo_2
	public function getActionPriceClientAndroidDocomo2(){
		return $this->action_price_client_android_docomo_2;
	}

	public function setActionPriceClientAndroidDocomo2($val){
		$this->action_price_client_android_docomo_2 = $val;
	}

	// action_price_client_android_softbank_2
	public function getActionPriceClientAndroidSoftbank2(){
		return $this->action_price_client_android_softbank_2;
	}

	public function setActionPriceClientAndroidSoftbank2($val){
		$this->action_price_client_android_softbank_2 = $val;
	}

	// action_price_client_android_au_2
	public function getActionPriceClientAndroidAu2(){
		return $this->action_price_client_android_au_2;
	}

	public function setActionPriceClientAndroidAu2($val){
		$this->action_price_client_android_au_2 = $val;
	}

	// action_price_client_android_pc_2
	public function getActionPriceClientAndroidPc2(){
		return $this->action_price_client_android_pc_2;
	}

	public function setActionPriceClientAndroidPc2($val){
		$this->action_price_client_android_pc_2 = $val;
	}

	// action_price_client_android_docomo_3
	public function getActionPriceClientAndroidDocomo3(){
		return $this->action_price_client_android_docomo_3;
	}

	public function setActionPriceClientAndroidDocomo3($val){
		$this->action_price_client_android_docomo_3 = $val;
	}

	// action_price_client_android_softbank_3
	public function getActionPriceClientAndroidSoftbank3(){
		return $this->action_price_client_android_softbank_3;
	}

	public function setActionPriceClientAndroidSoftbank3($val){
		$this->action_price_client_android_softbank_3 = $val;
	}

	// action_price_client_android_au_3
	public function getActionPriceClientAndroidAu3(){
		return $this->action_price_client_android_au_3;
	}

	public function setActionPriceClientAndroidAu3($val){
		$this->action_price_client_android_au_3 = $val;
	}

	// action_price_client_android_pc_3
	public function getActionPriceClientAndroidPc3(){
		return $this->action_price_client_android_pc_3;
	}

	public function setActionPriceClientAndroidPc3($val){
		$this->action_price_client_android_pc_3 = $val;
	}

	// action_price_client_android_docomo_4
	public function getActionPriceClientAndroidDocomo4(){
		return $this->action_price_client_android_docomo_4;
	}

	public function setActionPriceClientAndroidDocomo4($val){
		$this->action_price_client_android_docomo_4 = $val;
	}

	// action_price_client_android_softbank_4
	public function getActionPriceClientAndroidSoftbank4(){
		return $this->action_price_client_android_softbank_4;
	}

	public function setActionPriceClientAndroidSoftbank4($val){
		$this->action_price_client_android_softbank_4 = $val;
	}

	// action_price_client_android_au_4
	public function getActionPriceClientAndroidAu4(){
		return $this->action_price_client_android_au_4;
	}

	public function setActionPriceClientAndroidAu4($val){
		$this->action_price_client_android_au_4 = $val;
	}

	// action_price_client_android_pc_4
	public function getActionPriceClientAndroidPc4(){
		return $this->action_price_client_android_pc_4;
	}

	public function setActionPriceClientAndroidPc4($val){
		$this->action_price_client_android_pc_4 = $val;
	}

	// action_price_client_android_docomo_5
	public function getActionPriceClientAndroidDocomo5(){
		return $this->action_price_client_android_docomo_5;
	}

	public function setActionPriceClientAndroidDocomo5($val){
		$this->action_price_client_android_docomo_5 = $val;
	}

	// action_price_client_android_softbank_5
	public function getActionPriceClientAndroidSoftbank5(){
		return $this->action_price_client_android_softbank_5;
	}

	public function setActionPriceClientAndroidSoftbank5($val){
		$this->action_price_client_android_softbank_5 = $val;
	}

	// action_price_client_android_au_5
	public function getActionPriceClientAndroidAu5(){
		return $this->action_price_client_android_au_5;
	}

	public function setActionPriceClientAndroidAu5($val){
		$this->action_price_client_android_au_5 = $val;
	}

	// action_price_client_android_pc_5
	public function getActionPriceClientAndroidPc5(){
		return $this->action_price_client_android_pc_5;
	}

	public function setActionPriceClientAndroidPc5($val){
		$this->action_price_client_android_pc_5 = $val;
	}

	// androidメディア
	// action_price_media_android_docomo_1
	public function getActionPriceMediaAndroidDocomo1(){
		return $this->action_price_media_android_docomo_1;
	}

	public function setActionPriceMediaAndroidDocomo1($val){
		$this->action_price_media_android_docomo_1 = $val;
	}

	// action_price_media_android_softbank_1
	public function getActionPriceMediaAndroidSoftbank1(){
		return $this->action_price_media_android_softbank_1;
	}

	public function setActionPriceMediaAndroidSoftbank1($val){
		$this->action_price_media_android_softbank_1 = $val;
	}

	// action_price_media_android_au_1
	public function getActionPriceMediaAndroidAu1(){
		return $this->action_price_media_android_au_1;
	}

	public function setActionPriceMediaAndroidAu1($val){
		$this->action_price_media_android_au_1 = $val;
	}

	// action_price_media_android_pc_1
	public function getActionPriceMediaAndroidPc1(){
		return $this->action_price_media_android_pc_1;
	}

	public function setActionPriceMediaAndroidPc1($val){
		$this->action_price_media_android_pc_1 = $val;
	}

	// action_price_media_android_docomo_2
	public function getActionPriceMediaAndroidDocomo2(){
		return $this->action_price_media_android_docomo_2;
	}

	public function setActionPriceMediaAndroidDocomo2($val){
		$this->action_price_media_android_docomo_2 = $val;
	}

	// action_price_media_android_softbank_2
	public function getActionPriceMediaAndroidSoftbank2(){
		return $this->action_price_media_android_softbank_2;
	}

	public function setActionPriceMediaAndroidSoftbank2($val){
		$this->action_price_media_android_softbank_2 = $val;
	}

	// action_price_media_android_au_2
	public function getActionPriceMediaAndroidAu2(){
		return $this->action_price_media_android_au_2;
	}

	public function setActionPriceMediaAndroidAu2($val){
		$this->action_price_media_android_au_2 = $val;
	}

	// action_price_media_android_pc_2
	public function getActionPriceMediaAndroidPc2(){
		return $this->action_price_media_android_pc_2;
	}

	public function setActionPriceMediaAndroidPc2($val){
		$this->action_price_media_android_pc_2 = $val;
	}

	// action_price_media_android_docomo_3
	public function getActionPriceMediaAndroidDocomo3(){
		return $this->action_price_media_android_docomo_3;
	}

	public function setActionPriceMediaAndroidDocomo3($val){
		$this->action_price_media_android_docomo_3 = $val;
	}

	// action_price_media_android_softbank_3
	public function getActionPriceMediaAndroidSoftbank3(){
		return $this->action_price_media_android_softbank_3;
	}

	public function setActionPriceMediaAndroidSoftbank3($val){
		$this->action_price_media_android_softbank_3 = $val;
	}

	// action_price_media_android_au_3
	public function getActionPriceMediaAndroidAu3(){
		return $this->action_price_media_android_au_3;
	}

	public function setActionPriceMediaAndroidAu3($val){
		$this->action_price_media_android_au_3 = $val;
	}

	// action_price_media_android_pc_3
	public function getActionPriceMediaAndroidPc3(){
		return $this->action_price_media_android_pc_3;
	}

	public function setActionPriceMediaAndroidPc3($val){
		$this->action_price_media_android_pc_3 = $val;
	}

	// action_price_media_android_docomo_4
	public function getActionPriceMediaAndroidDocomo4(){
		return $this->action_price_media_android_docomo_4;
	}

	public function setActionPriceMediaAndroidDocomo4($val){
		$this->action_price_media_android_docomo_4 = $val;
	}

	// action_price_media_android_softbank_4
	public function getActionPriceMediaAndroidSoftbank4(){
		return $this->action_price_media_android_softbank_4;
	}

	public function setActionPriceMediaAndroidSoftbank4($val){
		$this->action_price_media_android_softbank_4 = $val;
	}

	// action_price_media_android_au_4
	public function getActionPriceMediaAndroidAu4(){
		return $this->action_price_media_android_au_4;
	}

	public function setActionPriceMediaAndroidAu4($val){
		$this->action_price_media_android_au_4 = $val;
	}

	// action_price_media_android_pc_4
	public function getActionPriceMediaAndroidPc4(){
		return $this->action_price_media_android_pc_4;
	}

	public function setActionPriceMediaAndroidPc4($val){
		$this->action_price_media_android_pc_4 = $val;
	}

	// action_price_media_android_docomo_5
	public function getActionPriceMediaAndroidDocomo5(){
		return $this->action_price_media_android_docomo_5;
	}

	public function setActionPriceMediaAndroidDocomo5($val){
		$this->action_price_media_android_docomo_5 = $val;
	}

	// action_price_media_android_softbank_5
	public function getActionPriceMediaAndroidSoftbank5(){
		return $this->action_price_media_android_softbank_5;
	}

	public function setActionPriceMediaAndroidSoftbank5($val){
		$this->action_price_media_android_softbank_5 = $val;
	}

	// action_price_media_android_au_5
	public function getActionPriceMediaAndroidAu5(){
		return $this->action_price_media_android_au_5;
	}

	public function setActionPriceMediaAndroidAu5($val){
		$this->action_price_media_android_au_5 = $val;
	}

	// action_price_media_android_pc_5
	public function getActionPriceMediaAndroidPc5(){
		return $this->action_price_media_android_pc_5;
	}

	public function setActionPriceMediaAndroidPc5($val){
		$this->action_price_media_android_pc_5 = $val;
	}

// -------------------------------------- 20111012 追加ここまで

	// action_price_client_pc_1
	public function getActionPriceClientPc1(){
		return $this->action_price_client_pc_1;
	}

	public function setActionPriceClientPc1($val){
		$this->action_price_client_pc_1 = $val;
	}

	// action_price_client_pc_2
	public function getActionPriceClientPc2(){
		return $this->action_price_client_pc_2;
	}

	public function setActionPriceClientPc2($val){
		$this->action_price_client_pc_2 = $val;
	}

	// action_price_client_pc_3
	public function getActionPriceClientPc3(){
		return $this->action_price_client_pc_3;
	}

	public function setActionPriceClientPc3($val){
		$this->action_price_client_pc_3 = $val;
	}

	// action_price_client_pc_4
	public function getActionPriceClientPc4(){
		return $this->action_price_client_pc_4;
	}

	public function setActionPriceClientPc4($val){
		$this->action_price_client_pc_4 = $val;
	}

	// action_price_client_pc_5
	public function getActionPriceClientPc5(){
		return $this->action_price_client_pc_5;
	}

	public function setActionPriceClientPc5($val){
		$this->action_price_client_pc_5 = $val;
	}

	// action_price_media_pc_1
	public function getActionPriceMediaPc1(){
		return $this->action_price_media_pc_1;
	}

	public function setActionPriceMediaPc1($val){
		$this->action_price_media_pc_1 = $val;
	}

	// action_price_media_pc_2
	public function getActionPriceMediaPc2(){
		return $this->action_price_media_pc_2;
	}

	public function setActionPriceMediaPc2($val){
		$this->action_price_media_pc_2 = $val;
	}

	// action_price_media_pc_3
	public function getActionPriceMediaPc3(){
		return $this->action_price_media_pc_3;
	}

	public function setActionPriceMediaPc3($val){
		$this->action_price_media_pc_3 = $val;
	}

	// action_price_media_pc_4
	public function getActionPriceMediaPc4(){
		return $this->action_price_media_pc_4;
	}

	public function setActionPriceMediaPc4($val){
		$this->action_price_media_pc_4 = $val;
	}

	// action_price_media_pc_5
	public function getActionPriceMediaPc5(){
		return $this->action_price_media_pc_5;
	}

	public function setActionPriceMediaPc5($val){
		$this->action_price_media_pc_5 = $val;
	}

	// approval_flag
	public function getApprovalFlag(){
		return $this->approval_flag;
	}

	public function setApprovalFlag($val){
		$this->approval_flag = $val;
	}

	// price_type
	public function getPriceType(){
		return $this->price_type;
	}

	public function setPriceType($val){
		$this->price_type = $val;
	}

	// ms_text_1
	public function getMsText1(){
		return $this->ms_text_1;
	}

	public function setMsText1($val){
		$this->ms_text_1 = $val;
	}

	// ms_email_1
	public function getMsEmail1(){
		return $this->ms_email_1;
	}

	public function setMsEmail1($val){
		$this->ms_email_1 = $val;
	}

	// ms_image_type_1
	public function getMsImageType1(){
		return $this->ms_image_type_1;
	}

	public function setMsImageType1($val){
		$this->ms_image_type_1 = $val;
	}

	// ms_image_url_1
	public function getMsImageUrl1(){
		return $this->ms_image_url_1;
	}

	public function setMsImageUrl1($val){
		$this->ms_image_url_1 = $val;
	}

	// ms_text_2
	public function getMsText2(){
		return $this->ms_text_2;
	}

	public function setMsText2($val){
		$this->ms_text_2 = $val;
	}

	// ms_email_2
	public function getMsEmail2(){
		return $this->ms_email_2;
	}

	public function setMsEmail2($val){
		$this->ms_email_2 = $val;
	}

	// ms_image_type_2
	public function getMsImageType2(){
		return $this->ms_image_type_2;
	}

	public function setMsImageType2($val){
		$this->ms_image_type_2 = $val;
	}

	// ms_image_url_2
	public function getMsImageUrl2(){
		return $this->ms_image_url_2;
	}

	public function setMsImageUrl2($val){
		$this->ms_image_url_2 = $val;
	}

	// ms_text_3
	public function getMsText3(){
		return $this->ms_text_3;
	}

	public function setMsText3($val){
		$this->ms_text_3 = $val;
	}

	// ms_email_3
	public function getMsEmail3(){
		return $this->ms_email_3;
	}

	public function setMsEmail3($val){
		$this->ms_email_3 = $val;
	}

	// ms_image_type_3
	public function getMsImageType3(){
		return $this->ms_image_type_3;
	}

	public function setMsImageType3($val){
		$this->ms_image_type_3 = $val;
	}

	// ms_image_url_3
	public function getMsImageUrl3(){
		return $this->ms_image_url_3;
	}

	public function setMsImageUrl3($val){
		$this->ms_image_url_3 = $val;
	}

	// ms_text_4
	public function getMsText4(){
		return $this->ms_text_4;
	}

	public function setMsText4($val){
		$this->ms_text_4 = $val;
	}

	// ms_email_4
	public function getMsEmail4(){
		return $this->ms_email_4;
	}

	public function setMsEmail4($val){
		$this->ms_email_4 = $val;
	}

	// ms_image_type_4
	public function getMsImageType4(){
		return $this->ms_image_type_4;
	}

	public function setMsImageType4($val){
		$this->ms_image_type_4 = $val;
	}

	// ms_image_url_4
	public function getMsImageUrl4(){
		return $this->ms_image_url_4;
	}

	public function setMsImageUrl4($val){
		$this->ms_image_url_4 = $val;
	}

	// ms_text_5
	public function getMsText5(){
		return $this->ms_text_5;
	}

	public function setMsText5($val){
		$this->ms_text_5 = $val;
	}

	// ms_email_5
	public function getMsEmail5(){
		return $this->ms_email_5;
	}

	public function setMsEmail5($val){
		$this->ms_email_5 = $val;
	}

	// ms_image_type_5
	public function getMsImageType5(){
		return $this->ms_image_type_5;
	}

	public function setMsImageType5($val){
		$this->ms_image_type_5 = $val;
	}

	// ms_image_url_5
	public function getMsImageUrl5(){
		return $this->ms_image_url_5;
	}

	public function setMsImageUrl5($val){
		$this->ms_image_url_5 = $val;
	}

	// unique_click_type
	public function getUniqueClickType(){
		return $this->unique_click_type;
	}

	public function setUniqueClickType($val){
		$this->unique_click_type = $val;
	}

	// point_back_flag
	public function getPointBackFlag(){
		return $this->point_back_flag;
	}

	public function setPointBackFlag($val){
		$this->point_back_flag = $val;
	}

	// adult_flag
	public function getAdultFlag(){
		return $this->adult_flag;
	}

	public function setAdultFlag($val){
		$this->adult_flag = $val;
	}

	// seo_flag
	public function getSeoFlag(){
		return $this->seo_flag;
	}

	public function setSeoFlag($val){
		$this->seo_flag = $val;
	}

	// listing_flag
	public function getListingFlag(){
		return $this->listing_flag;
	}

	public function setListingFlag($val){
		$this->listing_flag = $val;
	}

	// mail_magazine_flag
	public function getMailMagazineFlag(){
		return $this->mail_magazine_flag;
	}

	public function setMailMagazineFlag($val){
		$this->mail_magazine_flag = $val;
	}

	// community_flag
	public function getCommunityFlag(){
		return $this->community_flag;
	}

	public function setCommunityFlag($val){
		$this->community_flag = $val;
	}

	// advert_start_date
	public function getAdvertStartDate(){
		return $this->advert_start_date;
	}

	public function setAdvertStartDate($val){
		$this->advert_start_date = $val;
	}

	// advert_end_date
	public function getAdvertEndDate(){
		return $this->advert_end_date;
	}

	public function setAdvertEndDate($val){
		$this->advert_end_date = $val;
	}

	// unrestraint_flag
	public function getUnrestraintFlag(){
		return $this->unrestraint_flag;
	}

	public function setUnrestraintFlag($val){
		$this->unrestraint_flag = $val;
	}

	// test_flag
	public function getTestFlag(){
		return $this->test_flag;
	}

	public function setTestFlag($val){
		$this->test_flag = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}