<?php
class AdvertReserve {

	// プロパティ
	private $id = "";
	private $advert_id = "";
	private $site_url_pc = "";
	private $click_price_client = "";
	private $click_price_media = "";

	private $action_price_client_pc_1 = "";
	private $action_price_client_pc_2 = "";
	private $action_price_client_pc_3 = "";
	private $action_price_client_pc_4 = "";
	private $action_price_client_pc_5 = "";


	private $action_price_media_pc_1 = "";
	private $action_price_media_pc_2 = "";
	private $action_price_media_pc_3 = "";
	private $action_price_media_pc_4 = "";
	private $action_price_media_pc_5 = "";
	private $ms_text_1 = "";
	private $ms_email_1 = "";
	private $ms_image_type_1 = "";
	private $ms_image_url_1 = "";
	private $ms_text_2 = "";
	private $ms_email_2 = "";
	private $ms_image_type_2 = "";
	private $ms_image_url_2 = "";
	private $ms_text_3 = "";
	private $ms_email_3 = "";
	private $ms_image_type_3 = "";
	private $ms_image_url_3 = "";
	private $ms_text_4 = "";
	private $ms_email_4 = "";
	private $ms_image_type_4 = "";
	private $ms_image_url_4 = "";
	private $ms_text_5 = "";
	private $ms_email_5 = "";
	private $ms_image_type_5 = "";
	private $ms_image_url_5 = "";
	private $reserve_change_date = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->advert_id . ","

						. $this->site_url_pc . ","
						. $this->click_price_client . ","
						. $this->click_price_media . ","

						. $this->action_price_client_pc_1 . ","
						. $this->action_price_client_pc_2 . ","
						. $this->action_price_client_pc_3 . ","
						. $this->action_price_client_pc_4 . ","
						. $this->action_price_client_pc_5 . ","

						. $this->action_price_media_pc_1 . ","
						. $this->action_price_media_pc_2 . ","
						. $this->action_price_media_pc_3 . ","
						. $this->action_price_media_pc_4 . ","
						. $this->action_price_media_pc_5 . ","
						. $this->ms_text_1 . ","
						. $this->ms_email_1 . ","
						. $this->ms_image_type_1 . ","
						. $this->ms_image_url_1 . ","
						. $this->ms_text_2 . ","
						. $this->ms_email_2 . ","
						. $this->ms_image_type_2 . ","
						. $this->ms_image_url_2 . ","
						. $this->ms_text_3 . ","
						. $this->ms_email_3 . ","
						. $this->ms_image_type_3 . ","
						. $this->ms_image_url_3 . ","
						. $this->ms_text_4 . ","
						. $this->ms_email_4 . ","
						. $this->ms_image_type_4 . ","
						. $this->ms_image_url_4 . ","
						. $this->ms_text_5 . ","
						. $this->ms_email_5 . ","
						. $this->ms_image_type_5 . ","
						. $this->ms_image_url_5 . ","
						. $this->reserve_change_date . ","
						. $this->status . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}

	public function setAdvertId($val){
		$this->advert_id = $val;
	}

	// site_url_pc
	public function getSiteUrlPc(){
		return $this->site_url_pc;
	}

	public function setSiteUrlPc($val){
		$this->site_url_pc = $val;
	}

	// click_price_client
	public function getClickPriceClient(){
		return $this->click_price_client;
	}

	public function setClickPriceClient($val){
		$this->click_price_client = $val;
	}

	// click_price_media
	public function getClickPriceMedia(){
		return $this->click_price_media;
	}

	public function setClickPriceMedia($val){
		$this->click_price_media = $val;
	}

	// action_price_client_pc_1
	public function getActionPriceClientPc1(){
		return $this->action_price_client_pc_1;
	}

	public function setActionPriceClientPc1($val){
		$this->action_price_client_pc_1 = $val;
	}

	// action_price_client_pc_2
	public function getActionPriceClientPc2(){
		return $this->action_price_client_pc_2;
	}

	public function setActionPriceClientPc2($val){
		$this->action_price_client_pc_2 = $val;
	}

	// action_price_client_pc_3
	public function getActionPriceClientPc3(){
		return $this->action_price_client_pc_3;
	}

	public function setActionPriceClientPc3($val){
		$this->action_price_client_pc_3 = $val;
	}

	// action_price_client_pc_4
	public function getActionPriceClientPc4(){
		return $this->action_price_client_pc_4;
	}

	public function setActionPriceClientPc4($val){
		$this->action_price_client_pc_4 = $val;
	}

	// action_price_client_pc_5
	public function getActionPriceClientPc5(){
		return $this->action_price_client_pc_5;
	}

	public function setActionPriceClientPc5($val){
		$this->action_price_client_pc_5 = $val;
	}

	// action_price_media_pc_1
	public function getActionPriceMediaPc1(){
		return $this->action_price_media_pc_1;
	}

	public function setActionPriceMediaPc1($val){
		$this->action_price_media_pc_1 = $val;
	}

	// action_price_media_pc_2
	public function getActionPriceMediaPc2(){
		return $this->action_price_media_pc_2;
	}

	public function setActionPriceMediaPc2($val){
		$this->action_price_media_pc_2 = $val;
	}

	// action_price_media_pc_3
	public function getActionPriceMediaPc3(){
		return $this->action_price_media_pc_3;
	}

	public function setActionPriceMediaPc3($val){
		$this->action_price_media_pc_3 = $val;
	}

	// action_price_media_pc_4
	public function getActionPriceMediaPc4(){
		return $this->action_price_media_pc_4;
	}

	public function setActionPriceMediaPc4($val){
		$this->action_price_media_pc_4 = $val;
	}

	// action_price_media_pc_5
	public function getActionPriceMediaPc5(){
		return $this->action_price_media_pc_5;
	}

	public function setActionPriceMediaPc5($val){
		$this->action_price_media_pc_5 = $val;
	}

	// ms_text_1
	public function getMsText1(){
		return $this->ms_text_1;
	}

	public function setMsText1($val){
		$this->ms_text_1 = $val;
	}

	// ms_email_1
	public function getMsEmail1(){
		return $this->ms_email_1;
	}

	public function setMsEmail1($val){
		$this->ms_email_1 = $val;
	}

	// ms_image_type_1
	public function getMsImageType1(){
		return $this->ms_image_type_1;
	}

	public function setMsImageType1($val){
		$this->ms_image_type_1 = $val;
	}

	// ms_image_url_1
	public function getMsImageUrl1(){
		return $this->ms_image_url_1;
	}

	public function setMsImageUrl1($val){
		$this->ms_image_url_1 = $val;
	}

	// ms_text_2
	public function getMsText2(){
		return $this->ms_text_2;
	}

	public function setMsText2($val){
		$this->ms_text_2 = $val;
	}

	// ms_email_2
	public function getMsEmail2(){
		return $this->ms_email_2;
	}

	public function setMsEmail2($val){
		$this->ms_email_2 = $val;
	}

	// ms_image_type_2
	public function getMsImageType2(){
		return $this->ms_image_type_2;
	}

	public function setMsImageType2($val){
		$this->ms_image_type_2 = $val;
	}

	// ms_image_url_2
	public function getMsImageUrl2(){
		return $this->ms_image_url_2;
	}

	public function setMsImageUrl2($val){
		$this->ms_image_url_2 = $val;
	}

	// ms_text_3
	public function getMsText3(){
		return $this->ms_text_3;
	}

	public function setMsText3($val){
		$this->ms_text_3 = $val;
	}

	// ms_email_3
	public function getMsEmail3(){
		return $this->ms_email_3;
	}

	public function setMsEmail3($val){
		$this->ms_email_3 = $val;
	}

	// ms_image_type_3
	public function getMsImageType3(){
		return $this->ms_image_type_3;
	}

	public function setMsImageType3($val){
		$this->ms_image_type_3 = $val;
	}

	// ms_image_url_3
	public function getMsImageUrl3(){
		return $this->ms_image_url_3;
	}

	public function setMsImageUrl3($val){
		$this->ms_image_url_3 = $val;
	}

	// ms_text_4
	public function getMsText4(){
		return $this->ms_text_4;
	}

	public function setMsText4($val){
		$this->ms_text_4 = $val;
	}

	// ms_email_4
	public function getMsEmail4(){
		return $this->ms_email_4;
	}

	public function setMsEmail4($val){
		$this->ms_email_4 = $val;
	}

	// ms_image_type_4
	public function getMsImageType4(){
		return $this->ms_image_type_4;
	}

	public function setMsImageType4($val){
		$this->ms_image_type_4 = $val;
	}

	// ms_image_url_4
	public function getMsImageUrl4(){
		return $this->ms_image_url_4;
	}

	public function setMsImageUrl4($val){
		$this->ms_image_url_4 = $val;
	}

	// ms_text_5
	public function getMsText5(){
		return $this->ms_text_5;
	}

	public function setMsText5($val){
		$this->ms_text_5 = $val;
	}

	// ms_email_5
	public function getMsEmail5(){
		return $this->ms_email_5;
	}

	public function setMsEmail5($val){
		$this->ms_email_5 = $val;
	}

	// ms_image_type_5
	public function getMsImageType5(){
		return $this->ms_image_type_5;
	}

	public function setMsImageType5($val){
		$this->ms_image_type_5 = $val;
	}

	// ms_image_url_5
	public function getMsImageUrl5(){
		return $this->ms_image_url_5;
	}

	public function setMsImageUrl5($val){
		$this->ms_image_url_5 = $val;
	}

	// reserve_change_date
	public function getReserveChangeDate(){
		return $this->reserve_change_date;
	}

	public function setReserveChangeDate($val){
		$this->reserve_change_date = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}