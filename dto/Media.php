<?php
class Media {

	// プロパティ
	private $id = "";
	private $media_publisher_id = "";
	private $media_category_id = "";
	private $media_name = "";
	private $site_url_iphone = "";
	private $site_url_android = "";
	private $site_url_pc = "";
	private $media_type = "";
	private $page_view_day = "";
	private $site_outline = "";
	private $response_type = "";
	private $point_back_url = "";
	private $point_test_url = "";
	private $test_flag = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						 . $this->media_publisher_id . ","
						 . $this->media_category_id . ","
						 . $this->media_name . ","
						 . $this->site_url_iphone . ","
						 . $this->site_url_android . ","
						 . $this->site_url_pc . ","
						 . $this->media_type . ","
						 . $this->page_view_day . ","
						 . $this->site_outline . ","
						 . $this->response_type . ","
						 . $this->point_back_url . ","
						 . $this->point_test_url . ","
						 . $this->test_flag . ","
						 . $this->status . ","
						 . $this->created_at . ","
						 . $this->updated_at . ","
						 . $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// media_publisher_id
	public function getMediaPublisherId(){
		return $this->media_publisher_id;
	}

	public function setMediaPublisherId($val){
		$this->media_publisher_id = $val;
	}

	// media_category_id
	public function getMediaCategoryId(){
		return $this->media_category_id;
	}

	public function setMediaCategoryId($val){
		$this->media_category_id = $val;
	}

	// media_name
	public function getMediaName(){
		return $this->media_name;
	}

	public function setMediaName($val){
		$this->media_name = $val;
	}

	// site_url_iphone
	public function getSiteUrlIphone(){
		return $this->site_url_iphone;
	}

	public function setSiteUrlIphone($val){
		$this->site_url_iphone = $val;
	}

	// site_url_android
	public function getSiteUrlAndroid(){
		return $this->site_url_android;
	}

	public function setSiteUrlAndroid($val){
		$this->site_url_android = $val;
	}

	// site_url_pc
	public function getSiteUrlPc(){
		return $this->site_url_pc;
	}

	public function setSiteUrlPc($val){
		$this->site_url_pc = $val;
	}

	// media_type
	public function getMediaType(){
		return $this->media_type;
	}

	public function setMediaType($val){
		$this->media_type = $val;
	}

	// page_view_day
	public function getPageViewDay(){
		return $this->page_view_day;
	}

	public function setPageViewDay($val){
		$this->page_view_day = $val;
	}

	// site_outline
	public function getSiteOutline(){
		return $this->site_outline;
	}

	public function setSiteOutline($val){
		$this->site_outline = $val;
	}

	// response_type
	public function getResponseType(){
		return $this->response_type;
	}

	public function setResponseType($val){
		$this->response_type = $val;
	}

	// point_back_url
	public function getPointBackUrl(){
		return $this->point_back_url;
	}

	public function setPointBackUrl($val){
		$this->point_back_url = $val;
	}

	// point_test_url
	public function getPointTestUrl(){
		return $this->point_test_url;
	}

	public function setPointTestUrl($val){
		$this->point_test_url = $val;
	}

	// test_flag
	public function getTestFlag(){
		return $this->test_flag;
	}

	public function setTestFlag($val){
		$this->test_flag = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}