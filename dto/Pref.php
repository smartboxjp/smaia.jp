<?php
class Pref {

	// プロパティ
	private $id = "";
	private $name = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . "," . $this->name);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// name
	public function getName(){
		return $this->name;
	}

	public function setName($val){
		$this->name = $val;
	}

}