<?php
class ActionLog {

	// プロパティ
	private $id = "";
	private $session_id = "";
	private $carrier_id = "";
	private $user_agent = "";
	private $uid = "";
	private $browser_type = "";
	private $ip_address = "";
	private $host_name = "";
	private $media_id = "";
	private $media_publisher_id = "";
	private $advert_id = "";
	private $advert_client_id = "";
	private $click_price_client = "";
	private $click_price_media = "";
	private $action_price_client = "";
	private $action_price_media = "";

	private $approval_flag = "";
	private $price_type = "";
	private $order_num = "";

	private $link_url = "";
	private $action_complete_date = "";
	private $confirm_flag = "";
	private $point_back_parameter = "";
	private $point_back_url = "";
	private $status = "";
//---------------------------------------------------
//7/21 追加
	private $auid = "";
//---------------------------------------------------
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->session_id . ","
						. $this->carrier_id . ","
						. $this->user_agent . ","
						. $this->uid . ","
						. $this->browser_type . ","
						. $this->ip_address . ","
						. $this->host_name . ","
						. $this->media_id . ","
						. $this->media_publisher_id . ","
						. $this->advert_id . ","
						. $this->advert_client_id . ","
						. $this->click_price_client . ","
						. $this->click_price_media . ","
						. $this->action_price_client . ","
						. $this->action_price_media . ","

						. $this->approval_flag . ","
						. $this->price_type . ","
						. $this->order_num . ","

						. $this->link_url . ","
						. $this->action_complete_date . ","
						. $this->confirm_flag . ","
						. $this->point_back_parameter . ","
						. $this->point_back_url . ","
						. $this->status . ","
//---------------------------------------------------
	//7/21 追加
						. $this->auid . ","
//---------------------------------------------------

						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// session_id
	public function getSessionId(){
		return $this->session_id;
	}

	public function setSessionId($val){
		$this->session_id = $val;
	}

	// carrier_id
	public function getCarrierId(){
		return $this->carrier_id;
	}

	public function setCarrierId($val){
		$this->carrier_id = $val;
	}

	// user_agent
	public function getUserAgent(){
		return $this->user_agent;
	}

	public function setUserAgent($val){
		$this->user_agent = $val;
	}

	// uid
	public function getUid(){
		return $this->uid;
	}

	public function setUid($val){
		$this->uid = $val;
	}

	// uid
	public function getBrowserType(){
		return $this->browser_type;
	}

	public function setBrowserType($val){
		$this->browser_type = $val;
	}

	// ip_address
	public function getIpAddress(){
		return $this->ip_address;
	}

	public function setIpAddress($val){
		$this->ip_address = $val;
	}

	// host_name
	public function getHostName(){
		return $this->host_name;
	}

	public function setHostName($val){
		$this->host_name = $val;
	}

	// media_id
	public function getMediaId(){
		return $this->media_id;
	}

	public function setMediaId($val){
		$this->media_id = $val;
	}

	// media_publisher_id
	public function getMediaPublisherId(){
		return $this->media_publisher_id;
	}

	public function setMediaPublisherId($val){
		$this->media_publisher_id = $val;
	}

	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}

	public function setAdvertId($val){
		$this->advert_id = $val;
	}

	// advert_client_id
	public function getAdvertClientId(){
		return $this->advert_client_id;
	}

	public function setAdvertClientId($val){
		$this->advert_client_id = $val;
	}

	// click_price_client
	public function getClickPriceClient(){
		return $this->click_price_client;
	}

	public function setClickPriceClient($val){
		$this->click_price_client = $val;
	}

	// click_price_media
	public function getClickPriceMedia(){
		return $this->click_price_media;
	}

	public function setClickPriceMedia($val){
		$this->click_price_media = $val;
	}

	// action_price_client
	public function getActionPriceClient(){
		return $this->action_price_client;
	}

	public function setActionPriceClient($val){
		$this->action_price_client = $val;
	}

	// action_price_media
	public function getActionPriceMedia(){
		return $this->action_price_media;
	}

	public function setActionPriceMedia($val){
		$this->action_price_media = $val;
	}

// -------------------------------------------------- 2011/10/11

	// approval_flag
	public function getApprovalFlag(){
		return $this->approval_flag;
	}

	public function setApprovalFlag($val){
		$this->approval_flag = $val;
	}

	// price_type
	public function getPriceType(){
		return $this->price_type;
	}

	public function setPriceType($val){
		$this->price_type = $val;
	}

	// order_num
	public function getOrderNum(){
		return $this->order_num;
	}

	public function setOrderNum($val){
		$this->order_num = $val;
	}
// -------------------------------------------------------------

	// link_url
	public function getLinkUrl(){
		return $this->link_url;
	}

	public function setLinkUrl($val){
		$this->link_url = $val;
	}

	// action_complete_date
	public function getActionCompleteDate(){
		return $this->action_complete_date;
	}

	public function setActionCompleteDate($val){
		$this->action_complete_date = $val;
	}

	// confirm_flag
	public function getConfirmFlag(){
		return $this->confirm_flag;
	}

	public function setConfirmFlag($val){
		$this->confirm_flag = $val;
	}

	// point_back_parameter
	public function getPointBackParameter(){
		return $this->point_back_parameter;
	}

	public function setPointBackParameter($val){
		$this->point_back_parameter = $val;
	}

	// point_back_url
	public function getPointBackUrl(){
		return $this->point_back_url;
	}

	public function setPointBackUrl($val){
		$this->point_back_url = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}
//-------------------------------------------
	//7/22 追加
	public function getAuid(){
		return $this->auid;
	}

	public function setAuid($val){
		$this->auid = $val;
	}
//-------------------------------------------
	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}