<?php
class Asp {

	// プロパティ
	private $id = "";
	private $asp_name = "";
	private $company_name = "";
	private $contact_person = "";
	private $tel = "";
	private $fax = "";
	private $email = "";
	private $zipcode1 = "";
	private $zipcode2 = "";
	private $pref = "";
	private $address1 = "";
	private $address2 = "";
	private $transfer_type = "";
	private $bank_name = "";
	private $branch_name = "";
	private $account_type = "";
	private $account_holder = "";
	private $account_number = "";
	private $postal_account_holder = "";
	private $postal_account_number = "";
	private $result_action_url = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						 . $this->asp_name . ","
						 . $this->company_name . ","
						 . $this->contact_person . ","
						 . $this->tel . ","
						 . $this->fax . ","
						 . $this->email . ","
						 . $this->zipcode1 . ","
						 . $this->zipcode2 . ","
						 . $this->pref . ","
						 . $this->address1 . ","
						 . $this->address2 . ","
						 . $this->transfer_type . ","
						 . $this->bank_name . ","
						 . $this->branch_name . ","
						 . $this->account_type . ","
						 . $this->account_holder . ","
						 . $this->account_number . ","
						 . $this->postal_account_holder . ","
						 . $this->postal_account_number . ","
						 . $this->result_action_url . ","
						 . $this->status . ","
						 . $this->created_at . ","
						 . $this->updated_at . ","
						 . $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// asp_name
	public function getAspName(){
		return $this->asp_name;
	}

	public function setAspName($val){
		$this->asp_name = $val;
	}

	// company_name
	public function getCompanyName(){
		return $this->company_name;
	}

	public function setCompanyName($val){
		$this->company_name = $val;
	}

	// contact_person
	public function getContactPerson(){
		return $this->contact_person;
	}

	public function setContactPerson($val){
		$this->contact_person = $val;
	}

	// tel
	public function getTel(){
		return $this->tel;
	}

	public function setTel($val){
		$this->tel = $val;
	}

	// fax
	public function getFax(){
		return $this->fax;
	}

	public function setFax($val){
		$this->fax = $val;
	}

	// email
	public function getEmail(){
		return $this->email;
	}

	public function setEmail($val){
		$this->email = $val;
	}

	// zipcode1
	public function getZipcode1(){
		return $this->zipcode1;
	}

	public function setZipcode1($val){
		$this->zipcode1 = $val;
	}

	// zipcode2
	public function getZipcode2(){
		return $this->zipcode2;
	}

	public function setZipcode2($val){
		$this->zipcode2 = $val;
	}

	// pref
	public function getPref(){
		return $this->pref;
	}

	public function setPref($val){
		$this->pref = $val;
	}

	// address1
	public function getAddress1(){
		return $this->address1;
	}

	public function setAddress1($val){
		$this->address1 = $val;
	}

	// address2
	public function getAddress2(){
		return $this->address2;
	}

	public function setAddress2($val){
		$this->address2 = $val;
	}

	// transfer_type
	public function getTransferType(){
		return $this->transfer_type;
	}

	public function setTransferType($val){
		$this->transfer_type = $val;
	}

	// bank_name
	public function getBankName(){
		return $this->bank_name;
	}

	public function setBankName($val){
		$this->bank_name = $val;
	}

	// branch_name
	public function getBranchName(){
		return $this->branch_name;
	}

	public function setBranchName($val){
		$this->branch_name = $val;
	}

	// account_type
	public function getAccountType(){
		return $this->account_type;
	}

	public function setAccountType($val){
		$this->account_type = $val;
	}

	// account_holder
	public function getAccountHolder(){
		return $this->account_holder;
	}

	public function setAccountHolder($val){
		$this->account_holder = $val;
	}

	// account_number
	public function getAccountNumber(){
		return $this->account_number;
	}

	public function setAccountNumber($val){
		$this->account_number = $val;
	}

	// postal_account_holder
	public function getPostalAccountHolder(){
		return $this->postal_account_holder;
	}

	public function setPostalAccountHolder($val){
		$this->postal_account_holder = $val;
	}

	// postal_account_number
	public function getPostalAccountNumber(){
		return $this->postal_account_number;
	}

	public function setPostalAccountNumber($val){
		$this->postal_account_number = $val;
	}

	// result_action_url
	public function getResultActionUrl(){
		return $this->result_action_url;
	}

	public function setResultActionUrl($val){
		$this->result_action_url = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}