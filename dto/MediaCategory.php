<?php
class MediaCategory {

	// プロパティ
	private $id = "";
	private $name = "";
	private $input_name = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->name. ","
						. $this->input_name. ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// name
	public function getName(){
		return $this->name;
	}

	public function setName($val){
		$this->name = $val;
	}

	// input_name
	public function getInputName(){
		return $this->input_name;
	}

	public function setInputName($val){
		$this->input_name = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}