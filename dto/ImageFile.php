<?php
class ImageFile {

	// プロパティ
	private $id = "";
	private $original_name = "";
	private $file_name = "";
	private $file_type = "";
	private $image_tag = "";
	private $created_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->original_name . ","
						. $this->file_name . ","
						. $this->file_type . ","
						. $this->image_tag . ","
						. $this->created_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// original_name
	public function getOriginalName(){
		return $this->original_name;
	}

	public function setOriginalName($val){
		$this->original_name = $val;
	}

	// file_name
	public function getFileName(){
		return $this->file_name;
	}

	public function setFileName($val){
		$this->file_name = $val;
	}

	// file_type
	public function getFileType(){
		return $this->file_type;
	}

	public function setFileType($val){
		$this->file_type = $val;
	}

	// image_tag
	public function getImageTag(){
		return $this->image_tag;
	}

	public function setImageTag($val){
		$this->image_tag = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}