<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );

session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher = new MediaPublisher();
	$media_publisher = $media_publisher_dao->getMediaPublisherByLoginUserId($login_user_id);
	$media_publisher_id = $media_publisher->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	$common_dao = new CommonDao();

	if(isset($_GET['m_id'])) {
		$media_id = do_escape_quotes($_GET['m_id']);
	} elseif(isset($_POST['m_id'])) {
		$media_id = do_escape_quotes($_POST['m_id']);
	} else {
		$media_id = "";
	}

	$date_type = (isset($_POST['date_type'])) ? do_escape_quotes($_POST['date_type']) : 1;
	$carrier = (isset($_POST['carrier'])) ? do_escape_quotes($_POST['carrier']) : 1;

	$now_date = getdate();
	$s_year = (isset($_POST['s_year'])) ? do_escape_quotes($_POST['s_year']) : $now_date['year'];
	$s_month = (isset($_POST['s_month'])) ? do_escape_quotes($_POST['s_month']) : $now_date['mon'];
	$s_day = (isset($_POST['s_day'])) ? do_escape_quotes($_POST['s_day']) : $now_date['mday'];

	$smarty->assign("media_id", $media_id);
	$smarty->assign("advert_id", $advert_id);

	$smarty->assign("date_type", $date_type);
	$smarty->assign("carrier", $carrier);

	$smarty->assign("set_s_year", $s_year);
	$smarty->assign("set_s_month", $s_month);
	$smarty->assign("set_s_day", $s_day);

	//メディアサイト一覧取得
	$list_sql = " SELECT id, media_name "
				. " FROM media "
				. " WHERE deleted_at is NULL "
				. " AND media_publisher_id = '$media_publisher_id' "
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$media_array = $db_result;
		$smarty->assign("media_array", $media_array);
	}else{

	}

	$sql = " SELECT a.advert_name, ";

	if($carrier == 2) {
		$sql .= " al.carrier_id, al.browser_type, ";
	}

	$sql .= " SUM(al.click_price_media) as click_price_media, "
			. " SUM(IF(al.status <> 1, al.action_price_media, NULL)) as action_price_media, "
			. " COUNT(al.status) as click_count, "
			. " COUNT(IF(al.status <> 1, al.status, NULL)) as action_count "
			. " FROM action_logs as al "
			. " LEFT JOIN advert as a on al.advert_id = a.id "
			. " WHERE al.deleted_at is NULL "
			. " AND al.media_publisher_id = $media_publisher_id "
			. " AND (al.status = 1 OR al.status = 2 OR al.status = 3) ";

	if($media_id != "") {
		$sql .= " AND al.media_id = '$media_id' ";
	}

	if($advert_id != "") {
		$sql .= " AND al.advert_id = '$advert_id' ";
	}

	if($date_type == 1) {
		$sql .= " AND ( "
				. " (al.status = 1 AND DATE_FORMAT(al.created_at,'%Y%c') = '$s_year$s_month') "
				. " OR "
				. " (al.status = 2 AND DATE_FORMAT(al.action_complete_date,'%Y%c') = '$s_year$s_month') "
				// ステータス3 特殊なケース 例)ユーザークレーム等で成果を上げる
				. " OR "
				. " (al.status = 3 AND DATE_FORMAT(al.created_at,'%Y%c') = '$s_year$s_month') "
				. " ) ";
	} elseif($date_type == 2) {
		$sql .= " AND ( "
				. " (al.status = 1 AND DATE_FORMAT(al.created_at,'%Y%c%e') = '$s_year$s_month$s_day') "
				. " OR "
				. " (al.status = 2 AND DATE_FORMAT(al.action_complete_date,'%Y%c%e') = '$s_year$s_month$s_day') "
				// ステータス3 特殊なケース 例)ユーザークレーム等で成果を上げる
				. " OR "
				. " (al.status = 3 AND DATE_FORMAT(al.created_at,'%Y%c%e') = '$s_year$s_month$s_day') "
				. " ) ";
	}

	if($carrier == 1) {
		$sql .= " GROUP BY a.advert_name ";
	} elseif($carrier == 2) {
		$sql .= " GROUP BY a.advert_name, al.carrier_id, al.browser_type ";
	}

	$sql .= " ORDER BY a.id DESC ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){

		foreach($db_result as $row) {
			$name = $row['advert_name'];

			if($row['carrier_id'] == 1 && $row['browser_type'] == 2) {
				$c_id = "android_docomo";
			} elseif($row['carrier_id'] == 2 && $row['browser_type'] == 2) {
				$c_id = "android_softbank";
			} elseif($row['carrier_id'] == 3 && $row['browser_type'] == 2) {
				$c_id = "android_au";
			} elseif($row['carrier_id'] == 0 && $row['browser_type'] == 2) {
				$c_id = "android_pc";
			} elseif($row['browser_type'] == 1) {
				$c_id = "iphone";
			} elseif($row['browser_type'] == 3) {
				$c_id = "pc";
			}


			if($carrier == 1) {

				$summary[$name]['advert_name'] = $row['advert_name'];
				$summary[$name]['click_count'] = $row['click_count'];
				$summary[$name]['click_price'] = $row['click_price_media'];
				$summary[$name]['action_count'] = $row['action_count'];
				$summary[$name]['action_price'] = $row['action_price_media'];
				$summary[$name]['total_price'] = $row['click_price_media'] + $row['action_price_media'];

			} elseif($carrier == 2) {

				$summary[$name]['advert_name'] = $row['advert_name'];
				$summary[$name][$c_id]['click_count'] = $row['click_count'];
				$summary[$name][$c_id]['click_price'] = $row['click_price_media'];
				$summary[$name][$c_id]['action_count'] = $row['action_count'];
				$summary[$name][$c_id]['action_price'] = $row['action_price_media'];
				$summary[$name][$c_id]['total_price'] = $row['click_price_media'] + $row['action_price_media'];

			}

			$all['click_count'] += $row['click_count'];
			$all['click_price'] += $row['click_price_media'];
			$all['action_count'] += $row['action_count'];
			$all['action_price'] += $row['action_price_media'];
			$all['total_price'] += $row['click_price_media'] + $row['action_price_media'];
		}

		$smarty->assign("summary", $summary);
		$smarty->assign("all", $all);
	}else{
		$error_message .= "取得するデータがありません。";
	}
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./media_report_advert.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>