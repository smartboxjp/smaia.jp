<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );
require_once( '../dao/AdvertViewMediaSetDao.php' );
require_once( '../dto/AdvertViewMediaSet.php' );


session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher = new MediaPublisher();
	$media_publisher = $media_publisher_dao->getMediaPublisherByLoginUserId($login_user_id);
	$media_publisher_id = $media_publisher->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	//メディア一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM media "
				. " WHERE deleted_at is NULL "
				. " AND media_publisher_id = " . $media_publisher->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("media_array", $db_result);
	}else{
		//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	//広告カテゴリー
	$advert_category_dao = new AdvertCategoryDao();
	$advert_category_array = array();
	foreach($advert_category_dao->getAllAdvertCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$advert_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_category_array", $advert_category_array);

	$media_id = do_escape_quotes($_POST['media_id']);
	$smarty->assign("media_id", $media_id);

	$media_category_id = do_escape_quotes($_POST['media_category_id']);
	$smarty->assign("media_category_id", $media_category_id);

	$advert_id = do_escape_quotes($_POST['advert_id']);
	$smarty->assign("advert_id", $advert_id);

	$keyword = do_escape_quotes($_POST['keyword']);
	$category = (isset($_POST['category'])) ? $_POST['category'] : array();
	$support_docomo = do_escape_quotes($_POST['support_docomo']);
	$support_softbank = do_escape_quotes($_POST['support_softbank']);
	$support_au = do_escape_quotes($_POST['support_au']);
	$support_pc = do_escape_quotes($_POST['support_pc']);
	$min_price = do_escape_quotes($_POST['min_price']);
	$max_price = do_escape_quotes($_POST['max_price']);
	$advert_date_flag = do_escape_quotes($_POST['advert_date_flag']);
	$connect_status = do_escape_quotes($_POST['connect_status']);
	$point_back_flag = do_escape_quotes($_POST['point_back_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	if(isset($_POST['submit']) && $_POST['submit'] != '') {
		if($_POST['submit'] == '選択') {

			$media_dao = new MediaDao();
			$media = new Media();
			$media = $media_dao->getMediaBYId($media_id);
			$media_category_id = $media->getMediaCategoryId();

// ************************************************************************************
// 正規に登録された媒体以外は広告を表示させない

			// smarty変数へメッセージを格納
			$smarty->assign("media_status", $media->getStatus());

			// メッセージ作成
			if($media->getStatus() != 2){

				// 仮登録
				if($media->getStatus() == 1 || $media->getStatus() == 0){
					// smarty変数へメッセージを格納
					$smarty->assign("info_message", "現在承認待ちです。");

				// 退会
				} elseif($media->getStatus() == 3) {
					// smarty変数へメッセージを格納
					$smarty->assign("info_message", "この案件は終了しました。");

				}

				// ページを表示
				$smarty->display("./media_search.tpl");
				exit();
			}

// ************************************************************************************





			$smarty->assign("media_category_id", $media_category_id);

//			$list_sql = " SELECT DISTINCT a.*, ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " LEFT JOIN advert_view_media_sets as am on a.id = am.advert_id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND (am.media_id <> '$media_id' OR am.media_id is NULL) "
//						. " AND (am.media_publisher_id <> '$media_publisher_id' OR am.media_publisher_id is NULL) "
//						. " AND (am.media_category_id <> '$media_category_id' OR am.media_category_id is NULL) "
//						. " AND a.status = '2' "
//						. " ORDER BY a.id ASC ";

//			$list_sql = " SELECT a.id, a.advert_name, a.click_price_media, "
//						. " a.action_price_media_docomo_1, a.action_price_media_softbank_1, "
//						. " a.action_price_media_au_1, a.action_price_media_pc_1, "
//						. " a.support_docomo, a.support_softbank, a.support_au, a.support_pc, "
//						. " a.content_type, a.point_back_flag, a.unrestraint_flag, a.advert_end_date, "
//						. " ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND a.status = '2' ";

			$list_sql = " SELECT a.*, "
						. " ac.name as advert_category_name "
						. " FROM advert as a "
						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
						. " WHERE a.deleted_at is NULL "
						. " AND a.status = '2' ";


			$add_where = array();

			//キーワード
			if($keyword != "") {
				$list_sql .= " AND a.advert_name LIKE '%$keyword%' ";
			}

			//カテゴリー
			foreach($category as $key => $val) {
				if($val == 1) {
					$add_where[] = "a.advert_category_id = '$key'";
				}
			}
			if(count($add_where) > 0) {
				$list_sql .= " AND (".implode("OR ", $add_where).") ";
			}

			//対応キャリア
			if($support_docomo == 1) {
				$list_sql .= " AND a.support_docomo = '1' ";
			}
			if($support_softbank == 1) {
				$list_sql .= " AND a.support_softbank = '1' ";
			}
			if($support_au == 1) {
				$list_sql .= " AND a.support_au = '1' ";
			}
			if($support_pc == 1) {
				$list_sql .= " AND a.support_pc = '1' ";
			}

			//単価
			if($min_price != "") {
				if(is_numeric($min_price)) {
					$list_sql .= " AND (a.click_price_media >= '$min_price' OR a.action_price_media_pc >= '$min_price') ";
				}
			}
			if($max_price != "") {
				if(is_numeric($max_price)) {
					$list_sql .= " AND (a.action_price_media_pc <= '$max_price' OR a.action_price_media_pc <= '$max_price') ";
				}
			}

			//登録日時
			if($advert_date_flag == 1) {
				$list_sql .= " AND a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
			}

//			//提携状態
//			if($connect_status == 2) {
//				$list_sql .= " AND (cl.media_id <> '$media_id' OR cl.media_id is NULL) ";
//			} elseif($connect_status == 3) {
//				$list_sql .= " AND cl.media_id = '$media_id' AND cl.status = '2' ";
//			}

			//ポイントバック対応
			if($point_back_flag == 2) {
				$list_sql .= " AND a.point_back_flag = '1' ";
			} elseif($point_back_flag == 3) {
				$list_sql .= " AND a.point_back_flag = '2' ";
			}

			$list_sql .= " ORDER BY a.id ASC ";

			view_list();

		} elseif($_POST['submit'] == 'CSVダウンロード') {

// ************************************************************************************
// メディアのステータスを確認

			$media_dao = new MediaDao();
			$media = new Media();
			$media = $media_dao->getMediaBYId($media_id);
			$media_category_id = $media->getMediaCategoryId();

			$smarty->assign("media_status", $media->getStatus());

// ************************************************************************************

			$connect_log_dao = new ConnectLogDao();

			$search = array('keyword' => $keyword,
							'category' => $category,
							'support_docomo' => $support_docomo,
							'support_softbank' => $support_softbank,
							'support_au' => $support_au,
							'support_pc' => $support_pc,
							'min_price' => $min_price,
							'max_price' => $max_price,
							'advert_date_flag' => $advert_date_flag,
							'advert_start_date' => "$s_year-$s_month-$s_day",
							'advert_end_date' => "$e_year-$e_month-$e_day",
							'connect_status' => $connect_status,
							'point_back_flag' => $point_back_flag);

			$smarty->assign("search", $search);

//			$list_sql = "  SELECT DISTINCT a.*, ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " LEFT JOIN advert_view_media_sets as am on a.id = am.advert_id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND (am.media_id <> '$media_id' OR am.media_id is NULL) "
//						. " AND (am.media_publisher_id <> '$media_publisher_id' OR am.media_publisher_id is NULL) "
//						. " AND (am.media_category_id <> '$media_category_id' OR am.media_category_id is NULL) "
//						. " AND a.status = '2' ";

//			$list_sql = " SELECT a.id, a.advert_name, a.click_price_media, "
//						. " a.action_price_media_docomo_1, a.action_price_media_softbank_1, "
//						. " a.action_price_media_au_1, a.action_price_media_pc_1, "
//						. " a.support_docomo, a.support_softbank, a.support_au, a.support_pc, "
//						. " a.content_type, a.point_back_flag, a.unrestraint_flag, a.advert_end_date, "
//						. "a.ms_text_1, a.ms_text_2, a.ms_text_3, a.ms_text_4, a.ms_text_5, "
//						. " a.ms_image_type_1, a.ms_image_type_2, a.ms_image_type_3, a.ms_image_type_4, a.ms_image_type_5, "
//						. " a.ms_image_url_1, a.ms_image_url_2, a.ms_image_url_3, a.ms_image_url_4, a.ms_image_url_5, "
//						. " a.ms_email_1, a.ms_email_2, a.ms_email_3, a.ms_email_4, a.ms_email_5, "
//						. " ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND a.status = '2' ";

			$list_sql = " SELECT a.*, "
						. " ac.name as advert_category_name "
						. " FROM advert as a "
						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
						. " WHERE a.deleted_at is NULL "
						. " AND a.status = '2' ";

			$add_where = array();

			//キーワード
			if($keyword != "") {
				$list_sql .= " AND a.advert_name LIKE '%$keyword%' ";
			}

			//カテゴリー
			foreach($category as $key => $val) {
				if($val == 1) {
					$add_where[] = "a.advert_category_id = '$key'";
				}
			}
			if(count($add_where) > 0) {
				$list_sql .= " AND (".implode("OR ", $add_where).") ";
			}

			//対応キャリア
			if($support_docomo == 1) {
				$list_sql .= " AND a.support_docomo = '1' ";
			}
			if($support_softbank == 1) {
				$list_sql .= " AND a.support_softbank = '1' ";
			}
			if($support_au == 1) {
				$list_sql .= " AND a.support_au = '1' ";
			}
			if($support_pc == 1) {
				$list_sql .= " AND a.support_pc = '1' ";
			}

			//単価
			if($min_price != "") {
				if(is_numeric($min_price)) {
					$list_sql .= " AND (a.click_price_media >= '$min_price' OR a.action_price_media_pc >= '$min_price') ";
				}
			}
			if($max_price != "") {
				if(is_numeric($max_price)) {
					$list_sql .= " AND (a.action_price_media_pc <= '$max_price' OR a.action_price_media_pc <= '$max_price') ";
				}
			}

			//登録日時
			if($advert_date_flag == 1) {
				$list_sql .= " AND a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
			}

//			//提携状態
//			if($connect_status == 2) {
//				$list_sql .= " AND (cl.media_id <> '$media_id' OR cl.media_id is NULL) ";
//			} elseif($connect_status == 3) {
//				$list_sql .= " AND cl.media_id = '$media_id' AND cl.status = '2' ";
//			}

			//ポイントバック対応
			if($point_back_flag == 2) {
				$list_sql .= " AND a.point_back_flag = '1' ";
			} elseif($point_back_flag == 3) {
				$list_sql .= " AND a.point_back_flag = '2' ";
			}





			$list_sql .= " ORDER BY a.id ASC ";



			$db_result = $common_dao->db_query($list_sql);
			if($db_result){
				$outputFile = "../logs/output.cgi";
				touch($outputFile);

				$fp = fopen($outputFile, "w");

				mb_internal_encoding("UTF-8");
				mb_detect_order("ASCII, JIS, UTF-8, eucjp-win, sjis-win, EUC-JP, SJIS");

				$download_date = date("YmdHis");

				$file_name = "link_$download_date.csv";
				$csv_array[] = array('広告ID',
									'広告サイト名',
									'クリック単価',

									'アクション単価(iphone docomo)',
									'アクション単価(iphone softbank)',
									'アクション単価(iphone au)',
									'アクション単価(iphone pc)',

									'アクション単価(android docomo)',
									'アクション単価(android softbank)',
									'アクション単価(android au)',
									'アクション単価(android pc)',
									'アクション単価(pc)',

									'キャリア(iphone docomo)',
									'キャリア(iphone softbank)',
									'キャリア(iphone au)',
									'キャリア(iphone pc)',

									'キャリア(android docomo)',
									'キャリア(android softbank)',
									'キャリア(android au)',
									'キャリア(android pc)',
									'キャリア(pc)',

									'認証',
//									'カテゴリー',
//									'ポイントバック',
									'出稿終了日',
									'広告原稿1(テキスト)',
									'広告原稿2(テキスト)',
									'広告原稿3(テキスト)',
									'広告原稿4(テキスト)',
									'広告原稿5(テキスト)',
									'広告原稿1(イメージ)',
									'広告原稿2(イメージ)',
									'広告原稿3(イメージ)',
									'広告原稿4(イメージ)',
									'広告原稿5(イメージ)',
									'広告原稿1(メール)',
									'広告原稿2(メール)',
									'広告原稿3(メール)',
									'広告原稿4(メール)',
									'広告原稿5(メール)',
									'広告概要',
									'広告URL');

				$image_dir = "http://smaia.jp/advert_image/";

				foreach($db_result as $row) {
					$error_flag = 0;

					$advert_view_media_set_dao = new AdvertViewMediaSetDao();
					$result_view = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], $media_id, $media_publisher_id, $media_category_id);
					if(!is_null($result_view)) {
						$error_flag = 1;
					}

					$result_view = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, $media_publisher_id, $media_category_id);
					if(!is_null($result_view)) {
						$error_flag = 1;
					}

					$result_view = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, 0, $media_category_id);
					if(!is_null($result_view)) {
						$error_flag = 1;
					}

					if($connect_status == 2 || $connect_status == 3) {
						$connect_log_dao = new ConnectLogDao();
						$record = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
						if($connect_status == 2) {
							if(!is_null($record)) {
								$error_flag = 1;
							}
						} elseif($connect_status == 3) {
							if(is_null($record)) {
								$error_flag = 1;
							}
						}
					}

					if($error_flag == 0) {
						$data1 = $row['id'];
						$data2 = $row['advert_name'];

						$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
						$advert_price_media_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($row['id'], $media_id);

						if(!is_null($advert_price_media_set)) {
							$data3 = $advert_price_media_set->getClickPriceMedia();
							// iphone
							$data4 = $advert_price_media_set->getActionPriceMediaIphoneDocomo1();
							$data5 = $advert_price_media_set->getActionPriceMediaIphoneSoftbank1();
							$data6 = $advert_price_media_set->getActionPriceMediaIphoneAu1();
							$data7 = $advert_price_media_set->getActionPriceMediaIphonePc1();
							// android
							$data8 = $advert_price_media_set->getActionPriceMediaAndroidDocomo1();
							$data9 = $advert_price_media_set->getActionPriceMediaAndroidSoftbank1();
							$data10 = $advert_price_media_set->getActionPriceMediaAndroidAu1();
							$data11 = $advert_price_media_set->getActionPriceMediaAndroidPc1();
							// pc
							$data12 = $advert_price_media_set->getActionPriceMediaPc1();
						} else {
							$data3 = $row['click_price_media'];
							// iphone
							$data4 = $row['action_price_media_iphone_docomo_1'];
							$data5 = $row['action_price_media_iphone_softbank_1'];
							$data6 = $row['action_price_media_iphone_au_1'];
							$data7 = $row['action_price_media_iphone_pc_1'];
							// android
							$data8 = $row['action_price_media_android_docomo_1'];
							$data9 = $row['action_price_media_android_softbank_1'];
							$data10 = $row['action_price_media_android_au_1'];
							$data11 = $row['action_price_media_android_pc_1'];
							// pc
							$data12 = $row['action_price_media_pc_1'];
						}

						$data13 = ($row['support_iphone_docomo'] == 1) ? "○" : "-";
						$data14 = ($row['support_iphone_softbank'] == 1) ? "○" : "-";
						$data15 = ($row['support_iphone_au'] == 1) ? "○" : "-";
						$data16 = ($row['support_iphone_pc'] == 1) ? "○" : "-";
						$data17 = ($row['support_android_docomo'] == 1) ? "○" : "-";
						$data18 = ($row['support_android_softbank'] == 1) ? "○" : "-";
						$data19 = ($row['support_android_au'] == 1) ? "○" : "-";
						$data20 = ($row['support_android_pc'] == 1) ? "○" : "-";
						$data21 = ($row['support_pc'] == 1) ? "○" : "-";

						$data22 = ($row['approval_flag'] == 1) ? "全認証" : "手動";
//						$data23 = $row['advert_categeory_name'];
//						$data24 = ($row['point_back_flag'] == 1) ? "○" : "-";
						$data25 = ($row['unrestraint_flag'] == 1) ? "無制限" : $row['advert_end_date'];
						$data26 = $row['ms_text_1'];
						$data27 = $row['ms_text_2'];
						$data28 = $row['ms_text_3'];
						$data29 = $row['ms_text_4'];
						$data30 = $row['ms_text_5'];
						$data31 = ($row['ms_image_type_1'] == 1 && $row['ms_image_url_1'] != "") ? $image_dir.$row['ms_image_url_1'] : $row['ms_image_url_1'];
						$data32 = ($row['ms_image_type_2'] == 1 && $row['ms_image_url_2'] != "") ? $image_dir.$row['ms_image_url_2'] : $row['ms_image_url_2'];
						$data33 = ($row['ms_image_type_3'] == 1 && $row['ms_image_url_3'] != "") ? $image_dir.$row['ms_image_url_3'] : $row['ms_image_url_3'];
						$data34 = ($row['ms_image_type_4'] == 1 && $row['ms_image_url_4'] != "") ? $image_dir.$row['ms_image_url_4'] : $row['ms_image_url_4'];
						$data35 = ($row['ms_image_type_5'] == 1 && $row['ms_image_url_5'] != "") ? $image_dir.$row['ms_image_url_5'] : $row['ms_image_url_5'];
						$data36 = $row['ms_email_1'];
						$data37 = $row['ms_email_2'];
						$data38 = $row['ms_email_3'];
						$data39 = $row['ms_email_4'];
						$data40 = $row['ms_email_5'];
						$data41 = $row['site_outline'];
						$data42 = "http://smaia.jp/action/click.php?guid=ON&m=$media_id&a=".$row['id'];

						$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10,
											$data11, $data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20,
											$data21, $data22, $data25, $data26, $data27, $data28, $data29, $data30,
											$data31, $data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40,
											$data41, $data42);

						$result = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
						if(is_null($result)) {
							$connect_log_dao->transaction_start();

							$connect_log = new ConnectLog();
							$connect_log->setMediaId($media_id);
							$connect_log->setAdvertId($row['id']);
							$connect_log->setStatus(2);

							//INSERTを実行
							$db_result = $connect_log_dao->insertConnectLog($connect_log, $result_message);
							if($db_result) {
								$connect_log_dao->transaction_end();
							} else {
								$connect_log_dao->transaction_rollback();
							}
						}
					}
				}

				mb_convert_variables("SJIS-win", "UTF-8", $csv_array);
				foreach($csv_array as $line) {
					fputcsv($fp, $line);
				}

				fclose($fp);

				header("Content-Type: application/csv");
				header("Content-Disposition: attachment; filename=".$file_name);
				header("Content-Length:".filesize($outputFile));
				readfile($outputFile);
				exit();
			} else {
				exit();
			}
		}
	}

	// ページを表示
	$smarty->display("./media_search.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message, $advert_id, $media_id, $media_publisher_id, $media_category_id, $connect_status;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		foreach($db_result as $key => $row) {

			$error_flag = 0;

			$advert_view_media_set_dao = new AdvertViewMediaSetDao();
			$record = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], $media_id, $media_publisher_id, $media_category_id);
			if(!is_null($record)) {
				$error_flag = 1;
			}

			$record = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, $media_publisher_id, $media_category_id);
			if(!is_null($record)) {
				$error_flag = 1;
			}

			$record = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, 0, $media_category_id);
			if(!is_null($record)) {
				$error_flag = 1;
			}

//			if($connect_status == 2 || $connect_status == 3) {
//				$connect_log_dao = new ConnectLogDao();
//				$record = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
//				if($connect_status == 2) {
//					if(!is_null($record)) {
//						$error_flag = 1;
//					}
//				} elseif($connect_status == 3) {
//					if(is_null($record)) {
//						$error_flag = 1;
//					}
//				}
//			}
			$connect_log_dao = new ConnectLogDao();
			$record = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
			if(!is_null($record)) {
				$db_result[$key]['connect_flag'] = 1;
				if($connect_status == 2) {
					$error_flag = 1;
				}
			} else {
				$db_result[$key]['connect_flag'] = 0;
				if($connect_status == 3) {
					$error_flag = 1;
				}
			}

			if($error_flag == 0) {
				$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
				$advert_price_media_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($row['id'], $media_id);

				if(!is_null($advert_price_media_set)) {
					// クリック単価
					if($advert_price_media_set->getClickPriceMedia() != 0){
						$db_result[$key]['click_price_media'] = $advert_price_media_set->getClickPriceMedia();
					}
					// アクション iphone docomo単価
					if($advert_price_media_set->getActionPriceMediaIphonePc1() != 0){
						$db_result[$key]['action_price_media_iphone_docomo_1'] = $advert_price_media_set->getActionPriceMediaIphonePc1();
					}
					// アクション iphone softbank単価
					if($advert_price_media_set->getActionPriceMediaIphonePc1() != 0){
						$db_result[$key]['action_price_media_iphone_softbank_1'] = $advert_price_media_set->getActionPriceMediaIphonePc1();
					}
					// アクション iphone au単価
					if($advert_price_media_set->getActionPriceMediaIphonePc1() != 0){
						$db_result[$key]['action_price_media_iphone_au_1'] = $advert_price_media_set->getActionPriceMediaIphonePc1();
					}
					// アクション iphone pc単価
					if($advert_price_media_set->getActionPriceMediaIphonePc1() != 0){
						$db_result[$key]['action_price_media_iphone_pc_1'] = $advert_price_media_set->getActionPriceMediaIphonePc1();
					}
					// アクション android docomo単価
					if($advert_price_media_set->getActionPriceMediaAndroidDocomo1() != 0){
						$db_result[$key]['action_price_media_android_docomo_1'] = $advert_price_media_set->getActionPriceMediaAndroidDocomo1();
					}
					// アクション android softbank単価
					if($advert_price_media_set->getActionPriceMediaAndroidAu1() != 0){
						$db_result[$key]['action_price_media_android_au_1'] = $advert_price_media_set->getActionPriceMediaAndroidAu1();
					}
					// アクション android au単価
					if($advert_price_media_set->getActionPriceMediaAndroidSoftbank1() != 0){
						$db_result[$key]['action_price_media_android_softbank_1'] = $advert_price_media_set->getActionPriceMediaAndroidSoftbank1();
					}
					// アクション android pc単価
					if($advert_price_media_set->getActionPriceMediaAndroidPc1() != 0){
						$db_result[$key]['action_price_media_android_pc_1'] = $advert_price_media_set->getActionPriceMediaAndroidPc1();
					}
					// アクションpc単価
					if($advert_price_media_set->getActionPriceMediaPc1() != 0){
						$db_result[$key]['action_price_media_pc_1'] = $advert_price_media_set->getActionPriceMediaPc1();
					}

				}
			} else {
				unset($db_result[$key]);
			}
		}
//		echo "check1";

		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// テスト出力
//	echo $list_sql;

	// ページを表示
	$smarty->display("./media_search.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>