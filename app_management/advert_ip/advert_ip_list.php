<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title><?=global_site_name;?>広告IP管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<link rel="stylesheet" type="text/css" href="../include/admin.css" />

<script language="javascript"> 
	function fnChangeSel(i, j) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './advert_ip_del.php?advert_ip_id='+i+'&advert_id='+j;
		} 
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("news_title");
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
	});
//-->
</script>
</head>

<body>
<?

	$common_connect -> Fn_admin_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
		
	if($advert_ip_id!="")
	{

		$arr_db_field = array("advert_id", "ip_address");
			
		$sql = "SELECT advert_ip_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " regi_date FROM advert_ip where advert_ip_id='".$advert_ip_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	//広告主をArrayへ
	$sql = "SELECT id, client_name FROM advert_clients order by id desc";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_client_name[$db_result[$db_loop][id]] = $db_result[$db_loop][client_name];
		}
	}
		
	//広告をArrayへ
	$sql = "SELECT id, advert_name FROM advert where status=2 order by id desc";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_advert_name[$db_result[$db_loop][id]] = $db_result[$db_loop][advert_name];
		}
	}
	
	
?>
<div align="center">
<div id="container">

<!-------header---------->

<?php include("../include/head.php"); ?>

<!-------left---------->

<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="172" valign="top"><?php include("../include/side.php"); ?></td>
    <td>&nbsp;</td>
    <td width="100%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td bgcolor="#83695a"><span class="text_white">広告IP管理</span></td>
        </tr>
      </table>
      
      
      <section id="main" class="clearfix">
      <form action="./advert_ip_save.php" method="POST" name="form_write" id="form_regist">
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <th bgcolor="#E2DBD6">管理ID</th>
          <td bgcolor="#E2DBD6">
            <?php if($advert_ip_id=="") {echo "自動生成";} else { echo $advert_ip_id;}?>
            <input name="advert_ip_id" type="hidden" value="<?php echo $advert_ip_id;?>" />
          </td>
        </tr>
        <tr bgcolor="#E2DBD6">
          <th>広告</th>
          <td>
            <?php $var = "advert_id";?>
            <select name="<?=$var;?>" id="<?=$var;?>">
            <? 
            foreach($arr_advert_name as $key => $value)
            {
            ?>
              <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
            <?
            }
            ?>
            </select>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th bgcolor="#E2DBD6">IP</th>
          <td bgcolor="#E2DBD6">
            <?php $var = "ip_address";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
      </table>
      <br />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td align="center" valign="middle" bgcolor="#B19F92">
            <input type="submit" value="この内容で登録" style="width:150px;height:30px">
          </td>
        </tr>
      </table>
      </form>
      </section>
      
      <br />
      <form action="<?=$_SERVER["PHP_SELF"];?>" method="get" name="write1" style="margin:0px;">
      <input name="action_logs_id" type="hidden" value="<?php echo $action_logs_id;?>" />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <th bgcolor="#E2DBD6">IP</th>
          <td bgcolor="#E2DBD6">
            <?php $var = "s_ip_address";?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr>
          <th bgcolor="#E2DBD6">広告ID</th>
          <td bgcolor="#E2DBD6">
          	<?
							$var = "s_id";
						?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
          </td>
        </tr>
        <tr>
          <th bgcolor="#E2DBD6">広告名</th>
          <td bgcolor="#E2DBD6">
          	<?
							$var = "s_advert_name";
						?>
            <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
          </td>
        </tr>
        <tr>
          <th bgcolor="#E2DBD6">広告主</th>
          <td bgcolor="#E2DBD6">
          	<?php $var = "s_advert_client_id";?>
            <select name="<?=$var;?>" id="<?=$var;?>">
              <option value="" selected>---</option>
            <? 
            foreach($arr_client_name as $key => $value)
            {
            ?>
              <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
            <?
            }
            ?>
            </select>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>

      </table>
      <br />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td align="center" valign="middle" bgcolor="#B19F92">
            <input type="submit" value="この内容で検索" style="width:150px;height:30px">
          </td>
        </tr>
      </table>
      </form>
      <br />
      <?	

	
				$view_count=50;   // List count
				$offset=0;
			
				if(!$page)
				{
					$page=1;
				}
				Else
				{
					$offset=$view_count*($page-1);
				}
				
				$where = "";
				$where .= " and a.status=2 ";
				
				if($s_advert_name != "")
				{
					$where .= " and (a.advert_name like '%".$s_advert_name."%' ) ";
				}
				
				if($s_ip_address != "")
				{
					$where .= " and ip_address = '".$s_ip_address."' ";
				}
				
				if($s_id != "")
				{
					$where .= " and a.id = '".$s_id."' ";
				}
				
				if($s_advert_client_id != "")
				{
					$where .= " and a.advert_client_id='".$s_advert_client_id."' ";
				}
				
			//if($where != "")
			//{

				//合計
				$sql_count = "SELECT count(a.id) as all_count FROM advert a inner join advert_ip ai on a.id=ai.advert_id where 1 ".$where ;
				
				$db_result_count = $common_dao->db_query($sql_count);
				if($db_result_count)
				{
					$all_count = $db_result_count[0]["all_count"];
				}
				
				//リスト表示
				$arr_db_field = array("advert_client_id", "id", "advert_name", "status", "ip_address", "advert_ip_id");
				
				$sql = "SELECT ";
				foreach($arr_db_field as $val)
				{
					$sql .= $val.", ";
				}
				$sql .= " ai.regi_date, ai.up_date FROM advert a inner join advert_ip ai on a.id=ai.advert_id where 1 ".$where ;
				if($order_name != "")
				{
					$sql .= " order by ".$order_name." ".$order;
				}
				else
				{
					$sql .= " order by advert_client_id desc";
				}
				$sql .= " limit $offset,$view_count";
				
				$db_result = $common_dao->db_query($sql);
				if($db_result)
				{
					$inner_count = count($db_result);

			?>
      <div align="right"><a href="./advert_ip_list_csv.php?<? echo $_SERVER["QUERY_STRING"];?>" target="_blank">CSVダウンロード</a></div>
      すべて：<?=$all_count;?>
      <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <th align="center" bgcolor="#E2DBD6">管理ID</th>
          <th align="center" bgcolor="#E2DBD6">広告主</th>
          <th align="center" bgcolor="#E2DBD6">広告名</th>
          <th align="center" bgcolor="#E2DBD6">IP</th>
          <th align="center" bgcolor="#E2DBD6">編集</th>
          <th align="center" bgcolor="#E2DBD6">修正日<br />登録日</th>
        </tr>
        <tr>
          <td align="center"><a href="?order_name=advert_ip_id&order=desc">▼</a>｜<a href="?order_name=advert_ip_id">▲</a></th>
          <td align="center"><a href="?order_name=advert_client_id&order=desc">▼</a>｜<a href="?order_name=advert_client_id">▲</a></th>
          <td align="center"><a href="?order_name=advert_name&order=desc">▼</a>｜<a href="?order_name=advert_name">▲</a></th>
          <td align="center"><a href="?order_name=ip_address&order=desc">▼</a>｜<a href="?order_name=ip_address">▲</a></th>
          <td align="center"><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></th>
          <td align="center"></th>
        </tr>
				<?php
					for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
					{
						foreach($arr_db_field as $val)
						{
							$$val = $db_result[$db_loop][$val];
						}
						$regi_date = $db_result[$db_loop]["regi_date"];
						$up_date = $db_result[$db_loop]["up_date"];

        ?>
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $advert_ip_id;?></td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
						<?php echo $arr_client_name[$advert_client_id];?>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $advert_name;?></td>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $ip_address;?></td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
						<a href="?advert_ip_id=<?php echo $advert_ip_id;?>"><img src="/app_management/images/btn_02.gif" alt="編集" /></a>　
            <a href="#" onClick='fnChangeSel("<?php echo $advert_ip_id;?>", "<?php echo $id;?>");'><img src="/app_management/images/btn_03.gif" alt="削除" /></a>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $regi_date."<br />".$up_date;?></td>
        </tr>
			<?php
					}
      ?>
      </table>
      <?
				}
			?>

		<div style="text-align:center;">
			<?php $common_connect -> Fn_paging($view_count, $all_count); ?>
		</div><!-- //center-->
<?
			//} //$where
?>

      </td>
  </tr>
</table>

    </td>
  </tr>
</table>

<!-------footer---------->

<?php include("../include/footer.php"); ?>

<!-------end---------->

</div>
</div>
<?
	mysql_close($db);
?>
</body>
</html>
