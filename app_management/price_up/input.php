<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title><?=global_site_name;?>料金変更</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<link rel="stylesheet" type="text/css" href="../include/admin.css" />
</head>

<body>
<?

	$common_connect -> Fn_admin_check();

	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

?>
<div align="center">
<div id="container">

<!-------header---------->

<?php include("../include/head.php"); ?>

<!-------left---------->

<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="172" valign="top"><?php include("../include/side.php"); ?></td>
    <td>&nbsp;</td>
    <td width="100%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#83695a"><span class="text_white">アクション単価(net)</span></td>
      </tr>
    </table>
      <br />
      <form action="price_up.php" method="post" name="write1" style="margin:0px;">
      <input name="action_logs_id" type="hidden" value="<?php echo $action_logs_id;?>" />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td bgcolor="#E2DBD6">セッションID</td>
          <td bgcolor="#E2DBD6">
          		<? $var = "session_id_1";?>
             <input name="<? echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
             <br />
          		<? $var = "session_id_2";?>
             <input name="<? echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
             <br />
          		<? $var = "session_id_3";?>
             <input name="<? echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
             <br />
          		<? $var = "session_id_4";?>
             <input name="<? echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
             <br />
          		<? $var = "session_id_5";?>
             <input name="<? echo $var;?>" type="text" size="80" value="<?php echo $$var;?>" />
             <br />
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">広告ID</td>
          <td bgcolor="#E2DBD6">
          	<input name="advert_id" type="text" size="50" value="<?php echo $advert_id;?>" />
            <br />例）753
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">媒体ID</td>
          <td bgcolor="#E2DBD6">
          		<? $var = "media_id";?>
             <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" />
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">変更前<br />アクション単価(Gross)</td>
          <td bgcolor="#E2DBD6">
          		<? $var = "price_3";?>
             <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" />
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">変更後<br />アクション単価(Gross)</td>
          <td bgcolor="#E2DBD6">
          		<? $var = "price_4";?>
             <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" />
          </td>
        </tr>
        
        <tr>
          <td bgcolor="#E2DBD6">変更前<br />アクション単価(net)</td>
          <td bgcolor="#E2DBD6">
          		<? $var = "price_1";?>
             <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" />
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">変更後<br />アクション単価(net)</td>
          <td bgcolor="#E2DBD6">
          		<? $var = "price_2";?>
             <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" />
          </td>
        </tr>
      </table>
      <br />
      <br />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td align="center" valign="middle" bgcolor="#B19F92">
            <input type="submit" value="この内容で変更" style="width:150px;height:30px">
          </td>
        </tr>
      </table>
      </form>
      <br />

      </td>
  </tr>
</table>

    </td>
  </tr>
</table>

<!-------footer---------->

<?php include("../include/footer.php"); ?>

<!-------end---------->

</div>
</div>
<?
	mysql_close($db);
?>
</body>
</html>
