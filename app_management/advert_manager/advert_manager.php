<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title><?=global_site_name;?>提携解除</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<link rel="stylesheet" type="text/css" href="../include/admin.css" />
<script src="/app_management/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    $(function() {
			//チェックリストを解除する
      $('#check_on').click(function() {
        $('.check_del').prop('checked',true);
      });
	  
	//チェックリストを一括削除
      $('#check_delete').click(function() {
		if(confirm('変更しますか？'))
		{
			check_dell_list = $('[class="check_del"]:checked').map(function(){
				//$(this)でjQueryオブジェクトが取得できる。val()で値をvalue値を取得。
				return $(this).val();
			}).get().join('&c[]=');
			document.location.href = './advert_manager_del.php?media_publisher_id=<? echo $common_connect->h($_GET["media_publisher_id"]);?>&advert_client_id=<? echo $common_connect->h($_GET["advert_client_id"]);?>&media_id=<? echo $common_connect->h($_GET["media_id"]);?>&c[]='+check_dell_list;
		}
      });
	  
	  
		$('#media_publisher_id').change(function() {
			document.location.href = "advert_manager.php?media_publisher_id="+$('#media_publisher_id').val();
		});
	});
	
</script>

</head>

<body>
<?

	$common_connect -> Fn_admin_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	
	//媒体発行者をArrayへ
	$sql = "SELECT id, publisher_name FROM media_publishers where status=2 order by id desc";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_publisher_name[$db_result[$db_loop][id]] = $db_result[$db_loop][publisher_name];
		}
	}
  
	$sql = "SELECT id, client_name FROM advert_clients where status=2 order by id desc";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_client_name[$db_result[$db_loop][id]] = $db_result[$db_loop][client_name];
		}
	}
  
  
	$sql = "SELECT id, advert_name FROM advert order by id desc";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_advert_name[$db_result[$db_loop][id]] = $db_result[$db_loop][advert_name];
		}
	}
		
	//媒体をArrayへ
	$sql = "SELECT m.id, mp.publisher_name, m.media_name ";
	$sql .= " FROM media m ";
	$sql .= " INNER JOIN media_publishers mp ON m.media_publisher_id = mp.id ";
	$sql .= " WHERE m.status =2 ";
	if($media_publisher_id!="")
	{
		$sql .= " and m.media_publisher_id ='".$media_publisher_id."' ";
	}
	$sql .= " ORDER BY m.id DESC  ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_media_name[$db_result[$db_loop][id]] = $db_result[$db_loop][media_name];
		}
	}
	
	//媒体のIDが正しいかをチェック
	if($media_publisher_id!="" && $media_id!="")
	{
		$sql = "SELECT m.id, mp.publisher_name, m.media_name ";
		$sql .= " FROM media m ";
		$sql .= " INNER JOIN media_publishers mp ON m.media_publisher_id = mp.id ";
		$sql .= " WHERE m.status =2 ";
		$sql .= " and m.media_publisher_id ='".$media_publisher_id."' ";
		$sql .= " and m.id ='".$media_id."' ";
		$db_result = $common_dao->db_query($sql);
		if(!$db_result)
		{
			$common_connect -> Fn_javascript_back("一致してる媒体IDがありません。");
		}
	}
?>
<div align="center">
<div id="container">

<!-------header---------->

<?php include("../include/head.php"); ?>

<!-------left---------->

<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="172" valign="top"><?php include("../include/side.php"); ?></td>
    <td>&nbsp;</td>
    <td width="100%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="10">
        <tr>
          <td bgcolor="#83695a"><span class="text_white">メディアの広告管理</span></td>
        </tr>
      </table>
      
      <section id="main" class="clearfix">
      <form action="<? echo $_SERVER['PHP_SELF'];?>" method="GET" name="form_write" id="form_regist">
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
      
        <tr bgcolor="#E2DBD6">
          <th>媒体発行者</th>
          <td>
            <?php $var = "media_publisher_id";?>
            <select name="<?=$var;?>" id="<?=$var;?>">
            	<option value="">---</option>
            <? 
            foreach($arr_publisher_name as $key => $value)
            {
            ?>
              <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
            <?
            }
            ?>
            </select>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr bgcolor="#E2DBD6">
          <th>媒体</th>
          <td>
            <?php $var = "media_id";?>
            <select name="<?=$var;?>" id="<?=$var;?>" onchange="submit(this.form)">
            	<option value="">---</option>
            <? 
            foreach($arr_media_name as $key => $value)
            {
            ?>
              <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
            <?
            }
            ?>
            </select>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        <tr bgcolor="#E2DBD6">
          <th>広告主</th>
          <td>
            <?php $var = "advert_client_id";?>
            <select name="<?=$var;?>" id="<?=$var;?>" onchange="submit(this.form)">
            	<option value="">---</option>
            <? 
            foreach($arr_client_name as $key => $value)
            {
            ?>
              <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
            <?
            }
            ?>
            </select>
            <label id="err_<?php echo $var;?>"></label>
          </td>
        </tr>
        
      </table>
      <br />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td align="center" valign="middle" bgcolor="#B19F92">
            <input type="submit" value="この内容で検索" style="width:150px;height:30px">
          </td>
        </tr>
      </table>
      </form>
      </section>
      
      <br />
      <?	
	  $sql = " SELECT a.id, a.advert_name, advert_client_id FROM advert a inner join connect_logs c on a.id=c.advert_id ";
	  $sql .= " WHERE a.deleted_at IS NULL AND a.status =  '2' ";
	  $sql .= " and ";
	  $sql .= " c.deleted_at IS NULL AND c.status =  '2' ";
	  $sql .= " and media_id='".$media_id."' ";
	  if($advert_client_id!="")
	  {
		  $sql .= " and advert_client_id='".$advert_client_id."' ";
	  }
	  $sql .= " ORDER BY a.id ASC ";
				
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
	?>
      すべて：<?=$inner_count;?>
      <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <th align="center" bgcolor="#E2DBD6">管理ID</th>
          <th align="center" bgcolor="#E2DBD6">広告主</th>
          <th align="center" bgcolor="#E2DBD6">広告名</th>
          <th align="center" bgcolor="#E2DBD6">
          	<button id="check_delete" type="button" style="background-color:#C00;" >提携解除</button><br />
            
            <button id="check_on" type="button" >全て選択</button>
          </th>
        </tr>
		<?php
            for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
            {
				$id = $db_result[$db_loop]["id"];
				$advert_name = $db_result[$db_loop]["advert_name"];
				$advert_client_id = $db_result[$db_loop]["advert_client_id"];
        ?>
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $id;?></td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
          	<?php echo $arr_client_name[$advert_client_id];?>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $advert_name;?></td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
          	<input type="checkbox" class="check_del" value="<?php echo $id;?>" />
          </td>
        </tr>
	<?php
            }
      ?>
      </table>
      <?
		}
		?>



      </td>
  </tr>
</table>

    </td>
  </tr>
</table>

<!-------footer---------->

<?php include("../include/footer.php"); ?>

<!-------end---------->

</div>
</div>
<?
	mysql_close($db);
?>
</body>
</html>
