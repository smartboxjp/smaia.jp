<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title><?=global_site_name;?>セッション抽出</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<link rel="stylesheet" type="text/css" href="../include/admin.css" />
<script language="javascript"> 
	function fnChangeSel(i) { 
		var result = confirm('成果反映しますか？'); 
		if(result){ 
			document.location.href = '/action/result_20150714_push.php?at=2&bid='+i;
		} 
	}
</script>
</head>

<body>
<?

	$common_connect -> Fn_admin_check();

	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}


	$where = "";
	if($point_back_parameter != "")
	{
		$where .= " and point_back_parameter='".$point_back_parameter."' ";
	}
	if($session_id != "")
	{
		$where .= " and session_id='".$session_id."' ";
	}
	if($advert_client_id != "")
	{
		$where .= " and advert_client_id='".$advert_client_id."' ";
	}
	if($media_publisher_id != "")
	{
		$where .= " and media_publisher_id='".$media_publisher_id."' ";
	}
	if($advert_id != "")
	{
		$where .= " and advert_id='".$advert_id."' ";
	}
	if($media_id != "")
	{
		$where .= " and media_id='".$media_id."' ";
	}
	if($uid != "")
	{
		$where .= " and uid='".$uid."' ";
	}
	if($ip_address != "")
	{
		$where .= " and ip_address='".$ip_address."' ";
	}
	if($host_name != "")
	{
		$where .= " and host_name='".$host_name."' ";
	}
	if($status != "")
	{
		$where .= " and status='".$status."' ";
	}
		
	if($created_at_from!="" && $created_at_to!="")
	{
		$where .= " and (created_at>='".$created_at_from."' and created_at<='".$created_at_to." 23:59:59') ";
	}
	elseif($created_at_from!="")
	{
		$where .= " and created_at>='".$created_at_from."' ";
	}
	elseif($created_at_to!="")
	{
		$where .= " and created_at<='".$created_at_to." 23:59:59' ";
	}

	if($action_complete_date_from!="" && $action_complete_date_to!="")
	{
		$where .= " and status=2 and (action_complete_date>='".$action_complete_date_from."' and action_complete_date<='".$action_complete_date_to." 23:59:59') ";
	}
	elseif($action_complete_date_from!="")
	{
		$where .= " and status=2 and action_complete_date>='".$action_complete_date_from."' ";
	}
	elseif($action_complete_date_to!="")
	{
		$where .= " and status=2 and action_complete_date<='".$action_complete_date_to." 23:59:59' ";
	}
	
	
	if($s_link == "1")
	{
		$where .= " and (left(link_url, 7)!='http://' and left(link_url, 8)!='https://') ";
	}
	
	
	$view_count=100;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	
	if($where != "")
	{
		$numresults=mysql_query("select id FROM action_logs where 1 ".$where,$db);
		$sql = "SELECT id, session_id, carrier_id, user_agent, uid, ip_address, host_name, media_id, media_publisher_id, advert_id, advert_client_id, click_price_client, click_price_media, action_price_client, action_price_media, link_url, action_complete_date, confirm_flag, point_back_parameter, point_back_url, status, auid, created_at, updated_at, deleted_at FROM action_logs where 1 ".$where;
		
		if($order_name != "")
		{
			$sql .= " order by ".$order_name." ".$order;
		}
		else
		{
			$sql .= " order by created_at desc";
		}
		$sql .= " limit $offset,$view_count";

		$numrows=mysql_num_rows($numresults);
		$result=mysql_query($sql, $db);
	}
?>
<div align="center">
<div id="container">

<!-------header---------->

<?php include("../include/head.php"); ?>

<!-------left---------->

<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="172" valign="top"><?php include("../include/side.php"); ?></td>
    <td>&nbsp;</td>
    <td width="100%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
      <tr>
        <td bgcolor="#83695a"><span class="text_white">セッション抽出</span></td>
      </tr>
    </table>
      <br />
      <form action="<?=$_SERVER["PHP_SELF"];?>" method="get" name="write1" style="margin:0px;">
      <input name="action_logs_id" type="hidden" value="<?php echo $action_logs_id;?>" />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td bgcolor="#E2DBD6">セッションID</td>
          <td bgcolor="#E2DBD6"><input name="session_id" type="text" size="80" value="<?php echo $session_id;?>" /></td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">メディアとのセッション</td>
          <td bgcolor="#E2DBD6"><input name="point_back_parameter" type="text" size="80" value="<?php echo $point_back_parameter;?>" /></td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">広告主ID</td>
          <td bgcolor="#E2DBD6">
						<? $var = "advert_client_id";?>
            <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" /> 例）753
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">広告ID</td>
          <td bgcolor="#E2DBD6">
						<? $var = "advert_id";?>
            <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" /> 例）753
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">媒体発行者ID</td>
          <td bgcolor="#E2DBD6">
						<? $var = "media_publisher_id";?>
            <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" /> 例）753
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">媒体ID</td>
          <td bgcolor="#E2DBD6">
						<? $var = "media_id";?>
            <input name="<? echo $var;?>" type="text" size="50" value="<?php echo $$var;?>" /> 例）753
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">IPアドレス</td>
          <td bgcolor="#E2DBD6">
          	<input name="ip_address" type="text" size="50" value="<?php echo $ip_address;?>" />
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">ホスト名</td>
          <td bgcolor="#E2DBD6">
          	<input name="host_name" type="text" size="50" value="<?php echo $host_name;?>" />
          </td>
        </tr>
        	
        <tr>
          <td bgcolor="#E2DBD6">個体識別</td>
          <td bgcolor="#E2DBD6">
          	<input name="uid" type="text" size="50" value="<?php echo $uid;?>" />
            <br />例）IMY0rMH
          </td>
        </tr>
        
        <tr>
          <td bgcolor="#E2DBD6">クリック日時</td>
          <td bgcolor="#E2DBD6">
						<? $var = "created_at_from";?>
            <input name="<? echo $var;?>" type="text" size="20" value="<?php echo $$var;?>" />
            〜
						<? $var = "created_at_to";?>
            <input name="<? echo $var;?>" type="text" size="20" value="<?php echo $$var;?>" />
            <br />例）<?=date('Y-m-d');?>
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">成果反映日時</td>
          <td bgcolor="#E2DBD6">
						<? $var = "action_complete_date_from";?>
            <input name="<? echo $var;?>" type="text" size="20" value="<?php echo $$var;?>" />
            〜
						<? $var = "action_complete_date_to";?>
            <input name="<? echo $var;?>" type="text" size="20" value="<?php echo $$var;?>" />
            <br />例）<?=date('Y-m-d');?>
          </td>
        </tr>
        
        <tr>
          <td bgcolor="#E2DBD6">成果反映中</td>
          <td bgcolor="#E2DBD6">
          	<input name="status" type="checkbox" value="2" <?php if($status==2) { echo " checked ";}?> />
          </td>
        </tr>
        <tr>
          <td bgcolor="#E2DBD6">link URL</td>
          <td bgcolor="#E2DBD6">
          	<input name="s_link" type="checkbox" value="1" <?php if($s_link==2) { echo " checked ";}?> />
            <P>link URLがhttp://からでは無いものを調べる</P>
          </td>
        </tr>
      </table>
      <br />
      <br />
      <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <td align="center" valign="middle" bgcolor="#B19F92">
            <input type="submit" value="この内容で検索" style="width:150px;height:30px">
          </td>
        </tr>
      </table>
      </form>
      <br />
      <?
			if($where != "")
			{
			?>
      <div align="right"><a href="./session_csv.php?<? echo $_SERVER["QUERY_STRING"];?>" target="_blank">CSVダウンロード</a></div>
      すべて：<?=$numrows;?>
      <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#f1ebdb">
        <tr>
          <th align="center" bgcolor="#E2DBD6">管理ID</th>
          <th align="center" bgcolor="#E2DBD6">セッションID<br />個体識別</th>
          <th align="center" bgcolor="#E2DBD6">メディアとのセッション</th>
          <th align="center" bgcolor="#E2DBD6">成果反映</th>
          <th align="center" bgcolor="#E2DBD6">広告ID<br />媒体ID</th>
          <th align="center" bgcolor="#E2DBD6">登録日<br />修正日<br />削除日</th>
        </tr>
        <tr>
          <td align="center"><a href="?order_name=id&order=desc">▼</a>｜<a href="?order_name=id">▲</a></th>
          <td align="center"><a href="?order_name=session_id&order=desc">▼</a>｜<a href="?order_name=session_id">▲</a></th>
          <td align="center"><a href="?order_name=point_back_parameter&order=desc">▼</a>｜<a href="?order_name=point_back_parameter">▲</a></th>
          <td align="center"><a href="?order_name=status&order=desc">▼</a>｜<a href="?order_name=status">▲</a></th>
          <td align="center"><a href="?order_name=advert_id&order=desc">▼</a>｜<a href="?order_name=advert_id">▲</a></th>
          <td align="center"><a href="?order_name=created_at&order=desc">▼</a>｜<a href="?order_name=created_at">▲</a></th>
        </tr>
				<?php
          while($row=mysql_fetch_array($result))
          {
        ?>
        <tr>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
						<?php echo $row["id"];?><br />
            <?php 
						if(!($row["status"]=="2" && $row["action_complete_date"]!="0000-00-00 00:00:00"))
						{
						?>
            <a href="#" onClick='fnChangeSel("<?php echo $row["session_id"];?>");'>成果返却</a>
            <br />
            <?
						}
						?>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
            <?
								echo $row["session_id"];
						?><br />
            個体識別(UID)：<?php echo $row["uid"];?>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $row["point_back_parameter"];?></td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
						<?php 
						if($row["status"]=="2" && $row["action_complete_date"]!="0000-00-00 00:00:00")
						{
							echo "成果反映中<br >".$row["action_complete_date"];
						}
						else
						{
							echo "(".$row["status"].")<br >".$row["action_complete_date"];							
						}
						?>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF">
						広告ID：<?php echo $row["advert_id"];?><br />
            媒体ID：<?php echo $row["media_id"];?>
          </td>
          <td align="center" valign="middle" bgcolor="#FFFFFF"><?php echo $row["created_at"]."<br />".$row["updated_at"]."<br />".$row["deleted_at"];?></td>
        </tr>
        <tr>
        	<td colspan="6"><? echo $row["link_url"];?></td>
        </tr>
			<?php
        }
      ?>
      </table>
      <?
			}
			?>

		<div style="text-align:center;">
<?
		foreach($_GET as $key => $value){ 
			if($key!="page")
			{
				$query .= "&".$key."=".trim($value);
			}
		}
?>

<?
	$search_page_count = 10;

	If ($page!="1")
	{
		echo "[ <a href='".$_SERVER["PHP_SELF"]."?page=".((int)$page-1).$query."'>←前</a> ] ";
	}

	If ($view_count < $numrows)
	{

		//表示したいページ件数より少ない場合
		If(ceil(($numrows/$view_count)+1) < $search_page_count)
		{
			For ($i=1;$i<ceil(($numrows/$view_count)+1);$i++) 
			{
				If ($i == $page)
				{
					print "  [ <font color='gray'>".$i."</font> ]  ";
				}
				Else
				{
					print "  [ <a href='".$_SERVER["PHP_SELF"]."?page=".$i.$query."'>".$i."</a> ]  ";
				}
			}
		}
		else
		{
			//現在ページが表示したいページより少ない
			if(($page+($search_page_count/2)-1) < $search_page_count)
			{
				For ($i=1;$i<$search_page_count+1;$i++) 
				{
					If ($i == $page)
					{
						print "  [ <font color='gray'>".$i."</font> ]  ";
					}
					Else
					{
						print "  [ <a href='".$_SERVER["PHP_SELF"]."?page=".$i.$query."'>".$i."</a> ]  ";
					}
				}
			}
			//現在ページが表示したいページより多い
			else if((ceil(($numrows/$view_count)+1) > $search_page_count) && ((ceil($numrows/$view_count)+1)-($search_page_count/2) < $page))
			{
				$start = ($page-$search_page_count+(ceil(($numrows/$view_count)+1)-$page));
				For ($i=$start;$i<ceil(($numrows/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
						print "  [ <font color='gray'>".$i."</font> ]  ";
					}
					Else
					{
						print "  [ <a href='".$_SERVER["PHP_SELF"]."?page=".$i.$query."'>".$i."</a> ]  ";
					}
				}
			}
			else
			{
	
				$min_page = ($page-($search_page_count/2));
				if ($min_page<1)
				{
					$min_page = 1;
				}
				$max_page = ceil(($numrows/$view_count)+1);
				
				if ($max_page>($page+($search_page_count/2)-1))
				{
					$max_page = ($page+($search_page_count/2));
				}
				
				For ($i=$min_page;$i<$max_page;$i++) 
				{
					If ($i == $page)
					{
						print "  [ <font color='gray'>".$i."</font> ]  ";
					}
					Else
					{
						print "  [ <a href='".$_SERVER["PHP_SELF"]."?page=".$i.$query."'>".$i."</a> ]  ";
					}
				}
			}
		}
	}

	if (ceil(($numrows/$view_count)+1)!="1" && $page<ceil(($numrows/$view_count)))
	{
?>
		[ <a href="<?=$_SERVER["PHP_SELF"]."?page=".($page+1).$query;?>">次→</a> ]
<?	}

?>
		</div><!-- //center-->


      </td>
  </tr>
</table>

    </td>
  </tr>
</table>

<!-------footer---------->

<?php include("../include/footer.php"); ?>

<!-------end---------->

</div>
</div>
<?
	mysql_close($db);
?>
</body>
</html>
