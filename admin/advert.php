<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );

//require_once( '../dao/AdvertAttentionDao.php' );
//require_once( '../dto/AdvertAttention.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$advert_dao = new AdvertDao();

	$image_directory = "../advert_image/";


	//広告主
	$advert_client_dao = new AdvertClientDao();
	$advert_client_array = array();
	foreach($advert_client_dao->getAllAdvertClient() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getClientName());
		$advert_client_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_client_array", $advert_client_array);

	//広告カテゴリー
	$advert_category_dao = new AdvertCategoryDao();
	$advert_category_array = array();
	foreach($advert_category_dao->getAllAdvertCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$advert_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_category_array", $advert_category_array);


	// 媒体カテゴリー
	$sql = " SELECT * FROM media_categories WHERE deleted_at IS NULL ";
	$db_result = $common_dao->db_query($sql);

	if($db_result){
		$media_categories_array = $db_result;
		$smarty->assign("media_categories_array", $media_categories_array);
	}



	if(isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} elseif(isset($_POST['mode'])) {
		$mode = $_POST['mode'];
	}

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
	} else {
		$id = do_escape_quotes($_POST['id']);
	}

	if(isset($_POST['advert_client_id']) && $_POST['advert_client_id'] != ""){
		$advert_client_id = do_escape_quotes($_POST['advert_client_id']);
	} else {
		$advert_client_id = "0";
	}
	$advert_category_id = do_escape_quotes($_POST['advert_category_id']);

	$support_iphone_docomo = do_escape_quotes($_POST['support_iphone_docomo']);
	$support_iphone_softbank = do_escape_quotes($_POST['support_iphone_softbank']);
	$support_iphone_au = do_escape_quotes($_POST['support_iphone_au']);
	$support_iphone_pc = do_escape_quotes($_POST['support_iphone_pc']);
	$support_android_docomo = do_escape_quotes($_POST['support_android_docomo']);
	$support_android_softbank = do_escape_quotes($_POST['support_android_softbank']);
	$support_android_au = do_escape_quotes($_POST['support_android_au']);
	$support_android_pc = do_escape_quotes($_POST['support_android_pc']);
	$support_pc = do_escape_quotes($_POST['support_pc']);

	$advert_name = do_escape_quotes($_POST['advert_name']);

	$site_url_iphone_docomo = do_escape_quotes($_POST['site_url_iphone_docomo']);
	$site_url_iphone_softbank = do_escape_quotes($_POST['site_url_iphone_softbank']);
	$site_url_iphone_au = do_escape_quotes($_POST['site_url_iphone_au']);
	$site_url_android_docomo = do_escape_quotes($_POST['site_url_android_docomo']);
	$site_url_android_softbank = do_escape_quotes($_POST['site_url_android_softbank']);
	$site_url_android_au = do_escape_quotes($_POST['site_url_android_au']);
	$site_url_pc = do_escape_quotes($_POST['site_url_pc']);
	$site_outline = do_escape_quotes($_POST['site_outline']);
	$click_price_client = do_escape_quotes($_POST['click_price_client']);
	$click_price_media = do_escape_quotes($_POST['click_price_media']);


	$action_price_client_iphone_docomo_1 = do_escape_quotes($_POST['action_price_client_iphone_docomo_1']);
	$action_price_client_iphone_softbank_1 = do_escape_quotes($_POST['action_price_client_iphone_softbank_1']);
	$action_price_client_iphone_au_1 = do_escape_quotes($_POST['action_price_client_iphone_au_1']);
	$action_price_client_iphone_pc_1 = do_escape_quotes($_POST['action_price_client_iphone_pc_1']);

	$action_price_client_iphone_docomo_2 = do_escape_quotes($_POST['action_price_client_iphone_docomo_2']);
	$action_price_client_iphone_softbank_2 = do_escape_quotes($_POST['action_price_client_iphone_softbank_2']);
	$action_price_client_iphone_au_2 = do_escape_quotes($_POST['action_price_client_iphone_au_2']);
	$action_price_client_iphone_pc_2 = do_escape_quotes($_POST['action_price_client_iphone_pc_2']);

	$action_price_client_iphone_docomo_3 = do_escape_quotes($_POST['action_price_client_iphone_docomo_3']);
	$action_price_client_iphone_softbank_3 = do_escape_quotes($_POST['action_price_client_iphone_softbank_3']);
	$action_price_client_iphone_au_3 = do_escape_quotes($_POST['action_price_client_iphone_au_3']);
	$action_price_client_iphone_pc_3 = do_escape_quotes($_POST['action_price_client_iphone_pc_3']);

	$action_price_client_iphone_docomo_4 = do_escape_quotes($_POST['action_price_client_iphone_docomo_4']);
	$action_price_client_iphone_softbank_4 = do_escape_quotes($_POST['action_price_client_iphone_softbank_4']);
	$action_price_client_iphone_au_4 = do_escape_quotes($_POST['action_price_client_iphone_au_4']);
	$action_price_client_iphone_pc_4 = do_escape_quotes($_POST['action_price_client_iphone_pc_4']);

	$action_price_client_iphone_docomo_5 = do_escape_quotes($_POST['action_price_client_iphone_docomo_5']);
	$action_price_client_iphone_softbank_5 = do_escape_quotes($_POST['action_price_client_iphone_softbank_5']);
	$action_price_client_iphone_au_5 = do_escape_quotes($_POST['action_price_client_iphone_au_5']);
	$action_price_client_iphone_pc_5 = do_escape_quotes($_POST['action_price_client_iphone_pc_5']);


	$action_price_media_iphone_docomo_1 = do_escape_quotes($_POST['action_price_media_iphone_docomo_1']);
	$action_price_media_iphone_softbank_1 = do_escape_quotes($_POST['action_price_media_iphone_softbank_1']);
	$action_price_media_iphone_au_1 = do_escape_quotes($_POST['action_price_media_iphone_au_1']);
	$action_price_media_iphone_pc_1 = do_escape_quotes($_POST['action_price_media_iphone_pc_1']);

	$action_price_media_iphone_docomo_2 = do_escape_quotes($_POST['action_price_media_iphone_docomo_2']);
	$action_price_media_iphone_softbank_2 = do_escape_quotes($_POST['action_price_media_iphone_softbank_2']);
	$action_price_media_iphone_au_2 = do_escape_quotes($_POST['action_price_media_iphone_au_2']);
	$action_price_media_iphone_pc_2 = do_escape_quotes($_POST['action_price_media_iphone_pc_2']);

	$action_price_media_iphone_docomo_3 = do_escape_quotes($_POST['action_price_media_iphone_docomo_3']);
	$action_price_media_iphone_softbank_3 = do_escape_quotes($_POST['action_price_media_iphone_softbank_3']);
	$action_price_media_iphone_au_3 = do_escape_quotes($_POST['action_price_media_iphone_au_3']);
	$action_price_media_iphone_pc_3 = do_escape_quotes($_POST['action_price_media_iphone_pc_3']);

	$action_price_media_iphone_docomo_4 = do_escape_quotes($_POST['action_price_media_iphone_docomo_4']);
	$action_price_media_iphone_softbank_4 = do_escape_quotes($_POST['action_price_media_iphone_softbank_4']);
	$action_price_media_iphone_au_4 = do_escape_quotes($_POST['action_price_media_iphone_au_4']);
	$action_price_media_iphone_pc_4 = do_escape_quotes($_POST['action_price_media_iphone_pc_4']);

	$action_price_media_iphone_docomo_5 = do_escape_quotes($_POST['action_price_media_iphone_docomo_5']);
	$action_price_media_iphone_softbank_5 = do_escape_quotes($_POST['action_price_media_iphone_softbank_5']);
	$action_price_media_iphone_au_5 = do_escape_quotes($_POST['action_price_media_iphone_au_5']);
	$action_price_media_iphone_pc_5 = do_escape_quotes($_POST['action_price_media_iphone_pc_5']);


	$action_price_client_android_docomo_1 = do_escape_quotes($_POST['action_price_client_android_docomo_1']);
	$action_price_client_android_softbank_1 = do_escape_quotes($_POST['action_price_client_android_softbank_1']);
	$action_price_client_android_au_1 = do_escape_quotes($_POST['action_price_client_android_au_1']);
	$action_price_client_android_pc_1 = do_escape_quotes($_POST['action_price_client_android_pc_1']);

	$action_price_client_android_docomo_2 = do_escape_quotes($_POST['action_price_client_android_docomo_2']);
	$action_price_client_android_softbank_2 = do_escape_quotes($_POST['action_price_client_android_softbank_2']);
	$action_price_client_android_au_2 = do_escape_quotes($_POST['action_price_client_android_au_2']);
	$action_price_client_android_pc_2 = do_escape_quotes($_POST['action_price_client_android_pc_2']);

	$action_price_client_android_docomo_3 = do_escape_quotes($_POST['action_price_client_android_docomo_3']);
	$action_price_client_android_softbank_3 = do_escape_quotes($_POST['action_price_client_android_softbank_3']);
	$action_price_client_android_au_3 = do_escape_quotes($_POST['action_price_client_android_au_3']);
	$action_price_client_android_pc_3 = do_escape_quotes($_POST['action_price_client_android_pc_3']);

	$action_price_client_android_docomo_4 = do_escape_quotes($_POST['action_price_client_android_docomo_4']);
	$action_price_client_android_softbank_4 = do_escape_quotes($_POST['action_price_client_android_softbank_4']);
	$action_price_client_android_au_4 = do_escape_quotes($_POST['action_price_client_android_au_4']);
	$action_price_client_android_pc_4 = do_escape_quotes($_POST['action_price_client_android_pc_4']);

	$action_price_client_android_docomo_5 = do_escape_quotes($_POST['action_price_client_android_docomo_5']);
	$action_price_client_android_softbank_5 = do_escape_quotes($_POST['action_price_client_android_softbank_5']);
	$action_price_client_android_au_5 = do_escape_quotes($_POST['action_price_client_android_au_5']);
	$action_price_client_android_pc_5 = do_escape_quotes($_POST['action_price_client_android_pc_5']);


	$action_price_media_android_docomo_1 = do_escape_quotes($_POST['action_price_media_android_docomo_1']);
	$action_price_media_android_softbank_1 = do_escape_quotes($_POST['action_price_media_android_softbank_1']);
	$action_price_media_android_au_1 = do_escape_quotes($_POST['action_price_media_android_au_1']);
	$action_price_media_android_pc_1 = do_escape_quotes($_POST['action_price_media_android_pc_1']);

	$action_price_media_android_docomo_2 = do_escape_quotes($_POST['action_price_media_android_docomo_2']);
	$action_price_media_android_softbank_2 = do_escape_quotes($_POST['action_price_media_android_softbank_2']);
	$action_price_media_android_au_2 = do_escape_quotes($_POST['action_price_media_android_au_2']);
	$action_price_media_android_pc_2 = do_escape_quotes($_POST['action_price_media_android_pc_2']);

	$action_price_media_android_docomo_3 = do_escape_quotes($_POST['action_price_media_android_docomo_3']);
	$action_price_media_android_softbank_3 = do_escape_quotes($_POST['action_price_media_android_softbank_3']);
	$action_price_media_android_au_3 = do_escape_quotes($_POST['action_price_media_android_au_3']);
	$action_price_media_android_pc_3 = do_escape_quotes($_POST['action_price_media_android_pc_3']);

	$action_price_media_android_docomo_4 = do_escape_quotes($_POST['action_price_media_android_docomo_4']);
	$action_price_media_android_softbank_4 = do_escape_quotes($_POST['action_price_media_android_softbank_4']);
	$action_price_media_android_au_4 = do_escape_quotes($_POST['action_price_media_android_au_4']);
	$action_price_media_android_pc_4 = do_escape_quotes($_POST['action_price_media_android_pc_4']);

	$action_price_media_android_docomo_5 = do_escape_quotes($_POST['action_price_media_android_docomo_5']);
	$action_price_media_android_softbank_5 = do_escape_quotes($_POST['action_price_media_android_softbank_5']);
	$action_price_media_android_au_5 = do_escape_quotes($_POST['action_price_media_android_au_5']);
	$action_price_media_android_pc_5 = do_escape_quotes($_POST['action_price_media_android_pc_5']);


	$action_price_client_pc_1 = do_escape_quotes($_POST['action_price_client_pc_1']);
	$action_price_client_pc_2 = do_escape_quotes($_POST['action_price_client_pc_2']);
	$action_price_client_pc_3 = do_escape_quotes($_POST['action_price_client_pc_3']);
	$action_price_client_pc_4 = do_escape_quotes($_POST['action_price_client_pc_4']);
	$action_price_client_pc_5 = do_escape_quotes($_POST['action_price_client_pc_5']);

	$action_price_media_pc_1 = do_escape_quotes($_POST['action_price_media_pc_1']);
	$action_price_media_pc_2 = do_escape_quotes($_POST['action_price_media_pc_2']);
	$action_price_media_pc_3 = do_escape_quotes($_POST['action_price_media_pc_3']);
	$action_price_media_pc_4 = do_escape_quotes($_POST['action_price_media_pc_4']);
	$action_price_media_pc_5 = do_escape_quotes($_POST['action_price_media_pc_5']);

	$price_type = do_escape_quotes($_POST['price_type']);
	$approval_flag = do_escape_quotes($_POST['approval_flag']);

	$ms_text_1 = do_escape_quotes($_POST['ms_text_1']);
	$ms_email_1 = do_escape_quotes($_POST['ms_email_1']);
	$ms_image_type_1 = do_escape_quotes($_POST['ms_image_type_1']);
	$ms_image_url_1 = do_escape_quotes($_POST['ms_image_url_1']);
	$ms_text_2 = do_escape_quotes($_POST['ms_text_2']);
	$ms_email_2 = do_escape_quotes($_POST['ms_email_2']);
	$ms_image_type_2 = do_escape_quotes($_POST['ms_image_type_2']);
	$ms_image_url_2 = do_escape_quotes($_POST['ms_image_url_2']);
	$ms_text_3 = do_escape_quotes($_POST['ms_text_3']);
	$ms_email_3 = do_escape_quotes($_POST['ms_email_3']);
	$ms_image_type_3 = do_escape_quotes($_POST['ms_image_type_3']);
	$ms_image_url_3 = do_escape_quotes($_POST['ms_image_url_3']);
	$ms_text_4 = do_escape_quotes($_POST['ms_text_4']);
	$ms_email_4 = do_escape_quotes($_POST['ms_email_4']);
	$ms_image_type_4 = do_escape_quotes($_POST['ms_image_type_4']);
	$ms_image_url_4 = do_escape_quotes($_POST['ms_image_url_4']);
	$ms_text_5 = do_escape_quotes($_POST['ms_text_5']);
	$ms_email_5 = do_escape_quotes($_POST['ms_email_5']);
	$ms_image_type_5 = do_escape_quotes($_POST['ms_image_type_5']);
	$ms_image_url_5 = do_escape_quotes($_POST['ms_image_url_5']);
	$unique_click_type = do_escape_quotes($_POST['unique_click_type']);

	$point_back_flag = do_escape_quotes($_POST['point_back_flag']);
	$adult_flag = do_escape_quotes($_POST['adult_flag']);
	$seo_flag = do_escape_quotes($_POST['seo_flag']);
	$listing_flag = do_escape_quotes($_POST['listing_flag']);
	$mail_magazine_flag = do_escape_quotes($_POST['mail_magazine_flag']);
	$community_flag = do_escape_quotes($_POST['community_flag']);

	$as_year = do_escape_quotes($_POST['as_year']);
	$as_month = do_escape_quotes($_POST['as_month']);
	$as_day = do_escape_quotes($_POST['as_day']);
	$ae_year = do_escape_quotes($_POST['ae_year']);
	$ae_month = do_escape_quotes($_POST['ae_month']);
	$ae_day = do_escape_quotes($_POST['ae_day']);
	$unrestraint_flag = do_escape_quotes($_POST['unrestraint_flag']);
	$test_flag = do_escape_quotes($_POST['test_flag']);
	$status = do_escape_quotes($_POST['status']);

	$ms_image_file_path_1 = do_escape_quotes($_POST['ms_image_file_path_1']);
	$ms_image_file_path_2 = do_escape_quotes($_POST['ms_image_file_path_2']);
	$ms_image_file_path_3 = do_escape_quotes($_POST['ms_image_file_path_3']);
	$ms_image_file_path_4 = do_escape_quotes($_POST['ms_image_file_path_4']);
	$ms_image_file_path_5 = do_escape_quotes($_POST['ms_image_file_path_5']);

	$method = do_escape_quotes($_POST['method']);
	$reverse = do_escape_quotes($_POST['reverse']);
	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);
	$search_id = do_escape_quotes($_POST['search_id']);
	if(isset($_POST['search_name']) && $_POST['search_name'] != ""){
		$search_name = do_escape_quotes($_POST['search_name']);
	} else {
		$search_name = "";
	}
	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$advert_start_date = "$as_year-$as_month-$as_day 00:00:00";
	$advert_end_date = "$ae_year-$ae_month-$ae_day 23:59:59";

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_as_year", $now_date['year']);
	$smarty->assign("set_as_month", $now_date['mon']);
	$smarty->assign("set_as_day", $now_date['mday']);
	$smarty->assign("set_ae_year", $now_date['year']);
	$smarty->assign("set_ae_month", $now_date['mon']);
	$smarty->assign("set_ae_day", $now_date['mday']);

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);


// ---------------------------------------------------------------------------------------- 2011/02/01 修正
	$list_sql = " SELECT a.*, ac.client_name as advert_client_name "
				. " FROM advert as a "
				. " left join advert_clients as ac on a.advert_client_id = ac.id "
				. " WHERE a.deleted_at is NULL ";
// --------------------------------------------------------------------------------------------------------


	if($mode != ''){
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./advert_input.tpl");
			exit();
// **************************************************************************
// 新規
// **************************************************************************
		}elseif($mode == 'insert_commit'){

			$error_flag = 0;

			if($advert_name == "") {
				$error_message = "広告名を入力してください。";
				$error_flag = 1;
			}

			if($ms_image_type_1 == 1) {
				if($_FILES['ms_image_file_1']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_1 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_2 == 1) {
				if($_FILES['ms_image_file_2']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_2 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_3 == 1) {
				if($_FILES['ms_image_file_3']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_3 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_4 == 1) {
				if($_FILES['ms_image_file_4']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_4 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_5 == 1) {
				if($_FILES['ms_image_file_5']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_5 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($error_flag == 0) {
// **************************************************************************
// insert
// **************************************************************************
				$advert_dao->transaction_start();

				$advert = new Advert();
				$advert->setAdvertClientId($advert_client_id);
				$advert->setAdvertCategoryId($advert_category_id);

				$advert->setSupportIphoneDocomo($support_iphone_docomo);
				$advert->setSupportIphoneSoftbank($support_iphone_softbank);
				$advert->setSupportIphoneAu($support_iphone_au);
				$advert->setSupportIphonePc($support_iphone_pc);

				$advert->setSupportAndroidDocomo($support_android_docomo);
				$advert->setSupportAndroidSoftbank($support_android_softbank);
				$advert->setSupportAndroidAu($support_android_au);
				$advert->setSupportAndroidPc($support_android_pc);

				$advert->setSupportPc($support_pc);

				$advert->setAdvertName($advert_name);

				$advert->setSiteUrlIphoneDocomo($site_url_iphone_docomo);
				$advert->setSiteUrlIphoneSoftbank($site_url_iphone_softbank);
				$advert->setSiteUrlIphoneAu($site_url_iphone_au);
				$advert->setSiteUrlAndroidDocomo($site_url_android_docomo);
				$advert->setSiteUrlAndroidSoftbank($site_url_android_softbank);
				$advert->setSiteUrlAndroidAu($site_url_android_au);
				$advert->setSiteUrlPc($site_url_pc);
				$advert->setSiteOutline($site_outline);
				$advert->setClickPriceClient($click_price_client);
				$advert->setClickPriceMedia($click_price_media);

				$advert->setActionPriceClientIphoneDocomo1($action_price_client_iphone_docomo_1);
				$advert->setActionPriceClientIphoneSoftbank1($action_price_client_iphone_softbank_1);
				$advert->setActionPriceClientIphoneAu1($action_price_client_iphone_au_1);
				$advert->setActionPriceClientIphonePc1($action_price_client_iphone_pc_1);

				$advert->setActionPriceClientIphoneDocomo2($action_price_client_iphone_docomo_2);
				$advert->setActionPriceClientIphoneSoftbank2($action_price_client_iphone_softbank_2);
				$advert->setActionPriceClientIphoneAu2($action_price_client_iphone_au_2);
				$advert->setActionPriceClientIphonePc2($action_price_client_iphone_pc_2);

				$advert->setActionPriceClientIphoneDocomo3($action_price_client_iphone_docomo_3);
				$advert->setActionPriceClientIphoneSoftbank3($action_price_client_iphone_softbank_3);
				$advert->setActionPriceClientIphoneAu3($action_price_client_iphone_au_3);
				$advert->setActionPriceClientIphonePc3($action_price_client_iphone_pc_3);

				$advert->setActionPriceClientIphoneDocomo4($action_price_client_iphone_docomo_4);
				$advert->setActionPriceClientIphoneSoftbank4($action_price_client_iphone_softbank_4);
				$advert->setActionPriceClientIphoneAu4($action_price_client_iphone_au_4);
				$advert->setActionPriceClientIphonePc4($action_price_client_iphone_pc_4);

				$advert->setActionPriceClientIphoneDocomo5($action_price_client_iphone_docomo_5);
				$advert->setActionPriceClientIphoneSoftbank5($action_price_client_iphone_softbank_5);
				$advert->setActionPriceClientIphoneAu5($action_price_client_iphone_au_5);
				$advert->setActionPriceClientIphonePc5($action_price_client_iphone_pc_5);


				$advert->setActionPriceMediaIphoneDocomo1($action_price_media_iphone_docomo_1);
				$advert->setActionPriceMediaIphoneSoftbank1($action_price_media_iphone_softbank_1);
				$advert->setActionPriceMediaIphoneAu1($action_price_media_iphone_au_1);
				$advert->setActionPriceMediaIphonePc1($action_price_media_iphone_pc_1);

				$advert->setActionPriceMediaIphoneDocomo2($action_price_media_iphone_docomo_2);
				$advert->setActionPriceMediaIphoneSoftbank2($action_price_media_iphone_softbank_2);
				$advert->setActionPriceMediaIphoneAu2($action_price_media_iphone_au_2);
				$advert->setActionPriceMediaIphonePc2($action_price_media_iphone_pc_2);

				$advert->setActionPriceMediaIphoneDocomo3($action_price_media_iphone_docomo_3);
				$advert->setActionPriceMediaIphoneSoftbank3($action_price_media_iphone_softbank_3);
				$advert->setActionPriceMediaIphoneAu3($action_price_media_iphone_au_3);
				$advert->setActionPriceMediaIphonePc3($action_price_media_iphone_pc_3);

				$advert->setActionPriceMediaIphoneDocomo4($action_price_media_iphone_docomo_4);
				$advert->setActionPriceMediaIphoneSoftbank4($action_price_media_iphone_softbank_4);
				$advert->setActionPriceMediaIphoneAu4($action_price_media_iphone_au_4);
				$advert->setActionPriceMediaIphonePc4($action_price_media_iphone_pc_4);

				$advert->setActionPriceMediaIphoneDocomo5($action_price_media_iphone_docomo_5);
				$advert->setActionPriceMediaIphoneSoftbank5($action_price_media_iphone_softbank_5);
				$advert->setActionPriceMediaIphoneAu5($action_price_media_iphone_au_5);
				$advert->setActionPriceMediaIphonePc5($action_price_media_iphone_pc_5);


				$advert->setActionPriceClientAndroidDocomo1($action_price_client_android_docomo_1);
				$advert->setActionPriceClientAndroidSoftbank1($action_price_client_android_softbank_1);
				$advert->setActionPriceClientAndroidAu1($action_price_client_android_au_1);
				$advert->setActionPriceClientAndroidPc1($action_price_client_android_pc_1);

				$advert->setActionPriceClientAndroidDocomo2($action_price_client_android_docomo_2);
				$advert->setActionPriceClientAndroidSoftbank2($action_price_client_android_softbank_2);
				$advert->setActionPriceClientAndroidAu2($action_price_client_android_au_2);
				$advert->setActionPriceClientAndroidPc2($action_price_client_android_pc_2);

				$advert->setActionPriceClientAndroidDocomo3($action_price_client_android_docomo_3);
				$advert->setActionPriceClientAndroidSoftbank3($action_price_client_android_softbank_3);
				$advert->setActionPriceClientAndroidAu3($action_price_client_android_au_3);
				$advert->setActionPriceClientAndroidPc3($action_price_client_android_pc_3);

				$advert->setActionPriceClientAndroidDocomo4($action_price_client_android_docomo_4);
				$advert->setActionPriceClientAndroidSoftbank4($action_price_client_android_softbank_4);
				$advert->setActionPriceClientAndroidAu4($action_price_client_android_au_4);
				$advert->setActionPriceClientAndroidPc5($action_price_client_android_pc_4);

				$advert->setActionPriceClientAndroidDocomo5($action_price_client_android_docomo_5);
				$advert->setActionPriceClientAndroidSoftbank5($action_price_client_android_softbank_5);
				$advert->setActionPriceClientAndroidAu5($action_price_client_android_au_5);
				$advert->setActionPriceClientAndroidPc5($action_price_client_android_pc_5);


				$advert->setActionPriceMediaAndroidDocomo1($action_price_media_android_docomo_1);
				$advert->setActionPriceMediaAndroidSoftbank1($action_price_media_android_softbank_1);
				$advert->setActionPriceMediaAndroidAu1($action_price_media_android_au_1);
				$advert->setActionPriceMediaAndroidPc1($action_price_media_android_pc_1);

				$advert->setActionPriceMediaAndroidDocomo2($action_price_media_android_docomo_2);
				$advert->setActionPriceMediaAndroidSoftbank2($action_price_media_android_softbank_2);
				$advert->setActionPriceMediaAndroidAu2($action_price_media_android_au_2);
				$advert->setActionPriceMediaAndroidPc2($action_price_media_android_pc_2);

				$advert->setActionPriceMediaAndroidDocomo3($action_price_media_android_docomo_3);
				$advert->setActionPriceMediaAndroidSoftbank3($action_price_media_android_softbank_3);
				$advert->setActionPriceMediaAndroidAu3($action_price_media_android_au_3);
				$advert->setActionPriceMediaAndroidPc3($action_price_media_android_pc_3);


				$advert->setActionPriceMediaAndroidDocomo4($action_price_media_android_docomo_4);
				$advert->setActionPriceMediaAndroidSoftbank4($action_price_media_android_softbank_4);
				$advert->setActionPriceMediaAndroidAu4($action_price_media_android_au_4);
				$advert->setActionPriceMediaAndroidPc4($action_price_media_android_pc_4);

				$advert->setActionPriceMediaAndroidDocomo5($action_price_media_android_docomo_5);
				$advert->setActionPriceMediaAndroidSoftbank5($action_price_media_android_softbank_5);
				$advert->setActionPriceMediaAndroidAu5($action_price_media_android_au_5);
				$advert->setActionPriceMediaAndroidPc5($action_price_media_android_pc_5);


				$advert->setActionPriceClientPc1($action_price_client_pc_1);
				$advert->setActionPriceClientPc2($action_price_client_pc_2);
				$advert->setActionPriceClientPc3($action_price_client_pc_3);
				$advert->setActionPriceClientPc4($action_price_client_pc_4);
				$advert->setActionPriceClientPc5($action_price_client_pc_5);

				$advert->setActionPriceMediaPc1($action_price_media_pc_1);
				$advert->setActionPriceMediaPc2($action_price_media_pc_2);
				$advert->setActionPriceMediaPc3($action_price_media_pc_3);
				$advert->setActionPriceMediaPc4($action_price_media_pc_4);
				$advert->setActionPriceMediaPc5($action_price_media_pc_5);

				$advert->setPriceType($price_type);
				$advert->setApprovalFlag($approval_flag);

				$advert->setMsText1($ms_text_1);
				$advert->setMsEmail1($ms_email_1);
				$advert->setMsImageType1($ms_image_type_1);
				$advert->setMsImageUrl1($ms_image_url_1);
				$advert->setMsText2($ms_text_2);
				$advert->setMsEmail2($ms_email_2);
				$advert->setMsImageType2($ms_image_type_2);
				$advert->setMsImageUrl2($ms_image_url_2);
				$advert->setMsText3($ms_text_3);
				$advert->setMsEmail3($ms_email_3);
				$advert->setMsImageType3($ms_image_type_3);
				$advert->setMsImageUrl3($ms_image_url_3);
				$advert->setMsText4($ms_text_4);
				$advert->setMsEmail4($ms_email_4);
				$advert->setMsImageType4($ms_image_type_4);
				$advert->setMsImageUrl4($ms_image_url_4);
				$advert->setMsText5($ms_text_5);
				$advert->setMsEmail5($ms_email_5);
				$advert->setMsImageType5($ms_image_type_5);
				$advert->setMsImageUrl5($ms_image_url_5);
				$advert->setUniqueClickType($unique_click_type);

				$advert->setPointBackFlag($point_back_flag);
				$advert->setAdultFlag($adult_flag);
				$advert->setSeoFlag($seo_flag);
				$advert->setListingFlag($listing_flag);
				$advert->setMailMagazineFlag($mail_magazine_flag);
				$advert->setCommunityFlag($community_flag);

				$advert->setAdvertStartDate($advert_start_date);
				$advert->setAdvertEndDate($advert_end_date);
				$advert->setUnrestraintFlag($unrestraint_flag);
				$advert->setTestFlag($test_flag);
				$advert->setStatus($status);

				//INSERTを実行
				$db_result = $advert_dao->insertAdvert($advert, $result_message);
				if($db_result) {
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

// ----------------------------------------------------------------------------------------- 20111012ここまで
					$form_data = array('id' => $id,
										'advert_client_id' => $advert_client_id,
										'advert_category_id' => $advert_category_id,

										'support_iphone_docomo' => $support_iphone_docomo,
										'support_iphone_softbank' => $support_iphone_softbank,
										'support_iphone_au' => $support_iphone_au,
										'support_iphone_pc' => $support_iphone_pc,

										'support_android_docomo' => $support_android_docomo,
										'support_android_softbank' => $support_android_softbank,
										'support_android_au' => $support_android_au,
										'support_android_pc' => $support_android_pc,

										'support_pc' => $support_pc,

										'advert_name' => $advert_name,

										'site_url_iphone_docomo' => $site_url_iphone_docomo,
										'site_url_iphone_softbank' => $site_url_iphone_softbank,
										'site_url_iphone_au' => $site_url_iphone_au,
										'site_url_android_docomo' => $site_url_android_docomo,
										'site_url_android_softbank' => $site_url_android_softbank,
										'site_url_android_au' => $site_url_android_au,
										'site_url_pc' => $site_url_pc,
										'site_outline' => $site_outline,
										'click_price_client' => $click_price_client,
										'click_price_media' => $click_price_media,

										'action_price_client_iphone_docomo_1' => $action_price_client_iphone_docomo_1,
										'action_price_client_iphone_softbank_1' => $action_price_client_iphone_softbank_1,
										'action_price_client_iphone_au_1' => $action_price_client_iphone_au_1,
										'action_price_client_iphone_pc_1' => $action_price_client_iphone_pc_1,

										'action_price_client_iphone_docomo_2' => $action_price_client_iphone_docomo_2,
										'action_price_client_iphone_softbank_2' => $action_price_client_iphone_softbank_2,
										'action_price_client_iphone_au_2' => $action_price_client_iphone_au_2,
										'action_price_client_iphone_pc_2' => $action_price_client_iphone_pc_2,

										'action_price_client_iphone_docomo_3' => $action_price_client_iphone_docomo_3,
										'action_price_client_iphone_softbank_3' => $action_price_client_iphone_softbank_3,
										'action_price_client_iphone_au_3' => $action_price_client_iphone_au_3,
										'action_price_client_iphone_pc_3' => $action_price_client_iphone_pc_3,

										'action_price_client_iphone_docomo_4' => $action_price_client_iphone_docomo_4,
										'action_price_client_iphone_softbank_4' => $action_price_client_iphone_softbank_4,
										'action_price_client_iphone_au_4' => $action_price_client_iphone_au_4,
										'action_price_client_iphone_pc_4' => $action_price_client_iphone_pc_4,

										'action_price_client_iphone_docomo_5' => $action_price_client_iphone_docomo_5,
										'action_price_client_iphone_softbank_5' => $action_price_client_iphone_softbank_5,
										'action_price_client_iphone_au_5' => $action_price_client_iphone_au_5,
										'action_price_client_iphone_pc_5' => $action_price_client_iphone_pc_5,


										'action_price_media_iphone_docomo_1' => $action_price_media_iphone_docomo_1,
										'action_price_media_iphone_softbank_1' => $action_price_media_iphone_softbank_1,
										'action_price_media_iphone_au_1' => $action_price_media_iphone_au_1,
										'action_price_media_iphone_pc_1' => $action_price_media_iphone_pc_1,

										'action_price_media_iphone_docomo_2' => $action_price_media_iphone_docomo_2,
										'action_price_media_iphone_softbank_2' => $action_price_media_iphone_softbank_2,
										'action_price_media_iphone_au_2' => $action_price_media_iphone_au_2,
										'action_price_media_iphone_pc_2' => $action_price_media_iphone_pc_2,

										'action_price_media_iphone_docomo_3' => $action_price_media_iphone_docomo_3,
										'action_price_media_iphone_softbank_3' => $action_price_media_iphone_softbank_3,
										'action_price_media_iphone_au_3' => $action_price_media_iphone_au_3,
										'action_price_media_iphone_pc_3' => $action_price_media_iphone_pc_3,

										'action_price_media_iphone_docomo_4' => $action_price_media_iphone_docomo_4,
										'action_price_media_iphone_softbank_4' => $action_price_media_iphone_softbank_4,
										'action_price_media_iphone_au_4' => $action_price_media_iphone_au_4,
										'action_price_media_iphone_pc_4' => $action_price_media_iphone_pc_4,

										'action_price_media_iphone_docomo_5' => $action_price_media_iphone_docomo_5,
										'action_price_media_iphone_softbank_5' => $action_price_media_iphone_softbank_5,
										'action_price_media_iphone_au_5' => $action_price_media_iphone_au_5,
										'action_price_media_iphone_pc_5' => $action_price_media_iphone_pc_5,


										'action_price_client_android_docomo_1' => $action_price_client_android_docomo_1,
										'action_price_client_android_softbank_1' => $action_price_client_android_softbank_1,
										'action_price_client_android_au_1' => $action_price_client_android_au_1,
										'action_price_client_android_pc_1' => $action_price_client_android_pc_1,

										'action_price_client_android_docomo_2' => $action_price_client_android_docomo_2,
										'action_price_client_android_softbank_2' => $action_price_client_android_softbank_2,
										'action_price_client_android_au_2' => $action_price_client_android_au_2,
										'action_price_client_android_pc_2' => $action_price_client_android_pc_2,

										'action_price_client_android_docomo_3' => $action_price_client_android_docomo_3,
										'action_price_client_android_softbank_3' => $action_price_client_android_softbank_3,
										'action_price_client_android_au_3' => $action_price_client_android_au_3,
										'action_price_client_android_pc_3' => $action_price_client_android_pc_3,

										'action_price_client_android_docomo_4' => $action_price_client_android_docomo_4,
										'action_price_client_android_softbank_4' => $action_price_client_android_softbank_4,
										'action_price_client_android_au_4' => $action_price_client_android_au_4,
										'action_price_client_android_pc_4' => $action_price_client_android_pc_4,

										'action_price_client_android_docomo_5' => $action_price_client_android_docomo_5,
										'action_price_client_android_softbank_5' => $action_price_client_android_softbank_5,
										'action_price_client_android_au_5' => $action_price_client_android_au_5,
										'action_price_client_android_pc_5' => $action_price_client_android_pc_5,


										'action_price_media_android_docomo_1' => $action_price_media_android_docomo_1,
										'action_price_media_android_softbank_1' => $action_price_media_android_softbank_1,
										'action_price_media_android_au_1' => $action_price_media_android_au_1,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_1,

										'action_price_media_android_docomo_2' => $action_price_media_android_docomo_2,
										'action_price_media_android_softbank_2' => $action_price_media_android_softbank_2,
										'action_price_media_android_au_2' => $action_price_media_android_au_2,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_2,

										'action_price_media_android_docomo_3' => $action_price_media_android_docomo_3,
										'action_price_media_android_softbank_3' => $action_price_media_android_softbank_3,
										'action_price_media_android_au_3' => $action_price_media_android_au_3,
										'action_price_media_android_pc_3' => $action_price_media_android_pc_3,

										'action_price_media_android_docomo_4' => $action_price_media_android_docomo_4,
										'action_price_media_android_softbank_4' => $action_price_media_android_softbank_4,
										'action_price_media_android_au_4' => $action_price_media_android_au_4,
										'action_price_media_android_pc_4' => $action_price_media_android_pc_4,

										'action_price_media_android_docomo_5' => $action_price_media_android_docomo_5,
										'action_price_media_android_softbank_5' => $action_price_media_android_softbank_5,
										'action_price_media_android_au_5' => $action_price_media_android_au_5,
										'action_price_media_android_pc_5' => $action_price_media_android_pc_5,


										'action_price_client_pc_1' => $action_price_client_pc_1,
										'action_price_client_pc_2' => $action_price_client_pc_2,
										'action_price_client_pc_3' => $action_price_client_pc_3,
										'action_price_client_pc_4' => $action_price_client_pc_4,
										'action_price_client_pc_5' => $action_price_client_pc_5,

										'action_price_media_pc_1' => $action_price_media_pc_1,
										'action_price_media_pc_2' => $action_price_media_pc_2,
										'action_price_media_pc_3' => $action_price_media_pc_3,
										'action_price_media_pc_4' => $action_price_media_pc_4,
										'action_price_media_pc_5' => $action_price_media_pc_5,

										'price_type' => $price_type,
										'approval_flag' => $approval_flag,

										'ms_text_1' => $ms_text_1,
										'ms_email_1' => $ms_email_1,
										'ms_image_type_1' => $ms_image_type_1,
										'ms_image_url_1' => $ms_image_url_1,
										'ms_text_2' => $ms_text_2,
										'ms_email_2' => $ms_email_2,
										'ms_image_type_2' => $ms_image_type_2,
										'ms_image_url_2' => $ms_image_url_2,
										'ms_text_3' => $ms_text_3,
										'ms_email_3' => $ms_email_3,
										'ms_image_type_3' => $ms_image_type_3,
										'ms_image_url_3' => $ms_image_url_3,
										'ms_text_4' => $ms_text_4,
										'ms_email_4' => $ms_email_4,
										'ms_image_type_4' => $ms_image_type_4,
										'ms_image_url_4' => $ms_image_url_4,
										'ms_text_5' => $ms_text_5,
										'ms_email_5' => $ms_email_5,
										'ms_image_type_5' => $ms_image_type_5,
										'ms_image_url_5' => $ms_image_url_5,
										'unique_click_type' => $unique_click_type,

										'point_back_flag' => $point_back_flag,
										'adult_flag' => $adult_flag,
										'seo_flag' => $seo_flag,
										'listing_flag' => $listing_flag,
										'mail_magazine_flag' => $mail_magazine_flag,
										'community_flag' => $community_flag,

										'advert_start_date' => $advert_start_date,
										'advert_end_date' => $advert_end_date,
										'unrestraint_flag' => $unrestraint_flag,
										'test_flag' => $test_flag,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("set_as_year", $as_year);
					$smarty->assign("set_as_month", $as_month);
					$smarty->assign("set_as_day", $as_day);
					$smarty->assign("set_ae_year", $ae_year);
					$smarty->assign("set_ae_month", $ae_month);
					$smarty->assign("set_ae_day", $ae_day);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./advert_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'advert_client_id' => $advert_client_id,
									'advert_category_id' => $advert_category_id,

									'support_iphone_docomo' => $support_iphone_docomo,
									'support_iphone_softbank' => $support_iphone_softbank,
									'support_iphone_au' => $support_iphone_au,
									'support_iphone_pc' => $support_iphone_pc,

									'support_android_docomo' => $support_android_docomo,
									'support_android_softbank' => $support_android_softbank,
									'support_android_au' => $support_android_au,
									'support_android_pc' => $support_android_pc,

									'support_pc' => $support_pc,

									'advert_name' => $advert_name,

									'site_url_iphone_docomo' => $site_url_iphone_docomo,
									'site_url_iphone_softbank' => $site_url_iphone_softbank,
									'site_url_iphone_au' => $site_url_iphone_au,
									'site_url_android_docomo' => $site_url_android_docomo,
									'site_url_android_softbank' => $site_url_android_softbank,
									'site_url_android_au' => $site_url_android_au,
									'site_url_pc' => $site_url_pc,
									'site_outline' => $site_outline,
									'click_price_client' => $click_price_client,
									'click_price_media' => $click_price_media,


										'action_price_client_iphone_docomo_1' => $action_price_client_iphone_docomo_1,
										'action_price_client_iphone_softbank_1' => $action_price_client_iphone_softbank_1,
										'action_price_client_iphone_au_1' => $action_price_client_iphone_au_1,
										'action_price_client_iphone_pc_1' => $action_price_client_iphone_pc_1,

										'action_price_client_iphone_docomo_2' => $action_price_client_iphone_docomo_2,
										'action_price_client_iphone_softbank_2' => $action_price_client_iphone_softbank_2,
										'action_price_client_iphone_au_2' => $action_price_client_iphone_au_2,
										'action_price_client_iphone_pc_2' => $action_price_client_iphone_pc_2,

										'action_price_client_iphone_docomo_3' => $action_price_client_iphone_docomo_3,
										'action_price_client_iphone_softbank_3' => $action_price_client_iphone_softbank_3,
										'action_price_client_iphone_au_3' => $action_price_client_iphone_au_3,
										'action_price_client_iphone_pc_3' => $action_price_client_iphone_pc_3,

										'action_price_client_iphone_docomo_4' => $action_price_client_iphone_docomo_4,
										'action_price_client_iphone_softbank_4' => $action_price_client_iphone_softbank_4,
										'action_price_client_iphone_au_4' => $action_price_client_iphone_au_4,
										'action_price_client_iphone_pc_4' => $action_price_client_iphone_pc_4,

										'action_price_client_iphone_docomo_5' => $action_price_client_iphone_docomo_5,
										'action_price_client_iphone_softbank_5' => $action_price_client_iphone_softbank_5,
										'action_price_client_iphone_au_5' => $action_price_client_iphone_au_5,
										'action_price_client_iphone_pc_5' => $action_price_client_iphone_pc_5,


										'action_price_media_iphone_docomo_1' => $action_price_media_iphone_docomo_1,
										'action_price_media_iphone_softbank_1' => $action_price_media_iphone_softbank_1,
										'action_price_media_iphone_au_1' => $action_price_media_iphone_au_1,
										'action_price_media_iphone_pc_1' => $action_price_media_iphone_pc_1,

										'action_price_media_iphone_docomo_2' => $action_price_media_iphone_docomo_2,
										'action_price_media_iphone_softbank_2' => $action_price_media_iphone_softbank_2,
										'action_price_media_iphone_au_2' => $action_price_media_iphone_au_2,
										'action_price_media_iphone_pc_2' => $action_price_media_iphone_pc_2,

										'action_price_media_iphone_docomo_3' => $action_price_media_iphone_docomo_3,
										'action_price_media_iphone_softbank_3' => $action_price_media_iphone_softbank_3,
										'action_price_media_iphone_au_3' => $action_price_media_iphone_au_3,
										'action_price_media_iphone_pc_3' => $action_price_media_iphone_pc_3,

										'action_price_media_iphone_docomo_4' => $action_price_media_iphone_docomo_4,
										'action_price_media_iphone_softbank_4' => $action_price_media_iphone_softbank_4,
										'action_price_media_iphone_au_4' => $action_price_media_iphone_au_4,
										'action_price_media_iphone_pc_4' => $action_price_media_iphone_pc_4,

										'action_price_media_iphone_docomo_5' => $action_price_media_iphone_docomo_5,
										'action_price_media_iphone_softbank_5' => $action_price_media_iphone_softbank_5,
										'action_price_media_iphone_au_5' => $action_price_media_iphone_au_5,
										'action_price_media_iphone_pc_5' => $action_price_media_iphone_pc_5,


										'action_price_client_android_docomo_1' => $action_price_client_android_docomo_1,
										'action_price_client_android_softbank_1' => $action_price_client_android_softbank_1,
										'action_price_client_android_au_1' => $action_price_client_android_au_1,
										'action_price_client_android_pc_1' => $action_price_client_android_pc_1,

										'action_price_client_android_docomo_2' => $action_price_client_android_docomo_2,
										'action_price_client_android_softbank_2' => $action_price_client_android_softbank_2,
										'action_price_client_android_au_2' => $action_price_client_android_au_2,
										'action_price_client_android_pc_2' => $action_price_client_android_pc_2,

										'action_price_client_android_docomo_3' => $action_price_client_android_docomo_3,
										'action_price_client_android_softbank_3' => $action_price_client_android_softbank_3,
										'action_price_client_android_au_3' => $action_price_client_android_au_3,
										'action_price_client_android_pc_3' => $action_price_client_android_pc_3,

										'action_price_client_android_docomo_4' => $action_price_client_android_docomo_4,
										'action_price_client_android_softbank_4' => $action_price_client_android_softbank_4,
										'action_price_client_android_au_4' => $action_price_client_android_au_4,
										'action_price_client_android_pc_4' => $action_price_client_android_pc_4,

										'action_price_client_android_docomo_5' => $action_price_client_android_docomo_5,
										'action_price_client_android_softbank_5' => $action_price_client_android_softbank_5,
										'action_price_client_android_au_5' => $action_price_client_android_au_5,
										'action_price_client_android_pc_5' => $action_price_client_android_pc_5,


										'action_price_media_android_docomo_1' => $action_price_media_android_docomo_1,
										'action_price_media_android_softbank_1' => $action_price_media_android_softbank_1,
										'action_price_media_android_au_1' => $action_price_media_android_au_1,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_1,

										'action_price_media_android_docomo_2' => $action_price_media_android_docomo_2,
										'action_price_media_android_softbank_2' => $action_price_media_android_softbank_2,
										'action_price_media_android_au_2' => $action_price_media_android_au_2,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_2,

										'action_price_media_android_docomo_3' => $action_price_media_android_docomo_3,
										'action_price_media_android_softbank_3' => $action_price_media_android_softbank_3,
										'action_price_media_android_au_3' => $action_price_media_android_au_3,
										'action_price_media_android_pc_3' => $action_price_media_android_pc_3,

										'action_price_media_android_docomo_4' => $action_price_media_android_docomo_4,
										'action_price_media_android_softbank_4' => $action_price_media_android_softbank_4,
										'action_price_media_android_au_4' => $action_price_media_android_au_4,
										'action_price_media_android_pc_4' => $action_price_media_android_pc_4,

										'action_price_media_android_docomo_5' => $action_price_media_android_docomo_5,
										'action_price_media_android_softbank_5' => $action_price_media_android_softbank_5,
										'action_price_media_android_au_5' => $action_price_media_android_au_5,
										'action_price_media_android_pc_5' => $action_price_media_android_pc_5,



									'action_price_client_pc_1' => $action_price_client_pc_1,
									'action_price_client_pc_2' => $action_price_client_pc_2,
									'action_price_client_pc_3' => $action_price_client_pc_3,
									'action_price_client_pc_4' => $action_price_client_pc_4,
									'action_price_client_pc_5' => $action_price_client_pc_5,

									'action_price_media_pc_1' => $action_price_media_pc_1,
									'action_price_media_pc_2' => $action_price_media_pc_2,
									'action_price_media_pc_3' => $action_price_media_pc_3,
									'action_price_media_pc_4' => $action_price_media_pc_4,
									'action_price_media_pc_5' => $action_price_media_pc_5,
									'price_type' => $price_type,
									'approval_flag' => $approval_flag,

									'ms_text_1' => $ms_text_1,
									'ms_email_1' => $ms_email_1,
									'ms_image_type_1' => $ms_image_type_1,
									'ms_image_url_1' => $ms_image_url_1,
									'ms_text_2' => $ms_text_2,
									'ms_email_2' => $ms_email_2,
									'ms_image_type_2' => $ms_image_type_2,
									'ms_image_url_2' => $ms_image_url_2,
									'ms_text_3' => $ms_text_3,
									'ms_email_3' => $ms_email_3,
									'ms_image_type_3' => $ms_image_type_3,
									'ms_image_url_3' => $ms_image_url_3,
									'ms_text_4' => $ms_text_4,
									'ms_email_4' => $ms_email_4,
									'ms_image_type_4' => $ms_image_type_4,
									'ms_image_url_4' => $ms_image_url_4,
									'ms_text_5' => $ms_text_5,
									'ms_email_5' => $ms_email_5,
									'ms_image_type_5' => $ms_image_type_5,
									'ms_image_url_5' => $ms_image_url_5,
									'unique_click_type' => $unique_click_type,

									'point_back_flag' => $point_back_flag,
									'adult_flag' => $adult_flag,
									'seo_flag' => $seo_flag,
									'listing_flag' => $listing_flag,
									'mail_magazine_flag' => $mail_magazine_flag,
									'community_flag' => $community_flag,

									'advert_start_date' => $advert_start_date,
									'advert_end_date' => $advert_end_date,
									'unrestraint_flag' => $unrestraint_flag,
									'test_flag' => $test_flag,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("set_as_year", $as_year);
				$smarty->assign("set_as_month", $as_month);
				$smarty->assign("set_as_day", $as_day);
				$smarty->assign("set_ae_year", $ae_year);
				$smarty->assign("set_ae_month", $ae_month);
				$smarty->assign("set_ae_day", $ae_day);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			}

// **************************************************************************
// 編集
// **************************************************************************
		}elseif($mode == 'edit'){
			$advert = new Advert;
			$advert = $advert_dao->getAdvertById($id);

			if(!is_null($advert)) {
				$form_data = array('id' => $advert->getId(),
									'advert_client_id' => $advert->getAdvertClientId(),
									'advert_category_id' => $advert->getAdvertCategoryId(),

									'support_iphone_docomo' => $advert->getSupportIphoneDocomo(),
									'support_iphone_softbank' => $advert->getSupportIphoneSoftbank(),
									'support_iphone_au' => $advert->getSupportIphoneAu(),
									'support_iphone_pc' => $advert->getSupportIphonePc(),

									'support_android_docomo' => $advert->getSupportAndroidDocomo(),
									'support_android_softbank' => $advert->getSupportAndroidSoftbank(),
									'support_android_au' => $advert->getSupportAndroidAu(),
									'support_android_pc' => $advert->getSupportAndroidPc(),
									'support_pc' => $advert->getSupportPc(),

									'advert_name' => $advert->getAdvertName(),

									'site_url_iphone_docomo' => $advert->getSiteUrlIphoneDocomo(),
									'site_url_iphone_softbank' => $advert->getSiteUrlIphoneSoftbank(),
									'site_url_iphone_au' => $advert->getSiteUrlIphoneAu(),
									'site_url_android_docomo' => $advert->getSiteUrlAndroidDocomo(),
									'site_url_android_softbank' => $advert->getSiteUrlAndroidSoftbank(),
									'site_url_android_au' => $advert->getSiteUrlAndroidAu(),
									'site_url_pc' => $advert->getSiteUrlPc(),
									'site_outline' => $advert->getSiteOutline(),
									'click_price_client' => $advert->getClickPriceClient(),
									'click_price_media' => $advert->getClickPriceMedia(),


										'action_price_client_iphone_docomo_1' => $advert->getActionPriceClientIphoneDocomo1(),
										'action_price_client_iphone_softbank_1' => $advert->getActionPriceClientIphoneSoftbank1(),
										'action_price_client_iphone_au_1' => $advert->getActionPriceClientIphoneAu1(),
										'action_price_client_iphone_pc_1' => $advert->getActionPriceClientIphonePc1(),

										'action_price_client_iphone_docomo_2' => $advert->getActionPriceClientIphoneDocomo2(),
										'action_price_client_iphone_softbank_2' => $advert->getActionPriceClientIphoneSoftbank2(),
										'action_price_client_iphone_au_2' => $advert->getActionPriceClientIphoneAu2(),
										'action_price_client_iphone_pc_2' => $advert->getActionPriceClientIphonePc2(),

										'action_price_client_iphone_docomo_3' => $advert->getActionPriceClientIphoneDocomo3(),
										'action_price_client_iphone_softbank_3' => $advert->getActionPriceClientIphoneSoftbank3(),
										'action_price_client_iphone_au_3' => $advert->getActionPriceClientIphoneAu3(),
										'action_price_client_iphone_pc_3' => $advert->getActionPriceClientIphonePc3(),

										'action_price_client_iphone_docomo_4' => $advert->getActionPriceClientIphoneDocomo4(),
										'action_price_client_iphone_softbank_4' => $advert->getActionPriceClientIphoneSoftbank4(),
										'action_price_client_iphone_au_4' => $advert->getActionPriceClientIphoneAu4(),
										'action_price_client_iphone_pc_4' => $advert->getActionPriceClientIphonePc4(),

										'action_price_client_iphone_docomo_5' => $advert->getActionPriceClientIphoneDocomo5(),
										'action_price_client_iphone_softbank_5' => $advert->getActionPriceClientIphoneSoftbank5(),
										'action_price_client_iphone_au_5' => $advert->getActionPriceClientIphoneAu5(),
										'action_price_client_iphone_pc_5' => $advert->getActionPriceClientIphonePc5(),

										'action_price_media_iphone_docomo_1' => $advert->getActionPriceMediaIphoneDocomo1(),
										'action_price_media_iphone_softbank_1' => $advert->getActionPriceMediaIphoneSoftbank1(),
										'action_price_media_iphone_au_1' => $advert->getActionPriceMediaIphoneAu1(),
										'action_price_media_iphone_pc_1' => $advert->getActionPriceMediaIphonePc1(),

										'action_price_media_iphone_docomo_2' => $advert->getActionPriceMediaIphoneDocomo2(),
										'action_price_media_iphone_softbank_2' => $advert->getActionPriceMediaIphoneSoftbank2(),
										'action_price_media_iphone_au_2' => $advert->getActionPriceMediaIphoneAu2(),
										'action_price_media_iphone_pc_2' => $advert->getActionPriceMediaIphonePc2(),

										'action_price_media_iphone_docomo_3' => $advert->getActionPriceMediaIphoneDocomo3(),
										'action_price_media_iphone_softbank_3' => $advert->getActionPriceMediaIphoneSoftbank3(),
										'action_price_media_iphone_au_3' => $advert->getActionPriceMediaIphoneAu3(),
										'action_price_media_iphone_pc_3' => $advert->getActionPriceMediaIphonePc3(),

										'action_price_media_iphone_docomo_4' => $advert->getActionPriceMediaIphoneDocomo4(),
										'action_price_media_iphone_softbank_4' => $advert->getActionPriceMediaIphoneSoftbank4(),
										'action_price_media_iphone_au_4' => $advert->getActionPriceMediaIphoneAu4(),
										'action_price_media_iphone_pc_4' => $advert->getActionPriceMediaIphonePc4(),

										'action_price_media_iphone_docomo_5' => $advert->getActionPriceMediaIphoneDocomo5(),
										'action_price_media_iphone_softbank_5' => $advert->getActionPriceMediaIphoneSoftbank5(),
										'action_price_media_iphone_au_5' => $advert->getActionPriceMediaIphoneAu5(),
										'action_price_media_iphone_pc_5' => $advert->getActionPriceMediaIphonePc5(),


										'action_price_client_android_docomo_1' => $advert->getActionPriceClientAndroidDocomo1(),
										'action_price_client_android_softbank_1' => $advert->getActionPriceClientAndroidSoftbank1(),
										'action_price_client_android_au_1' => $advert->getActionPriceClientAndroidAu1(),
										'action_price_client_android_pc_1' => $advert->getActionPriceClientAndroidPc1(),

										'action_price_client_android_docomo_2' => $advert->getActionPriceClientAndroidDocomo2(),
										'action_price_client_android_softbank_2' => $advert->getActionPriceClientAndroidSoftbank2(),
										'action_price_client_android_au_2' => $advert->getActionPriceClientAndroidAu2(),
										'action_price_client_android_pc_2' => $advert->getActionPriceClientAndroidPc2(),

										'action_price_client_android_docomo_3' => $advert->getActionPriceClientAndroidDocomo3(),
										'action_price_client_android_softbank_3' => $advert->getActionPriceClientAndroidSoftbank3(),
										'action_price_client_android_au_3' => $advert->getActionPriceClientAndroidAu3(),
										'action_price_client_android_pc_3' => $advert->getActionPriceClientAndroidPc3(),

										'action_price_client_android_docomo_4' => $advert->getActionPriceClientAndroidDocomo4(),
										'action_price_client_android_softbank_4' => $advert->getActionPriceClientAndroidSoftbank4(),
										'action_price_client_android_au_4' => $advert->getActionPriceClientAndroidAu4(),
										'action_price_client_android_pc_4' => $advert->getActionPriceClientAndroidPc4(),

										'action_price_client_android_docomo_5' => $advert->getActionPriceClientAndroidDocomo5(),
										'action_price_client_android_softbank_5' => $advert->getActionPriceClientAndroidSoftbank5(),
										'action_price_client_android_au_5' => $advert->getActionPriceClientAndroidAu5(),
										'action_price_client_android_pc_5' => $advert->getActionPriceClientAndroidPc5(),

										'action_price_media_android_docomo_1' => $advert->getActionPriceMediaAndroidDocomo1(),
										'action_price_media_android_softbank_1' => $advert->getActionPriceMediaAndroidSoftbank1(),
										'action_price_media_android_au_1' => $advert->getActionPriceMediaAndroidAu1(),
										'action_price_media_android_pc_1' => $advert->getActionPriceMediaAndroidPc1(),

										'action_price_media_android_docomo_2' => $advert->getActionPriceMediaAndroidDocomo2(),
										'action_price_media_android_softbank_2' => $advert->getActionPriceMediaAndroidSoftbank2(),
										'action_price_media_android_au_2' => $advert->getActionPriceMediaAndroidAu2(),
										'action_price_media_android_pc_2' => $advert->getActionPriceMediaAndroidPc2(),

										'action_price_media_android_docomo_3' => $advert->getActionPriceMediaAndroidDocomo3(),
										'action_price_media_android_softbank_3' => $advert->getActionPriceMediaAndroidSoftbank3(),
										'action_price_media_android_au_3' => $advert->getActionPriceMediaAndroidAu3(),
										'action_price_media_android_pc_3' => $advert->getActionPriceMediaAndroidPc3(),

										'action_price_media_android_docomo_4' => $advert->getActionPriceMediaAndroidDocomo4(),
										'action_price_media_android_softbank_4' => $advert->getActionPriceMediaAndroidSoftbank4(),
										'action_price_media_android_au_4' => $advert->getActionPriceMediaAndroidAu4(),
										'action_price_media_android_pc_4' => $advert->getActionPriceMediaAndroidPc4(),

										'action_price_media_android_docomo_5' => $advert->getActionPriceMediaAndroidDocomo5(),
										'action_price_media_android_softbank_5' => $advert->getActionPriceMediaAndroidSoftbank5(),
										'action_price_media_android_au_5' => $advert->getActionPriceMediaAndroidAu5(),
										'action_price_media_android_pc_5' => $advert->getActionPriceMediaAndroidPc5(),


									'action_price_client_pc_1' => $advert->getActionPriceClientPc1(),
									'action_price_client_pc_2' => $advert->getActionPriceClientPc2(),
									'action_price_client_pc_3' => $advert->getActionPriceClientPc3(),
									'action_price_client_pc_4' => $advert->getActionPriceClientPc4(),
									'action_price_client_pc_5' => $advert->getActionPriceClientPc5(),

									'action_price_media_pc_1' => $advert->getActionPriceMediaPc1(),
									'action_price_media_pc_2' => $advert->getActionPriceMediaPc2(),
									'action_price_media_pc_3' => $advert->getActionPriceMediaPc3(),
									'action_price_media_pc_4' => $advert->getActionPriceMediaPc4(),
									'action_price_media_pc_5' => $advert->getActionPriceMediaPc5(),

									'price_type' => $advert->getPriceType(),
									'approval_flag' => $advert->getApprovalFlag(),

									'ms_text_1' => $advert->getMsText1(),
									'ms_email_1' => $advert->getMsEmail1(),
									'ms_image_type_1' => $advert->getMsImageType1(),
									'ms_image_url_1' => $advert->getMsImageUrl1(),
									'ms_text_2' => $advert->getMsText2(),
									'ms_email_2' => $advert->getMsEmail2(),
									'ms_image_type_2' => $advert->getMsImageType2(),
									'ms_image_url_2' => $advert->getMsImageUrl2(),
									'ms_text_3' => $advert->getMsText3(),
									'ms_email_3' => $advert->getMsEmail3(),
									'ms_image_type_3' => $advert->getMsImageType3(),
									'ms_image_url_3' => $advert->getMsImageUrl3(),
									'ms_text_4' => $advert->getMsText4(),
									'ms_email_4' => $advert->getMsEmail4(),
									'ms_image_type_4' => $advert->getMsImageType4(),
									'ms_image_url_4' => $advert->getMsImageUrl4(),
									'ms_text_5' => $advert->getMsText5(),
									'ms_email_5' => $advert->getMsEmail5(),
									'ms_image_type_5' => $advert->getMsImageType5(),
									'ms_image_url_5' => $advert->getMsImageUrl5(),
									'unique_click_type' => $advert->getUniqueClickType(),

									'point_back_flag' => $advert->getPointBackFlag(),
									'adult_flag' => $advert->getAdultFlag(),
									'seo_flag' => $advert->getSeoFlag(),
									'listing_flag' => $advert->getListingFlag(),
									'mail_magazine_flag' => $advert->getMailMagazineFlag(),
									'community_flag' => $advert->getCommunityFlag(),

									'advert_start_date' => $advert->getAdvertStartDate(),
									'advert_end_date' => $advert->getAdvertEndDate(),
									'unrestraint_flag' => $advert->getUnrestraintFlag(),
									'test_flag' => $advert->getTestFlag(),
									'status' => $advert->getStatus());

				if($advert->getMsImageType1() == 1 && $advert->getMsImageUrl1() != "") {
					$form_data['ms_image_path_1'] = $image_directory . $advert->getMsImageUrl1();
					$form_data['ms_image_file_path_1'] = $advert->getMsImageUrl1();
				} else {
					$form_data['ms_image_path_1'] = $advert->getMsImageUrl1();
				}

				if($advert->getMsImageType2() == 1 && $advert->getMsImageUrl2() != "") {
					$form_data['ms_image_path_2'] = $image_directory . $advert->getMsImageUrl2();
					$form_data['ms_image_file_path_2'] = $advert->getMsImageUrl2();
				} else {
					$form_data['ms_image_path_2'] = $advert->getMsImageUrl2();
				}

				if($advert->getMsImageType3() == 1 && $advert->getMsImageUrl3() != "") {
					$form_data['ms_image_path_3'] = $image_directory . $advert->getMsImageUrl3();
					$form_data['ms_image_file_path_3'] = $advert->getMsImageUrl3();
				} else {
					$form_data['ms_image_path_3'] = $advert->getMsImageUrl3();
				}

				if($advert->getMsImageType4() == 1 && $advert->getMsImageUrl4() != "") {
					$form_data['ms_image_path_4'] = $image_directory . $advert->getMsImageUrl4();
					$form_data['ms_image_file_path_4'] = $advert->getMsImageUrl4();
				} else {
					$form_data['ms_image_path_4'] = $advert->getMsImageUrl4();
				}

				if($advert->getMsImageType5() == 1 && $advert->getMsImageUrl5() != "") {
					$form_data['ms_image_path_5'] = $image_directory . $advert->getMsImageUrl5();
					$form_data['ms_image_file_path_5'] = $advert->getMsImageUrl5();
				} else {
					$form_data['ms_image_path_5'] = $advert->getMsImageUrl5();
				}

				$smarty->assign("form_data", $form_data);

				$as = explode("-", $advert->getAdvertStartDate());
				$ae = explode("-", $advert->getAdvertEndDate());

				$smarty->assign("set_as_year", $as[0]);
				$smarty->assign("set_as_month", $as[1]);
				$smarty->assign("set_as_day", $as[2]);
				$smarty->assign("set_ae_year", $ae[0]);
				$smarty->assign("set_ae_month", $ae[1]);
				$smarty->assign("set_ae_day", $ae[2]);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			}

		}elseif($mode == 'update_commit'){

			$error_flag = 0;

			if($advert_name == "") {
				$error_message = "広告名を入力してください。";
				$error_flag = 1;
			}

			if($ms_image_type_1 == 1) {
				if($_FILES['ms_image_file_1']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_1 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				} elseif($ms_image_file_path_1 != "") {
					$ms_image_url_1 = $ms_image_file_path_1;
				}
			}

			if($ms_image_type_2 == 1) {
				if($_FILES['ms_image_file_2']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_2 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				} elseif($ms_image_file_path_2 != "") {
					$ms_image_url_2 = $ms_image_file_path_2;
				}
			}

			if($ms_image_type_3 == 1) {
				if($_FILES['ms_image_file_3']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_3 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				} elseif($ms_image_file_path_3 != "") {
					$ms_image_url_3 = $ms_image_file_path_3;
				}
			}

			if($ms_image_type_4 == 1) {
				if($_FILES['ms_image_file_4']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_4 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				} elseif($ms_image_file_path_4 != "") {
					$ms_image_url_4 = $ms_image_file_path_4;
				}
			}

			if($ms_image_type_5 == 1) {
				if($_FILES['ms_image_file_5']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_5 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				} elseif($ms_image_file_path_5 != "") {
					$ms_image_url_5 = $ms_image_file_path_5;
				}
			}

			if($error_flag == 0) {

// **************************************************************************
// update
// **************************************************************************
				$advert_dao->transaction_start();

				$advert = new Advert();
				$advert->setId($id);
				$advert->setAdvertClientId($advert_client_id);
				$advert->setAdvertCategoryId($advert_category_id);

				$advert->setSupportIphoneDocomo($support_iphone_docomo);
				$advert->setSupportIphoneSoftbank($support_iphone_softbank);
				$advert->setSupportIphoneAu($support_iphone_au);
				$advert->setSupportIphonePc($support_iphone_pc);

				$advert->setSupportAndroidDocomo($support_android_docomo);
				$advert->setSupportAndroidSoftbank($support_android_softbank);
				$advert->setSupportAndroidAu($support_android_au);
				$advert->setSupportAndroidPc($support_android_pc);

				$advert->setSupportPc($support_pc);

				$advert->setAdvertName($advert_name);

				$advert->setSiteUrlIphoneDocomo($site_url_iphone_docomo);
				$advert->setSiteUrlIphoneSoftbank($site_url_iphone_softbank);
				$advert->setSiteUrlIphoneAu($site_url_iphone_au);
				$advert->setSiteUrlAndroidDocomo($site_url_android_docomo);
				$advert->setSiteUrlAndroidSoftbank($site_url_android_softbank);
				$advert->setSiteUrlAndroidAu($site_url_android_au);
				$advert->setSiteUrlPc($site_url_pc);
				$advert->setSiteOutline($site_outline);
				$advert->setClickPriceClient($click_price_client);
				$advert->setClickPriceMedia($click_price_media);


				$advert->setActionPriceClientIphoneDocomo1($action_price_client_iphone_docomo_1);
				$advert->setActionPriceClientIphoneSoftbank1($action_price_client_iphone_softbank_1);
				$advert->setActionPriceClientIphoneAu1($action_price_client_iphone_au_1);
				$advert->setActionPriceClientIphonePc1($action_price_client_iphone_pc_1);

				$advert->setActionPriceClientIphoneDocomo2($action_price_client_iphone_docomo_2);
				$advert->setActionPriceClientIphoneSoftbank2($action_price_client_iphone_softbank_2);
				$advert->setActionPriceClientIphoneAu2($action_price_client_iphone_au_2);
				$advert->setActionPriceClientIphonePc2($action_price_client_iphone_pc_2);

				$advert->setActionPriceClientIphoneDocomo3($action_price_client_iphone_docomo_3);
				$advert->setActionPriceClientIphoneSoftbank3($action_price_client_iphone_softbank_3);
				$advert->setActionPriceClientIphoneAu3($action_price_client_iphone_au_3);
				$advert->setActionPriceClientIphonePc3($action_price_client_iphone_pc_3);

				$advert->setActionPriceClientIphoneDocomo4($action_price_client_iphone_docomo_4);
				$advert->setActionPriceClientIphoneSoftbank4($action_price_client_iphone_softbank_4);
				$advert->setActionPriceClientIphoneAu4($action_price_client_iphone_au_4);
				$advert->setActionPriceClientIphonePc4($action_price_client_iphone_pc_4);

				$advert->setActionPriceClientIphoneDocomo5($action_price_client_iphone_docomo_5);
				$advert->setActionPriceClientIphoneSoftbank5($action_price_client_iphone_softbank_5);
				$advert->setActionPriceClientIphoneAu5($action_price_client_iphone_au_5);
				$advert->setActionPriceClientIphonePc5($action_price_client_iphone_pc_5);


				$advert->setActionPriceMediaIphoneDocomo1($action_price_media_iphone_docomo_1);
				$advert->setActionPriceMediaIphoneSoftbank1($action_price_media_iphone_softbank_1);
				$advert->setActionPriceMediaIphoneAu1($action_price_media_iphone_au_1);
				$advert->setActionPriceMediaIphonePc1($action_price_media_iphone_pc_1);

				$advert->setActionPriceMediaIphoneDocomo2($action_price_media_iphone_docomo_2);
				$advert->setActionPriceMediaIphoneSoftbank2($action_price_media_iphone_softbank_2);
				$advert->setActionPriceMediaIphoneAu2($action_price_media_iphone_au_2);
				$advert->setActionPriceMediaIphonePc2($action_price_media_iphone_pc_2);

				$advert->setActionPriceMediaIphoneDocomo3($action_price_media_iphone_docomo_3);
				$advert->setActionPriceMediaIphoneSoftbank3($action_price_media_iphone_softbank_3);
				$advert->setActionPriceMediaIphoneAu3($action_price_media_iphone_au_3);
				$advert->setActionPriceMediaIphonePc3($action_price_media_iphone_pc_3);

				$advert->setActionPriceMediaIphoneDocomo4($action_price_media_iphone_docomo_4);
				$advert->setActionPriceMediaIphoneSoftbank4($action_price_media_iphone_softbank_4);
				$advert->setActionPriceMediaIphoneAu4($action_price_media_iphone_au_4);
				$advert->setActionPriceMediaIphonePc4($action_price_media_iphone_pc_4);

				$advert->setActionPriceMediaIphoneDocomo5($action_price_media_iphone_docomo_5);
				$advert->setActionPriceMediaIphoneSoftbank5($action_price_media_iphone_softbank_5);
				$advert->setActionPriceMediaIphoneAu5($action_price_media_iphone_au_5);
				$advert->setActionPriceMediaIphonePc5($action_price_media_iphone_pc_5);


				$advert->setActionPriceClientAndroidDocomo1($action_price_client_android_docomo_1);
				$advert->setActionPriceClientAndroidSoftbank1($action_price_client_android_softbank_1);
				$advert->setActionPriceClientAndroidAu1($action_price_client_android_au_1);
				$advert->setActionPriceClientAndroidPc1($action_price_client_android_pc_1);

				$advert->setActionPriceClientAndroidDocomo2($action_price_client_android_docomo_2);
				$advert->setActionPriceClientAndroidSoftbank2($action_price_client_android_softbank_2);
				$advert->setActionPriceClientAndroidAu2($action_price_client_android_au_2);
				$advert->setActionPriceClientAndroidPc2($action_price_client_android_pc_2);

				$advert->setActionPriceClientAndroidDocomo3($action_price_client_android_docomo_3);
				$advert->setActionPriceClientAndroidSoftbank3($action_price_client_android_softbank_3);
				$advert->setActionPriceClientAndroidAu3($action_price_client_android_au_3);
				$advert->setActionPriceClientAndroidPc3($action_price_client_android_pc_3);

				$advert->setActionPriceClientAndroidDocomo4($action_price_client_android_docomo_4);
				$advert->setActionPriceClientAndroidSoftbank4($action_price_client_android_softbank_4);
				$advert->setActionPriceClientAndroidAu4($action_price_client_android_au_4);
				$advert->setActionPriceClientAndroidPc4($action_price_client_android_pc_4);

				$advert->setActionPriceClientAndroidDocomo5($action_price_client_android_docomo_5);
				$advert->setActionPriceClientAndroidSoftbank5($action_price_client_android_softbank_5);
				$advert->setActionPriceClientAndroidAu5($action_price_client_android_au_5);
				$advert->setActionPriceClientAndroidPc5($action_price_client_android_pc_5);


				$advert->setActionPriceMediaAndroidDocomo1($action_price_media_android_docomo_1);
				$advert->setActionPriceMediaAndroidSoftbank1($action_price_media_android_softbank_1);
				$advert->setActionPriceMediaAndroidAu1($action_price_media_android_au_1);
				$advert->setActionPriceMediaAndroidPc1($action_price_media_android_pc_1);

				$advert->setActionPriceMediaAndroidDocomo2($action_price_media_android_docomo_2);
				$advert->setActionPriceMediaAndroidSoftbank2($action_price_media_android_softbank_2);
				$advert->setActionPriceMediaAndroidAu2($action_price_media_android_au_2);
				$advert->setActionPriceMediaAndroidPc2($action_price_media_android_pc_2);

				$advert->setActionPriceMediaAndroidDocomo3($action_price_media_android_docomo_3);
				$advert->setActionPriceMediaAndroidSoftbank3($action_price_media_android_softbank_3);
				$advert->setActionPriceMediaAndroidAu3($action_price_media_android_au_3);
				$advert->setActionPriceMediaAndroidPc3($action_price_media_android_pc_3);


				$advert->setActionPriceMediaAndroidDocomo4($action_price_media_android_docomo_4);
				$advert->setActionPriceMediaAndroidSoftbank4($action_price_media_android_softbank_4);
				$advert->setActionPriceMediaAndroidAu4($action_price_media_android_au_4);
				$advert->setActionPriceMediaAndroidPc4($action_price_media_android_pc_4);

				$advert->setActionPriceMediaAndroidDocomo5($action_price_media_android_docomo_5);
				$advert->setActionPriceMediaAndroidSoftbank5($action_price_media_android_softbank_5);
				$advert->setActionPriceMediaAndroidAu5($action_price_media_android_au_5);
				$advert->setActionPriceMediaAndroidPc5($action_price_media_android_pc_5);


				$advert->setActionPriceClientPc1($action_price_client_pc_1);
				$advert->setActionPriceClientPc2($action_price_client_pc_2);
				$advert->setActionPriceClientPc3($action_price_client_pc_3);
				$advert->setActionPriceClientPc4($action_price_client_pc_4);
				$advert->setActionPriceClientPc5($action_price_client_pc_5);

				$advert->setActionPriceMediaPc1($action_price_media_pc_1);
				$advert->setActionPriceMediaPc2($action_price_media_pc_2);
				$advert->setActionPriceMediaPc3($action_price_media_pc_3);
				$advert->setActionPriceMediaPc4($action_price_media_pc_4);
				$advert->setActionPriceMediaPc5($action_price_media_pc_5);

				$advert->setPriceType($price_type);
				$advert->setApprovalFlag($approval_flag);

				$advert->setMsText1($ms_text_1);
				$advert->setMsEmail1($ms_email_1);
				$advert->setMsImageType1($ms_image_type_1);
				$advert->setMsImageUrl1($ms_image_url_1);
				$advert->setMsText2($ms_text_2);
				$advert->setMsEmail2($ms_email_2);
				$advert->setMsImageType2($ms_image_type_2);
				$advert->setMsImageUrl2($ms_image_url_2);
				$advert->setMsText3($ms_text_3);
				$advert->setMsEmail3($ms_email_3);
				$advert->setMsImageType3($ms_image_type_3);
				$advert->setMsImageUrl3($ms_image_url_3);
				$advert->setMsText4($ms_text_4);
				$advert->setMsEmail4($ms_email_4);
				$advert->setMsImageType4($ms_image_type_4);
				$advert->setMsImageUrl4($ms_image_url_4);
				$advert->setMsText5($ms_text_5);
				$advert->setMsEmail5($ms_email_5);
				$advert->setMsImageType5($ms_image_type_5);
				$advert->setMsImageUrl5($ms_image_url_5);
				$advert->setUniqueClickType($unique_click_type);

				$advert->setPointBackFlag($point_back_flag);
				$advert->setAdultFlag($adult_flag);
				$advert->setSeoFlag($seo_flag);
				$advert->setListingFlag($listing_flag);
				$advert->setMailMagazineFlag($mail_magazine_flag);
				$advert->setCommunityFlag($community_flag);

				$advert->setAdvertStartDate($advert_start_date);
				$advert->setAdvertEndDate($advert_end_date);
				$advert->setUnrestraintFlag($unrestraint_flag);
				$advert->setTestFlag($test_flag);
				$advert->setStatus($status);

				//UPDATEを実行
				$db_result = $advert_dao->updateAdvert($advert, $result_message);
				if($db_result) {
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

// ****************************************************************************
// おすすめ広告に設定れている広告であった場合
// おすすめ広告テーブルをupdate 箇所

// ****************************************************************************

					view_list();
				} else {
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
									'advert_client_id' => $advert_client_id,
									'advert_category_id' => $advert_category_id,

									'support_iphone_docomo' => $support_iphone_docomo,
									'support_iphone_softbank' => $support_iphone_softbank,
									'support_iphone_au' => $support_iphone_au,
									'support_iphone_pc' => $support_iphone_pc,

									'support_android_docomo' => $support_android_docomo,
									'support_android_softbank' => $support_android_softbank,
									'support_android_au' => $support_android_au,
									'support_android_pc' => $support_android_pc,

									'support_pc' => $support_pc,

									'advert_name' => $advert_name,

									'site_url_iphone_docomo' => $site_url_iphone_docomo,
									'site_url_iphone_softbank' => $site_url_iphone_softbank,
									'site_url_iphone_au' => $site_url_iphone_au,
									'site_url_android_docomo' => $site_url_android_docomo,
									'site_url_android_softbank' => $site_url_android_softbank,
									'site_url_android_au' => $site_url_android_au,
									'site_url_pc' => $site_url_pc,
									'site_outline' => $site_outline,
									'click_price_client' => $click_price_client,
									'click_price_media' => $click_price_media,

										'action_price_client_iphone_docomo_1' => $action_price_client_iphone_docomo_1,
										'action_price_client_iphone_softbank_1' => $action_price_client_iphone_softbank_1,
										'action_price_client_iphone_au_1' => $action_price_client_iphone_au_1,
										'action_price_client_iphone_pc_1' => $action_price_client_iphone_pc_1,

										'action_price_client_iphone_docomo_2' => $action_price_client_iphone_docomo_2,
										'action_price_client_iphone_softbank_2' => $action_price_client_iphone_softbank_2,
										'action_price_client_iphone_au_2' => $action_price_client_iphone_au_2,
										'action_price_client_iphone_pc_2' => $action_price_client_iphone_pc_2,

										'action_price_client_iphone_docomo_3' => $action_price_client_iphone_docomo_3,
										'action_price_client_iphone_softbank_3' => $action_price_client_iphone_softbank_3,
										'action_price_client_iphone_au_3' => $action_price_client_iphone_au_3,
										'action_price_client_iphone_pc_3' => $action_price_client_iphone_pc_3,

										'action_price_client_iphone_docomo_4' => $action_price_client_iphone_docomo_4,
										'action_price_client_iphone_softbank_4' => $action_price_client_iphone_softbank_4,
										'action_price_client_iphone_au_4' => $action_price_client_iphone_au_4,
										'action_price_client_iphone_pc_4' => $action_price_client_iphone_pc_4,

										'action_price_client_iphone_docomo_5' => $action_price_client_iphone_docomo_5,
										'action_price_client_iphone_softbank_5' => $action_price_client_iphone_softbank_5,
										'action_price_client_iphone_au_5' => $action_price_client_iphone_au_5,
										'action_price_client_iphone_pc_5' => $action_price_client_iphone_pc_5,


										'action_price_media_iphone_docomo_1' => $action_price_media_iphone_docomo_1,
										'action_price_media_iphone_softbank_1' => $action_price_media_iphone_softbank_1,
										'action_price_media_iphone_au_1' => $action_price_media_iphone_au_1,
										'action_price_media_iphone_pc_1' => $action_price_media_iphone_pc_1,

										'action_price_media_iphone_docomo_2' => $action_price_media_iphone_docomo_2,
										'action_price_media_iphone_softbank_2' => $action_price_media_iphone_softbank_2,
										'action_price_media_iphone_au_2' => $action_price_media_iphone_au_2,
										'action_price_media_iphone_pc_2' => $action_price_media_iphone_pc_2,

										'action_price_media_iphone_docomo_3' => $action_price_media_iphone_docomo_3,
										'action_price_media_iphone_softbank_3' => $action_price_media_iphone_softbank_3,
										'action_price_media_iphone_au_3' => $action_price_media_iphone_au_3,
										'action_price_media_iphone_pc_3' => $action_price_media_iphone_pc_3,

										'action_price_media_iphone_docomo_4' => $action_price_media_iphone_docomo_4,
										'action_price_media_iphone_softbank_4' => $action_price_media_iphone_softbank_4,
										'action_price_media_iphone_au_4' => $action_price_media_iphone_au_4,
										'action_price_media_iphone_pc_4' => $action_price_media_iphone_pc_4,

										'action_price_media_iphone_docomo_5' => $action_price_media_iphone_docomo_5,
										'action_price_media_iphone_softbank_5' => $action_price_media_iphone_softbank_5,
										'action_price_media_iphone_au_5' => $action_price_media_iphone_au_5,
										'action_price_media_iphone_pc_5' => $action_price_media_iphone_pc_5,


										'action_price_client_android_docomo_1' => $action_price_client_android_docomo_1,
										'action_price_client_android_softbank_1' => $action_price_client_android_softbank_1,
										'action_price_client_android_au_1' => $action_price_client_android_au_1,
										'action_price_client_android_pc_1' => $action_price_client_android_pc_1,

										'action_price_client_android_docomo_2' => $action_price_client_android_docomo_2,
										'action_price_client_android_softbank_2' => $action_price_client_android_softbank_2,
										'action_price_client_android_au_2' => $action_price_client_android_au_2,
										'action_price_client_android_pc_2' => $action_price_client_android_pc_2,

										'action_price_client_android_docomo_3' => $action_price_client_android_docomo_3,
										'action_price_client_android_softbank_3' => $action_price_client_android_softbank_3,
										'action_price_client_android_au_3' => $action_price_client_android_au_3,
										'action_price_client_android_pc_3' => $action_price_client_android_pc_3,

										'action_price_client_android_docomo_4' => $action_price_client_android_docomo_4,
										'action_price_client_android_softbank_4' => $action_price_client_android_softbank_4,
										'action_price_client_android_au_4' => $action_price_client_android_au_4,
										'action_price_client_android_pc_4' => $action_price_client_android_pc_4,

										'action_price_client_android_docomo_5' => $action_price_client_android_docomo_5,
										'action_price_client_android_softbank_5' => $action_price_client_android_softbank_5,
										'action_price_client_android_au_5' => $action_price_client_android_au_5,
										'action_price_client_android_pc_5' => $action_price_client_android_pc_5,


										'action_price_media_android_docomo_1' => $action_price_media_android_docomo_1,
										'action_price_media_android_softbank_1' => $action_price_media_android_softbank_1,
										'action_price_media_android_au_1' => $action_price_media_android_au_1,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_1,

										'action_price_media_android_docomo_2' => $action_price_media_android_docomo_2,
										'action_price_media_android_softbank_2' => $action_price_media_android_softbank_2,
										'action_price_media_android_au_2' => $action_price_media_android_au_2,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_2,

										'action_price_media_android_docomo_3' => $action_price_media_android_docomo_3,
										'action_price_media_android_softbank_3' => $action_price_media_android_softbank_3,
										'action_price_media_android_au_3' => $action_price_media_android_au_3,
										'action_price_media_android_pc_3' => $action_price_media_android_pc_3,

										'action_price_media_android_docomo_4' => $action_price_media_android_docomo_4,
										'action_price_media_android_softbank_4' => $action_price_media_android_softbank_4,
										'action_price_media_android_au_4' => $action_price_media_android_au_4,
										'action_price_media_android_pc_4' => $action_price_media_android_pc_4,

										'action_price_media_android_docomo_5' => $action_price_media_android_docomo_5,
										'action_price_media_android_softbank_5' => $action_price_media_android_softbank_5,
										'action_price_media_android_au_5' => $action_price_media_android_au_5,
										'action_price_media_android_pc_5' => $action_price_media_android_pc_5,

									'action_price_client_pc_1' => $action_price_client_pc_1,
									'action_price_client_pc_2' => $action_price_client_pc_2,
									'action_price_client_pc_3' => $action_price_client_pc_3,
									'action_price_client_pc_4' => $action_price_client_pc_4,
									'action_price_client_pc_5' => $action_price_client_pc_5,

									'action_price_media_pc_1' => $action_price_media_pc_1,
									'action_price_media_pc_2' => $action_price_media_pc_2,
									'action_price_media_pc_3' => $action_price_media_pc_3,
									'action_price_media_pc_4' => $action_price_media_pc_4,
									'action_price_media_pc_5' => $action_price_media_pc_5,
									'price_type' => $price_type,
									'approval_flag' => $approval_flag,

									'ms_text_1' => $ms_text_1,
									'ms_email_1' => $ms_email_1,
									'ms_image_type_1' => $ms_image_type_1,
									'ms_image_url_1' => $ms_image_url_1,
									'ms_text_2' => $ms_text_2,
									'ms_email_2' => $ms_email_2,
									'ms_image_type_2' => $ms_image_type_2,
									'ms_image_url_2' => $ms_image_url_2,
									'ms_text_3' => $ms_text_3,
									'ms_email_3' => $ms_email_3,
									'ms_image_type_3' => $ms_image_type_3,
									'ms_image_url_3' => $ms_image_url_3,
									'ms_text_4' => $ms_text_4,
									'ms_email_4' => $ms_email_4,
									'ms_image_type_4' => $ms_image_type_4,
									'ms_image_url_4' => $ms_image_url_4,
									'ms_text_5' => $ms_text_5,
									'ms_email_5' => $ms_email_5,
									'ms_image_type_5' => $ms_image_type_5,
									'ms_image_url_5' => $ms_image_url_5,
									'unique_click_type' => $unique_click_type,

									'point_back_flag' => $point_back_flag,
									'adult_flag' => $adult_flag,
									'seo_flag' => $seo_flag,
									'listing_flag' => $listing_flag,
									'mail_magazine_flag' => $mail_magazine_flag,
									'community_flag' => $community_flag,

									'advert_start_date' => $advert_start_date,
									'advert_end_date' => $advert_end_date,
									'unrestraint_flag' => $unrestraint_flag,
									'test_flag' => $test_flag,
									'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("set_as_year", $as_year);
					$smarty->assign("set_as_month", $as_month);
					$smarty->assign("set_as_day", $as_day);
					$smarty->assign("set_ae_year", $ae_year);
					$smarty->assign("set_ae_month", $ae_month);
					$smarty->assign("set_ae_day", $ae_day);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '編集');

					// ページを表示
					$smarty->display("./advert_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'advert_client_id' => $advert_client_id,
									'advert_category_id' => $advert_category_id,

									'support_iphone_docomo' => $support_iphone_docomo,
									'support_iphone_softbank' => $support_iphone_softbank,
									'support_iphone_au' => $support_iphone_au,
									'support_iphone_pc' => $support_iphone_pc,

									'support_android_docomo' => $support_android_docomo,
									'support_android_softbank' => $support_android_softbank,
									'support_android_au' => $support_android_au,
									'support_android_pc' => $support_android_pc,
									'support_pc' => $support_pc,

									'advert_name' => $advert_name,

									'site_url_iphone_docomo' => $site_url_iphone_docomo,
									'site_url_iphone_softbank' => $site_url_iphone_softbank,
									'site_url_iphone_au' => $site_url_iphone_au,
									'site_url_android_docomo' => $site_url_android_docomo,
									'site_url_android_softbank' => $site_url_android_softbank,
									'site_url_android_au' => $site_url_android_au,
									'site_url_pc' => $site_url_pc,
									'site_outline' => $site_outline,
									'click_price_client' => $click_price_client,
									'click_price_media' => $click_price_media,

										'action_price_client_iphone_docomo_1' => $action_price_client_iphone_docomo_1,
										'action_price_client_iphone_softbank_1' => $action_price_client_iphone_softbank_1,
										'action_price_client_iphone_au_1' => $action_price_client_iphone_au_1,
										'action_price_client_iphone_pc_1' => $action_price_client_iphone_pc_1,

										'action_price_client_iphone_docomo_2' => $action_price_client_iphone_docomo_2,
										'action_price_client_iphone_softbank_2' => $action_price_client_iphone_softbank_2,
										'action_price_client_iphone_au_2' => $action_price_client_iphone_au_2,
										'action_price_client_iphone_pc_2' => $action_price_client_iphone_pc_2,

										'action_price_client_iphone_docomo_3' => $action_price_client_iphone_docomo_3,
										'action_price_client_iphone_softbank_3' => $action_price_client_iphone_softbank_3,
										'action_price_client_iphone_au_3' => $action_price_client_iphone_au_3,
										'action_price_client_iphone_pc_3' => $action_price_client_iphone_pc_3,

										'action_price_client_iphone_docomo_4' => $action_price_client_iphone_docomo_4,
										'action_price_client_iphone_softbank_4' => $action_price_client_iphone_softbank_4,
										'action_price_client_iphone_au_4' => $action_price_client_iphone_au_4,
										'action_price_client_iphone_pc_4' => $action_price_client_iphone_pc_4,

										'action_price_client_iphone_docomo_5' => $action_price_client_iphone_docomo_5,
										'action_price_client_iphone_softbank_5' => $action_price_client_iphone_softbank_5,
										'action_price_client_iphone_au_5' => $action_price_client_iphone_au_5,
										'action_price_client_iphone_pc_5' => $action_price_client_iphone_pc_5,


										'action_price_media_iphone_docomo_1' => $action_price_media_iphone_docomo_1,
										'action_price_media_iphone_softbank_1' => $action_price_media_iphone_softbank_1,
										'action_price_media_iphone_au_1' => $action_price_media_iphone_au_1,
										'action_price_media_iphone_pc_1' => $action_price_media_iphone_pc_1,

										'action_price_media_iphone_docomo_2' => $action_price_media_iphone_docomo_2,
										'action_price_media_iphone_softbank_2' => $action_price_media_iphone_softbank_2,
										'action_price_media_iphone_au_2' => $action_price_media_iphone_au_2,
										'action_price_media_iphone_pc_2' => $action_price_media_iphone_pc_2,

										'action_price_media_iphone_docomo_3' => $action_price_media_iphone_docomo_3,
										'action_price_media_iphone_softbank_3' => $action_price_media_iphone_softbank_3,
										'action_price_media_iphone_au_3' => $action_price_media_iphone_au_3,
										'action_price_media_iphone_pc_3' => $action_price_media_iphone_pc_3,

										'action_price_media_iphone_docomo_4' => $action_price_media_iphone_docomo_4,
										'action_price_media_iphone_softbank_4' => $action_price_media_iphone_softbank_4,
										'action_price_media_iphone_au_4' => $action_price_media_iphone_au_4,
										'action_price_media_iphone_pc_4' => $action_price_media_iphone_pc_4,

										'action_price_media_iphone_docomo_5' => $action_price_media_iphone_docomo_5,
										'action_price_media_iphone_softbank_5' => $action_price_media_iphone_softbank_5,
										'action_price_media_iphone_au_5' => $action_price_media_iphone_au_5,
										'action_price_media_iphone_pc_5' => $action_price_media_iphone_pc_5,


										'action_price_client_android_docomo_1' => $action_price_client_android_docomo_1,
										'action_price_client_android_softbank_1' => $action_price_client_android_softbank_1,
										'action_price_client_android_au_1' => $action_price_client_android_au_1,
										'action_price_client_android_pc_1' => $action_price_client_android_pc_1,

										'action_price_client_android_docomo_2' => $action_price_client_android_docomo_2,
										'action_price_client_android_softbank_2' => $action_price_client_android_softbank_2,
										'action_price_client_android_au_2' => $action_price_client_android_au_2,
										'action_price_client_android_pc_2' => $action_price_client_android_pc_2,

										'action_price_client_android_docomo_3' => $action_price_client_android_docomo_3,
										'action_price_client_android_softbank_3' => $action_price_client_android_softbank_3,
										'action_price_client_android_au_3' => $action_price_client_android_au_3,
										'action_price_client_android_pc_3' => $action_price_client_android_pc_3,

										'action_price_client_android_docomo_4' => $action_price_client_android_docomo_4,
										'action_price_client_android_softbank_4' => $action_price_client_android_softbank_4,
										'action_price_client_android_au_4' => $action_price_client_android_au_4,
										'action_price_client_android_pc_4' => $action_price_client_android_pc_4,

										'action_price_client_android_docomo_5' => $action_price_client_android_docomo_5,
										'action_price_client_android_softbank_5' => $action_price_client_android_softbank_5,
										'action_price_client_android_au_5' => $action_price_client_android_au_5,
										'action_price_client_android_pc_5' => $action_price_client_android_pc_5,


										'action_price_media_android_docomo_1' => $action_price_media_android_docomo_1,
										'action_price_media_android_softbank_1' => $action_price_media_android_softbank_1,
										'action_price_media_android_au_1' => $action_price_media_android_au_1,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_1,

										'action_price_media_android_docomo_2' => $action_price_media_android_docomo_2,
										'action_price_media_android_softbank_2' => $action_price_media_android_softbank_2,
										'action_price_media_android_au_2' => $action_price_media_android_au_2,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_2,

										'action_price_media_android_docomo_3' => $action_price_media_android_docomo_3,
										'action_price_media_android_softbank_3' => $action_price_media_android_softbank_3,
										'action_price_media_android_au_3' => $action_price_media_android_au_3,
										'action_price_media_android_pc_3' => $action_price_media_android_pc_3,

										'action_price_media_android_docomo_4' => $action_price_media_android_docomo_4,
										'action_price_media_android_softbank_4' => $action_price_media_android_softbank_4,
										'action_price_media_android_au_4' => $action_price_media_android_au_4,
										'action_price_media_android_pc_4' => $action_price_media_android_pc_4,

										'action_price_media_android_docomo_5' => $action_price_media_android_docomo_5,
										'action_price_media_android_softbank_5' => $action_price_media_android_softbank_5,
										'action_price_media_android_au_5' => $action_price_media_android_au_5,
										'action_price_media_android_pc_5' => $action_price_media_android_pc_5,

									'action_price_client_pc_1' => $action_price_client_pc_1,
									'action_price_client_pc_2' => $action_price_client_pc_2,
									'action_price_client_pc_3' => $action_price_client_pc_3,
									'action_price_client_pc_4' => $action_price_client_pc_4,
									'action_price_client_pc_5' => $action_price_client_pc_5,

									'action_price_media_pc_1' => $action_price_media_pc_1,
									'action_price_media_pc_2' => $action_price_media_pc_2,
									'action_price_media_pc_3' => $action_price_media_pc_3,
									'action_price_media_pc_4' => $action_price_media_pc_4,
									'action_price_media_pc_5' => $action_price_media_pc_5,
									'price_type' => $price_type,
									'approval_flag' => $approval_flag,

									'ms_text_1' => $ms_text_1,
									'ms_email_1' => $ms_email_1,
									'ms_image_type_1' => $ms_image_type_1,
									'ms_image_url_1' => $ms_image_url_1,
									'ms_text_2' => $ms_text_2,
									'ms_email_2' => $ms_email_2,
									'ms_image_type_2' => $ms_image_type_2,
									'ms_image_url_2' => $ms_image_url_2,
									'ms_text_3' => $ms_text_3,
									'ms_email_3' => $ms_email_3,
									'ms_image_type_3' => $ms_image_type_3,
									'ms_image_url_3' => $ms_image_url_3,
									'ms_text_4' => $ms_text_4,
									'ms_email_4' => $ms_email_4,
									'ms_image_type_4' => $ms_image_type_4,
									'ms_image_url_4' => $ms_image_url_4,
									'ms_text_5' => $ms_text_5,
									'ms_email_5' => $ms_email_5,
									'ms_image_type_5' => $ms_image_type_5,
									'ms_image_url_5' => $ms_image_url_5,
									'unique_click_type' => $unique_click_type,

									'point_back_flag' => $point_back_flag,
									'adult_flag' => $adult_flag,
									'seo_flag' => $seo_flag,
									'listing_flag' => $listing_flag,
									'mail_magazine_flag' => $mail_magazine_flag,
										'community_flag' => $community_flag,

									'advert_start_date' => $advert_start_date,
									'advert_end_date' => $advert_end_date,
									'unrestraint_flag' => $unrestraint_flag,
									'test_flag' => $test_flag,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("set_as_year", $as_year);
				$smarty->assign("set_as_month", $as_month);
				$smarty->assign("set_as_day", $as_day);
				$smarty->assign("set_ae_year", $ae_year);
				$smarty->assign("set_ae_month", $ae_month);
				$smarty->assign("set_ae_day", $ae_day);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			}
		}elseif($mode == 'search'){

			$search_list_sql = " SELECT a.*, ac.client_name as advert_client_name "
								. " FROM advert as a "
								. " left join advert_clients as ac on a.advert_client_id = ac.id "
								. " WHERE a.deleted_at is NULL ";

// ---------------------------------------------------------------------------------------- 2011/06/09 追加
// 絞込みWHERE文格納配列

			if($search_name != "") {

				$search_list_sql .= " AND ";

				switch($method) {
					case 1:	//「完全一致」
						if($reverse == 1){
							// 否定
							$search_list_sql .= " a.advert_name <> '$search_name' ";
						} else {
							// 公定
							$search_list_sql .= " a.advert_name = '$search_name' ";
						}
						break;

					case 2:	//「前方一致」
						if($reverse == 1){
							// 否定
							$search_list_sql .= " a.advert_name NOT LIKE '$search_name%' ";
						} else {
							// 公定
							$search_list_sql .= " a.advert_name LIKE '$search_name%' ";
						}
						break;

					case 3:	//「後方一致」
						if($reverse == 1){
							// 否定
							$search_list_sql .= " a.advert_name NOT LIKE '%$search_name' ";
						} else {
							// 公定
							$search_list_sql .= " a.advert_name LIKE '%$search_name' ";
						}
						break;

					case 4:	//「あいまい」
						if($reverse == 1){
							// 否定
							$search_list_sql .= " a.advert_name NOT LIKE '%$search_name%' ";
						} else {
							// 公定
							$search_list_sql .= " a.advert_name LIKE '%$search_name%' ";
						}
						break;

				}
			}



// --------------------------------------------------------------------------------------------------------

			if($created_at_flag == 1) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
				} else {
					$search_list_sql .= "AND a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
				}

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

// ---------------------------------------------------------------------------------------- 2011/06/09 追加
// 広告主検索条件
			if($advert_client_id != "0"){
				if($reverse == 1) {
					$search_list_sql .= " AND advert_client_id <> $advert_client_id ";
				} else {
					$search_list_sql .= " AND advert_client_id = $advert_client_id ";
				}
			}
			$smarty->assign("advert_client_id_num", $advert_client_id);

// ----------------------------------------------------------------------------------------- 2011/06/09 追加
// 定額/定率検索条件
			if($price_type == "1") {
				$search_list_sql .= " AND price_type = $price_type ";
			} elseif($price_type == "2") {
				$search_list_sql .= " AND price_type = $price_type ";
			}

// ---------------------------------------------------------------------------------------- 2011/02/01 追加
			$search_list_sql2 = urlencode($search_list_sql);
			$smarty->assign("sql", $search_list_sql2);
// --------------------------------------------------------------------------------------------------------

			$search_list_sql .= "ORDER BY a.$sort $order ";

			$db_result = $common_dao->db_query($search_list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			$search['method'] = $method;
			$search['reverse'] = $reverse;
			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['id'] = $search_id;
			$search['name'] = $search_name;
			$search['created_at_flag'] = $created_at_flag;
			$search['price_type'] = $price_type;

			$smarty->assign("search", $search);

			$smarty->assign("mode", 'search');
			$smarty->assign("sub_title", '検索結果');

			// ページを表示
			$smarty->display("./advert_list.tpl");
			exit();
		}elseif($_POST['mode'] == 'delete'){
			$advert_dao->transaction_start();

			if(!is_null($advert_dao->getAdvertById($id))) {
				$db_result = $advert_dao->deleteAdvert($id, $result_message);
				if($db_result){
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				}else{
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					view_list();
				}
			}else{
				$advert_dao->transaction_rollback();

				$error_message = "ＤＢの更新に失敗しました。";
				$smarty->assign("error_message", $error_message);

				view_list();
			}

// ---------------------------------------------------------------------------------------- 2011/02/01 追加
		// ソート
		}elseif($mode == "sort"){
			$sort_sql = $_GET['sql'];
			$sort_type = $_GET['sort'];
			$list_count = 0;

			// \を除去
			$sort_sql = str_replace("\\", "", $sort_sql);

			// $sort_sqlをエンコード
			$sort_sql = urlencode($sort_sql);

			$smarty->assign("sql", $sort_sql);
			$smarty->assign("sort", $sort_type);

			// $sort_sqlをデコード
			$sort_sql = urldecode($sort_sql);

			if($sort_type == "asc"){
				$sort_sql .= " ORDER BY ac.client_name ASC ";
			}else{
				$sort_sql .= " ORDER BY ac.client_name DESC ";
			}

			$db_sort_result = $common_dao->db_query($sort_sql);
			if($db_sort_result){
				$smarty->assign("list", $db_sort_result);
				$list_count = count($db_sort_result);
			}else{
				$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			// ページを表示
			$smarty->display("./advert_list.tpl");
			exit();
// --------------------------------------------------------------------------------------------------------

		}else{
			view_list();
		}
	}else{
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

// ---------------------------------------------------------------------------------------- 2011/02/01 追加
	$smarty->assign("sql", $list_sql);

	$list_sql .= " ORDER BY a.id ASC ";
// --------------------------------------------------------------------------------------------------------

//	echo $list_sql;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./advert_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

function insert_image_file($up_file, $up_directory, $up_name, &$result_message = "") {
	$image_size = $up_file['size'];
	$image_tmp = $up_file['tmp_name'];

	if($image_size <= 4000000) {
		$image_info = getimagesize($image_tmp);
		if($image_info[2] == 1) {
			$file_type = "gif";
		} elseif($image_info[2] == 2) {
			$file_type = "jpg";
		} elseif($image_info[2] == 3) {
			$file_type = "png";
		} else {
			$error_message = "対応画像ファイルではありません。";
			return false;
		}

		$up_file_path = $up_directory . $up_name . "." . $file_type;

		if(move_uploaded_file($image_tmp, $up_file_path)) {
			chmod($up_file_path,0666);
			$result_message = $up_name . "." . $file_type;
			return true;
		} else {
			$result_message = "画像ファイルの保存に失敗しました。";
			return false;
		}
	} else {
		$result_message = "画像ファイルのサイズが大きすぎます。(4000KBまで）";
		return false;
	}
}
?>