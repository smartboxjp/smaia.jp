<?php
// ********************************************************
// 共通設定 ファイルインクルード
// ********************************************************
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaCategoryDao.php' );
require_once( '../dto/MediaCategory.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

// ********************************************************
// sessionスタート
// ********************************************************
session_start();

// ********************************************************
// 変数設定
// ********************************************************
$media_id_array = array();			// メディアIDを格納する配列
$advert_id_array = array();			// 広告(クライアント)IDを格納する配列





if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$media_category_dao = new MediaCategoryDao();
	$advert_price_media_set_dao = new AdvertPriceMediaSetDao();

	if(isset($_POST['advert_id']) && $_POST['advert_id'] != '') {
		$advert_id = $_POST['advert_id'];
	} elseif(isset($_GET['advert_id']) && $_GET['advert_id'] != '') {
		$advert_id = $_GET['advert_id'];
	} else {
		$davert_id = "";
	}
	$smarty->assign("advert_id", $advert_id);

	if(isset($_POST['category_id']) && $_POST['category_id'] != '') {
		$category_id = $_POST['category_id'];
	} elseif(isset($_GET['category_id']) && $_GET['category_id'] != '') {
		$category_id = $_GET['category_id'];
	} else {
		$category_id = "";
	}
	$smarty->assign("category_id", $category_id);

	if(isset($_POST['publisher_id']) && $_POST['publisher_id'] != '') {
		$publisher_id = $_POST['publisher_id'];
	} elseif(isset($_GET['publisher_id']) && $_GET['publisher_id'] != '') {
		$publisher_id = $_GET['publisher_id'];
	} else {
		$publisher_id = "";
	}
	$smarty->assign("publisher_id", $publisher_id);

	if(isset($_POST['media_id']) && $_POST['media_id'] != '') {
		$media_id = $_POST['media_id'];
	} elseif(isset($_GET['media_id']) && $_GET['media_id'] != '') {
		$media_id = $_GET['media_id'];
	} else {
		$media_id = "";
	}
	$smarty->assign("media_id", $media_id);

	$check_category = array();
	$all_category = array();
	$check_publisher = array();
	$all_publisher = array();


	//広告カテゴリー
	$media_category_array = array();
	foreach($media_category_dao->getAllMediaCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$media_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_category_array", $media_category_array);

	$sql = " SELECT m.*, mp.publisher_name as publisher_name "
		. " FROM media as m "
		. " LEFT JOIN media_publishers as mp on m.media_publisher_id = mp.id "
		. " WHERE m.deleted_at is NULL ";
		if($category_id != ""){
			$sql .= " AND m.media_category_id = '$category_id' ";
		}
		$sql .= " ORDER BY m.id ASC ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){
		foreach($db_result as $row) {
			$mp_id = $row['media_publisher_id'];
			if($mp_id != "0"){
				$m_id = $row['id'];
				$media_array[$mp_id]['publisher_id'] = $row['media_publisher_id'];

				$media_array[$mp_id]['publisher_name'] = $row['publisher_name'];
				$media_array[$mp_id]['media'][$m_id]['media_id'] = $row['id'];
				$media_array[$mp_id]['media'][$m_id]['media_name'] = $row['media_name'];
			}
		}
		$smarty->assign("media_array", $media_array);

	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	$id = do_escape_quotes($_POST['id']);
	$click_price_client = do_escape_quotes($_POST['click_price_client']);
	$click_price_media = do_escape_quotes($_POST['click_price_media']);

	$action_price_client_iphone_docomo_1 = do_escape_quotes($_POST['action_price_client_iphone_docomo_1']);
	$action_price_client_iphone_softbank_1 = do_escape_quotes($_POST['action_price_client_iphone_softbank_1']);
	$action_price_client_iphone_au_1 = do_escape_quotes($_POST['action_price_client_iphone_au_1']);
	$action_price_client_iphone_pc_1 = do_escape_quotes($_POST['action_price_client_iphone_pc_1']);

	$action_price_client_iphone_docomo_2 = do_escape_quotes($_POST['action_price_client_iphone_docomo_2']);
	$action_price_client_iphone_softbank_2 = do_escape_quotes($_POST['action_price_client_iphone_softbank_2']);
	$action_price_client_iphone_au_2 = do_escape_quotes($_POST['action_price_client_iphone_au_2']);
	$action_price_client_iphone_pc_2 = do_escape_quotes($_POST['action_price_client_iphone_pc_2']);

	$action_price_client_iphone_docomo_3 = do_escape_quotes($_POST['action_price_client_iphone_docomo_3']);
	$action_price_client_iphone_softbank_3 = do_escape_quotes($_POST['action_price_client_iphone_softbank_3']);
	$action_price_client_iphone_au_3 = do_escape_quotes($_POST['action_price_client_iphone_au_3']);
	$action_price_client_iphone_pc_3 = do_escape_quotes($_POST['action_price_client_iphone_pc_3']);

	$action_price_client_iphone_docomo_4 = do_escape_quotes($_POST['action_price_client_iphone_docomo_4']);
	$action_price_client_iphone_softbank_4 = do_escape_quotes($_POST['action_price_client_iphone_softbank_4']);
	$action_price_client_iphone_au_4 = do_escape_quotes($_POST['action_price_client_iphone_au_4']);
	$action_price_client_iphone_pc_4 = do_escape_quotes($_POST['action_price_client_iphone_pc_4']);

	$action_price_client_iphone_docomo_5 = do_escape_quotes($_POST['action_price_client_iphone_docomo_5']);
	$action_price_client_iphone_softbank_5 = do_escape_quotes($_POST['action_price_client_iphone_softbank_5']);
	$action_price_client_iphone_au_5 = do_escape_quotes($_POST['action_price_client_iphone_au_5']);
	$action_price_client_iphone_pc_5 = do_escape_quotes($_POST['action_price_client_iphone_pc_5']);


	$action_price_media_iphone_docomo_1 = do_escape_quotes($_POST['action_price_media_iphone_docomo_1']);
	$action_price_media_iphone_softbank_1 = do_escape_quotes($_POST['action_price_media_iphone_softbank_1']);
	$action_price_media_iphone_au_1 = do_escape_quotes($_POST['action_price_media_iphone_au_1']);
	$action_price_media_iphone_pc_1 = do_escape_quotes($_POST['action_price_media_iphone_pc_1']);

	$action_price_media_iphone_docomo_2 = do_escape_quotes($_POST['action_price_media_iphone_docomo_2']);
	$action_price_media_iphone_softbank_2 = do_escape_quotes($_POST['action_price_media_iphone_softbank_2']);
	$action_price_media_iphone_au_2 = do_escape_quotes($_POST['action_price_media_iphone_au_2']);
	$action_price_media_iphone_pc_2 = do_escape_quotes($_POST['action_price_media_iphone_pc_2']);

	$action_price_media_iphone_docomo_3 = do_escape_quotes($_POST['action_price_media_iphone_docomo_3']);
	$action_price_media_iphone_softbank_3 = do_escape_quotes($_POST['action_price_media_iphone_softbank_3']);
	$action_price_media_iphone_au_3 = do_escape_quotes($_POST['action_price_media_iphone_au_3']);
	$action_price_media_iphone_pc_3 = do_escape_quotes($_POST['action_price_media_iphone_pc_3']);

	$action_price_media_iphone_docomo_4 = do_escape_quotes($_POST['action_price_media_iphone_docomo_4']);
	$action_price_media_iphone_softbank_4 = do_escape_quotes($_POST['action_price_media_iphone_softbank_4']);
	$action_price_media_iphone_au_4 = do_escape_quotes($_POST['action_price_media_iphone_au_4']);
	$action_price_media_iphone_pc_4 = do_escape_quotes($_POST['action_price_media_iphone_pc_4']);

	$action_price_media_iphone_docomo_5 = do_escape_quotes($_POST['action_price_media_iphone_docomo_5']);
	$action_price_media_iphone_softbank_5 = do_escape_quotes($_POST['action_price_media_iphone_softbank_5']);
	$action_price_media_iphone_au_5 = do_escape_quotes($_POST['action_price_media_iphone_au_5']);
	$action_price_media_iphone_pc_5 = do_escape_quotes($_POST['action_price_media_iphone_pc_5']);


	$action_price_client_android_docomo_1 = do_escape_quotes($_POST['action_price_client_android_docomo_1']);
	$action_price_client_android_softbank_1 = do_escape_quotes($_POST['action_price_client_android_softbank_1']);
	$action_price_client_android_au_1 = do_escape_quotes($_POST['action_price_client_android_au_1']);
	$action_price_client_android_pc_1 = do_escape_quotes($_POST['action_price_client_android_pc_1']);

	$action_price_client_android_docomo_2 = do_escape_quotes($_POST['action_price_client_android_docomo_2']);
	$action_price_client_android_softbank_2 = do_escape_quotes($_POST['action_price_client_android_softbank_2']);
	$action_price_client_android_au_2 = do_escape_quotes($_POST['action_price_client_android_au_2']);
	$action_price_client_android_pc_2 = do_escape_quotes($_POST['action_price_client_android_pc_2']);

	$action_price_client_android_docomo_3 = do_escape_quotes($_POST['action_price_client_android_docomo_3']);
	$action_price_client_android_softbank_3 = do_escape_quotes($_POST['action_price_client_android_softbank_3']);
	$action_price_client_android_au_3 = do_escape_quotes($_POST['action_price_client_android_au_3']);
	$action_price_client_android_pc_3 = do_escape_quotes($_POST['action_price_client_android_pc_3']);

	$action_price_client_android_docomo_4 = do_escape_quotes($_POST['action_price_client_android_docomo_4']);
	$action_price_client_android_softbank_4 = do_escape_quotes($_POST['action_price_client_android_softbank_4']);
	$action_price_client_android_au_4 = do_escape_quotes($_POST['action_price_client_android_au_4']);
	$action_price_client_android_pc_4 = do_escape_quotes($_POST['action_price_client_android_pc_4']);

	$action_price_client_android_docomo_5 = do_escape_quotes($_POST['action_price_client_android_docomo_5']);
	$action_price_client_android_softbank_5 = do_escape_quotes($_POST['action_price_client_android_softbank_5']);
	$action_price_client_android_au_5 = do_escape_quotes($_POST['action_price_client_android_au_5']);
	$action_price_client_android_pc_5 = do_escape_quotes($_POST['action_price_client_android_pc_5']);


	$action_price_media_android_docomo_1 = do_escape_quotes($_POST['action_price_media_android_docomo_1']);
	$action_price_media_android_softbank_1 = do_escape_quotes($_POST['action_price_media_android_softbank_1']);
	$action_price_media_android_au_1 = do_escape_quotes($_POST['action_price_media_android_au_1']);
	$action_price_media_android_pc_1 = do_escape_quotes($_POST['action_price_media_android_pc_1']);

	$action_price_media_android_docomo_2 = do_escape_quotes($_POST['action_price_media_android_docomo_2']);
	$action_price_media_android_softbank_2 = do_escape_quotes($_POST['action_price_media_android_softbank_2']);
	$action_price_media_android_au_2 = do_escape_quotes($_POST['action_price_media_android_au_2']);
	$action_price_media_android_pc_2 = do_escape_quotes($_POST['action_price_media_android_pc_2']);

	$action_price_media_android_docomo_3 = do_escape_quotes($_POST['action_price_media_android_docomo_3']);
	$action_price_media_android_softbank_3 = do_escape_quotes($_POST['action_price_media_android_softbank_3']);
	$action_price_media_android_au_3 = do_escape_quotes($_POST['action_price_media_android_au_3']);
	$action_price_media_android_pc_3 = do_escape_quotes($_POST['action_price_media_android_pc_3']);

	$action_price_media_android_docomo_4 = do_escape_quotes($_POST['action_price_media_android_docomo_4']);
	$action_price_media_android_softbank_4 = do_escape_quotes($_POST['action_price_media_android_softbank_4']);
	$action_price_media_android_au_4 = do_escape_quotes($_POST['action_price_media_android_au_4']);
	$action_price_media_android_pc_4 = do_escape_quotes($_POST['action_price_media_android_pc_4']);

	$action_price_media_android_docomo_5 = do_escape_quotes($_POST['action_price_media_android_docomo_5']);
	$action_price_media_android_softbank_5 = do_escape_quotes($_POST['action_price_media_android_softbank_5']);
	$action_price_media_android_au_5 = do_escape_quotes($_POST['action_price_media_android_au_5']);
	$action_price_media_android_pc_5 = do_escape_quotes($_POST['action_price_media_android_pc_5']);


	$action_price_client_pc_1 = do_escape_quotes($_POST['action_price_client_pc_1']);
	$action_price_client_pc_2 = do_escape_quotes($_POST['action_price_client_pc_2']);
	$action_price_client_pc_3 = do_escape_quotes($_POST['action_price_client_pc_3']);
	$action_price_client_pc_4 = do_escape_quotes($_POST['action_price_client_pc_4']);
	$action_price_client_pc_5 = do_escape_quotes($_POST['action_price_client_pc_5']);

	$action_price_media_pc_1 = do_escape_quotes($_POST['action_price_media_pc_1']);
	$action_price_media_pc_2 = do_escape_quotes($_POST['action_price_media_pc_2']);
	$action_price_media_pc_3 = do_escape_quotes($_POST['action_price_media_pc_3']);
	$action_price_media_pc_4 = do_escape_quotes($_POST['action_price_media_pc_4']);
	$action_price_media_pc_5 = do_escape_quotes($_POST['action_price_media_pc_5']);


	if($advert_id != 0 && $media_id != 0) {
		$advert_price_madia_set = new AdvertPriceMediaSet();
		$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

		if(!is_null($advert_price_madia_set)) {
			$form_data = array('id' => $advert_price_madia_set->getId(),
							'click_price_client' => $advert_price_madia_set->getClickPriceClient(),
							'click_price_media' => $advert_price_madia_set->getClickPriceMedia(),

//										'action_price_client_iphone_docomo_1' => $advert_price_media_set->getActionPriceClientIphoneDocomo1(),
//										'action_price_client_iphone_softbank_1' => $advert_price_media_set->getActionPriceClientIphoneSoftbank1(),
//										'action_price_client_iphone_au_1' => $advert_price_media_set->getActionPriceClientIphoneAu1(),
										'action_price_client_iphone_pc_1' => $advert_price_madia_set->getActionPriceClientIphonePc1(),

//										'action_price_client_iphone_docomo_2' => $advert_price_media_set->getActionPriceClientIphoneDocomo2(),
//										'action_price_client_iphone_softbank_2' => $advert_price_media_set->getActionPriceClientIphoneSoftbank2(),
//										'action_price_client_iphone_au_2' => $advert_price_media_set->getActionPriceClientIphoneAu2(),
										'action_price_client_iphone_pc_2' => $advert_price_madia_set->getActionPriceClientIphonePc2(),

//										'action_price_client_iphone_docomo_3' => $advert_price_media_set->getActionPriceClientIphoneDocomo3(),
//										'action_price_client_iphone_softbank_3' => $advert_price_media_set->getActionPriceClientIphoneSoftbank3(),
//										'action_price_client_iphone_au_3' => $advert_price_media_set->getActionPriceClientIphoneAu3(),
										'action_price_client_iphone_pc_3' => $advert_price_madia_set->getActionPriceClientIphonePc3(),

//										'action_price_client_iphone_docomo_4' => $advert_price_media_set->getActionPriceClientIphoneDocomo4(),
//										'action_price_client_iphone_softbank_4' => $advert_price_media_set->getActionPriceClientIphoneSoftbank4(),
//										'action_price_client_iphone_au_4' => $advert_price_media_set->getActionPriceClientIphoneAu4(),
										'action_price_client_iphone_pc_4' => $advert_price_madia_set->getActionPriceClientIphonePc4(),

//										'action_price_client_iphone_docomo_5' => $advert_price_media_set->getActionPriceClientIphoneDocomo5(),
//										'action_price_client_iphone_softbank_5' => $advert_price_media_set->getActionPriceClientIphoneSoftbank5(),
//										'action_price_client_iphone_au_5' => $advert_price_media_set->getActionPriceClientIphoneAu5(),
										'action_price_client_iphone_pc_5' => $advert_price_madia_set->getActionPriceClientIphonePc5(),

//										'action_price_media_iphone_docomo_1' => $advert_price_media_set->getActionPriceMediaIphoneDocomo1(),
//										'action_price_media_iphone_softbank_1' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank1(),
//										'action_price_media_iphone_au_1' => $advert_price_media_set->getActionPriceMediaIphoneAu1(),
										'action_price_media_iphone_pc_1' => $advert_price_madia_set->getActionPriceMediaIphonePc1(),

//										'action_price_media_iphone_docomo_2' => $advert_price_media_set->getActionPriceMediaIphoneDocomo2(),
//										'action_price_media_iphone_softbank_2' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank2(),
//										'action_price_media_iphone_au_2' => $advert_price_media_set->getActionPriceMediaIphoneAu2(),
										'action_price_media_iphone_pc_2' => $advert_price_madia_set->getActionPriceMediaIphonePc2(),

//										'action_price_media_iphone_docomo_3' => $advert_price_media_set->getActionPriceMediaIphoneDocomo3(),
//										'action_price_media_iphone_softbank_3' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank3(),
//										'action_price_media_iphone_au_3' => $advert_price_media_set->getActionPriceMediaIphoneAu3(),
										'action_price_media_iphone_pc_3' => $advert_price_madia_set->getActionPriceMediaIphonePc3(),

//										'action_price_media_iphone_docomo_4' => $advert_price_media_set->getActionPriceMediaIphoneDocomo4(),
//										'action_price_media_iphone_softbank_4' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank4(),
//										'action_price_media_iphone_au_4' => $advert_price_media_set->getActionPriceMediaIphoneAu4(),
										'action_price_media_iphone_pc_4' => $advert_price_madia_set->getActionPriceMediaIphonePc4(),

//										'action_price_media_iphone_docomo_5' => $advert_price_media_set->getActionPriceMediaIphoneDocomo5(),
//										'action_price_media_iphone_softbank_5' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank5(),
//										'action_price_media_iphone_au_5' => $advert_price_media_set->getActionPriceMediaIphoneAu5(),
										'action_price_media_iphone_pc_5' => $advert_price_madia_set->getActionPriceMediaIphonePc5(),


										'action_price_client_android_docomo_1' => $advert_price_madia_set->getActionPriceClientAndroidDocomo1(),
										'action_price_client_android_softbank_1' => $advert_price_madia_set->getActionPriceClientAndroidSoftbank1(),
										'action_price_client_android_au_1' => $advert_price_madia_set->getActionPriceClientAndroidAu1(),
										'action_price_client_android_pc_1' => $advert_price_madia_set->getActionPriceClientAndroidPc1(),

										'action_price_client_android_docomo_2' => $advert_price_madia_set->getActionPriceClientAndroidDocomo2(),
										'action_price_client_android_softbank_2' => $advert_price_madia_set->getActionPriceClientAndroidSoftbank2(),
										'action_price_client_android_au_2' => $advert_price_madia_set->getActionPriceClientAndroidAu2(),
										'action_price_client_android_pc_2' => $advert_price_madia_set->getActionPriceClientAndroidPc2(),

										'action_price_client_android_docomo_3' => $advert_price_madia_set->getActionPriceClientAndroidDocomo3(),
										'action_price_client_android_softbank_3' => $advert_price_madia_set->getActionPriceClientAndroidSoftbank3(),
										'action_price_client_android_au_3' => $advert_price_madia_set->getActionPriceClientAndroidAu3(),
										'action_price_client_android_pc_3' => $advert_price_madia_set->getActionPriceClientAndroidPc3(),

										'action_price_client_android_docomo_4' => $advert_price_madia_set->getActionPriceClientAndroidDocomo4(),
										'action_price_client_android_softbank_4' => $advert_price_madia_set->getActionPriceClientAndroidSoftbank4(),
										'action_price_client_android_au_4' => $advert_price_madia_set->getActionPriceClientAndroidAu4(),
										'action_price_client_android_pc_4' => $advert_price_madia_set->getActionPriceClientAndroidPc4(),

										'action_price_client_android_docomo_5' => $advert_price_madia_set->getActionPriceClientAndroidDocomo5(),
										'action_price_client_android_softbank_5' => $advert_price_madia_set->getActionPriceClientAndroidSoftbank5(),
										'action_price_client_android_au_5' => $advert_price_madia_set->getActionPriceClientAndroidAu5(),
										'action_price_client_android_pc_5' => $advert_price_madia_set->getActionPriceClientAndroidPc5(),

										'action_price_media_android_docomo_1' => $advert_price_madia_set->getActionPriceMediaAndroidDocomo1(),
										'action_price_media_android_softbank_1' => $advert_price_madia_set->getActionPriceMediaAndroidSoftbank1(),
										'action_price_media_android_au_1' => $advert_price_madia_set->getActionPriceMediaAndroidAu1(),
										'action_price_media_android_pc_1' => $advert_price_madia_set->getActionPriceMediaAndroidPc1(),

										'action_price_media_android_docomo_2' => $advert_price_madia_set->getActionPriceMediaAndroidDocomo2(),
										'action_price_media_android_softbank_2' => $advert_price_madia_set->getActionPriceMediaAndroidSoftbank2(),
										'action_price_media_android_au_2' => $advert_price_madia_set->getActionPriceMediaAndroidAu2(),
										'action_price_media_android_pc_2' => $advert_price_madia_set->getActionPriceMediaAndroidPc2(),

										'action_price_media_android_docomo_3' => $advert_price_madia_set->getActionPriceMediaAndroidDocomo3(),
										'action_price_media_android_softbank_3' => $advert_price_madia_set->getActionPriceMediaAndroidSoftbank3(),
										'action_price_media_android_au_3' => $advert_price_madia_set->getActionPriceMediaAndroidAu3(),
										'action_price_media_android_pc_3' => $advert_price_madia_set->getActionPriceMediaAndroidPc3(),

										'action_price_media_android_docomo_4' => $advert_price_madia_set->getActionPriceMediaAndroidDocomo4(),
										'action_price_media_android_softbank_4' => $advert_price_madia_set->getActionPriceMediaAndroidSoftbank4(),
										'action_price_media_android_au_4' => $advert_price_madia_set->getActionPriceMediaAndroidAu4(),
										'action_price_media_android_pc_4' => $advert_price_madia_set->getActionPriceMediaAndroidPc4(),

										'action_price_media_android_docomo_5' => $advert_price_madia_set->getActionPriceMediaAndroidDocomo5(),
										'action_price_media_android_softbank_5' => $advert_price_madia_set->getActionPriceMediaAndroidSoftbank5(),
										'action_price_media_android_au_5' => $advert_price_madia_set->getActionPriceMediaAndroidAu5(),
										'action_price_media_android_pc_5' => $advert_price_madia_set->getActionPriceMediaAndroidPc5(),


							'action_price_client_pc_1' => $advert_price_madia_set->getActionPriceClientPc1(),
							'action_price_client_pc_2' => $advert_price_madia_set->getActionPriceClientPc2(),
							'action_price_client_pc_3' => $advert_price_madia_set->getActionPriceClientPc3(),
							'action_price_client_pc_4' => $advert_price_madia_set->getActionPriceClientPc4(),
							'action_price_client_pc_5' => $advert_price_madia_set->getActionPriceClientPc5(),

							'action_price_media_pc_1' => $advert_price_madia_set->getActionPriceMediaPc1(),
							'action_price_media_pc_2' => $advert_price_madia_set->getActionPriceMediaPc2(),
							'action_price_media_pc_3' => $advert_price_madia_set->getActionPriceMediaPc3(),
							'action_price_media_pc_4' => $advert_price_madia_set->getActionPriceMediaPc4(),
							'action_price_media_pc_5' => $advert_price_madia_set->getActionPriceMediaPc5());

			$smarty->assign("form_data", $form_data);
		}
	}

	// *********************************************************************************
	// 一括設定
	// *********************************************************************************
	if(isset($_GET['mode']) && $_GET['mode'] == "all_set"){

		// GETで受け取ったパラメータを変数へ格納
		$mode = $_GET['mode'];
		// smarty変数へ格納
		$smarty->assign("mode", $mode);

		if(isset($_SESSION['form_data'])){
			$smarty->assign("form_data", $_SESSION['form_data']);
		}

	}

	// DBへ接続
	if (isset($_POST['mode']) && $_POST['mode'] == "all_set_decision"){

		// GETで受け取ったパラメータを変数へ格納
		$mode = $_POST['mode'];
		// smarty変数へ格納
		$smarty->assign("mode", $mode);

		// 広告データを取得するクラス生成
		$media_dao = new MediaDao();

		// トランザクションスタート
		$media_dao -> transaction_start();

		// advert_idを指定して広告データを取得
		if($category_id == ""){
			$result = $media_dao -> getMediaByPublisherId($publisher_id, $category_id);
		} else {
			$result = $media_dao -> getMediaByPublisherCategoryId($publisher_id, $category_id);
		}

		// DB結果
		if($result){
			// 成功
			echo "成功";
			// 取得レコード分ループ
			foreach($result as $row => $val ) {

				$media_id = $val->getId();

				$advert_price_media_set_dao->transaction_start();
				$result = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);


// **************************************************************************************
// insert
// **************************************************************************************
						if(is_null($result)) {
							$advert_price_media_set = new AdvertPriceMediaSet();
							$advert_price_media_set->setAdvertId($advert_id);
							$advert_price_media_set->setMediaId($media_id);
							$advert_price_media_set->setClickPriceClient($click_price_client);
							$advert_price_media_set->setClickPriceMedia($click_price_media);


				$advert_price_media_set->setActionPriceClientIphoneDocomo1($action_price_client_iphone_docomo_1);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank1($action_price_client_iphone_softbank_1);
				$advert_price_media_set->setActionPriceClientIphoneAu1($action_price_client_iphone_au_1);
				$advert_price_media_set->setActionPriceClientIphonePc1($action_price_client_iphone_pc_1);

				$advert_price_media_set->setActionPriceClientIphoneDocomo2($action_price_client_iphone_docomo_2);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank2($action_price_client_iphone_softbank_2);
				$advert_price_media_set->setActionPriceClientIphoneAu2($action_price_client_iphone_au_2);
				$advert_price_media_set->setActionPriceClientIphonePc2($action_price_client_iphone_pc_2);

				$advert_price_media_set->setActionPriceClientIphoneDocomo3($action_price_client_iphone_docomo_3);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank3($action_price_client_iphone_softbank_3);
				$advert_price_media_set->setActionPriceClientIphoneAu3($action_price_client_iphone_au_3);
				$advert_price_media_set->setActionPriceClientIphonePc3($action_price_client_iphone_pc_3);

				$advert_price_media_set->setActionPriceClientIphoneDocomo4($action_price_client_iphone_docomo_4);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank4($action_price_client_iphone_softbank_4);
				$advert_price_media_set->setActionPriceClientIphoneAu4($action_price_client_iphone_au_4);
				$advert_price_media_set->setActionPriceClientIphonePc4($action_price_client_iphone_pc_4);

				$advert_price_media_set->setActionPriceClientIphoneDocomo5($action_price_client_iphone_docomo_5);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank5($action_price_client_iphone_softbank_5);
				$advert_price_media_set->setActionPriceClientIphoneAu5($action_price_client_iphone_au_5);
				$advert_price_media_set->setActionPriceClientIphonePc5($action_price_client_iphone_pc_5);


				$advert_price_media_set->setActionPriceMediaIphoneDocomo1($action_price_media_iphone_docomo_1);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank1($action_price_media_iphone_softbank_1);
				$advert_price_media_set->setActionPriceMediaIphoneAu1($action_price_media_iphone_au_1);
				$advert_price_media_set->setActionPriceMediaIphonePc1($action_price_media_iphone_pc_1);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo2($action_price_media_iphone_docomo_2);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank2($action_price_media_iphone_softbank_2);
				$advert_price_media_set->setActionPriceMediaIphoneAu2($action_price_media_iphone_au_2);
				$advert_price_media_set->setActionPriceMediaIphonePc2($action_price_media_iphone_pc_2);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo3($action_price_media_iphone_docomo_3);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank3($action_price_media_iphone_softbank_3);
				$advert_price_media_set->setActionPriceMediaIphoneAu3($action_price_media_iphone_au_3);
				$advert_price_media_set->setActionPriceMediaIphonePc3($action_price_media_iphone_pc_3);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo4($action_price_media_iphone_docomo_4);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank4($action_price_media_iphone_softbank_4);
				$advert_price_media_set->setActionPriceMediaIphoneAu4($action_price_media_iphone_au_4);
				$advert_price_media_set->setActionPriceMediaIphonePc4($action_price_media_iphone_pc_4);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo5($action_price_media_iphone_docomo_5);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank5($action_price_media_iphone_softbank_5);
				$advert_price_media_set->setActionPriceMediaIphoneAu5($action_price_media_iphone_au_5);
				$advert_price_media_set->setActionPriceMediaIphonePc5($action_price_media_iphone_pc_5);


				$advert_price_media_set->setActionPriceClientAndroidDocomo1($action_price_client_android_docomo_1);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank1($action_price_client_android_softbank_1);
				$advert_price_media_set->setActionPriceClientAndroidAu1($action_price_client_android_au_1);
				$advert_price_media_set->setActionPriceClientAndroidPc1($action_price_client_android_pc_1);

				$advert_price_media_set->setActionPriceClientAndroidDocomo2($action_price_client_android_docomo_2);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank2($action_price_client_android_softbank_2);
				$advert_price_media_set->setActionPriceClientAndroidAu2($action_price_client_android_au_2);
				$advert_price_media_set->setActionPriceClientAndroidPc2($action_price_client_android_pc_2);

				$advert_price_media_set->setActionPriceClientAndroidDocomo3($action_price_client_android_docomo_3);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank3($action_price_client_android_softbank_3);
				$advert_price_media_set->setActionPriceClientAndroidAu3($action_price_client_android_au_3);
				$advert_price_media_set->setActionPriceClientAndroidPc3($action_price_client_android_pc_3);

				$advert_price_media_set->setActionPriceClientAndroidDocomo4($action_price_client_android_docomo_4);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank4($action_price_client_android_softbank_4);
				$advert_price_media_set->setActionPriceClientAndroidAu4($action_price_client_android_au_4);
				$advert_price_media_set->setActionPriceClientAndroidPc4($action_price_client_android_pc_4);

				$advert_price_media_set->setActionPriceClientAndroidDocomo5($action_price_client_android_docomo_5);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank5($action_price_client_android_softbank_5);
				$advert_price_media_set->setActionPriceClientAndroidAu5($action_price_client_android_au_5);
				$advert_price_media_set->setActionPriceClientAndroidPc5($action_price_client_android_pc_5);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo1($action_price_media_android_docomo_1);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank1($action_price_media_android_softbank_1);
				$advert_price_media_set->setActionPriceMediaAndroidAu1($action_price_media_android_au_1);
				$advert_price_media_set->setActionPriceMediaAndroidPc1($action_price_media_android_pc_1);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo2($action_price_media_android_docomo_2);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank2($action_price_media_android_softbank_2);
				$advert_price_media_set->setActionPriceMediaAndroidAu2($action_price_media_android_au_2);
				$advert_price_media_set->setActionPriceMediaAndroidPc2($action_price_media_android_pc_2);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo3($action_price_media_android_docomo_3);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank3($action_price_media_android_softbank_3);
				$advert_price_media_set->setActionPriceMediaAndroidAu3($action_price_media_android_au_3);
				$advert_price_media_set->setActionPriceMediaAndroidPc3($action_price_media_android_pc_3);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo4($action_price_media_android_docomo_4);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank4($action_price_media_android_softbank_4);
				$advert_price_media_set->setActionPriceMediaAndroidAu4($action_price_media_android_au_4);
				$advert_price_media_set->setActionPriceMediaAndroidPc4($action_price_media_android_pc_4);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo5($action_price_media_android_docomo_5);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank5($action_price_media_android_softbank_5);
				$advert_price_media_set->setActionPriceMediaAndroidAu5($action_price_media_android_au_5);
				$advert_price_media_set->setActionPriceMediaAndroidPc5($action_price_media_android_pc_5);


							$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
							$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
							$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
							$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
							$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);

							$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
							$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
							$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
							$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
							$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

							//INSERTを実行
							$db_result = $advert_price_media_set_dao->insertAdvertPriceMediaSet($advert_price_media_set, $result_message);
						} else {

// **************************************************************************************
// update
// **************************************************************************************
							$advert_price_media_set = new AdvertPriceMediaSet();
							$advert_price_media_set->setId($id);
							$advert_price_media_set->setAdvertId($advert_id);
							$advert_price_media_set->setMediaId($media_id);
							$advert_price_media_set->setClickPriceClient($click_price_client);
							$advert_price_media_set->setClickPriceMedia($click_price_media);


				$advert_price_media_set->setActionPriceClientIphoneDocomo1($action_price_client_iphone_docomo_1);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank1($action_price_client_iphone_softbank_1);
				$advert_price_media_set->setActionPriceClientIphoneAu1($action_price_client_iphone_au_1);
				$advert_price_media_set->setActionPriceClientIphonePc1($action_price_client_iphone_pc_1);

				$advert_price_media_set->setActionPriceClientIphoneDocomo2($action_price_client_iphone_docomo_2);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank2($action_price_client_iphone_softbank_2);
				$advert_price_media_set->setActionPriceClientIphoneAu2($action_price_client_iphone_au_2);
				$advert_price_media_set->setActionPriceClientIphonePc2($action_price_client_iphone_pc_2);

				$advert_price_media_set->setActionPriceClientIphoneDocomo3($action_price_client_iphone_docomo_3);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank3($action_price_client_iphone_softbank_3);
				$advert_price_media_set->setActionPriceClientIphoneAu3($action_price_client_iphone_au_3);
				$advert_price_media_set->setActionPriceClientIphonePc3($action_price_client_iphone_pc_3);

				$advert_price_media_set->setActionPriceClientIphoneDocomo4($action_price_client_iphone_docomo_4);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank4($action_price_client_iphone_softbank_4);
				$advert_price_media_set->setActionPriceClientIphoneAu4($action_price_client_iphone_au_4);
				$advert_price_media_set->setActionPriceClientIphonePc4($action_price_client_iphone_pc_4);

				$advert_price_media_set->setActionPriceClientIphoneDocomo5($action_price_client_iphone_docomo_5);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank5($action_price_client_iphone_softbank_5);
				$advert_price_media_set->setActionPriceClientIphoneAu5($action_price_client_iphone_au_5);
				$advert_price_media_set->setActionPriceClientIphonePc5($action_price_client_iphone_pc_5);


				$advert_price_media_set->setActionPriceMediaIphoneDocomo1($action_price_media_iphone_docomo_1);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank1($action_price_media_iphone_softbank_1);
				$advert_price_media_set->setActionPriceMediaIphoneAu1($action_price_media_iphone_au_1);
				$advert_price_media_set->setActionPriceMediaIphonePc1($action_price_media_iphone_pc_1);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo2($action_price_media_iphone_docomo_2);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank2($action_price_media_iphone_softbank_2);
				$advert_price_media_set->setActionPriceMediaIphoneAu2($action_price_media_iphone_au_2);
				$advert_price_media_set->setActionPriceMediaIphonePc2($action_price_media_iphone_pc_2);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo3($action_price_media_iphone_docomo_3);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank3($action_price_media_iphone_softbank_3);
				$advert_price_media_set->setActionPriceMediaIphoneAu3($action_price_media_iphone_au_3);
				$advert_price_media_set->setActionPriceMediaIphonePc3($action_price_media_iphone_pc_3);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo4($action_price_media_iphone_docomo_4);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank4($action_price_media_iphone_softbank_4);
				$advert_price_media_set->setActionPriceMediaIphoneAu4($action_price_media_iphone_au_4);
				$advert_price_media_set->setActionPriceMediaIphonePc4($action_price_media_iphone_pc_4);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo5($action_price_media_iphone_docomo_5);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank5($action_price_media_iphone_softbank_5);
				$advert_price_media_set->setActionPriceMediaIphoneAu5($action_price_media_iphone_au_5);
				$advert_price_media_set->setActionPriceMediaIphonePc5($action_price_media_iphone_pc_5);


				$advert_price_media_set->setActionPriceClientAndroidDocomo1($action_price_client_android_docomo_1);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank1($action_price_client_android_softbank_1);
				$advert_price_media_set->setActionPriceClientAndroidAu1($action_price_client_android_au_1);
				$advert_price_media_set->setActionPriceClientAndroidPc1($action_price_client_android_pc_1);

				$advert_price_media_set->setActionPriceClientAndroidDocomo2($action_price_client_android_docomo_2);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank2($action_price_client_android_softbank_2);
				$advert_price_media_set->setActionPriceClientAndroidAu2($action_price_client_android_au_2);
				$advert_price_media_set->setActionPriceClientAndroidPc2($action_price_client_android_pc_2);

				$advert_price_media_set->setActionPriceClientAndroidDocomo3($action_price_client_android_docomo_3);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank3($action_price_client_android_softbank_3);
				$advert_price_media_set->setActionPriceClientAndroidAu3($action_price_client_android_au_3);
				$advert_price_media_set->setActionPriceClientAndroidPc3($action_price_client_android_pc_3);

				$advert_price_media_set->setActionPriceClientAndroidDocomo4($action_price_client_android_docomo_4);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank4($action_price_client_android_softbank_4);
				$advert_price_media_set->setActionPriceClientAndroidAu4($action_price_client_android_au_4);
				$advert_price_media_set->setActionPriceClientAndroidPc4($action_price_client_android_pc_4);

				$advert_price_media_set->setActionPriceClientAndroidDocomo5($action_price_client_android_docomo_5);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank5($action_price_client_android_softbank_5);
				$advert_price_media_set->setActionPriceClientAndroidAu5($action_price_client_android_au_5);
				$advert_price_media_set->setActionPriceClientAndroidPc5($action_price_client_android_pc_5);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo1($action_price_media_android_docomo_1);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank1($action_price_media_android_softbank_1);
				$advert_price_media_set->setActionPriceMediaAndroidAu1($action_price_media_android_au_1);
				$advert_price_media_set->setActionPriceMediaAndroidPc1($action_price_media_android_pc_1);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo2($action_price_media_android_docomo_2);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank2($action_price_media_android_softbank_2);
				$advert_price_media_set->setActionPriceMediaAndroidAu2($action_price_media_android_au_2);
				$advert_price_media_set->setActionPriceMediaAndroidPc2($action_price_media_android_pc_2);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo3($action_price_media_android_docomo_3);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank3($action_price_media_android_softbank_3);
				$advert_price_media_set->setActionPriceMediaAndroidAu3($action_price_media_android_au_3);
				$advert_price_media_set->setActionPriceMediaAndroidPc3($action_price_media_android_pc_3);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo4($action_price_media_android_docomo_4);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank4($action_price_media_android_softbank_4);
				$advert_price_media_set->setActionPriceMediaAndroidAu4($action_price_media_android_au_4);
				$advert_price_media_set->setActionPriceMediaAndroidPc4($action_price_media_android_pc_4);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo5($action_price_media_android_docomo_5);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank5($action_price_media_android_softbank_5);
				$advert_price_media_set->setActionPriceMediaAndroidAu5($action_price_media_android_au_5);
				$advert_price_media_set->setActionPriceMediaAndroidPc5($action_price_media_android_pc_5);



							$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
							$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
							$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
							$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
							$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);

							$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
							$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
							$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
							$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
							$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

							//UPDATEを実行
							$db_result = $advert_price_media_set_dao->updateAdvertPriceMediaAdvertMediaSet($advert_price_media_set, $advert_id, $media_id);
						}
						if($db_result) {
							$advert_price_media_set_dao->transaction_end();
						} else {
							$advert_view_media_set_dao->transaction_rollback();
						}
					}
					$media_dao -> transaction_end();
//					// ページを表示
//					header('Location: ./advert.php');
//					exit();

				//-----------------------------------------------------------------------------------------------
				$form_data = array(


										'action_price_client_iphone_docomo_1' => $action_price_client_iphone_docomo_1,
										'action_price_client_iphone_softbank_1' => $action_price_client_iphone_softbank_1,
										'action_price_client_iphone_au_1' => $action_price_client_iphone_au_1,
										'action_price_client_iphone_pc_1' => $action_price_client_iphone_pc_1,

										'action_price_client_iphone_docomo_2' => $action_price_client_iphone_docomo_2,
										'action_price_client_iphone_softbank_2' => $action_price_client_iphone_softbank_2,
										'action_price_client_iphone_au_2' => $action_price_client_iphone_au_2,
										'action_price_client_iphone_pc_2' => $action_price_client_iphone_pc_2,

										'action_price_client_iphone_docomo_3' => $action_price_client_iphone_docomo_3,
										'action_price_client_iphone_softbank_3' => $action_price_client_iphone_softbank_3,
										'action_price_client_iphone_au_3' => $action_price_client_iphone_au_3,
										'action_price_client_iphone_pc_3' => $action_price_client_iphone_pc_3,

										'action_price_client_iphone_docomo_4' => $action_price_client_iphone_docomo_4,
										'action_price_client_iphone_softbank_4' => $action_price_client_iphone_softbank_4,
										'action_price_client_iphone_au_4' => $action_price_client_iphone_au_4,
										'action_price_client_iphone_pc_4' => $action_price_client_iphone_pc_4,

										'action_price_client_iphone_docomo_5' => $action_price_client_iphone_docomo_5,
										'action_price_client_iphone_softbank_5' => $action_price_client_iphone_softbank_5,
										'action_price_client_iphone_au_5' => $action_price_client_iphone_au_5,
										'action_price_client_iphone_pc_5' => $action_price_client_iphone_pc_5,


										'action_price_media_iphone_docomo_1' => $action_price_media_iphone_docomo_1,
										'action_price_media_iphone_softbank_1' => $action_price_media_iphone_softbank_1,
										'action_price_media_iphone_au_1' => $action_price_media_iphone_au_1,
										'action_price_media_iphone_pc_1' => $action_price_media_iphone_pc_1,

										'action_price_media_iphone_docomo_2' => $action_price_media_iphone_docomo_2,
										'action_price_media_iphone_softbank_2' => $action_price_media_iphone_softbank_2,
										'action_price_media_iphone_au_2' => $action_price_media_iphone_au_2,
										'action_price_media_iphone_pc_2' => $action_price_media_iphone_pc_2,

										'action_price_media_iphone_docomo_3' => $action_price_media_iphone_docomo_3,
										'action_price_media_iphone_softbank_3' => $action_price_media_iphone_softbank_3,
										'action_price_media_iphone_au_3' => $action_price_media_iphone_au_3,
										'action_price_media_iphone_pc_3' => $action_price_media_iphone_pc_3,

										'action_price_media_iphone_docomo_4' => $action_price_media_iphone_docomo_4,
										'action_price_media_iphone_softbank_4' => $action_price_media_iphone_softbank_4,
										'action_price_media_iphone_au_4' => $action_price_media_iphone_au_4,
										'action_price_media_iphone_pc_4' => $action_price_media_iphone_pc_4,

										'action_price_media_iphone_docomo_5' => $action_price_media_iphone_docomo_5,
										'action_price_media_iphone_softbank_5' => $action_price_media_iphone_softbank_5,
										'action_price_media_iphone_au_5' => $action_price_media_iphone_au_5,
										'action_price_media_iphone_pc_5' => $action_price_media_iphone_pc_5,


										'action_price_client_android_docomo_1' => $action_price_client_android_docomo_1,
										'action_price_client_android_softbank_1' => $action_price_client_android_softbank_1,
										'action_price_client_android_au_1' => $action_price_client_android_au_1,
										'action_price_client_android_pc_1' => $action_price_client_android_pc_1,

										'action_price_client_android_docomo_2' => $action_price_client_android_docomo_2,
										'action_price_client_android_softbank_2' => $action_price_client_android_softbank_2,
										'action_price_client_android_au_2' => $action_price_client_android_au_2,
										'action_price_client_android_pc_2' => $action_price_client_android_pc_2,

										'action_price_client_android_docomo_3' => $action_price_client_android_docomo_3,
										'action_price_client_android_softbank_3' => $action_price_client_android_softbank_3,
										'action_price_client_android_au_3' => $action_price_client_android_au_3,
										'action_price_client_android_pc_3' => $action_price_client_android_pc_3,

										'action_price_client_android_docomo_4' => $action_price_client_android_docomo_4,
										'action_price_client_android_softbank_4' => $action_price_client_android_softbank_4,
										'action_price_client_android_au_4' => $action_price_client_android_au_4,
										'action_price_client_android_pc_4' => $action_price_client_android_pc_4,

										'action_price_client_android_docomo_5' => $action_price_client_android_docomo_5,
										'action_price_client_android_softbank_5' => $action_price_client_android_softbank_5,
										'action_price_client_android_au_5' => $action_price_client_android_au_5,
										'action_price_client_android_pc_5' => $action_price_client_android_pc_5,


										'action_price_media_android_docomo_1' => $action_price_media_android_docomo_1,
										'action_price_media_android_softbank_1' => $action_price_media_android_softbank_1,
										'action_price_media_android_au_1' => $action_price_media_android_au_1,
										'action_price_media_android_pc_1' => $action_price_media_android_pc_1,

										'action_price_media_android_docomo_2' => $action_price_media_android_docomo_2,
										'action_price_media_android_softbank_2' => $action_price_media_android_softbank_2,
										'action_price_media_android_au_2' => $action_price_media_android_au_2,
										'action_price_media_android_pc_2' => $action_price_media_android_pc_2,

										'action_price_media_android_docomo_3' => $action_price_media_android_docomo_3,
										'action_price_media_android_softbank_3' => $action_price_media_android_softbank_3,
										'action_price_media_android_au_3' => $action_price_media_android_au_3,
										'action_price_media_android_pc_3' => $action_price_media_android_pc_3,

										'action_price_media_android_docomo_4' => $action_price_media_android_docomo_4,
										'action_price_media_android_softbank_4' => $action_price_media_android_softbank_4,
										'action_price_media_android_au_4' => $action_price_media_android_au_4,
										'action_price_media_android_pc_4' => $action_price_media_android_pc_4,

										'action_price_media_android_docomo_5' => $action_price_media_android_docomo_5,
										'action_price_media_android_softbank_5' => $action_price_media_android_softbank_5,
										'action_price_media_android_au_5' => $action_price_media_android_au_5,
										'action_price_media_android_pc_5' => $action_price_media_android_pc_5,


								'action_price_client_pc_1' => $action_price_client_pc_1,
								'action_price_client_pc_2' => $action_price_client_pc_2,
								'action_price_client_pc_3' => $action_price_client_pc_3,
								'action_price_client_pc_4' => $action_price_client_pc_4,
								'action_price_client_pc_5' => $action_price_client_pc_5,

								'action_price_media_pc_1' => $action_price_media_pc_1,
								'action_price_media_pc_2' => $action_price_media_pc_2,
								'action_price_media_pc_3' => $action_price_media_pc_3,
								'action_price_media_pc_4' => $action_price_media_pc_4,
								'action_price_media_pc_5' => $action_price_media_pc_5);

				// session変数へ配列を格納
//				$_SESSION['form_data'] = $form_data;

				$smarty->assign("form_data", $form_data);
				//-----------------------------------------------------------------------------------------------


			}else {
			// 失敗
			$error_Message = "DBからデータの取得に失敗しました。";
			// smarty変数へ格納
			$smarty->assign("error_Message", $error_Message);

		}


	// *********************************************************************************
	// 個別設定
	// *********************************************************************************
	} elseif(isset($_POST['mode']) && $_POST['mode'] == "setup") {

		$advert_price_media_set_dao->transaction_start();

		$result = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

// **************************************************************************************
// insert
// **************************************************************************************
		if(is_null($result)) {
			$advert_price_media_set = new AdvertPriceMediaSet();
			$advert_price_media_set->setAdvertId($advert_id);
			$advert_price_media_set->setMediaId($media_id);
			$advert_price_media_set->setClickPriceClient($click_price_client);
			$advert_price_media_set->setClickPriceMedia($click_price_media);

				$advert_price_media_set->setActionPriceClientIphoneDocomo1($action_price_client_iphone_docomo_1);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank1($action_price_client_iphone_softbank_1);
				$advert_price_media_set->setActionPriceClientIphoneAu1($action_price_client_iphone_au_1);
				$advert_price_media_set->setActionPriceClientIphonePc1($action_price_client_iphone_pc_1);

				$advert_price_media_set->setActionPriceClientIphoneDocomo2($action_price_client_iphone_docomo_2);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank2($action_price_client_iphone_softbank_2);
				$advert_price_media_set->setActionPriceClientIphoneAu2($action_price_client_iphone_au_2);
				$advert_price_media_set->setActionPriceClientIphonePc2($action_price_client_iphone_pc_2);

				$advert_price_media_set->setActionPriceClientIphoneDocomo3($action_price_client_iphone_docomo_3);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank3($action_price_client_iphone_softbank_3);
				$advert_price_media_set->setActionPriceClientIphoneAu3($action_price_client_iphone_au_3);
				$advert_price_media_set->setActionPriceClientIphonePc3($action_price_client_iphone_pc_3);

				$advert_price_media_set->setActionPriceClientIphoneDocomo4($action_price_client_iphone_docomo_4);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank4($action_price_client_iphone_softbank_4);
				$advert_price_media_set->setActionPriceClientIphoneAu4($action_price_client_iphone_au_4);
				$advert_price_media_set->setActionPriceClientIphonePc4($action_price_client_iphone_pc_4);

				$advert_price_media_set->setActionPriceClientIphoneDocomo5($action_price_client_iphone_docomo_5);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank5($action_price_client_iphone_softbank_5);
				$advert_price_media_set->setActionPriceClientIphoneAu5($action_price_client_iphone_au_5);
				$advert_price_media_set->setActionPriceClientIphonePc5($action_price_client_iphone_pc_5);


				$advert_price_media_set->setActionPriceMediaIphoneDocomo1($action_price_media_iphone_docomo_1);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank1($action_price_media_iphone_softbank_1);
				$advert_price_media_set->setActionPriceMediaIphoneAu1($action_price_media_iphone_au_1);
				$advert_price_media_set->setActionPriceMediaIphonePc1($action_price_media_iphone_pc_1);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo2($action_price_media_iphone_docomo_2);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank2($action_price_media_iphone_softbank_2);
				$advert_price_media_set->setActionPriceMediaIphoneAu2($action_price_media_iphone_au_2);
				$advert_price_media_set->setActionPriceMediaIphonePc2($action_price_media_iphone_pc_2);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo3($action_price_media_iphone_docomo_3);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank3($action_price_media_iphone_softbank_3);
				$advert_price_media_set->setActionPriceMediaIphoneAu3($action_price_media_iphone_au_3);
				$advert_price_media_set->setActionPriceMediaIphonePc3($action_price_media_iphone_pc_3);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo4($action_price_media_iphone_docomo_4);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank4($action_price_media_iphone_softbank_4);
				$advert_price_media_set->setActionPriceMediaIphoneAu4($action_price_media_iphone_au_4);
				$advert_price_media_set->setActionPriceMediaIphonePc4($action_price_media_iphone_pc_4);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo5($action_price_media_iphone_docomo_5);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank5($action_price_media_iphone_softbank_5);
				$advert_price_media_set->setActionPriceMediaIphoneAu5($action_price_media_iphone_au_5);
				$advert_price_media_set->setActionPriceMediaIphonePc5($action_price_media_iphone_pc_5);


				$advert_price_media_set->setActionPriceClientAndroidDocomo1($action_price_client_android_docomo_1);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank1($action_price_client_android_softbank_1);
				$advert_price_media_set->setActionPriceClientAndroidAu1($action_price_client_android_au_1);
				$advert_price_media_set->setActionPriceClientAndroidPc1($action_price_client_android_pc_1);

				$advert_price_media_set->setActionPriceClientAndroidDocomo2($action_price_client_android_docomo_2);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank2($action_price_client_android_softbank_2);
				$advert_price_media_set->setActionPriceClientAndroidAu2($action_price_client_android_au_2);
				$advert_price_media_set->setActionPriceClientAndroidPc2($action_price_client_android_pc_2);

				$advert_price_media_set->setActionPriceClientAndroidDocomo3($action_price_client_android_docomo_3);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank3($action_price_client_android_softbank_3);
				$advert_price_media_set->setActionPriceClientAndroidAu3($action_price_client_android_au_3);
				$advert_price_media_set->setActionPriceClientAndroidPc3($action_price_client_android_pc_3);

				$advert_price_media_set->setActionPriceClientAndroidDocomo4($action_price_client_android_docomo_4);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank4($action_price_client_android_softbank_4);
				$advert_price_media_set->setActionPriceClientAndroidAu4($action_price_client_android_au_4);
				$advert_price_media_set->setActionPriceClientAndroidPc4($action_price_client_android_pc_4);

				$advert_price_media_set->setActionPriceClientAndroidDocomo5($action_price_client_android_docomo_5);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank5($action_price_client_android_softbank_5);
				$advert_price_media_set->setActionPriceClientAndroidAu5($action_price_client_android_au_5);
				$advert_price_media_set->setActionPriceClientAndroidPc5($action_price_client_android_pc_5);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo1($action_price_media_android_docomo_1);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank1($action_price_media_android_softbank_1);
				$advert_price_media_set->setActionPriceMediaAndroidAu1($action_price_media_android_au_1);
				$advert_price_media_set->setActionPriceMediaAndroidPc1($action_price_media_android_pc_1);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo2($action_price_media_android_docomo_2);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank2($action_price_media_android_softbank_2);
				$advert_price_media_set->setActionPriceMediaAndroidAu2($action_price_media_android_au_2);
				$advert_price_media_set->setActionPriceMediaAndroidPc2($action_price_media_android_pc_2);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo3($action_price_media_android_docomo_3);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank3($action_price_media_android_softbank_3);
				$advert_price_media_set->setActionPriceMediaAndroidAu3($action_price_media_android_au_3);
				$advert_price_media_set->setActionPriceMediaAndroidPc3($action_price_media_android_pc_3);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo4($action_price_media_android_docomo_4);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank4($action_price_media_android_softbank_4);
				$advert_price_media_set->setActionPriceMediaAndroidAu4($action_price_media_android_au_4);
				$advert_price_media_set->setActionPriceMediaAndroidPc4($action_price_media_android_pc_4);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo5($action_price_media_android_docomo_5);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank5($action_price_media_android_softbank_5);
				$advert_price_media_set->setActionPriceMediaAndroidAu5($action_price_media_android_au_5);
				$advert_price_media_set->setActionPriceMediaAndroidPc5($action_price_media_android_pc_5);


			$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
			$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
			$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
			$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
			$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);

			$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
			$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
			$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
			$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
			$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

			//INSERTを実行
			$db_result = $advert_price_media_set_dao->insertAdvertPriceMediaSet($advert_price_media_set, $result_message);
		} else {

// **************************************************************************************
// update
// **************************************************************************************
			$advert_price_media_set = new AdvertPriceMediaSet();
			$advert_price_media_set->setId($id);
			$advert_price_media_set->setAdvertId($advert_id);
			$advert_price_media_set->setMediaId($media_id);
			$advert_price_media_set->setClickPriceClient($click_price_client);
			$advert_price_media_set->setClickPriceMedia($click_price_media);

				$advert_price_media_set->setActionPriceClientIphoneDocomo1($action_price_client_iphone_docomo_1);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank1($action_price_client_iphone_softbank_1);
				$advert_price_media_set->setActionPriceClientIphoneAu1($action_price_client_iphone_au_1);
				$advert_price_media_set->setActionPriceClientIphonePc1($action_price_client_iphone_pc_1);

				$advert_price_media_set->setActionPriceClientIphoneDocomo2($action_price_client_iphone_docomo_2);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank2($action_price_client_iphone_softbank_2);
				$advert_price_media_set->setActionPriceClientIphoneAu2($action_price_client_iphone_au_2);
				$advert_price_media_set->setActionPriceClientIphonePc2($action_price_client_iphone_pc_2);

				$advert_price_media_set->setActionPriceClientIphoneDocomo3($action_price_client_iphone_docomo_3);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank3($action_price_client_iphone_softbank_3);
				$advert_price_media_set->setActionPriceClientIphoneAu3($action_price_client_iphone_au_3);
				$advert_price_media_set->setActionPriceClientIphonePc3($action_price_client_iphone_pc_3);

				$advert_price_media_set->setActionPriceClientIphoneDocomo4($action_price_client_iphone_docomo_4);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank4($action_price_client_iphone_softbank_4);
				$advert_price_media_set->setActionPriceClientIphoneAu4($action_price_client_iphone_au_4);
				$advert_price_media_set->setActionPriceClientIphonePc4($action_price_client_iphone_pc_4);

				$advert_price_media_set->setActionPriceClientIphoneDocomo5($action_price_client_iphone_docomo_5);
				$advert_price_media_set->setActionPriceClientIphoneSoftbank5($action_price_client_iphone_softbank_5);
				$advert_price_media_set->setActionPriceClientIphoneAu5($action_price_client_iphone_au_5);
				$advert_price_media_set->setActionPriceClientIphonePc5($action_price_client_iphone_pc_5);


				$advert_price_media_set->setActionPriceMediaIphoneDocomo1($action_price_media_iphone_docomo_1);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank1($action_price_media_iphone_softbank_1);
				$advert_price_media_set->setActionPriceMediaIphoneAu1($action_price_media_iphone_au_1);
				$advert_price_media_set->setActionPriceMediaIphonePc1($action_price_media_iphone_pc_1);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo2($action_price_media_iphone_docomo_2);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank2($action_price_media_iphone_softbank_2);
				$advert_price_media_set->setActionPriceMediaIphoneAu2($action_price_media_iphone_au_2);
				$advert_price_media_set->setActionPriceMediaIphonePc2($action_price_media_iphone_pc_2);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo3($action_price_media_iphone_docomo_3);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank3($action_price_media_iphone_softbank_3);
				$advert_price_media_set->setActionPriceMediaIphoneAu3($action_price_media_iphone_au_3);
				$advert_price_media_set->setActionPriceMediaIphonePc3($action_price_media_iphone_pc_3);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo4($action_price_media_iphone_docomo_4);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank4($action_price_media_iphone_softbank_4);
				$advert_price_media_set->setActionPriceMediaIphoneAu4($action_price_media_iphone_au_4);
				$advert_price_media_set->setActionPriceMediaIphonePc4($action_price_media_iphone_pc_4);

				$advert_price_media_set->setActionPriceMediaIphoneDocomo5($action_price_media_iphone_docomo_5);
				$advert_price_media_set->setActionPriceMediaIphoneSoftbank5($action_price_media_iphone_softbank_5);
				$advert_price_media_set->setActionPriceMediaIphoneAu5($action_price_media_iphone_au_5);
				$advert_price_media_set->setActionPriceMediaIphonePc5($action_price_media_iphone_pc_5);


				$advert_price_media_set->setActionPriceClientAndroidDocomo1($action_price_client_android_docomo_1);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank1($action_price_client_android_softbank_1);
				$advert_price_media_set->setActionPriceClientAndroidAu1($action_price_client_android_au_1);
				$advert_price_media_set->setActionPriceClientAndroidPc1($action_price_client_android_pc_1);

				$advert_price_media_set->setActionPriceClientAndroidDocomo2($action_price_client_android_docomo_2);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank2($action_price_client_android_softbank_2);
				$advert_price_media_set->setActionPriceClientAndroidAu2($action_price_client_android_au_2);
				$advert_price_media_set->setActionPriceClientAndroidPc2($action_price_client_android_pc_2);

				$advert_price_media_set->setActionPriceClientAndroidDocomo3($action_price_client_android_docomo_3);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank3($action_price_client_android_softbank_3);
				$advert_price_media_set->setActionPriceClientAndroidAu3($action_price_client_android_au_3);
				$advert_price_media_set->setActionPriceClientAndroidPc3($action_price_client_android_pc_3);

				$advert_price_media_set->setActionPriceClientAndroidDocomo4($action_price_client_android_docomo_4);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank4($action_price_client_android_softbank_4);
				$advert_price_media_set->setActionPriceClientAndroidAu4($action_price_client_android_au_4);
				$advert_price_media_set->setActionPriceClientAndroidPc4($action_price_client_android_pc_4);

				$advert_price_media_set->setActionPriceClientAndroidDocomo5($action_price_client_android_docomo_5);
				$advert_price_media_set->setActionPriceClientAndroidSoftbank5($action_price_client_android_softbank_5);
				$advert_price_media_set->setActionPriceClientAndroidAu5($action_price_client_android_au_5);
				$advert_price_media_set->setActionPriceClientAndroidPc5($action_price_client_android_pc_5);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo1($action_price_media_android_docomo_1);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank1($action_price_media_android_softbank_1);
				$advert_price_media_set->setActionPriceMediaAndroidAu1($action_price_media_android_au_1);
				$advert_price_media_set->setActionPriceMediaAndroidPc1($action_price_media_android_pc_1);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo2($action_price_media_android_docomo_2);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank2($action_price_media_android_softbank_2);
				$advert_price_media_set->setActionPriceMediaAndroidAu2($action_price_media_android_au_2);
				$advert_price_media_set->setActionPriceMediaAndroidPc2($action_price_media_android_pc_2);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo3($action_price_media_android_docomo_3);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank3($action_price_media_android_softbank_3);
				$advert_price_media_set->setActionPriceMediaAndroidAu3($action_price_media_android_au_3);
				$advert_price_media_set->setActionPriceMediaAndroidPc3($action_price_media_android_pc_3);


				$advert_price_media_set->setActionPriceMediaAndroidDocomo4($action_price_media_android_docomo_4);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank4($action_price_media_android_softbank_4);
				$advert_price_media_set->setActionPriceMediaAndroidAu4($action_price_media_android_au_4);
				$advert_price_media_set->setActionPriceMediaAndroidPc4($action_price_media_android_pc_4);

				$advert_price_media_set->setActionPriceMediaAndroidDocomo5($action_price_media_android_docomo_5);
				$advert_price_media_set->setActionPriceMediaAndroidSoftbank5($action_price_media_android_softbank_5);
				$advert_price_media_set->setActionPriceMediaAndroidAu5($action_price_media_android_au_5);
				$advert_price_media_set->setActionPriceMediaAndroidPc5($action_price_media_android_pc_5);


			$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
			$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
			$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
			$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
			$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);

			$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
			$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
			$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
			$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
			$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

			//UPDATEを実行
			$db_result = $advert_price_media_set_dao->updateAdvertPriceMediaSet($advert_price_media_set, $result_message);
		}
		if($db_result) {
			$advert_price_media_set_dao->transaction_end();

			// ページを表示
//			header('Location: ./advert.php');
//			exit();
		} else {
			$advert_view_media_set_dao->transaction_rollback();
		}

		// -------------------------------------------------------------------------------------
		// 再度DBへ接続し、変更または、登録したデータを取得
		if($advert_id != 0 && $media_id != 0) {
			$advert_price_madia_set = new AdvertPriceMediaSet();
			$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

			if(!is_null($advert_price_madia_set)) {
				$form_data = array('id' => $advert_price_madia_set->getId(),
								'click_price_client' => $advert_price_madia_set->getClickPriceClient(),
								'click_price_media' => $advert_price_madia_set->getClickPriceMedia(),

										'action_price_client_iphone_docomo_1' => $advert_price_media_set->getActionPriceClientIphoneDocomo1(),
										'action_price_client_iphone_softbank_1' => $advert_price_media_set->getActionPriceClientIphoneSoftbank1(),
										'action_price_client_iphone_au_1' => $advert_price_media_set->getActionPriceClientIphoneAu1(),
										'action_price_client_iphone_pc_1' => $advert_price_media_set->getActionPriceClientIphonePc1(),

										'action_price_client_iphone_docomo_2' => $advert_price_media_set->getActionPriceClientIphoneDocomo2(),
										'action_price_client_iphone_softbank_2' => $advert_price_media_set->getActionPriceClientIphoneSoftbank2(),
										'action_price_client_iphone_au_2' => $advert_price_media_set->getActionPriceClientIphoneAu2(),
										'action_price_client_iphone_pc_2' => $advert_price_media_set->getActionPriceClientIphonePc2(),

										'action_price_client_iphone_docomo_3' => $advert_price_media_set->getActionPriceClientIphoneDocomo3(),
										'action_price_client_iphone_softbank_3' => $advert_price_media_set->getActionPriceClientIphoneSoftbank3(),
										'action_price_client_iphone_au_3' => $advert_price_media_set->getActionPriceClientIphoneAu3(),
										'action_price_client_iphone_pc_3' => $advert_price_media_set->getActionPriceClientIphonePc3(),

										'action_price_client_iphone_docomo_4' => $advert_price_media_set->getActionPriceClientIphoneDocomo4(),
										'action_price_client_iphone_softbank_4' => $advert_price_media_set->getActionPriceClientIphoneSoftbank4(),
										'action_price_client_iphone_au_4' => $advert_price_media_set->getActionPriceClientIphoneAu4(),
										'action_price_client_iphone_pc_4' => $advert_price_media_set->getActionPriceClientIphonePc4(),

										'action_price_client_iphone_docomo_5' => $advert_price_media_set->getActionPriceClientIphoneDocomo5(),
										'action_price_client_iphone_softbank_5' => $advert_price_media_set->getActionPriceClientIphoneSoftbank5(),
										'action_price_client_iphone_au_5' => $advert_price_media_set->getActionPriceClientIphoneAu5(),
										'action_price_client_iphone_pc_5' => $advert_price_media_set->getActionPriceClientIphonePc5(),

										'action_price_media_iphone_docomo_1' => $advert_price_media_set->getActionPriceMediaIphoneDocomo1(),
										'action_price_media_iphone_softbank_1' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank1(),
										'action_price_media_iphone_au_1' => $advert_price_media_set->getActionPriceMediaIphoneAu1(),
										'action_price_media_iphone_pc_1' => $advert_price_media_set->getActionPriceMediaIphonePc1(),

										'action_price_media_iphone_docomo_2' => $advert_price_media_set->getActionPriceMediaIphoneDocomo2(),
										'action_price_media_iphone_softbank_2' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank2(),
										'action_price_media_iphone_au_2' => $advert_price_media_set->getActionPriceMediaIphoneAu2(),
										'action_price_media_iphone_pc_2' => $advert_price_media_set->getActionPriceMediaIphonePc2(),

										'action_price_media_iphone_docomo_3' => $advert_price_media_set->getActionPriceMediaIphoneDocomo3(),
										'action_price_media_iphone_softbank_3' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank3(),
										'action_price_media_iphone_au_3' => $advert_price_media_set->getActionPriceMediaIphoneAu3(),
										'action_price_media_iphone_pc_3' => $advert_price_media_set->getActionPriceMediaIphonePc3(),

										'action_price_media_iphone_docomo_4' => $advert_price_media_set->getActionPriceMediaIphoneDocomo4(),
										'action_price_media_iphone_softbank_4' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank4(),
										'action_price_media_iphone_au_4' => $advert_price_media_set->getActionPriceMediaIphoneAu4(),
										'action_price_media_iphone_pc_4' => $advert_price_media_set->getActionPriceMediaIphonePc4(),

										'action_price_media_iphone_docomo_5' => $advert_price_media_set->getActionPriceMediaIphoneDocomo5(),
										'action_price_media_iphone_softbank_5' => $advert_price_media_set->getActionPriceMediaIphoneSoftbank5(),
										'action_price_media_iphone_au_5' => $advert_price_media_set->getActionPriceMediaIphoneAu5(),
										'action_price_media_iphone_pc_5' => $advert_price_media_set->getActionPriceMediaIphonePc5(),


										'action_price_client_android_docomo_1' => $advert_price_media_set->getActionPriceClientAndroidDocomo1(),
										'action_price_client_android_softbank_1' => $advert_price_media_set->getActionPriceClientAndroidSoftbank1(),
										'action_price_client_android_au_1' => $advert_price_media_set->getActionPriceClientAndroidAu1(),
										'action_price_client_android_pc_1' => $advert_price_media_set->getActionPriceClientAndroidPc1(),

										'action_price_client_android_docomo_2' => $advert_price_media_set->getActionPriceClientAndroidDocomo2(),
										'action_price_client_android_softbank_2' => $advert_price_media_set->getActionPriceClientAndroidSoftbank2(),
										'action_price_client_android_au_2' => $advert_price_media_set->getActionPriceClientAndroidAu2(),
										'action_price_client_android_pc_2' => $advert_price_media_set->getActionPriceClientAndroidPc2(),

										'action_price_client_android_docomo_3' => $advert_price_media_set->getActionPriceClientAndroidDocomo3(),
										'action_price_client_android_softbank_3' => $advert_price_media_set->getActionPriceClientAndroidSoftbank3(),
										'action_price_client_android_au_3' => $advert_price_media_set->getActionPriceClientAndroidAu3(),
										'action_price_client_android_pc_3' => $advert_price_media_set->getActionPriceClientAndroidPc3(),

										'action_price_client_android_docomo_4' => $advert_price_media_set->getActionPriceClientAndroidDocomo4(),
										'action_price_client_android_softbank_4' => $advert_price_media_set->getActionPriceClientAndroidSoftbank4(),
										'action_price_client_android_au_4' => $advert_price_media_set->getActionPriceClientAndroidAu4(),
										'action_price_client_android_pc_4' => $advert_price_media_set->getActionPriceClientAndroidPc4(),

										'action_price_client_android_docomo_5' => $advert_price_media_set->getActionPriceClientAndroidDocomo5(),
										'action_price_client_android_softbank_5' => $advert_price_media_set->getActionPriceClientAndroidSoftbank5(),
										'action_price_client_android_au_5' => $advert_price_media_set->getActionPriceClientAndroidAu5(),
										'action_price_client_android_pc_5' => $advert_price_media_set->getActionPriceClientAndroidPc5(),

										'action_price_media_android_docomo_1' => $advert_price_media_set->getActionPriceMediaAndroidDocomo1(),
										'action_price_media_android_softbank_1' => $advert_price_media_set->getActionPriceMediaAndroidSoftbank1(),
										'action_price_media_android_au_1' => $advert_price_media_set->getActionPriceMediaAndroidAu1(),
										'action_price_media_android_pc_1' => $advert_price_media_set->getActionPriceMediaAndroidPc1(),

										'action_price_media_android_docomo_2' => $advert_price_media_set->getActionPriceMediaAndroidDocomo2(),
										'action_price_media_android_softbank_2' => $advert_price_media_set->getActionPriceMediaAndroidSoftbank2(),
										'action_price_media_android_au_2' => $advert_price_media_set->getActionPriceMediaAndroidAu2(),
										'action_price_media_android_pc_2' => $advert_price_media_set->getActionPriceMediaAndroidPc2(),

										'action_price_media_android_docomo_3' => $advert_price_media_set->getActionPriceMediaAndroidDocomo3(),
										'action_price_media_android_softbank_3' => $advert_price_media_set->getActionPriceMediaAndroidSoftbank3(),
										'action_price_media_android_au_3' => $advert_price_media_set->getActionPriceMediaAndroidAu3(),
										'action_price_media_android_pc_3' => $advert_price_media_set->getActionPriceMediaAndroidPc3(),

										'action_price_media_android_docomo_4' => $advert_price_media_set->getActionPriceMediaAndroidDocomo4(),
										'action_price_media_android_softbank_4' => $advert_price_media_set->getActionPriceMediaAndroidSoftbank4(),
										'action_price_media_android_au_4' => $advert_price_media_set->getActionPriceMediaAndroidAu4(),
										'action_price_media_android_pc_4' => $advert_price_media_set->getActionPriceMediaAndroidPc4(),

										'action_price_media_android_docomo_5' => $advert_price_media_set->getActionPriceMediaAndroidDocomo5(),
										'action_price_media_android_softbank_5' => $advert_price_media_set->getActionPriceMediaAndroidSoftbank5(),
										'action_price_media_android_au_5' => $advert_price_media_set->getActionPriceMediaAndroidAu5(),
										'action_price_media_android_pc_5' => $advert_price_media_set->getActionPriceMediaAndroidPc5(),

								'action_price_client_pc_1' => $advert_price_madia_set->getActionPriceClientPc1(),
								'action_price_client_pc_2' => $advert_price_madia_set->getActionPriceClientPc2(),
								'action_price_client_pc_3' => $advert_price_madia_set->getActionPriceClientPc3(),
								'action_price_client_pc_4' => $advert_price_madia_set->getActionPriceClientPc4(),
								'action_price_client_pc_5' => $advert_price_madia_set->getActionPriceClientPc5(),

								'action_price_media_pc_1' => $advert_price_madia_set->getActionPriceMediaPc1(),
								'action_price_media_pc_2' => $advert_price_madia_set->getActionPriceMediaPc2(),
								'action_price_media_pc_3' => $advert_price_madia_set->getActionPriceMediaPc3(),
								'action_price_media_pc_4' => $advert_price_madia_set->getActionPriceMediaPc4(),
								'action_price_media_pc_5' => $advert_price_madia_set->getActionPriceMediaPc5());

				// smarty変数へ格納
				$smarty->assign("form_data", $form_data);
			}
		}
		// -------------------------------------------------------------------------------------

	} // set_up 終了

	// ページを表示
	$smarty->display("./advert_price_setup.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

?>