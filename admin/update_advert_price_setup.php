<?php

// DB接続ファイル
require_once( '../common/CommonDao.php'  );

class updateAdvertPriceSetup {

	// advert_idを条件に0レコードの金額をクリア
	public function getUpdateAdvertPriceSetupId($advert_id){
// --------------------------------------------------------------------------------2011/06/09
// 媒体別単価 0クリア

				// DB接続クラス生成
				$common_dao = new CommonDao();

				$am_sets_sql = " update advert_price_media_sets set "
							. " click_price_client = '0', "
							. " click_price_media = '0', "

							. " action_price_client_docomo_1 = '0', "
							. " action_price_client_softbank_1 = '0', "
							. " action_price_client_au_1 = '0', "
							. " action_price_client_pc_1 = '0', "

							. " action_price_client_docomo_2 = '0', "
							. " action_price_client_softbank_2 = '0', "
							. " action_price_client_au_2 = '0', "
							. " action_price_client_pc_2 = '0', "

							. " action_price_client_docomo_3 = '0', "
							. " action_price_client_softbank_3 = '0', "
							. " action_price_client_au_3 = '0', "
							. " action_price_client_pc_3 = '0', "

							. " action_price_client_docomo_4 = '0', "
							. " action_price_client_softbank_4 = '0', "
							. " action_price_client_au_4 = '0', "
							. " action_price_client_pc_4 = '0', "

							. " action_price_client_docomo_5 = '0', "
							. " action_price_client_softbank_5 = '0', "
							. " action_price_client_au_5 = '0', "
							. " action_price_client_pc_5 = '0', "

							. " action_price_media_docomo_1 = '0', "
							. " action_price_media_softbank_1 = '0', "
							. " action_price_media_au_1 = '0', "
							. " action_price_media_pc_1 = '0', "

							. " action_price_media_docomo_2 = '0', "
							. " action_price_media_softbank_2 = '0', "
							. " action_price_media_au_2 = '0', "
							. " action_price_media_pc_2 = '0', "

							. " action_price_media_docomo_3 = '0', "
							. " action_price_media_softbank_3 = '0', "
							. " action_price_media_au_3 = '0', "
							. " action_price_media_pc_3 = '0', "

							. " action_price_media_docomo_4 = '0', "
							. " action_price_media_softbank_4 = '0', "
							. " action_price_media_au_4 = '0', "
							. " action_price_media_pc_4 = '0', "

							. " action_price_media_docomo_5 = '0', "
							. " action_price_media_softbank_5 = '0', "
							. " action_price_media_au_5 = '0', "
							. " action_price_media_pc_5 = '0', "

							. " updated_at = now()"

							. " where advert_id = '$advert_id' AND deleted_at is NULL ";

				$db_update_result = $common_dao->db_update($am_sets_sql);

// ------------------------------------------------------------------------------------------

	}
}
?>