<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	//広告主一覧データ取得
	$advert_client_dao = new AdvertClientDao();
	$advert_client_array = array();
	foreach($advert_client_dao->getAllAdvertClient() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getClientName());
		$advert_client_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_client_array", $advert_client_array);

	//現在日時取得
	$now_date = getdate();
	$now_year = $now_date['year'];
	$now_month = $now_date['mon'];

	$select_date_type = 1;
	$monthly_year = $now_year;
	$monthly_month = $now_month;
	$between_start_year = $now_year;
	$between_start_month = $now_month;
	$between_start_day = 1;
	$between_end_year = $now_year;
	$between_end_month = $now_month;
	$between_end_day = date("d", mktime(0, 0, 0, $now_month + 1, 0, $now_year));

	if(isset($_POST['mode']) && $_POST['mode'] == 'search') {
		$advert_client_id = do_escape_quotes($_POST['advert_client_id']);
		$select_date_type = do_escape_quotes($_POST['select_date_type']);
		$monthly_year = $common_dao->db_string_escape(do_escape_quotes($_POST['monthly_year']));
		$monthly_month = $common_dao->db_string_escape(do_escape_quotes($_POST['monthly_month']));
		$between_start_year = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_year']));
		$between_start_month = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_month']));
		$between_start_day = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_day']));
		$between_end_year = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_year']));
		$between_end_month = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_month']));
		$between_end_day = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_day']));
	}

	$view_date = $monthly_year.$monthly_month;
	$view_start_date = "$between_start_year-$between_start_month-$between_start_day";
	$view_end_date = "$between_end_year-$between_end_month-$between_end_day";

	//データ取得用のSQL文作成
	$list_sql = " SELECT al.advert_id, al.advert_client_id, "
				. " a.advert_name, ac.client_name, "
				. " al.action_price_client as apc, "
				. " SUM(al.click_price_client) as click_price_client, "
				. " SUM(al.click_price_media) as click_price_media, "
				. " SUM(IF(al.status = 2 OR al.status = 4, al.action_price_client, NULL)) as action_price_client, "
				. " SUM(IF(al.status = 2 OR al.status = 4, al.action_price_media, NULL)) as action_price_media, "
				. " COUNT(al.status) as click_count, "
				. " COUNT(IF(al.status = 2 OR al.status = 4, al.status, NULL)) as action_count, "
				. " COUNT(IF(al.status = 2 OR al.status = 4, al.order_num, NULL)) as order_num "
				. " FROM action_logs as al "
				. " LEFT JOIN advert as a on al.advert_id = a.id "
				. " LEFT JOIN advert_clients as ac on al.advert_client_id = ac.id "
				. " WHERE al.deleted_at is NULL ";

	if($advert_client_id != 0) {
		$list_sql .= " AND al.advert_client_id = '$advert_client_id' ";
	}

	if($select_date_type == 1) {

		//年月指定
		$list_sql .= " AND ( "
					. " (al.status = 1 AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " OR "
					. " (al.status = 2 AND DATE_FORMAT(al.action_complete_date,'%Y%c') = '$view_date') "
					. " OR "
					. " (al.status = 4 AND DATE_FORMAT(al.action_complete_date,'%Y%c') = '$view_date') "
					. " ) ";

	} elseif($select_date_type == 2) {

		//期間指定
		$list_sql .= " AND ( "
					. " (al.status = 1 AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " OR "
					. " (al.status = 2 AND al.action_complete_date BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " OR "
					. " (al.status = 4 AND al.action_complete_date BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " ) ";

	}

	$list_sql .= " GROUP BY al.advert_id "
				. " ORDER BY al.advert_client_id ASC, al.advert_id ASC ";

// 	echo $list_sql;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $row) {
			$ac_id = $row['advert_client_id'];

			$summary[$ac_id]['advert_id'] = $row['advert_id'];
			$summary[$ac_id]['advert_name'] = $row['advert_name'];
			$summary[$ac_id]['advert_client_id'] = $row['advert_client_id'];
			$summary[$ac_id]['client_name'] = $row['client_name'];

			$a_count[$ac_id][] = $row['advert_id'];
			$a_count[$ac_id] = array_unique($a_count[$ac_id]);
			$summary[$ac_id]['advert_count'] = count($a_count[$ac_id]);

			$summary[$ac_id]['click_count'] += $row['click_count'];
			$summary[$ac_id]['action_count'] += $row['action_count'];
			$summary[$ac_id]['total_price'] += $row['click_price_client'] + $row['action_price_client'];
// 			$summary[$ac_id]['total_price'] += $row['click_price_client'] + $row['apc'] * $row['order_num'];
		}

		$smarty->assign("list", $summary);
		$list_count = count($summary);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	$search['advert_client_id'] = $advert_client_id;
	$search['select_date_type'] = $select_date_type;
	$search['monthly_year'] = $monthly_year;
	$search['monthly_month'] = $monthly_month;
	$search['between_start_year'] = $between_start_year;
	$search['between_start_month'] = $between_start_month;
	$search['between_start_day'] = $between_start_day;
	$search['between_end_year'] = $between_end_year;
	$search['between_end_month'] = $between_end_month;
	$search['between_end_day'] = $between_end_day;

	$smarty->assign("search", $search);

	$smarty->assign("type", $select_date_type);
	$smarty->assign("date", $view_date);
	$smarty->assign("start_date", $view_start_date);
	$smarty->assign("end_date", $view_end_date);

	// ページを表示
	$smarty->display("./summary_advert_client.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>