<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertGroupDao.php' );
require_once( '../dto/AdvertGroup.php' );
require_once( '../dao/PrefDao.php' );
require_once( '../dto/Pref.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_client_dao = new AdvertClientDao();

	//広告主グループ
	$advert_group_dao = new AdvertGroupDao();
	$advert_group_array = array();
	foreach($advert_group_dao->getAllAdvertGroup() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$advert_group_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_group_array", $advert_group_array);

	//都道府県
	$pref_dao = new PrefDao();
	$pref_array = array();
	foreach($pref_dao->getAllPref() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$pref_array[$val->getId()] = $row_array;
	}
	$smarty->assign("pref_array", $pref_array);

	if(isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} elseif(isset($_POST['mode'])) {
		$mode = $_POST['mode'];
	}

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
	} else {
		$id = do_escape_quotes($_POST['id']);
	}

	$advert_group_id = do_escape_quotes($_POST['advert_group_id']);
	$login_id = do_escape_quotes($_POST['login_id']);
	$login_pass = do_escape_quotes($_POST['login_pass']);
	$client_name = do_escape_quotes($_POST['client_name']);
	$contact_person = do_escape_quotes($_POST['contact_person']);
	$tel = do_escape_quotes($_POST['tel']);
	$fax = do_escape_quotes($_POST['fax']);
	$email = do_escape_quotes($_POST['email']);
	$zipcode1 = do_escape_quotes($_POST['zipcode1']);
	$zipcode2 = do_escape_quotes($_POST['zipcode2']);
	$pref = do_escape_quotes($_POST['pref']);
	$address1 = do_escape_quotes($_POST['address1']);
	$address2 = do_escape_quotes($_POST['address2']);
	$status = do_escape_quotes($_POST['status']);

	$method = do_escape_quotes($_POST['method']);
	$reverse = do_escape_quotes($_POST['reverse']);
	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);
	$search_id = do_escape_quotes($_POST['search_id']);
	$search_name = do_escape_quotes($_POST['search_name']);
	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	$list_sql = " SELECT ac.*, al.login_id, al.login_pass "
				. " FROM advert_clients as ac "
				. " LEFT JOIN advert_login_users as al on ac.login_user_id = al.id "
				. " WHERE ac.deleted_at is NULL "
				. " ORDER BY ac.id ASC ";

	if($mode != ''){
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./advert_client_input.tpl");
			exit();
		}elseif($mode == 'insert_commit'){

			$error_flag = 0;

			if($client_name == "") {
				$error_message = "会社名を入力してください。";
				$error_flag = 1;
			} elseif($login_id == "") {
				$error_message = "ログインIDを入力してください。";
				$error_flag = 1;
			} elseif($login_pass == "") {
				$error_message = "パスワードを入力してください。";
				$error_flag = 1;
			}

			//既に登録されたログイン名か確認
			if(is_null($advert_login_user_dao->getAdvertLoginUserByLoginId($login_id))) {
				$advert_login_user_dao->transaction_start();

				$advert_login_user = new AdvertLoginUser();
				$advert_login_user->setUserName($client_name);
				$advert_login_user->setLoginId($login_id);
				$advert_login_user->setLoginPass($login_pass);

				//INSERTを実行
				$db_result = $advert_login_user_dao->insertAdvertLoginUser($advert_login_user, $result_message);
				if($db_result) {
					$advert_login_user_dao->transaction_end();

					$insert_record = $advert_login_user_dao->getAdvertLoginUserByLoginId($login_id);
					if(!is_null($insert_record)) {
						$login_user_id = $insert_record->getId();
					} else {
						$error_message = "ＤＢからのデータの取得に失敗しました。(su0000)";
						$error_flag = 1;
					}
				} else {
					$error_message = $result_message;

					$advert_login_user_dao->transaction_rollback();

					$error_flag = 1;
				}
			} else {
				$error_message = "入力されたログインIDは登録されてます。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$advert_client_dao->transaction_start();

				$advert_client = new AdvertClient();
				$advert_client->setLoginUserId($login_user_id);
				$advert_client->setAdvertGroupId($advert_group_id);
				$advert_client->setClientName($client_name);
				$advert_client->setContactPerson($contact_person);
				$advert_client->setTel($tel);
				$advert_client->setFax($fax);
				$advert_client->setEmail($email);
				$advert_client->setZipcode1($zipcode1);
				$advert_client->setZipcode2($zipcode2);
				$advert_client->setPref($pref);
				$advert_client->setAddress1($address1);
				$advert_client->setAddress2($address2);
				$advert_client->setStatus($status);

				//INSERTを実行
				$db_result = $advert_client_dao->insertAdvertClient($advert_client, $result_message);
				if($db_result) {
					$advert_client_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$advert_client_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'login_id' => $login_id,
										'login_pass' => $login_pass,
										'advert_group_id' => $advert_group_id,
										'client_name' => $client_name,
										'contact_person' => $contact_person,
										'tel' => $tel,
										'fax' => $fax,
										'email' => $email,
										'zipcode1' => $zipcode1,
										'zipcode2' => $zipcode2,
										'pref' => $pref,
										'address1' => $address1,
										'address2' => $address2,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./advert_client_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'login_id' => $login_id,
									'login_pass' => $login_pass,
									'advert_group_id' => $advert_group_id,
									'client_name' => $client_name,
									'contact_person' => $contact_person,
									'tel' => $tel,
									'fax' => $fax,
									'email' => $email,
									'zipcode1' => $zipcode1,
									'zipcode2' => $zipcode2,
									'pref' => $pref,
									'address1' => $address1,
									'address2' => $address2,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_client_input.tpl");
				exit();
			}
		}elseif($mode == 'edit'){
			$advert_client = new AdvertClient;
			$advert_client = $advert_client_dao->getAdvertClientById($id);
			$login_user_id = $advert_client->getLoginUserId();

			$advert_login_user = new AdvertLoginUser();
			$advert_login_user = $advert_login_user_dao->getAdvertLoginUserById($login_user_id);

			if(!is_null($advert_client) && !is_null($advert_login_user)) {
				$form_data = array('id' => $advert_client->getId(),
									'login_id' => $advert_login_user->getLoginId(),
									'login_pass' => $advert_login_user->getLoginPass(),
									'advert_group_id' => $advert_client->getAdvertGroupId(),
									'client_name' => $advert_client->getClientName(),
									'contact_person' => $advert_client->getContactPerson(),
									'tel' => $advert_client->getTel(),
									'fax' => $advert_client->getFax(),
									'email' => $advert_client->getEmail(),
									'zipcode1' => $advert_client->getZipcode1(),
									'zipcode2' => $advert_client->getZipcode2(),
									'pref' => $advert_client->getPref(),
									'address1' => $advert_client->getAddress1(),
									'address2' => $advert_client->getAddress2(),
									'status' => $advert_client->getStatus());

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_client_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_client_input.tpl");
				exit();
			}
		}elseif($mode == 'update_commit'){

			$error_flag = 0;

			if($client_name == "") {
				$error_message = "会社名を入力してください。";
				$error_flag = 1;
			} elseif($login_id == "") {
				$error_message = "ログインIDを入力してください。";
				$error_flag = 1;
			} elseif($login_pass == "") {
				$error_message = "パスワードを入力してください。";
				$error_flag = 1;
			}

			$advert_client = new AdvertClient;
			$advert_client = $advert_client_dao->getAdvertClientById($id);
			$login_user_id = $advert_client->getLoginUserId();

			//既に登録されたログイン名か確認
			$result_data = $advert_login_user_dao->getAdvertLoginUserByLoginId($login_id);

			if(is_null($result_data) || $login_user_id == $result_data->getId()) {
				$advert_login_user_dao->transaction_start();

				$advert_login_user = new AdvertLoginUser();
				$advert_login_user->setId($login_user_id);
				$advert_login_user->setUserName($client_name);
				$advert_login_user->setLoginId($login_id);
				$advert_login_user->setLoginPass($login_pass);

				//UPDATEを実行
				$db_result = $advert_login_user_dao->updateAdvertLoginUser($advert_login_user, $result_message);
				if($db_result) {
					$advert_login_user_dao->transaction_end();

					$insert_record = $advert_login_user_dao->getAdvertLoginUserByLoginId($login_id);
					if(!is_null($insert_record)) {
						$login_user_id = $insert_record->getId();
					} else {
						$error_message = "ＤＢからのデータの取得に失敗しました。(su0000)";
						$error_flag = 1;
					}
				} else {
					$error_message = $result_message;

					$advert_login_user_dao->transaction_rollback();

					$error_flag = 1;
				}
			} else {
				$error_message = "入力されたログインIDは登録されてます。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$advert_client_dao->transaction_start();

				$advert_client = new AdvertClient();
				$advert_client->setId($id);
				$advert_client->setLoginUserId($login_user_id);
				$advert_client->setAdvertGroupId($advert_group_id);
				$advert_client->setClientName($client_name);
				$advert_client->setContactPerson($contact_person);
				$advert_client->setTel($tel);
				$advert_client->setFax($fax);
				$advert_client->setEmail($email);
				$advert_client->setZipcode1($zipcode1);
				$advert_client->setZipcode2($zipcode2);
				$advert_client->setPref($pref);
				$advert_client->setAddress1($address1);
				$advert_client->setAddress2($address2);
				$advert_client->setStatus($status);

				//UPDATEを実行
				$db_result = $advert_client_dao->updateAdvertClient($advert_client, $result_message);
				if($db_result) {
					$advert_client_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$advert_client_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'login_id' => $login_id,
										'login_pass' => $login_pass,
										'advert_group_id' => $advert_group_id,
										'login_id' => $login_id,
										'password' => $password,
										'client_name' => $client_name,
										'contact_person' => $contact_person,
										'tel' => $tel,
										'fax' => $fax,
										'email' => $email,
										'zipcode1' => $zipcode1,
										'zipcode2' => $zipcode2,
										'pref' => $pref,
										'address1' => $address1,
										'address2' => $address2,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '編集');

					// ページを表示
					$smarty->display("./advert_client_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'login_id' => $login_id,
									'login_pass' => $login_pass,
									'advert_group_id' => $advert_group_id,
									'login_id' => $login_id,
									'password' => $password,
									'client_name' => $client_name,
									'contact_person' => $contact_person,
									'tel' => $tel,
									'fax' => $fax,
									'email' => $email,
									'zipcode1' => $zipcode1,
									'zipcode2' => $zipcode2,
									'pref' => $pref,
									'address1' => $address1,
									'address2' => $address2,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_client_input.tpl");
				exit();
			}
		}elseif($mode == 'search'){

			$search_list_sql = " SELECT ac.*, al.login_id, al.login_pass "
							. " FROM advert_clients as ac "
							. " LEFT JOIN advert_login_users as al on ac.login_user_id = al.id "
							. " WHERE ac.deleted_at is NULL ";

			//絞込みWHERE文格納配列
			$add_where = array();

			switch($method) {
				case 1:	//「完全一致」
					if($search_id != "") {
						$add_where[] = "ac.id = '$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "ac.client_name = '$search_name' ";
					}
					break;
				case 2:	//「前方一致」
					if($search_id != "") {
						$add_where[] = "ac.id LIKE '$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "ac.client_name LIKE '$search_name%' ";
					}
					break;
				case 3:	//「後方一致」
					if($search_id != "") {
						$add_where[] = "ac.id LIKE '%$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "ac.client_name LIKE '%$search_name' ";
					}
					break;
				case 4:	//「あいまい」
					if($search_id != "") {
						$add_where[] = "ac.id LIKE '%$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "ac.client_name LIKE '%$search_name%' ";
					}
					break;
			}

			if(count($add_where) > 0) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(".implode("OR ", $add_where).") ";
				} else {
					$search_list_sql .= "AND ".implode("AND ", $add_where);
				}
			}

			if($created_at_flag == 1) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(ac.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
				} else {
					$search_list_sql .= "AND ac.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
				}

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

			$search_list_sql .= "ORDER BY ac.$sort $order ";

			$db_result = $common_dao->db_query($search_list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			$search['method'] = $method;
			$search['reverse'] = $reverse;
			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['id'] = $search_id;
			$search['name'] = $search_name;
			$search['created_at_flag'] = $created_at_flag;

			$smarty->assign("search", $search);

			$smarty->assign("mode", 'search');
			$smarty->assign("sub_title", '検索結果');

			// ページを表示
			$smarty->display("./advert_client_list.tpl");
			exit();
		}elseif($mode == 'delete'){
			$advert_client_dao->transaction_start();

			$record = $advert_client_dao->getAdvertClientById($id);

			if(!is_null($record)) {
				$login_user_id = $record->getLoginUserId();

				$db_result = $advert_client_dao->deleteAdvertClient($id, $result_message);
				if($db_result){
					$advert_client_dao->transaction_end();

					$advert_login_user_dao->transaction_start();

					if(!is_null($advert_login_user_dao->getAdvertLoginUserById($login_user_id))) {
						$db_result = $advert_login_user_dao->deleteAdvertLoginUser($login_user_id, $result_message);
						if($db_result) {
							$advert_login_user_dao->transaction_end();

							$smarty->assign("info_message", $result_message);

							view_list();
						} else {
							$media_login_user_dao->transaction_rollback();

							$smarty->assign("error_message", $result_message);

							view_list();
						}
					} else {
						$advert_login_user_dao->transaction_rollback();

						$error_message = "ＤＢの更新に失敗しました。";
						$smarty->assign("error_message", $error_message);

						view_list();
					}
				}else{
					$advert_client_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					view_list();
				}
			}else{
				$advert_client_dao->transaction_rollback();

				$error_message = "ＤＢの更新に失敗しました。";
				$smarty->assign("error_message", $error_message);

				view_list();
			}
		}else{
			view_list();
		}
	}else{
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./advert_client_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>