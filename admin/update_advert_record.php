<?php
//
//   update_advert_record.php
//   DATA:2010/06/11 T.Yamada
//
//-------------------------------------------

// 共通設定
require_once( '../common/CommonDao.php'  );

$debug_flag = 1;

$common_dao = new CommonDao();

if($_GET['day'] != '' && $_GET['lday'] != ''){
	$today = $common_dao->db_string_escape($_GET['day']);
	$last_day = $common_dao->db_string_escape($_GET['lday']);
}else{
	$today = date("Ymd");
	$last_day = date("Ymd", strtotime("-1 day"));
}


	// advert_reserve
	$select_sql = " select * from advert_reserve "
				. " where reserve_change_date = '$today' and status = 0 and deleted_at is NULL ";

	$db_select_result = $common_dao->db_query($select_sql);
	if($db_select_result) {
		foreach($db_select_result as $row) {
			$id = $row['id'];
			$advert_id = $row['advert_id'];
			$support_docomo = $row['support_docomo'];
			$support_softbank = $row['support_softbank'];
			$support_au = $row['support_au'];
			$support_pc = $row['support_pc'];
			$site_url_docomo = $row['site_url_docomo'];
			$site_url_softbank = $row['site_url_softbank'];
			$site_url_au = $row['site_url_au'];
			$site_url_pc = $row['site_url_pc'];
			$click_price_client = $row['click_price_client'];
			$click_price_media = $row['click_price_media'];
			$action_price_client_docomo_1 = $row['action_price_client_docomo_1'];
			$action_price_client_softbank_1 = $row['action_price_client_softbank_1'];
			$action_price_client_au_1 = $row['action_price_client_au_1'];
			$action_price_client_pc_1 = $row['action_price_client_pc_1'];
			$action_price_client_docomo_2 = $row['action_price_client_docomo_2'];
			$action_price_client_softbank_2 = $row['action_price_client_softbank_2'];
			$action_price_client_au_2 = $row['action_price_client_au_2'];
			$action_price_client_pc_2 = $row['action_price_client_pc_2'];
			$action_price_client_docomo_3 = $row['action_price_client_docomo_3'];
			$action_price_client_softbank_3 = $row['action_price_client_softbank_3'];
			$action_price_client_au_3 = $row['action_price_client_au_3'];
			$action_price_client_pc_3 = $row['action_price_client_pc_3'];
			$action_price_client_docomo_4 = $row['action_price_client_docomo_4'];
			$action_price_client_softbank_4 = $row['action_price_client_softbank_4'];
			$action_price_client_au_4 = $row['action_price_client_au_4'];
			$action_price_client_pc_4 = $row['action_price_client_pc_4'];
			$action_price_client_docomo_5 = $row['action_price_client_docomo_5'];
			$action_price_client_softbank_5 = $row['action_price_client_softbank_5'];
			$action_price_client_au_5 = $row['action_price_client_au_5'];
			$action_price_client_pc_5 = $row['action_price_client_pc_5'];
			$action_price_media_docomo_1 = $row['action_price_media_docomo_1'];
			$action_price_media_softbank_1 = $row['action_price_media_softbank_1'];
			$action_price_media_au_1 = $row['action_price_media_au_1'];
			$action_price_media_pc_1 = $row['action_price_media_pc_1'];
			$action_price_media_docomo_2 = $row['action_price_media_docomo_2'];
			$action_price_media_softbank_2 = $row['action_price_media_softbank_2'];
			$action_price_media_au_2 = $row['action_price_media_au_2'];
			$action_price_media_pc_2 = $row['action_price_media_pc_2'];
			$action_price_media_docomo_3 = $row['action_price_media_docomo_3'];
			$action_price_media_softbank_3 = $row['action_price_media_softbank_3'];
			$action_price_media_au_3 = $row['action_price_media_au_3'];
			$action_price_media_pc_3 = $row['action_price_media_pc_3'];
			$action_price_media_docomo_4 = $row['action_price_media_docomo_4'];
			$action_price_media_softbank_4 = $row['action_price_media_softbank_4'];
			$action_price_media_au_4 = $row['action_price_media_au_4'];
			$action_price_media_pc_4 = $row['action_price_media_pc_4'];
			$action_price_media_docomo_5 = $row['action_price_media_docomo_5'];
			$action_price_media_softbank_5 = $row['action_price_media_softbank_5'];
			$action_price_media_au_5 = $row['action_price_media_au_5'];
			$action_price_media_pc_5 = $row['action_price_media_pc_5'];
			$ms_text_1 = $row['ms_text_1'];
			$ms_email_1 = $row['ms_email_1'];
			$ms_image_type_1 = $row['ms_image_type_1'];
			$ms_image_url_1 = $row['ms_image_url_1'];
			$ms_text_2 = $row['ms_text_2'];
			$ms_email_2 = $row['ms_email_2'];
			$ms_image_type_2 = $row['ms_image_type_2'];
			$ms_image_url_2 = $row['ms_image_url_2'];
			$ms_text_3 = $row['ms_text_3'];
			$ms_email_3 = $row['ms_email_3'];
			$ms_image_type_3 = $row['ms_image_type_3'];
			$ms_image_url_3 = $row['ms_image_url_3'];
			$ms_text_4 = $row['ms_text_4'];
			$ms_email_4 = $row['ms_email_4'];
			$ms_image_type_4 = $row['ms_image_type_4'];
			$ms_image_url_4 = $row['ms_image_url_4'];
			$ms_text_5 = $row['ms_text_5'];
			$ms_email_5 = $row['ms_email_5'];
			$ms_image_type_5 = $row['ms_image_type_5'];
			$ms_image_url_5 = $row['ms_image_url_5'];

			$reserve_sql = " update advert set "
						. " support_docomo = '$support_docomo', "
						. " support_softbank = '$support_softbank', "
						. " support_au = '$support_au', "
						. " support_pc = '$support_pc', "
						. " site_url_docomo = '$site_url_docomo', "
						. " site_url_softbank = '$site_url_softbank', "
						. " site_url_au = '$site_url_au', "
						. " site_url_pc = '$site_url_pc', "
						. " click_price_client = '$click_price_client', "
						. " click_price_media = '$click_price_media', "
						. " action_price_client_docomo_1 = '$action_price_client_docomo_1', "
						. " action_price_client_softbank_1 = '$action_price_client_softbank_1', "
						. " action_price_client_au_1 = '$action_price_client_au_1', "
						. " action_price_client_pc_1 = '$action_price_client_pc_1', "
						. " action_price_client_docomo_2 = '$action_price_client_docomo_2', "
						. " action_price_client_softbank_2 = '$action_price_client_softbank_2', "
						. " action_price_client_au_2 = '$action_price_client_au_2', "
						. " action_price_client_pc_2 = '$action_price_client_pc_2', "
						. " action_price_client_docomo_3 = '$action_price_client_docomo_3', "
						. " action_price_client_softbank_3 = '$action_price_client_softbank_3', "
						. " action_price_client_au_3 = '$action_price_client_au_3', "
						. " action_price_client_pc_3 = '$action_price_client_pc_3', "
						. " action_price_client_docomo_4 = '$action_price_client_docomo_4', "
						. " action_price_client_softbank_4 = '$action_price_client_softbank_4', "
						. " action_price_client_au_4 = '$action_price_client_au_4', "
						. " action_price_client_pc_4 = '$action_price_client_pc_4', "
						. " action_price_client_docomo_5 = '$action_price_client_docomo_5', "
						. " action_price_client_softbank_5 = '$action_price_client_softbank_5', "
						. " action_price_client_au_5 = '$action_price_client_au_5', "
						. " action_price_client_pc_5 = '$action_price_client_pc_5', "
						. " action_price_media_docomo_1 = '$action_price_media_docomo_1', "
						. " action_price_media_softbank_1 = '$action_price_media_softbank_1', "
						. " action_price_media_au_1 = '$action_price_media_au_1', "
						. " action_price_media_pc_1 = '$action_price_media_pc_1', "
						. " action_price_media_docomo_2 = '$action_price_media_docomo_2', "
						. " action_price_media_softbank_2 = '$action_price_media_softbank_2', "
						. " action_price_media_au_2 = '$action_price_media_au_2', "
						. " action_price_media_pc_2 = '$action_price_media_pc_2', "
						. " action_price_media_docomo_3 = '$action_price_media_docomo_3', "
						. " action_price_media_softbank_3 = '$action_price_media_softbank_3', "
						. " action_price_media_au_3 = '$action_price_media_au_3', "
						. " action_price_media_pc_3 = '$action_price_media_pc_3', "
						. " action_price_media_docomo_4 = '$action_price_media_docomo_4', "
						. " action_price_media_softbank_4 = '$action_price_media_softbank_4', "
						. " action_price_media_au_4 = '$action_price_media_au_4', "
						. " action_price_media_pc_4 = '$action_price_media_pc_4', "
						. " action_price_media_docomo_5 = '$action_price_media_docomo_5', "
						. " action_price_media_softbank_5 = '$action_price_media_softbank_5', "
						. " action_price_media_au_5 = '$action_price_media_au_5', "
						. " action_price_media_pc_5 = '$action_price_media_pc_5', "
						. " ms_text_1 = '$ms_text_1', "
						. " ms_email_1 = '$ms_email_1', "
						. " ms_image_type_1 = '$ms_image_type_1', "
						. " ms_image_url_1 = '$ms_image_url_1', "
						. " ms_text_2 = '$ms_text_2', "
						. " ms_email_2 = '$ms_email_2', "
						. " ms_image_type_2 = '$ms_image_type_2', "
						. " ms_image_url_2 = '$ms_image_url_2', "
						. " ms_text_3 = '$ms_text_3', "
						. " ms_email_3 = '$ms_email_3', "
						. " ms_image_type_3 = '$ms_image_type_3', "
						. " ms_image_url_3 = '$ms_image_url_3', "
						. " ms_text_4 = '$ms_text_4', "
						. " ms_email_4 = '$ms_email_4', "
						. " ms_image_type_4 = '$ms_image_type_4', "
						. " ms_image_url_4 = '$ms_image_url_4', "
						. " ms_text_5 = '$ms_text_5', "
						. " ms_email_5 = '$ms_email_5', "
						. " ms_image_type_5 = '$ms_image_type_5', "
						. " ms_image_url_5 = '$ms_image_url_5' "
						. " where id = '$advert_id' and deleted_at is NULL ";

			$db_reserve_result = $common_dao->db_update($reserve_sql);

			if($db_reserve_result) {
				$update_sql = " update advert_reserve set status = 1 "
						 	 . " where id = '$id' and deleted_at is NULL ";
				$db_update_result = $common_dao->db_update($update_sql);


				if($db_update_result) {

					if($debug_flag){
						$insert_sql = " insert into system_logs values( "
									. " '', 200, 'advert_reserve id($id) on OK ($db_reserve_result)', Now() )";

						$db_insert_result = $common_dao->db_update($insert_sql);
						if($db_insert_result) {

						}else{

						}
					}

				}else{

				}

			}else{

			}
		}	// foreach終了

	} else {

	}
?>
