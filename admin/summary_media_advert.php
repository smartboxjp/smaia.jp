<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );

require_once( './class_page_calculate.php' );

session_start();

// クリック総数 変数初期化
$click_count = '0';
// アクション総数 変数初期化
$acton_count = '0';
// 金額総数 変数初期化
$total_count = '0';

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	//媒体発行者一覧データ取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher_array = array();
	foreach($media_publisher_dao->getAllMediaPublisher() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getPublisherName());
		$media_publisher_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_publisher_array", $media_publisher_array);

	//現在日時取得
	$now_date = getdate();
	$now_year = $now_date['year'];
	$now_month = $now_date['mon'];

	$select_date_type = 1;
	$monthly_year = $now_year;
	$monthly_month = $now_month;
	$between_start_year = $now_year;
	$between_start_month = $now_month;
	$between_start_day = 1;
	$between_end_year = $now_year;
	$between_end_month = $now_month;
	$between_end_day = date("d", mktime(0, 0, 0, $now_month + 1, 0, $now_year));

	if(isset($_POST['mode']) && $_POST['mode'] == 'search') {
		$media_publisher_id = do_escape_quotes($_POST['media_publisher_id']);
		$media_id = do_escape_quotes($_POST['media_id']);
		$select_date_type = do_escape_quotes($_POST['select_date_type']);
		$monthly_year = $common_dao->db_string_escape(do_escape_quotes($_POST['monthly_year']));
		$monthly_month = $common_dao->db_string_escape(do_escape_quotes($_POST['monthly_month']));
		$between_start_year = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_year']));
		$between_start_month = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_month']));
		$between_start_day = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_day']));
		$between_end_year = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_year']));
		$between_end_month = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_month']));
		$between_end_day = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_day']));
	}

	$view_date = $monthly_year.$monthly_month;
	$view_start_date = "$between_start_year-$between_start_month-$between_start_day";
	$view_end_date = "$between_end_year-$between_end_month-$between_end_day";

	if(isset($_GET['mp_id']) && $_GET['mp_id'] != "") {
		$media_publisher_id = $_GET['mp_id'];
	}

	if(isset($_GET['type']) && $_GET['type'] != "") {
		$select_date_type = $_GET['type'];
	}

	if(isset($_GET['date']) && $_GET['date'] != "") {
		$view_date = $_GET['date'];
	}

	if(isset($_GET['start_date']) && $_GET['start_date'] != "") {
		$view_start_date = $_GET['start_date'];
	}

	if(isset($_GET['end_date']) && $_GET['end_date'] != "") {
		$view_end_date = $_GET['end_date'];
	}

	if(isset($_GET['sort_price']) && $_GET['sort_price'] != "") {
		$sort_price = $_GET['sort_price'];
	}

	// ページ遷移カウント
	if(isset($_GET['limit']) && $_GET['limit'] != "") {
		$limit = $_GET['limit'];
	} else {
		$limit = 0;
	}

	//データ取得用のSQL文作成

	$list_sql= " SELECT al.media_id, al.advert_id, a.advert_name, al.media_publisher_id, "
				. " m.media_name, mp.publisher_name, "
				. " SUM(al.click_price_client) as click_price_client, "
				. " SUM(al.click_price_media) as click_price_media, "
				. " SUM(IF(al.status <> 1, al.action_price_client, NULL)) as action_price_client, "
				. " SUM(IF(al.status <> 1, al.action_price_media, NULL)) as total_price, "
// 				. " SUM(IF(al.status <> 1, al.action_price_client * al.order_num, NULL)) as action_price_client, "
// 				. " SUM(IF(al.status <> 1, al.action_price_media * al.order_num, NULL)) as total_price, "
				. " COUNT(al.status) as click_count, "
				. " COUNT(IF(al.status <> 1, al.status, NULL)) as action_count "
//				. " SUM(IF(al.status <> 1, al.order_num, NULL) as order_num "
				. " FROM action_logs as al "
				. " LEFT JOIN media as m on al.media_id = m.id "
				. " LEFT JOIN media_publishers as mp on al.media_publisher_id = mp.id "
				. " LEFT JOIN advert as a on al.advert_id = a.id "
				. " WHERE al.deleted_at is NULL ";


	if($media_publisher_id != 0) {
		$list_sql .= " AND al.media_publisher_id = '$media_publisher_id' ";
	}

	if($media_id != 0) {
		$list_sql .= " AND al.media_id = '$media_publisher_id' ";
	}

	if($select_date_type == 1) {

		//年月指定
		$list_sql .= " AND ( "
					. " (al.status = 1 AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " OR "
					. " (al.status = 2 AND DATE_FORMAT(al.action_complete_date,'%Y%c') = '$view_date') "
					// ステータス3 特殊なケース 例)ユーザークレーム等で成果を上げる
					. " OR "
					. " (al.status = 3 AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " ) ";

	} elseif($select_date_type == 2) {

		//期間指定
		$list_sql .= " AND ( "
					. " (al.status = 1 AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " OR "
					. " (al.status = 2 AND al.action_complete_date BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					// ステータス3 特殊なケース 例)ユーザークレーム等で成果を上げる
					. " OR "
					. " (al.status = 3 AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " ) ";

	}

	$list_sql .= " GROUP BY al.advert_id ,al.media_id "
				. " ORDER BY al.media_publisher_id ASC, al.media_id ASC ";

	$list_count = 0;

//	echo $list_sql;

	$db_result = $common_dao->db_query($list_sql);
	// 統合件数獲得
	$list_count = count($db_result);

	if($db_result){
	$summary = $db_result;
	// 合計値を取得
	foreach($db_result as $row=>$var) {

		$click_count += $var['click_count'];
		$acton_count += $var['action_count'];
		$total_count += $var['total_price'];

	}


// ------------------------------------------------------------------------------- 2011/06/03

	// レコードが100以上だった場合
	if($list_count > 100){
		// ページカウントクラス生成
		$c_page_claculate = new C_pageClaculate();
		$smarty->assign("link_page_count", $c_page_claculate->M_getpageClaculate($list_count, $limit, "./summary_media_advert.php"));

		// SQLにlimit付け足し
		$list_sql .= " limit $limit, 100 ";
		// クエリ実行
		$db_result = $common_dao->db_query($list_sql);

		// DB結果
		if($db_result){
			$summary = $db_result;
			// 合計値を取得
			foreach($db_result as $row=>$var) {

				$sub_click_count += $var['click_count'];
				$sub_acton_count += $var['action_count'];
				$sub_total_count += $var['total_price'];

			}
		// テスト出力
//		echo $list_sql;
		}

	}
// ------------------------------------------------------------------------------------------


		//金額合計でソート
		if($sort_price != "") {
			foreach($summary as $key => $val) {
				$sort[$key] = $val['total_price'];
			}

			if($sort_price == "asc") {
				array_multisort($sort, SORT_ASC, $summary);
				$sort_price = "desc";
				$mark_sort_prise = "[▼]";
			} elseif($sort_price == "desc") {
				array_multisort($sort, SORT_DESC, $summary);
				$sort_price = "asc";
				$mark_sort_prise = "[▲]";
			}
		}

		$smarty->assign("list", $summary);

	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	$search['media_publisher_id'] = $media_publisher_id;
	$search['media_id'] = $media_id;
	$search['select_date_type'] = $select_date_type;
	$search['monthly_year'] = $monthly_year;
	$search['monthly_month'] = $monthly_month;
	$search['between_start_year'] = $between_start_year;
	$search['between_start_month'] = $between_start_month;
	$search['between_start_day'] = $between_start_day;
	$search['between_end_year'] = $between_end_year;
	$search['between_end_month'] = $between_end_month;
	$search['between_end_day'] = $between_end_day;

	$smarty->assign("search", $search);
	$smarty->assign("limit", $limit);

	$smarty->assign("mp_id", $media_publisher_id);
	$smarty->assign("type", $select_date_type);
	$smarty->assign("date", $view_date);
	$smarty->assign("start_date", $view_start_date);
	$smarty->assign("end_date", $view_end_date);

	$smarty->assign("sort_price", $sort_price);
	$smarty->assign("mark_sort_prise", $mark_sort_prise);

	// クリック小計数 smarty変数へ格納
	$smarty->assign("sub_click_count", $sub_click_count);
	// アクション小計数 smarty変数へ格納
	$smarty->assign("sub_acton_count", $sub_acton_count);
	// 金額小計 smarty変数へ格納
	$smarty->assign("sub_total_count", $sub_total_count);

	// クリック総数 smarty変数へ格納
	$smarty->assign("click_count", $click_count);
	// アクション総数 smarty変数へ格納
	$smarty->assign("acton_count", $acton_count);
	// 金額総数 smarty変数へ格納
	$smarty->assign("total_count", $total_count);


	// ページを表示
	$smarty->display("./summary_media_advert.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>