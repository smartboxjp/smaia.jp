<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );


	//現在日時取得
	$now_date = getdate();
	$now_year = $now_date['year'];
	$now_month = $now_date['mon'];

	$exception_status = 0;

	// 20110804 追加 ********************************
	// 広告集計経由か媒体集計経由か
	// 集計フラグ
	if(isset($_GET['aggregate_flag']) && $_GET['aggregate_flag'] != ""){
		$aggregate_flag = $_GET['aggregate_flag'];

		if($aggregate_flag == "advert"){
			// 広告集計経由
			// 例外的な処理 広告集計経由の場合4
			$exception_status = 4;
		} elseif ($aggregate_flag == "media") {
			// 媒体集計経由
			// 例外的な処理 媒体集計経由の場合3
			$exception_status = 3;
		} else {
			// その他
			$exception_status = 0;
		}

		// Smarty変数へ格納
		$smarty->assign("aggregate_flag", $aggregate_flag);
	}
	// **********************************************


	//集計日時タイプ 取得
	if(isset($_GET['type']) && $_GET['type'] != "") {
		$select_date_type = $_GET['type'];
	} else {
		$select_date_type = 1;
	}

	//年月指定 取得
	if(isset($_GET['date']) && $_GET['date'] != "") {
		$view_date = $_GET['date'];
	} else {
		$view_date = $now_year.$now_month;
	}

	//期間指定開始日 取得
	if(isset($_GET['start_date']) && $_GET['start_date'] != "") {
		$view_start_date = $_GET['start_date'];
	} else {
		$between_start_year = $now_year;
		$between_start_month = $now_month;
		$between_start_day = 1;

		$view_start_date = "$between_start_year-$between_start_month-$between_start_day";
	}

	//期間指定終了日 取得
	if(isset($_GET['end_date']) && $_GET['end_date'] != "") {
		$view_end_date = $_GET['end_date'];
	} else {
		$between_end_year = $now_year;
		$between_end_month = $now_month;
		$between_end_day = date("d", mktime(0, 0, 0, $now_month + 1, 0, $now_year));

		$view_end_date = "$between_end_year-$between_end_month-$between_end_day";
	}

	//広告ID,広告主ID,媒体ID,媒体発行者ID 取得
	if(isset($_GET['m_id']) && $_GET['m_id'] != "") {
		$m_id = $_GET['m_id'];
	} else {
		$m_id = "";
	}

	if(isset($_GET['mp_id']) && $_GET['mp_id'] != "") {
		$mp_id = $_GET['mp_id'];
	} else {
		$mp_id = "";
	}

	if(isset($_GET['a_id']) && $_GET['a_id'] != "") {
		$a_id = $_GET['a_id'];
	} else {
		$a_id = "";
	}

	if(isset($_GET['ac_id']) && $_GET['ac_id'] != "") {
		$ac_id = $_GET['ac_id'];
	} else {
		$ac_id = "";
	}

	//開始ページ数 取得
	if(isset($_GET['page']) && $_GET['page'] != "") {
		$page = $_GET['page'];
	} else {
		$page = 1;
	}

	$common_dao = new CommonDao();

	//追加するWHERE句を作成
	$add_where = "";

	if($m_id != "") {
		$add_where .= "AND al.media_id = $m_id ";
	}

	if($mp_id != "") {
		$add_where .= "AND al.media_publisher_id = $mp_id ";
	}

	if($a_id != "") {
		$add_where .= "AND al.advert_id = $a_id ";
	}

	if($ac_id != "") {
		$add_where .= "AND al.advert_client_id = $ac_id ";
	}

	if($select_date_type == 1) {

		//年月指定
		$add_where .= " AND ( "
					. " (al.status = 1 AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " OR "
					. " (al.status = 2 AND DATE_FORMAT(al.action_complete_date,'%Y%c') = '$view_date') "
					. " OR "
					. " (al.status = $exception_status AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " ) ";

	} elseif($select_date_type == 2) {

		//期間指定
		$add_where .= " AND ( "
					. " (al.status = 1 AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " OR "
					. " (al.status = 2 AND al.action_complete_date BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " OR "
					. " (al.status = $exception_status AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " ) ";

	}

	$list_count = 0;

	//表示最大件数
	$limit_max = 1000;

	$sql = " SELECT count(*) as count "
			. " FROM action_logs as al "
			. " LEFT JOIN media as m on al.media_id = m.id "
			. " LEFT JOIN advert as a on al.advert_id = a.id "
			. " WHERE al.deleted_at is NULL "
			. " AND (al.status = 1 OR al.status = 2 OR al.status = $exception_status) ";

	$sql .= $add_where;

	$db_result1 = $common_dao->db_query($sql);
	if($db_result1){
		$list_count = $db_result1[0]['count'];
		$page_max = ceil($list_count / $limit_max);

		$page = ($page < 1) ? 1 : $page;
		$page = ($page > $page_max) ? $page_max : $page;

		//PREV値 設定
		$prev = ($page > 1) ? $page - 1 : 0;

		//NEXT値 設定
		$next = ($page < $page_max) ? $page + 1 : 0;

		//LIMITの開始 設定
		$limit_start = ($page -1) * $limit_max;
	}

	$list_sql = " SELECT al.created_at, al.media_id, al.advert_id, al.user_agent, "
				. " al.ip_address, al.host_name, al.click_price_client, al.status, "
				. " m.media_name, a.advert_name "
				. " FROM action_logs as al "
				. " LEFT JOIN media as m on al.media_id = m.id "
				. " LEFT JOIN advert as a on al.advert_id = a.id "
				. " WHERE al.deleted_at is NULL "
				. " AND (al.status = 1 OR al.status = 2 OR al.status = $exception_status) ";

	$list_sql .= $add_where;

	$list_sql .= " ORDER BY al.created_at ASC "
				. " LIMIT $limit_start, $limit_max ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $key => $row) {

			if($row['status'] == 1) {
				$db_result[$key]['status_show'] = "クリック";
			} elseif($row['status'] == 2) {
				$db_result[$key]['status_show'] = "登録";
			}
		}

		$smarty->assign("list", $db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("page", $page);
	$smarty->assign("page_max", $page_max);
	$smarty->assign("prev", $prev);
	$smarty->assign("next", $next);

	$smarty->assign("type", $select_date_type);
	$smarty->assign("date", $view_date);
	$smarty->assign("start_date", $view_start_date);
	$smarty->assign("end_date", $view_end_date);
	$smarty->assign("m_id", $m_id);
	$smarty->assign("mp_id", $mp_id);
	$smarty->assign("a_id", $a_id);
	$smarty->assign("ac_id", $ac_id);

	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_click_detail.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>