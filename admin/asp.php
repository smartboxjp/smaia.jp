<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AspDao.php' );
require_once( '../dto/Asp.php' );
require_once( '../dao/PrefDao.php' );
require_once( '../dto/Pref.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$asp_dao = new AspDao();

	//都道府県
	$pref_dao = new PrefDao();
	$pref_array = array();
	foreach($pref_dao->getAllPref() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$pref_array[$val->getId()] = $row_array;
	}
	$smarty->assign("pref_array", $pref_array);

	$id = do_escape_quotes($_POST['id']);
	$asp_name = do_escape_quotes($_POST['asp_name']);
	$company_name = do_escape_quotes($_POST['company_name']);
	$contact_person = do_escape_quotes($_POST['contact_person']);
	$tel = do_escape_quotes($_POST['tel']);
	$fax = do_escape_quotes($_POST['fax']);
	$email = do_escape_quotes($_POST['email']);
	$zipcode1 = do_escape_quotes($_POST['zipcode1']);
	$zipcode2 = do_escape_quotes($_POST['zipcode2']);
	$pref = do_escape_quotes($_POST['pref']);
	$address1 = do_escape_quotes($_POST['address1']);
	$address2 = do_escape_quotes($_POST['address2']);
	$transfer_type = do_escape_quotes($_POST['transfer_type']);
	$bank_name = do_escape_quotes($_POST['bank_name']);
	$branch_name = do_escape_quotes($_POST['branch_name']);
	$account_type = do_escape_quotes($_POST['account_type']);
	$account_holder = do_escape_quotes($_POST['account_holder']);
	$account_number = do_escape_quotes($_POST['account_number']);
	$postal_account_holder = do_escape_quotes($_POST['postal_account_holder']);
	$postal_account_number = do_escape_quotes($_POST['postal_account_number']);
	$result_action_url = do_escape_quotes($_POST['result_action_url']);
	$status = do_escape_quotes($_POST['status']);

	$method = do_escape_quotes($_POST['method']);
	$reverse = do_escape_quotes($_POST['reverse']);
	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);
	$search_id = do_escape_quotes($_POST['search_id']);
	$search_name = do_escape_quotes($_POST['search_name']);
	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	$list_sql = " SELECT * FROM asps WHERE deleted_at is NULL ORDER BY id ASC ";

	if(isset($_POST['mode']) && $_POST['mode'] != ''){
		if($_POST['mode'] == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./asp_input.tpl");
			exit();
		}elseif($_POST['mode'] == 'insert_commit'){

			$error_flag = 0;

			if($asp_name == "") {
				$error_message = "ASP名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$asp_dao->transaction_start();

				$asp = new Asp();
				$asp->setAspName($asp_name);
				$asp->setCompanyName($company_name);
				$asp->setContactPerson($contact_person);
				$asp->setTel($tel);
				$asp->setFax($fax);
				$asp->setEmail($email);
				$asp->setZipcode1($zipcode1);
				$asp->setZipcode2($zipcode2);
				$asp->setPref($pref);
				$asp->setAddress1($address1);
				$asp->setAddress2($address2);
				$asp->setTransferType($transfer_type);
				$asp->setBankName($bank_name);
				$asp->setBranchName($branch_name);
				$asp->setAccountType($account_type);
				$asp->setAccountHolder($account_holder);
				$asp->setAccountNumber($account_number);
				$asp->setPostalAccountHolder($postal_account_holder);
				$asp->setPostalAccountNumber($postal_account_number);
				$asp->setResultActionUrl($result_action_url);
				$asp->setStatus($status);

				//INSERTを実行
				$db_result = $asp_dao->insertAsp($asp, $result_message);
				if($db_result) {
					$asp_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$asp->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'asp_name' => $asp_name,
										'company_name' => $company_name,
										'contact_person' => $contact_person,
										'tel' => $tel,
										'fax' => $fax,
										'email' => $email,
										'zipcode1' => $zipcode1,
										'zipcode2' => $zipcode2,
										'pref' => $pref,
										'address1' => $address1,
										'address2' => $address2,
										'transfer_type' => $transfer_type,
										'bank_name' => $bank_name,
										'branch_name' => $branch_name,
										'account_type' => $account_type,
										'account_holder' => $account_holder,
										'account_number' => $account_number,
										'postal_account_holder' => $postal_account_holder,
										'postal_account_number' => $postal_account_number,
										'result_action_url' => $result_action_url,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./asp_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'asp_name' => $asp_name,
									'company_name' => $company_name,
									'contact_person' => $contact_person,
									'tel' => $tel,
									'fax' => $fax,
									'email' => $email,
									'zipcode1' => $zipcode1,
									'zipcode2' => $zipcode2,
									'pref' => $pref,
									'address1' => $address1,
									'address2' => $address2,
									'transfer_type' => $transfer_type,
									'bank_name' => $bank_name,
									'branch_name' => $branch_name,
									'account_type' => $account_type,
									'account_holder' => $account_holder,
									'account_number' => $account_number,
									'postal_account_holder' => $postal_account_holder,
									'postal_account_number' => $postal_account_number,
									'result_action_url' => $result_action_url,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./asp_input.tpl");
				exit();
			}
		}elseif($_POST['mode'] == 'edit'){
			$asp = new Asp;
			$asp = $asp_dao->getAspById($id);

			if(!is_null($asp)) {
				$form_data = array('id' => $asp->getId(),
									'asp_name' => $asp->getAspName(),
									'company_name' => $asp->getCompanyName(),
									'contact_person' => $asp->getContactPerson(),
									'tel' => $asp->getTel(),
									'fax' => $asp->getFax(),
									'email' => $asp->getEmail(),
									'zipcode1' => $asp->getZipcode1(),
									'zipcode2' => $asp->getZipcode2(),
									'pref' => $asp->getPref(),
									'address1' => $asp->getAddress1(),
									'address2' => $asp->getAddress2(),
									'transfer_type' => $asp->getTransferType(),
									'bank_name' => $asp->getBankName(),
									'branch_name' => $asp->getBranchName(),
									'account_type' => $asp->getAccountType(),
									'account_holder' => $asp->getAccountHolder(),
									'account_number' => $asp->getAccountNumber(),
									'postal_account_holder' => $asp->getPostalAccountHolder(),
									'postal_account_number' => $asp->getPostalAccountNumber(),
									'result_action_url' => $asp->getResultActionUrl(),
									'status' => $asp->getStatus());

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./asp_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./asp_input.tpl");
				exit();
			}
		}elseif($_POST['mode'] == 'update_commit'){
			$asp_dao->transaction_start();

			$asp = new Asp();
			$asp->setId($id);
			$asp->setAspName($asp_name);
			$asp->setCompanyName($company_name);
			$asp->setContactPerson($contact_person);
			$asp->setTel($tel);
			$asp->setFax($fax);
			$asp->setEmail($email);
			$asp->setZipcode1($zipcode1);
			$asp->setZipcode2($zipcode2);
			$asp->setPref($pref);
			$asp->setAddress1($address1);
			$asp->setAddress2($address2);
			$asp->setTransferType($transfer_type);
			$asp->setBankName($bank_name);
			$asp->setBranchName($branch_name);
			$asp->setAccountType($account_type);
			$asp->setAccountHolder($account_holder);
			$asp->setAccountNumber($account_number);
			$asp->setPostalAccountHolder($postal_account_holder);
			$asp->setPostalAccountNumber($postal_account_number);
			$asp->setResultActionUrl($result_action_url);
			$asp->setStatus($status);

			//UPDATEを実行
			$db_result = $asp_dao->updateAsp($asp, $result_message);
			if($db_result) {
				$asp_dao->transaction_end();

				$smarty->assign("info_message", $result_message);

				view_list();
			} else {
				$asp_dao->transaction_rollback();

				$smarty->assign("error_message", $result_message);

				$form_data = array('id' => $id,
									'asp_name' => $asp_name,
									'company_name' => $company_name,
									'contact_person' => $contact_person,
									'tel' => $tel,
									'fax' => $fax,
									'email' => $email,
									'zipcode1' => $zipcode1,
									'zipcode2' => $zipcode2,
									'pref' => $pref,
									'address1' => $address1,
									'address2' => $address2,
									'transfer_type' => $transfer_type,
									'bank_name' => $bank_name,
									'branch_name' => $branch_name,
									'account_type' => $account_type,
									'account_holder' => $account_holder,
									'account_number' => $account_number,
									'postal_account_holder' => $postal_account_holder,
									'postal_account_number' => $postal_account_number,
									'result_action_url' => $result_action_url,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./asp_input.tpl");
				exit();
			}
		}elseif($_POST['mode'] == 'search'){

			$search_list_sql = " SELECT * FROM asps "
							. " WHERE deleted_at is NULL ";

			//絞込みWHERE文格納配列
			$add_where = array();

			//「媒体カテゴリーID」「媒体カテゴリー名」絞込み
			switch($method) {
				case 1:	//「完全一致」
					if($search_id != "") {
						$add_where[] = "id = '$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "asp_name = '$search_name' ";
					}
					break;
				case 2:	//「前方一致」
					if($search_id != "") {
						$add_where[] = "id LIKE '$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "asp_name LIKE '$search_name%' ";
					}
					break;
				case 3:	//「後方一致」
					if($search_id != "") {
						$add_where[] = "id LIKE '%$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "asp_name LIKE '%$search_name' ";
					}
					break;
				case 4:	//「あいまい」
					if($search_id != "") {
						$add_where[] = "id LIKE '%$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "asp_name LIKE '%$search_name%' ";
					}
					break;
			}

			if(count($add_where) > 0) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(".implode("OR ", $add_where).") ";
				} else {
					$search_list_sql .= "AND ".implode("AND ", $add_where);
				}
			}

			if($created_at_flag == 1) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
				} else {
					$search_list_sql .= "AND created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
				}

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

			$search_list_sql .= "ORDER BY $sort $order ";

			$db_result = $common_dao->db_query($search_list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			$search['method'] = $method;
			$search['reverse'] = $reverse;
			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['id'] = $search_id;
			$search['name'] = $search_name;
			$search['created_at_flag'] = $created_at_flag;

			$smarty->assign("search", $search);

			$smarty->assign("mode", 'search');
			$smarty->assign("sub_title", '検索結果');

			// ページを表示
			$smarty->display("./asp_list.tpl");
			exit();
		}elseif($_POST['mode'] == 'delete'){
			$asp_dao->transaction_start();

			if(!is_null($asp_dao->getAspById($id))) {
				$db_result = $asp_dao->deleteAsp($id, $result_message);

				if($db_result){
					$asp_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				}else{
					$asp_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					view_list();
				}
			}else{
				$asp_dao->transaction_rollback();

				$error_message = "ＤＢの更新に失敗しました。";
				$smarty->assign("error_message", $error_message);

				view_list();
			}
		}else{
			view_list();
		}
	}else{
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./asp_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>