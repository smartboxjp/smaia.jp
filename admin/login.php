<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/LoginUserDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

// Smartyオブジェクト取得
$smarty =& getSmartyObj();

if($_POST['logon'] == 'yy'){
	$login_user = new LoginUser();
	$login_user_dao = new LoginUserDao();
	$common_dao = new CommonDao();

	$login_id = $common_dao->db_string_escape($_POST['login_id']);
	$login_pass = $common_dao->db_string_escape($_POST['login_pass']);

	$login_user = $login_user_dao->getLoginUserByIdPass($login_id, $login_pass);

	if($login_user){
		$_SESSION['login_user'] = $login_user;
		$_SESSION['logon_token'] = md5(uniqid(mt_rand(), TRUE));

		// オブジェクト ラストログイン日付に本日の日付を格納
		$login_user->setLastLoginAt( date("YmdHms"));
		// login_userテーブルを更新
		$db_result = $login_user_dao->updateLoginUser($login_user);

		$smarty->assign("info_message", "ログオンしました。");

		header('Location: ./top.php');
		exit();
	}else{
		$smarty->assign("error_message", "ログインIDまたはログインパスワードが違います。");

		// ページを表示
		$smarty->display("login.tpl");
		exit();
	}
}elseif($_GET['logout'] == 'y'){
	$_SESSION['login_user'] = null;
	$_SESSION['logon_token'] = "";

	// セッション変数を全て解除する
	$_SESSION = array();

	// セッションを切断するにはセッションクッキーも削除する。
	// Note: セッション情報だけでなくセッションを破壊する。
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-42000, '/');
	}

	// 最終的に、セッションを破壊する
	session_destroy();

	// ページを表示
	$smarty->display("login.tpl");
	exit();
}else{
	// ページを表示
	$smarty->display("login.tpl");
	exit();
}
?>