<?php

$bid = NULL;

# GET・POSTであった場合の記述
if(isset($_GET['bid']) && $_GET['bid'] != "") {
	$bid = $_GET['bid'];
} elseif(isset($_POST['bid']) && $_POST['bid'] != "") {
	$bid = $_POST['bid'];
}

if(isset($_GET['at'])) {
	$at = $_GET['at'];
}elseif(isset($_POST['at'])) {
	$at = $_POST['at'];
}

# session変数を使用する場合
/**
 * GET・POST・SESSIONの比較は基本的にissetかemptyを使うのが好ましい
 * != "" のみ場合仮に値がNULLでもifの中に入ってしまう
 * またSESSIONはDBにデータを格納する設定でもしていない場合は
 * ルートが変化したときにSESSIONを使用できなくなるので注意が必要
 */
if(isset($_SESSION['bid'])) {
	$bid = $_SESSION['bid'];
}

# 下記は参考にしなくて良い
// ブラウザの判別
if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false) {
	// iPhoneの場合
	$browser_type = "1";
} else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false) {
	// Androidの場合
	$browser_type = "2";
} else {
	// PCの場合
	$browser_type = "3";
}

$check = 1;


# ソケット送信
if(!empty($bid)) {

	$server = "smaia.jp";	// 送信したいサーバのアドレス
	$port = 80;				// HTTP なので80
	$timeout = 30;			// 接続に失敗した場合の待ち時間

	$sock = fsockopen($server, $port, $errno, $errstr, $timeout);	// サーバに接続する
	// サーバーに接続できない
	if($sock === FALSE){
		echo "SOCK OPEN ERROR<br />";
		exit(-1);
	}

	// HTTP ヘッダ部分の送信になる。
	fwrite($sock, "GET http://" . $server . "/action/result.php?bid=$bid&at=$at HTTP/1.0\r\n");
	// ヘッダの終了を通知
	fwrite($sock, "\r\n\r\n");

	fclose($sock);

	$check = 2;
} else {
	$check = 1;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?if($browser_type <> "3") {?>
	<meta name = "viewport" content = "width = device-width, initial-scale = 1, user-scalable = no">
<?}?>
<title>送信テスト</title>
</head>
<body width="100%">

<div id="container" width="100%">

<?if($check == 1) {?>
	<p>bidを入力してください。</p>
	<form action="<?=$_SERVER['PHP_SELF']?>" method="GET">
		<input type="text" name="bid" value="" />
		<input type="hidden" name="at" value="2" />
		<input type="submit" value="送信" />
	</form>

<?} elseif($check == 2) {?>

	<p>登録完了しました。</p>
	<p><a href="<?=$_SERVER['PHP_SELF']?>">戻る</a></p>

<?}?>
</div>

</body>
</html>