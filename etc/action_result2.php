<?php

if(isset($_GET['bid'])) {
	$bid = $_GET['bid'];
} elseif(isset($_POST['bid'])) {
	$bid = $_POST['bid'];
}

// ブラウザの判別
if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false) {
	// iPhoneの場合
	$browser_type = "1";
} else if (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false) {
	// Androidの場合
	$browser_type = "2";
} else {
	// PCの場合
	$browser_type = "3";
}



$check = 1;

if($_POST['action'] == "result") {

$server = "adbond.jp";  // 送信したいサーバのアドレス
$port = 80;             // HTTP なので80
$timeout = 30;             // 接続に失敗した場合の待ち時間

$sock = fsockopen($server, $port, $errno, $errstr, $timeout);  // サーバに接続する
if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
	echo "SOCK OPEN ERROR<br>";
	exit(-1);
}

$ac = date('YmdHis');

// HTTP ヘッダ部分の送信になる。
fwrite($sock, "GET http://" . $server . "/action/result2.php?bid=$bid&ac=$ac HTTP/1.0\r\n");
// ヘッダの終了を通知
fwrite($sock, "\r\n\r\n");

fclose($sock);

$check = 2;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php if($browser_type <> "3") { ?>
	<meta name = "viewport" content = "width = device-width, initial-scale = 1, user-scalable = no">
<?php } ?>

<title>テストサイト</title>

<meta http-equiv="content-style-type" content="text/css">

</head>
<body>

<div id="topmenu">
<ul>
<li><a href="1.html">メニュー1</a>
<li><a href="1.html">メニュー2</a>
<li><a href="1.html">メニュー3</a>
<li><a href="1.html">メニュー4</a>
<li><a href="1.html">メニュー5</a>
<li><a href="1.html"><div>メニューのテキストが長くなっても問題ないように対応</div></a>
</ul>
</div>


</body>
</html>